// Hex math defined here: http://blog.ruslans.com/2011/02/hexagonal-grid-math.html

function HexagonGrid(canvasId, radius,width=window.innerWidth, preview = true, isDebug = false, data = []) {
    this.radius = radius;
    this.data = data;
    this.height = Math.sqrt(3) * radius;
    this.width = 2 * radius;
    this.side = (3 / 2) * radius;
    this.preview = preview;
    this.canvasId = canvasId;
    this.canvas = document.getElementById(canvasId);
    this.context = this.canvas.getContext('2d');
    this.imageCache = {};
    this.canvasOriginX = 0;
    this.canvasOriginY = 0;
    this.canvas.width = width;
    this.isDebug = isDebug
    this.drag = false;
    this.dragData = null;
    this.inModal = false;
    this.disableDbClick = true;
    this.zoomTo = [];
    this.imageCount = 0;
    this.notFitted = false;
    this.imageLoaded = 0;
    this.drawedTitle = {};
    this.type = "navigation";
    this.navigationStack = [];
    this.breadcrumb = [];
    this.navigation = "root";
    this.clickTimeout = null;
    this.isDoubleClick = false;
    this.breadcrumbsContainer = null;
    // this.canvas.height = window.innerHeight;
    this.drawedHex = {};
    this.scale = 1;           // Échelle de zoom
    this.offsetX = 0;         // Décalage en X pour le scroll
    this.offsetY = 0; 
    this.lineStroke = 1;
    this.colorStroke = "#000";
    this.activeCircle = false;
    this.circleCallback = null;
    this.canvas.addEventListener("wheel", (e) => {
        e.preventDefault();
        const zoomIntensity = 0.1; // Intensité du zoom
        const newScale = this.scale * (e.deltaY > 0 ? 1 - zoomIntensity : 1 + zoomIntensity);
        this.scale = Math.max(0.1, Math.min(newScale, 10)); // Limite du zoom
        // this.drawHexGrid(/*arguments ici*/);
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

        this.context.save();
        this.context.translate(this.offsetX, this.offsetY);
        this.context.scale(this.scale, this.scale);
        if(this.preview == false){
            this.drawHexEditMode();
        }
        if(this.zoomTo.length > 0){
            this.drawData(this.zoomTo);
        }else{
            this.drawData();
        }
    });
    let isDragging = false;
    let startX, startY;
    let clickTimeout;
    
    this.canvas.addEventListener("mousedown", (e) => {
        // isDragging = true;
        startX = e.clientX - this.offsetX;
        startY = e.clientY - this.offsetY;
        clickTimeout = setTimeout(() => {
            isDragging = true;
        }, 150);
    });
    this.canvas.addEventListener("mouseup", (e) => {
        if(!isDragging){
            clearTimeout(clickTimeout);
            this.clickEvent(e);
        }
        isDragging = false;
    }, false);
    
    this.canvas.addEventListener("dblclick", (e) => {
        if(!this.disableDbClick){
            clearTimeout(this.clickTimeout);
            this.isDoubleClick = true;
            if(typeof this.dbClickEvent != "undefined"){
                this.dbClickEvent(e);
            }
        }
    }, false);
    this.canvas.addEventListener("mousemove", (e) => {
        if (isDragging) {
            this.offsetX = e.clientX - startX;
            this.offsetY = e.clientY - startY;
            this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.context.save();
            this.context.translate(this.offsetX, this.offsetY);
            this.context.scale(this.scale, this.scale);
            if(this.preview == false){
                this.drawHexEditMode();
            }
            if(this.zoomTo.length > 0){
                this.drawData(this.zoomTo);
            }else{
                this.drawData();
            }
        }else{
            const rect = this.canvas.getBoundingClientRect();
            let mouseX = e.pageX - this.canvasOriginX;
            let mouseY = e.pageY - this.canvasOriginY;
            
            const tile = this.getSelectedTile(mouseX, mouseY);
            // this.drawHexGrid(10, 10, 0, 0, false);
            const hexKey = `${tile.column},${tile.row}`;
            this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.context.save();
            this.context.translate(this.offsetX, this.offsetY);
            this.context.scale(this.scale, this.scale);
            if(this.preview == false){
                this.drawHexEditMode();
                this.canvas.style.cursor = "pointer";
                this.drawHexAtColRow(tile.column, tile.row, "#f8f8f8", "", null, this.radius, false, { color: "#696969", width: 3 }); 
            }
            if(this.zoomTo.length > 0){
                this.drawData(this.zoomTo);
            }else{
                this.drawData();
            }
            // Obtenir l'hexagone sous la souris
            
            if(typeof this.drawedHex[hexKey] != "undefined"){
                this.canvas.style.cursor = "pointer";
                var offset = this.getRelativeCanvasOffset();
                mouseX -= offset.x + this.offsetX;
                mouseY -= offset.y + this.offsetY;
                mouseX /= this.scale;
                mouseY /= this.scale;
                this.displayHexTitle(this.drawedHex[hexKey].text, mouseX, mouseY);    
            }else{
                if(this.preview){
                    this.canvas.style.cursor = "default";
                }
            }
            this.context.restore();
        }
    });

    this.canvas.addEventListener("mouseup", () => { isDragging = false});
    this.canvas.addEventListener("mouseleave", () => { 
        isDragging = false;
        if (this.preview == false) {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.save();
        this.context.translate(this.offsetX, this.offsetY);
        this.context.scale(this.scale, this.scale);
            this.drawHexEditMode();
            if(this.zoomTo.length > 0){
                this.drawData(this.zoomTo);
            }else{
                this.drawData();
            }
            // Remettre le curseur par défaut
            this.canvas.style.cursor = "default";
            this.context.restore();
        }
    });
};

HexagonGrid.prototype.setType = function(type){
    this.type = type;
}

HexagonGrid.prototype.setActiveCircle = function(active, circleCallback = null){
    this.activeCircle = active;
    this.circleCallback = circleCallback;
    if(active){
        this.addBtn();
    }
}

HexagonGrid.prototype.addBtn = function(){
    const canvasElement = d3.select(`#${this.canvasId}`)
    const container = canvasElement.select(function(){ return this.parentNode; })
    
    var _navigationNode = container
        .append("xhtml:div")
        .style("border-radius", "4px")
        .style("box-shadow", "rgb(0 0 0 / 50%) 0px 0px 5px 0px")
        .style("background-color", "white")
        /*.style("z-index", 20000)*/
        .style("border-color", "#acacaa")
        .style("position", "absolute")
        .style("top", "10px")
        .style("right", "10px")
        .style("display", "flex")
        .style("align-items", "center")
        .style("justify-content", "space-around")
        .style("z-index", "1001")
        .classed("group-button-switcher", true)
    mylog.log("Container data", container, _navigationNode, this.canvasId, canvasElement)
    const iconSize = 20;
    const svgListIcon = `
        <svg style="height: ${iconSize}px; width: ${iconSize}px;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="svg-list-icon" x="0px" y="0px" viewBox="0 0 297 297" style="enable-background:new 0 0 297 297;" xml:space="preserve">
            <path d="M66.102,0H15.804C7.089,0,0,7.089,0,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804V15.804C81.907,7.089,74.817,0,66.102,0z"/>
            <path d="M173.649,0h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804V15.804C189.453,7.089,182.364,0,173.649,0z"/>
            <path d="M66.102,107.547H15.804C7.089,107.547,0,114.636,0,123.351v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C81.907,114.636,74.817,107.547,66.102,107.547z"/>
            <path d="M173.649,107.547h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C189.453,114.636,182.364,107.547,173.649,107.547z"/>
            <path d="M281.196,0h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804V15.804C297,7.089,289.911,0,281.196,0z"/>
            <path d="M281.196,107.547h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C297,114.636,289.911,107.547,281.196,107.547z"/>
            <path d="M66.102,215.093H15.804C7.089,215.093,0,222.183,0,230.898v50.298C0,289.911,7.089,297,15.804,297h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C81.907,222.183,74.817,215.093,66.102,215.093z"/>
            <path d="M173.649,215.093h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C189.453,222.183,182.364,215.093,173.649,215.093z"/>
            <path d="M281.196,215.093h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C297,222.183,289.911,215.093,281.196,215.093z"/>
        </svg>
    `
    const svgGraphIcon = `
    <svg style="height: ${iconSize}px; width: ${iconSize}px;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="svg-graph-icon" x="0px" y="0px" viewBox="0 0 349.899 349.898" style="enable-background:new 0 0 349.899 349.898;" xml:space="preserve">
        <path d="M175.522,12.235c-42.6,0-77.256,34.649-77.256,77.25c0,42.6,34.656,77.255,77.256,77.255    c42.591,0,77.257-34.656,77.257-77.255C252.779,46.895,218.113,12.235,175.522,12.235z"/>
        <path d="M77.255,337.663c42.599,0,77.255-34.641,77.255-77.251c0-42.594-34.656-77.25-77.255-77.25    C34.653,183.162,0,217.818,0,260.412C0,303.012,34.653,337.663,77.255,337.663z"/>
        <path d="M272.648,183.151c-42.603,0-77.256,34.65-77.256,77.256c0,42.604,34.653,77.25,77.256,77.25    c42.6,0,77.251-34.646,77.251-77.25C349.909,217.818,315.248,183.151,272.648,183.151z"/>
    </svg>
    `
    const svgHexagoneIcon = `
        <svg xmlns="http://www.w3.org/2000/svg" style="height: ${iconSize}px; width: ${iconSize}px" viewBox="0 0 256 256">
            <path d="M223.68,66.15,135.68,18h0a15.88,15.88,0,0,0-15.36,0l-88,48.17a16,16,0,0,0-8.32,14v95.64a16,16,0,0,0,8.32,14l88,48.17a15.88,15.88,0,0,0,15.36,0l88-48.17a16,16,0,0,0,8.32-14V80.18A16,16,0,0,0,223.68,66.15ZM216,175.82,128,224,40,175.82V80.18L128,32h0l88,48.17Z"></path>
        </svg>
    `
    _navigationNode.append("xhtml:div")
        .style("padding", "10px")
        .classed("button-switcher-mode", true)
        .style("display", "flex")
        .style("justify-content", "center")
        .style("align-items", "center")
        .style("font-size", "12pt")
        .style("font-weight", "bold")
        .style("cursor", "pointer")
        .html(svgGraphIcon+" <span class='padding-left-5'> Mode Mapping</span>")
        .on("click", () => {
            if(this.circleCallback){
                this.circleCallback("graph");
            }
        })
    _navigationNode.append("xhtml:div")
        .style("padding", "10px")
        .classed("button-switcher-mode", true)
        .style("display", "flex")
        .style("justify-content", "center")
        .style("align-items", "center")
        .style("font-size", "12pt")
        .style("font-weight", "bold")
        .style("cursor", "pointer")
        .html(svgListIcon+" <span class='padding-left-5'> Mode Liste</span>")
        .on("click", () => {
            if(this.circleCallback){
                this.circleCallback("list");
            }
        })

    _navigationNode.append("xhtml:div")
        .style("padding", "10px")
        .classed("button-switcher-mode", true)
        .classed("active", true)
        .style("display", "flex")
        .style("justify-content", "center")
        .style("align-items", "center")
        .style("font-size", "12pt")
        .style("font-weight", "bold")
        .style("cursor", "pointer")
        .html(svgHexagoneIcon+" <span class='padding-left-5'> Mode Hexagone</span>")
        .on("click", () => {
            
        })
    
}

HexagonGrid.prototype.setLineStroke = function(width){
    this.lineStroke = width;
}

HexagonGrid.prototype.setColorStroke = function(color){
    this.colorStroke = color;
}

HexagonGrid.prototype.setDBClickOption = function(dbClick){
    this.disableDbClick = dbClick;
}

HexagonGrid.prototype.slugify = function(text) {
    if(!text){
        return "noslug";
    }
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(/[^\w\-]+/g, '') // Remove all non-word chars
        .replace(/\-\-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, ''); // Trim - from end of text
}

HexagonGrid.prototype.setBreadCrumbContainer = function(container){
    this.breadcrumbsContainer = container;
}

HexagonGrid.prototype.addBreadCrumb = function(){
    this.breadcrumb.push(this.navigation);
    this.renderBreadCrumbs();
}

HexagonGrid.prototype.dbClickEvent = function(e){
    if(this.type == "depth"){
      if(this.preview == true){
        let previewData = !this.preview;
        this.switchMode(previewData);
        if(previewData){
          $(".button-switch").text("Changer en mode édition");
        }else{
          $(".button-switch").text("Changer en mode preview");
        }
      }
      var mouseX = e.pageX;
      var mouseY = e.pageY;

      var localX = mouseX - this.canvasOriginX;
      var localY = mouseY - this.canvasOriginY;

      var tile = this.getSelectedTile(localX, localY);
      if(this.hasHexagon(tile.column, tile.row)){
        let hexaData = this.drawedHex[`${tile.column},${tile.row}`];
        this.navigationStack.push({
            scale: this.scale,
            offsetX: this.offsetX,
            offsetY: this.offsetY,
            zoomTo: this.navigation
        });
        let group = Object.values(this.data[hexaData.text]);
        this.drawedHex = {};
        this.drawedTitle = {};
        this.navigation = hexaData.text;
        this.clearCanvas();
        this.addBreadCrumb();
        this.placeGroupsWithOffsets(
        group, tile, false, false, 3);
        this.fitBounds();
      }
    }else{
      let hexaMode = !this.preview;
      this.switchMode(hexaMode);
      if(hexaMode){
          $(".button-switch").text("Changer en mode édition");
        }else{
          $(".button-switch").text("Changer en mode preview");
        }
      if(!hexaMode){
        if(typeof this.clickEvent != "undefined"){
          this.clickEvent(e);
        }
      }
    }
  }

HexagonGrid.prototype.popBreadCrumb = function(){
    this.breadcrumb.pop();
    this.renderBreadCrumbs();
}
HexagonGrid.prototype.navigateBreadCrumb = function(index){
    const crumb = this.navigationStack[index];
    if (!crumb) return;

    // Restaurer l'état (zoom et décalage)
    this.scale = crumb.scale;
    this.offsetX = crumb.offsetX;
    this.offsetY = crumb.offsetY;

    this.drawedHex = {};
    this.drawedTitle = {};
    this.clearCanvas();
    this.breadcrumb = this.breadcrumb.slice(0, index + 1);
    this.navigationStack = this.navigationStack.slice(0, index);
    this.navigation = crumb.zoomTo;
    this.renderBreadCrumbs();
    let group = Object.values(this.data[crumb.zoomTo]);
    if(crumb.zoomTo == "root"){
      this.placeGroupsWithOffsets(
        group,  {column:0, row:0}, false, false, 2);
    }else{
      this.placeGroupsWithOffsets(
        group,  {column:0, row:0}, false, false, 3);
    }
}


HexagonGrid.prototype.renderBreadCrumbs = function(){
    const $breadcrumbsContainer = $(this.breadcrumbsContainer);
    if (!$breadcrumbsContainer) return;

    $breadcrumbsContainer.html('<ul></ul>');
    
    this.breadcrumb.forEach((crumb, index) => {
        const breadcrumb = `<li><a href="javascript:void(0)" class="breadcrumb-item" data-index="${index}">${index != 0 ? crumb : '<i class="fa fa-home icon"></i>'}</a></li>`;
        $breadcrumbsContainer.find("ul").append(breadcrumb);
    });
    $(".breadcrumb-item").off("click").on("click", (e) => {
        const index = $(e.currentTarget).data("index");
        this.navigateBreadCrumb(index);
        //this.renderBreadCrumbs();
    });

}

HexagonGrid.prototype.prepareData = function(authorizedTags, data){
    let group = {};
    authorizedTags.forEach((tag) => {
        group[tag] = {
            group: tag,
            text: tag,
            childs : []
        };
    });
    Object.values(data).forEach((element) => {
        if (typeof element.tags != "undefined") {
            element.tags.forEach((tag) => {
                if(authorizedTags.includes(tag)){
                    group[tag].childs.push({
                        text: element.name,
                        image: typeof element.profilMediumImageUrl != "undefined" ? element.profilMediumImageUrl : null,
                        slug: element.slug,
                        description: element.description,
                        collection: element.collection,
                        group: tag,
                        id: element._id.$id,
                        color: `#eeeeee`
                    })
                }
            });
        }
    });
    return group;
}



HexagonGrid.prototype.getBoundsOfContent = function() {
    // Initialiser les limites avec la première position disponible
    let minCol = Infinity;
    let maxCol = -Infinity;
    let minRow = Infinity;
    let maxRow = -Infinity;

    // Parcourir tous les hexagones dessinés pour trouver les limites
    for (let key in this.drawedHex) {
        const hex = this.drawedHex[key];
        minCol = Math.min(minCol, hex.column);
        maxCol = Math.max(maxCol, hex.column);
        minRow = Math.min(minRow, hex.row);
        maxRow = Math.max(maxRow, hex.row);
    }

    return {
        min: { column: minCol, row: minRow },
        max: { column: maxCol, row: maxRow }
    };
};
HexagonGrid.prototype.placeGroupsWithOffsets = function(data, center = { column: 0, row: 0 },filterNull = true, addChild = true, offset = 10) {
    if(filterNull){
        data = data.filter(item => item.childs && item.childs.length > 0)
    }
    // Fonction pour générer des offsets supplémentaires de manière récursive
    let ringLevel = this.findRingLevel(data.length);
    let offsets = this.getHexagonRing(ringLevel, center, offset);
    // while (offsets.length < data.length) {
    //     const additionalRingOffsets = generateAdditionalOffsets(ringLevel);
    //     offsets = offsets.concat(additionalRingOffsets);
    //     ringLevel++;
    // }
    if (this.preview === false) {
        this.drawHexEditMode();
    }
    data.forEach((groupData, index) => {
        const groupCenter = {
            column: center.column + offsets[index % offsets.length].column,
            row: center.row + offsets[index % offsets.length].row
        };
        if(addChild){
            const ringLevel = this.findRingLevel(groupData.childs.length);
            const ringCoords = this.getHexagonRing(ringLevel, groupCenter,1, true);
            
            /* if(addContainer){
                console.log("Place Group with center", addContainer, ringLevel, this.radius * ringLevel)
                this.drawHexAtColRow(
                    groupCenter.column,
                    groupCenter.row,
                    "blue",
                    groupData.group,
                    null,
                    this.radius * ringLevel, 
                );
            } */
    
            const x = groupCenter.column * this.side + this.canvasOriginX; 
            const y = groupCenter.row * this.height + (groupCenter.column % 2 === 0 ? 0 : this.height / 2) + this.canvasOriginY;
            let tile = this.getRowCol(x, y - this.radius * (1 + (ringLevel >= 3 ? ringLevel + 1 : ringLevel)));
            this.drawedTitle[groupData.group] = { x: x, y: y - this.radius * (1 +  (ringLevel >= 3 ? ringLevel + 1 : ringLevel)), tile: tile};
            this.drawGroupTitle(groupData.group, x, y - this.radius * (1 +  (ringLevel >= 3 ? ringLevel + 1 : ringLevel)));
    
            groupData.childs.forEach((child, childIndex) => {
                const coord = ringCoords[childIndex % ringCoords.length];
    
                this.data[groupData.group] = this.data[groupData.group] || {};
                this.data[groupData.group][`${coord.column},${coord.row}`] = { ...child ,column: coord.column, row: coord.row, group: groupData.group, extra: child};
                this.drawHexAtColRow(
                    coord.column,
                    coord.row,
                    child.color,
                    child.text,
                    child.image,
                    null, 
                    true,
                    {},
                    groupData.group,
                    child
                );
            });
        }else{
            if(typeof this.data[groupData.text] == "undefined"){
                this.data[groupData.text] = groupData.childs || [];
            }
            const x = groupCenter.column * this.side + this.side / 2 + this.canvasOriginX; 
            const y = groupCenter.row * this.height + (groupCenter.column % 2 === 0 ? 0 : this.height / 2) + this.canvasOriginY;
            this.drawedTitle[groupData.text] = { x: x, y: y - this.radius / 2};
            this.drawGroupTitle(groupData.text, x, y - this.radius / 2 );
            this.drawHexAtColRow(
                groupCenter.column,
                groupCenter.row,
                groupData.color,
                groupData.text,
                groupData.image,
                null, 
                true,
                {},
                typeof groupData.group != "undefined" ? groupData.group : groupData.text,
                groupData
            );
        }
        this.fitBounds();
    });
};
HexagonGrid.prototype.placeGroupsChildsWithOffsets = function(data, center = { column: 0, row: 0 }, offset = 10) {
    let ringLevel = this.findRingLevel(data.length);
    let offsets = this.getHexagonRing(ringLevel, center, offset);
    // while (offsets.length < data.length) {
    //     const additionalRingOffsets = generateAdditionalOffsets(ringLevel);
    //     offsets = offsets.concat(additionalRingOffsets);
    //     ringLevel++;
    // }
    if (this.preview === false) {
        this.drawHexEditMode();
    }
    data.forEach((groupData, index) => {
        const groupCenter = {
            column: center.column + offsets[index % offsets.length].column,
            row: center.row + offsets[index % offsets.length].row
        };
        const ringLevel = this.findRingLevel(groupData.childs.length);
        const ringCoords = this.getHexagonRing(ringLevel, groupCenter,1, true);
        
        /* if(addContainer){
            console.log("Place Group with center", addContainer, ringLevel, this.radius * ringLevel)
            this.drawHexAtColRow(
                groupCenter.column,
                groupCenter.row,
                "blue",
                groupData.group,
                null,
                this.radius * ringLevel, 
            );
        } */

        const x = groupCenter.column * this.side + this.canvasOriginX; 
        const y = groupCenter.row * this.height + (groupCenter.column % 2 === 0 ? 0 : this.height / 2) + this.canvasOriginY;
        let tile = this.getRowCol(x, y - this.radius * (1 + ringLevel));
        this.drawedTitle[groupData.group] = { x: x, y: y - this.radius * (1 + ringLevel), tile: tile};
        this.drawGroupTitle(groupData.group, x, y - this.radius * (1 + ringLevel));

        groupData.childs.forEach((child, childIndex) => {
            const coord = ringCoords[childIndex % ringCoords.length];

            this.data[groupData.group] = this.data[groupData.group] || {};
            this.data[groupData.group][`${coord.column},${coord.row}`] = { ...child ,column: coord.column, row: coord.row, group: groupData.group, extra: child};
            this.drawHexAtColRow(
                coord.column,
                coord.row,
                child.color,
                child.text,
                child.image,
                null, 
                true,
                {},
                groupData.group,
                child
            );
        });
    });
};

HexagonGrid.prototype.fitBounds = function(padding = 50) {
    const bounds = this.getBoundsOfContent();
    
    if (bounds.min.column === Infinity) return; // Pas de contenu

    // Calcul des coordonnées en pixels pour les limites
    const height = Math.sqrt(3) * this.radius;
    const minX = (bounds.min.column * this.side) + this.canvasOriginX;
    const maxX = ((bounds.max.column + 1) * this.side) + this.canvasOriginX;
    
    const minY = bounds.min.column % 2 === 0 
        ? (bounds.min.row * height) + this.canvasOriginY 
        : (bounds.min.row * height) + this.canvasOriginY + (height / 2);
    const maxY = bounds.max.column % 2 === 0 
        ? ((bounds.max.row + 1) * height) + this.canvasOriginY 
        : ((bounds.max.row + 1) * height) + this.canvasOriginY + (height / 2);

    // Calcul de la largeur et la hauteur du contenu
    const contentWidth = maxX - minX + (padding * 2);
    const contentHeight = maxY - minY + (padding * 2);

    // calcul l'échelle nécessaire pour afficher tout le contenu
    const scaleX = this.canvas.width / contentWidth;
    const scaleY = this.canvas.height / contentHeight;
    this.scale = Math.min(scaleX, scaleY);
    
    // Limiter l'échelle entre 0.1 et 10
    this.scale = Math.max(0.1, Math.min(this.scale, 10));

    // calcul du centre du contenu
    const contentCenterX = (minX + maxX) / 2;
    const contentCenterY = (minY + maxY) / 2;

    // Calcul les offsets nécessaires pour centrer le contenu
    this.offsetX = (this.canvas.width / 2) - (contentCenterX * this.scale);
    this.offsetY = (this.canvas.height / 2) - (contentCenterY * this.scale);

    // Designer la grille
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.context.save();
    this.context.translate(this.offsetX, this.offsetY);
    this.context.scale(this.scale, this.scale);
    
    if (this.preview === false) {
        this.drawHexEditMode();
    }
    if(this.zoomTo.length > 0){
        this.drawData(this.zoomTo);
    }else{
        this.drawData();
    }
    this.context.restore();
};

HexagonGrid.prototype.setInModal = function(inModal){
    this.inModal = inModal;
}

HexagonGrid.prototype.switchDrag = function(drag, dragData){
    this.drag = drag;
    this.dragData = dragData;
}

HexagonGrid.prototype.switchMode = function(mode){
    this.preview = mode;
    if(this.preview == false){
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.save();
        this.context.translate(this.offsetX, this.offsetY);
        this.context.scale(this.scale, this.scale);
        this.drawHexEditMode();
        this.drawData();
    }else{
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.save();
        this.context.translate(this.offsetX, this.offsetY);
        this.context.scale(this.scale, this.scale);
        this.drawData();
    }
}

HexagonGrid.prototype.drawData = function(dataToShow = null){
    if(dataToShow){
        if(Array.isArray(dataToShow)){
            $.each(dataToShow, (key, value) => {
                this.drawHexAtColRow(value.column, value.row, value.color, value.text, value.image, value.radius, true, value.hoverConfig, value.group, value.extra);
            });
        }else{
            this.drawHexAtColRow(dataToShow.column, dataToShow.row, dataToShow.color, dataToShow.text, dataToShow.image, dataToShow.radius, true, dataToShow.hoverConfig, dataToShow.group, dataToShow.extra);
        }
    }else{
        $.each(this.drawedTitle, (title, coordinates) => {
            this.drawGroupTitle(title, coordinates.x, coordinates.y);
        });
        $.each(this.drawedHex, (key, value) => {
            this.drawHexAtColRow(value.column, value.row, value.color, value.text, value.image, value.radius, true, value.hoverConfig, value.group, value.extra);
        });
    }
    this.context.restore();
}

HexagonGrid.prototype.displayHexTitle = function(title, x, y) {
    this.context.save();

    // Annuler les transformations actuelles pour le texte
    this.context.setTransform(1, 0, 0, 1, 0, 0);
    
    // Calculer la position réelle en tenant compte du zoom et de l'offset
    const screenX = (x * this.scale) + this.offsetX;
    const screenY = (y * this.scale) + this.offsetY;

    // Définir les styles pour le texte
    this.context.font = "12px Arial";
    this.context.fillStyle = "#000";

    // Mesurer la largeur et la hauteur du texte
    const textMetrics = this.context.measureText(title);
    const padding = 4;

    // Définir la taille de l'arrière-plan
    const backgroundWidth = textMetrics.width + padding * 2;
    const backgroundHeight = 12 + padding * 2;

    // Définir la couleur du fond et dessiner le rectangle
    this.context.fillStyle = "#FFFFFF";
    this.context.fillRect(
        screenX + 10 - padding, 
        screenY - 20 - padding, 
        backgroundWidth, 
        backgroundHeight
    );

    // Redéfinir la couleur pour le texte et le dessiner
    this.context.fillStyle = "#000";
    this.context.fillText(title, screenX + 10, screenY - 10);

    this.context.restore();
};

HexagonGrid.prototype.findRingLevel = (Hc) => {
    const a = 3;
    const b = -3;
    const c = 1 - Hc;

    const discriminant = Math.sqrt(b * b - 4 * a * c);

    if (discriminant < 0) {
        return null;
    }

    const n1 = (-b + discriminant) / (2 * a);
    const n2 = (-b - discriminant) / (2 * a);

    const n = n1 > 0 ? n1 : n2;

    return Math.ceil(n);
}

HexagonGrid.prototype.drawHexGrid = function (rows, cols, originX, originY, isDebug) {
    
    this.canvasOriginX = originX;
    this.canvasOriginY = originY;
    
    var currentHexX;
    var currentHexY;
    var text = "";

    var offsetColumn = false;

    for (var col = 0; col < cols; col++) {
        for (var row = 0; row < rows; row++) {

            // if (!offsetColumn) {
            //     currentHexX = (col * this.side) + originX;
            //     currentHexY = (row * this.height) + originY;
            // } else {
            //     currentHexX = col * this.side + originX;
            //     currentHexY = (row * this.height) + originY + (this.height * 0.5);
            // }

            // if (isDebug) {
            //     text = col + "," + row;
            // }

            this.drawHexAtColRow(col, row, "#F8F8F8", text);
        }
        offsetColumn = !offsetColumn;
    }
};

HexagonGrid.prototype.drawHexEditMode = function() {
    const radius = this.radius;
    const height = Math.sqrt(3) * radius;
    const side = (3 / 2) * radius;

    // Dimensions visibles en tenant compte du zoom et du décalage
    const visibleWidth = this.canvas.width / this.scale;
    const visibleHeight = this.canvas.height / this.scale;

    // Calculer les limites visibles en fonction du décalage et de l'origine
    const minX = -this.offsetX / this.scale;
    const minY = -this.offsetY / this.scale;
    const maxX = minX + visibleWidth;
    const maxY = minY + visibleHeight;

    // Déterminer la première et dernière colonne/lignes visibles
    const startCol = Math.floor(minX / side) - 1;
    const endCol = Math.ceil(maxX / side) + 1;
    const startRow = Math.floor(minY / height) - 1;
    const endRow = Math.ceil(maxY / height) + 1;

    // Boucler sur les colonnes et les lignes visibles
    for (let col = startCol; col <= endCol; col++) {
        for (let row = startRow; row <= endRow; row++) {
            const color = "#F8F8F8"; // Couleur pour le mode preview
            this.drawHexAtColRow(col, row, color, "", null, radius, false);
        }
    }
};
HexagonGrid.prototype.drawHexGridFromList = function(dataList, rows, cols) {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.context.save();
    this.context.translate(this.offsetX, this.offsetY);
    this.context.scale(this.scale, this.scale);

    let dataIndex = 0;

    for (let row = 0; row < rows; row++) {
        for (let col = 0; col < cols; col++) {
            if (dataIndex >= dataList.length) break;

            const hexData = dataList[dataIndex];

            this.drawHexAtColRow(
                col,
                row,
                hexData.color,
                hexData.text,
                hexData.image,
                null, 
                true,
                {},
                hexData.group,
                hexData
            );

            dataIndex++;
        }
    }

    this.context.restore();
};

HexagonGrid.prototype.drawHexAtColRow = function(column, row, color, text, image = null, customRadius = null, saved = true, hoverConfig = {}, group = null, extraData = {}) {
    const radius = customRadius || this.radius;
    const height = Math.sqrt(3) * radius;
    const side = (3 / 2) * radius;
    const drawy = column % 2 == 0 
        ? (row * height) + this.canvasOriginY 
        : (row * height) + this.canvasOriginY + (height / 2);
    const drawx = (column * side) + this.canvasOriginX;
    // var drawy = column % 2 == 0 ? (row * this.height) + this.canvasOriginY : (row * this.height) + this.canvasOriginY + (this.height / 2);
    // var drawx = (column * this.side) + this.canvasOriginX;
    const borderWidth = hoverConfig.width || this.lineStroke;

    this.context.lineWidth = borderWidth;
    // if (!this.hasHexagon(column, row)) {
        this.drawHex(drawx, drawy, radius, hoverConfig.color || color, text, image);
        // Stockez cet hexagone dans la grille
        if(saved){
            this.drawedHex[`${column},${row}`] = { column: column, row: row, color: color, text: text ,image: image, radius: radius,hoverConfig: hoverConfig, group: group, extra : extraData};
        }
    // } else {
    //     console.log(`Hexagone déjà présent à la colonne ${column} et la ligne ${row}`);
    // }
};

HexagonGrid.prototype.removeHexAtColRow = function(column, row) {
    const hexKey = `${column},${row}`;
    if (this.hasHexagon(column, row)) {
        delete this.drawedHex[hexKey]; // Supprime l'hexagone de l'objet drawedHex

        // Efface l'hexagone du canvas
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.save();
        this.context.translate(this.offsetX, this.offsetY);
        this.context.scale(this.scale, this.scale);
        
        // Redessine tous les hexagones restants
        $.each(this.drawedHex, (key, value) => {
            this.drawHexAtColRow(value.column, value.row, value.color, value.text, value.image, value.radius, true, value.hoverConfig, value.group, value.extra);
        });
        this.context.restore();
    }
}

HexagonGrid.prototype.drawHex = function(x0, y0, radius, fillColor, text, image = null) {
    const height = Math.sqrt(3) * radius;
    const width = 2 * radius;
    const side = (3 / 2) * radius;
    
    this.context.strokeStyle = this.colorStroke;
    this.context.beginPath();
    this.context.moveTo(x0 + width - side, y0);
    this.context.lineTo(x0 + side, y0);
    this.context.lineTo(x0 + width, y0 + (height / 2));
    this.context.lineTo(x0 + side, y0 + height);
    this.context.lineTo(x0 + width - side, y0 + height);
    this.context.lineTo(x0, y0 + (height / 2));

    if (fillColor) {
        this.context.fillStyle = fillColor;
        this.context.fill();
    }

    this.context.closePath();
    this.context.stroke();
    if(image){
        // Vérifier si l'image est dans le cache
        if (typeof this.imageCache[image] === "undefined") {
            this.imageCount++;
            let hexSvg = new Image();
            hexSvg.onload = () => {
                this.imageLoaded++;
                this.imageCache[image] = hexSvg;
                // this.context.translate(this.offsetX, this.offsetY);
                // this.context.scale(this.scale, this.scale);

                this.drawImageInHex(hexSvg, x0, y0, radius);
                if(this.imageCount == this.imageLoaded && this.notFitted == false){
                    this.fitBounds();
                    this.notFitted = true;
                }
                // this.fitBounds();

            };
            hexSvg.onerror = () => {
                this.imageCount--;
                if(this.imageCount == this.imageLoaded && this.notFitted == false){
                    this.fitBounds();
                    this.notFitted = true;
                }
            }
            hexSvg.src = baseUrl + image;
        } else {
            // Utiliser l'image depuis le cache
            this.drawImageInHex(this.imageCache[image], x0, y0, radius);
        }
    }

    if (this.isDebug == true && text) {
        this.context.font = "8px";
        this.context.fillStyle = "#000";
        this.context.fillText(text, x0 + (this.width / 2) - (this.width/4), y0 + (this.height - 5));
    }
};

HexagonGrid.prototype.focus = function(d){
    this.zoomToGroup(d);
}
HexagonGrid.prototype.unfocus = async function(){
    this.zoomTo = [];
    this.fitBounds();
}
// Fonction pour dessiner l'image dans les limites de l'hexagone
HexagonGrid.prototype.drawImageInHex = function(img, hexX, hexY, radius, type = "cover") {
    const height = Math.sqrt(3) * radius;
    const width = 2 * radius;
    const imgRatio = img.height / img.width;
    const hexRatio = height / width;

    let drawWidth, drawHeight;
    let offsetX = 0, offsetY = 0;

    if (imgRatio > hexRatio && type === 'cover') {
        // Ajuste la hauteur pour qu'elle corresponde à l'hexagone
        drawWidth = width;
        drawHeight = width * imgRatio;
        offsetY = (height - drawHeight) / 2;
    } else {
        // Ajuste la largeur pour qu'elle corresponde à l'hexagone
        drawHeight = height;
        drawWidth = height / imgRatio;
        offsetX = (width - drawWidth) / 2;
    }

    // Clip pour définir les limites de l'hexagone une seule fois
    this.context.save();
    this.context.beginPath();
    this.context.moveTo(hexX + width - (3 / 2) * radius, hexY);
    this.context.lineTo(hexX + (3 / 2) * radius, hexY);
    this.context.lineTo(hexX + width, hexY + (height / 2));
    this.context.lineTo(hexX + (3 / 2) * radius, hexY + height);
    this.context.lineTo(hexX + width - (3 / 2) * radius, hexY + height);
    this.context.lineTo(hexX, hexY + (height / 2));
    this.context.closePath();
    this.context.clip();

    // Dessine l'image ajustée sans redessiner le contour
    this.context.drawImage(img, hexX + offsetX, hexY + offsetY, drawWidth, drawHeight);

    this.context.restore()
};

HexagonGrid.prototype.oddrToAxial = (hex) => {
    return {column: hex.column, row: hex.row - ((hex.column - (hex.column&1)) / 2)}
}


HexagonGrid.prototype.getRingLevel = (center, level, offSet = 1) => {
    
    const directions = [
        {col: 1 * offSet, row: 0 * offSet},
        {col: 1 * offSet, row: -1 * offSet},
        {col: 0 * offSet, row: -1 * offSet},
        {col: -1 * offSet, row: 0 * offSet},
        {col: -1 * offSet, row: +1 * offSet},
        {col: 0 * offSet, row: 1 * offSet}
    ];

    hexScale = (direction, level) => {
        return { column: directions[direction].col * level, row: directions[direction].row * level };
    }
    axialToOddr = (hex) => {
        return {column: hex.column, row: hex.row + ((hex.column - (hex.column&1)) / 2)}
    }

    hexNeighboors = (hexagon, direction) => {
        return {column: hexagon.column + directions[direction].col, row: hexagon.row + directions[direction].row};
    }
    
    let hexRings = [];
    let scaleHex = hexScale(4, level);
    let current = {
        column: center.column + scaleHex.column,
        row: center.row + scaleHex.row
    }
    for (let i = 0; i < 6; i++) {
        for (let j = 0; j < level; j++) {
            hexRings.push(axialToOddr(current));
            current = hexNeighboors(current, i);
        }
    }
    return hexRings;
}

HexagonGrid.prototype.getHexagonRing = function(ring, center = { column: 0, row: 0 },offset = 1, addCenter = false) {
    let coordinates = [
        
    ];
    
    if(addCenter){
        coordinates.push(center);
    }
    for (let i = 1; i <= ring; i++) {
        coordinates = coordinates.concat(this.getRingLevel(this.oddrToAxial(center), i, offset));
    }

    return coordinates.filter(
        (item, index, self) =>
            index === self.findIndex(t => t.row === item.row && t.column === item.column)
    );
};

//Recusivly step up to the body to calculate canvas offset.
HexagonGrid.prototype.getRelativeCanvasOffset = function() {
	// var x = 0, y = 0;
	// var layoutElement = this.canvas;
    // if (layoutElement.offsetParent) {
    //     do {
    //         x += layoutElement.offsetLeft;
    //         y += layoutElement.offsetTop;
    //     } while (layoutElement = layoutElement.offsetParent);
    //     return { x: x, y: y };
    // }
    const rect = this.canvas.getBoundingClientRect(); // Obtenir la position absolue du canevas
    return {
        x: rect.left + window.scrollX, // Ajuste avec le décalage du scrolling horizontal
        y: rect.top + window.scrollY  // Ajuste avec le décalage du scrolling vertical
    };
}

//Uses a grid overlay algorithm to determine hexagon location
//Left edge of grid has a test to acuratly determin correct hex
HexagonGrid.prototype.getSelectedTile = function(mouseX, mouseY) {
	var offSet = this.getRelativeCanvasOffset();
    mouseX -= offSet.x + this.offsetX;
    mouseY -= offSet.y + this.offsetY;
    mouseX /=  this.scale;
    mouseY /=  this.scale;
    return this.getRowCol(mouseX, mouseY);
};
HexagonGrid.prototype.drawGroupTitle = function(title, x, y) {
    this.context.save();

    // Définir les styles de texte
    const fontSize = Math.max(12, 12 / this.scale);
    this.context.font = `${fontSize}px Arial`;
    this.context.fillStyle = "#000"; // Couleur du texte
    this.context.textAlign = "center";

    // Dessiner le texte
    this.context.fillText(title, x, y);

    this.context.restore();
};

HexagonGrid.prototype.isPointInHexagon = function(px, py, hexX, hexY, radius) {
    const height = Math.sqrt(3) * radius;
    const width = 2 * radius;

    // Calcul des sommets de l'hexagone
    const points = [
        { x: hexX + width - (3 / 2) * radius, y: hexY },
        { x: hexX + (3 / 2) * radius, y: hexY },
        { x: hexX + width, y: hexY + (height / 2) },
        { x: hexX + (3 / 2) * radius, y: hexY + height },
        { x: hexX + width - (3 / 2) * radius, y: hexY + height },
        { x: hexX, y: hexY + (height / 2) }
    ];

    // Vérifier si le point est dans tous les triangles formant l'hexagone
    for (let i = 0; i < points.length; i++) {
        const p1 = points[i];
        const p2 = points[(i + 1) % points.length];
        if (this.sign({ x: px, y: py }, p1, p2) < 0) {
            return false;
        }
    }
    return true;
};

HexagonGrid.prototype.getRowCol = function(mouseX, mouseY){

    for (let key in this.drawedHex) {
        let hex = this.drawedHex[key];
        let height = Math.sqrt(3) * hex.radius;
        let hexX = (hex.column * this.side) + this.canvasOriginX;
        let hexY = hex.column % 2 == 0
            ? (hex.row * height) + this.canvasOriginY
            : (hex.row * height) + this.canvasOriginY + (height / 2);
        if (this.isPointInHexagon(mouseX, mouseY, hexX, hexY, hex.radius)) {
            return { column: hex.column, row:hex.row }; // Retourner la colonne et la ligne de l'hexagone trouvé
        }
    }

    var column = Math.floor((mouseX) / this.side);
    var row = Math.floor(
        column % 2 == 0
            ? Math.floor((mouseY) / this.height)
            : Math.floor(((mouseY + (this.height * 0.5)) / this.height)) - 1);


    //Test if on left side of frame            
    if (mouseX > (column * this.side) && mouseX < (column * this.side) + this.width - this.side) {


        //Now test which of the two triangles we are in 
        //Top left triangle points
        var p1 = new Object();
        p1.x = column * this.side;
        p1.y = column % 2 == 0
            ? row * this.height
            : (row * this.height) + (this.height / 2);

        var p2 = new Object();
        p2.x = p1.x;
        p2.y = p1.y + (this.height / 2);

        var p3 = new Object();
        p3.x = p1.x + this.width - this.side;
        p3.y = p1.y;

        var mousePoint = new Object();
        mousePoint.x = mouseX;
        mousePoint.y = mouseY;

        if (this.isPointInTriangle(mousePoint, p1, p2, p3)) {
            column--;

            if (column % 2 != 0) {
                row--;
            }
        }

        //Bottom left triangle points
        var p4 = new Object();
        p4 = p2;

        var p5 = new Object();
        p5.x = p4.x;
        p5.y = p4.y + (this.height / 2);

        var p6 = new Object();
        p6.x = p5.x + (this.width - this.side);
        p6.y = p5.y;

        if (this.isPointInTriangle(mousePoint, p4, p5, p6)) {
            column--;

            if (column % 2 == 0) {
                row++;
            }
        }
    }

    return  { row: row, column: column };
}

HexagonGrid.prototype.sign = function(p1, p2, p3) {
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
};

HexagonGrid.prototype.isPointInTriangle = function isPointInTriangle(pt, v1, v2, v3) {
    var b1, b2, b3;

    b1 = this.sign(pt, v1, v2) < 0.0;
    b2 = this.sign(pt, v2, v3) < 0.0;
    b3 = this.sign(pt, v3, v1) < 0.0;

    return ((b1 == b2) && (b2 == b3));
};

HexagonGrid.prototype.hasHexagon = function(column, row) {
    return this.drawedHex.hasOwnProperty(`${column},${row}`);
};
HexagonGrid.prototype.getCenterTile = function() {
    // Coordonnées du centre du canevas
    const centerX = this.canvas.width / 2;
    const centerY = this.canvas.height / 2;

    // Ajuster le centre pour le décalage et l'échelle (zoom)
    const adjustedCenterX = (centerX - this.offsetX) / this.scale;
    const adjustedCenterY = (centerY - this.offsetY) / this.scale;

    // Utiliser getSelectedTile pour obtenir la colonne et la ligne au centre du canevas
    return this.getRowCol(adjustedCenterX, adjustedCenterY);
};

HexagonGrid.prototype.clickEvent = function (e) {
    var mouseX = e.pageX;
    var mouseY = e.pageY;

    var localX = mouseX - this.canvasOriginX;
    var localY = mouseY - this.canvasOriginY;

    var tile = this.getSelectedTile(localX, localY);
    if(this.preview == false){
        if (!this.hasHexagon(tile.column, tile.row)) {
            this.context.save();
            this.context.translate(this.offsetX, this.offsetY);
            this.context.scale(this.scale, this.scale); 
            var drawy = tile.column % 2 == 0 ? (tile.row * this.height) + this.canvasOriginY + 6 : (tile.row * this.height) + this.canvasOriginY + 6 + (this.height / 2);
            var drawx = (tile.column * this.side) + this.canvasOriginX;
            this.drawHex(drawx, drawy - 6, "#ddd", "");
            this.drawedHex[`${tile.column},${tile.row}`] = { column: tile.column, row: tile.row, color: "#ddd", image: null, text: "" };  // Stocker la position
            this.context.restore();
        }
    }
};


HexagonGrid.prototype.zoomToGroup = function(groupName, padding = 50) {
    if (!this.data[groupName]) {
        return;
    }

    // Récupérer les limites du groupe
    let minCol = Infinity, maxCol = -Infinity;
    let minRow = Infinity, maxRow = -Infinity;

    Object.values(this.data[groupName]).forEach(hex => {
        minCol = Math.min(minCol, hex.column);
        maxCol = Math.max(maxCol, hex.column);
        minRow = Math.min(minRow, hex.row);
        maxRow = Math.max(maxRow, hex.row);
    });

    const bounds = {
        min: { column: minCol, row: minRow },
        max: { column: maxCol, row: maxRow }
    };

    // Utiliser les limites pour recalculer le zoom
    const height = Math.sqrt(3) * this.radius;
    const minX = bounds.min.column * this.side;
    const maxX = (bounds.max.column + 1) * this.side;

    const minY = bounds.min.column % 2 === 0
        ? bounds.min.row * height
        : bounds.min.row * height + height / 2;
    const maxY = bounds.max.column % 2 === 0
        ? (bounds.max.row + 1) * height
        : (bounds.max.row + 1) * height + height / 2;

    const contentWidth = maxX - minX + this.height * 2;
    const contentHeight = maxY - minY + this.height * 2;

    const scaleX = this.canvas.width / contentWidth;
    const scaleY = this.canvas.height / contentHeight;
    this.scale = Math.min(scaleX, scaleY);

    this.scale = Math.max(0.1, Math.min(this.scale, 10));

    const contentCenterX = (minX + maxX) / 2;
    const contentCenterY = (minY + maxY) / 2;

    this.offsetX = (this.canvas.width / 2) - (contentCenterX * this.scale);
    this.offsetY = (this.canvas.height / 2) - (contentCenterY * this.scale);

    this.zoomTo = Object.values(this.data[groupName]);

    // Redessiner
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.context.save();
    this.context.translate(this.offsetX, this.offsetY);
    this.context.scale(this.scale, this.scale);

    if (this.preview === false) {
        this.drawHexEditMode();
    }
    this.drawData(Object.values(this.data[groupName]));
    this.context.restore();
};

HexagonGrid.prototype.clearCanvas = function(){
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.context.save();
    this.context.translate(this.offsetX, this.offsetY);
    this.context.scale(this.scale, this.scale);
}

HexagonGrid.prototype.zoomToElement = function(column, row, padding = 50) {
    const element = Object.values(this.drawedHex).find(hex => hex.column === column && hex.row === row);
    if (!element) {
        console.error(`Aucun élément trouvé aux coordonnées : column=${column}, row=${row}`);
        return;
    }
    this.zoomTo = [element];
    const height = Math.sqrt(3) * this.radius;
    const minX = column * this.side - this.height;
    const maxX = (column + 1) * this.side + this.height;

    const minY = column % 2 === 0
        ? row * height - this.height
        : row * height + height / 2 - this.height;
    const maxY = column % 2 === 0
        ? (row + 1) * height + this.height
        : (row + 1) * height + height / 2 + this.height;

    const contentWidth = maxX - minX;
    const contentHeight = maxY - minY;

    const scaleX = this.canvas.width / contentWidth;
    const scaleY = this.canvas.height / contentHeight;
    this.scale = Math.min(scaleX, scaleY);

    this.scale = Math.max(0.1, Math.min(this.scale, 10));

    const contentCenterX = (minX + maxX) / 2;
    const contentCenterY = (minY + maxY) / 2;

    this.offsetX = (this.canvas.width / 2) - (contentCenterX * this.scale);
    this.offsetY = (this.canvas.height / 2) - (contentCenterY * this.scale);

    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.context.save();
    this.context.translate(this.offsetX, this.offsetY);
    this.context.scale(this.scale, this.scale);

    if (this.preview === false) {
        this.drawHexEditMode();
    }
    this.drawData(element);
    this.context.restore();
};