class Graph {
    _isDrawing = false;
    _isUpdating = false;
    _isDrawed = false;
    _lastResults = [];
    _rawData = [];
    _data = [];
    _zoom = null;
    _rootSvg = null;
    _rootG = null;
    _leaves = [];
    _colored = [];
    _labeled = [];
    _width = 800;
    _height = GraphUtils.heightByViewportRatio(this._width);
    _authorizedTags = [];
    _zoomInK = 3;
    _zoomOutK = 0.8;
    _navigationNode = null;
    _adaptInsideContainer = false;
    _resizeObserver = null;
    _timeoutResize = null;
    _canResize = false;
    _containerId = null;
    tags = [];
    _focusMode = false;
    _lastZoom = null;
    _zoomInText = "Zoom Avant";
    _zoomOutText = "Zoom Arrière";
    _zoomMagnifyGlassText = "Loupe";
    _zoomResetText = "Recadrer";
    _zoomFsText = "Plein Ecran";
    _currentMode = "graph";
    _buttonGraph = null;
    _buttonList = null;
    _mobileSection = null;
    _activeHexagone = false;
    _hexagoneBtnCallback = null;
    _labelFunc = (d,i,n) => d.data.label;
    _beforeDraw = () => {
        console.log("BEFORE DRAW")
    };
    _afterDraw = () => {
        this.initZoom();
        const container = document.querySelector(this._containerId);
        this.adaptViewBoxByRatio(container.clientWidth / container.clientHeight);
        console.log("AFTER DRAW")
        this._isDrawed = true;
    };
    _beforeUpdate = () => {
        console.log("BEFORE UPDATE")
    };
    _afterUpdate = () => {
        console.log("AFTER UPDATE")
    };

    setBeforeUpdate(callback){
        this._beforeUpdate = callback;
    }
    setAfterUpdate(callback){
        this._afterUpdate = callback;
    }

    setHexagoneBtnCallback(callback){
        this._hexagoneBtnCallback = callback;
    }

    setLabelFunc(callback){
        this._labelFunc = callback;
    }
    setOnFocusChange(callback){
        this._onFocusChange = callback;
    }
    _onFocusChange = (newFocus) => {
        console.log(`FOCUS: ${newFocus}`)
    }
    _onClickNode = (event, data) => {
        console.log("CLICKED", data);
    };
    _onMouseoverNode = (event,data) => {
        console.log("MOUSEOVER", data);
    }
    _onMouseoutNode = (event,data) => {
        console.log("MOUSEOUT", data);
    }
    _onFullScreenChanged = () => {
        console.log("FULLSCREEN QUIT")
    }
    _onZoom = () => {};
    _defaultColor = d3.scaleOrdinal([
        "#F9C1C8",
        "#DCEEC2",
        "#FBE5C1",
        "#B6C5F0",
        "#CCEFFC",
    ]);

    /**
     *
     * @param {any} d Data inside current node
     * @param {number} i Index of current node
     * @param {node[]} n array of node[]
     * @returns
     */
    _color = (d, i, n) => {
        return this._defaultColor(i);
    };
    updateData(data, draw = true) {
        this._rawData = data;
        this._data = this._preprocessData(data);
        if(draw){
            this._update(this._data);
        }
    }
    setActivateHexagone(value, callback = null){
        this._activeHexagone = value;
        this._hexagoneBtnCallback = callback;
    }
    _update(data) {
        if(this._mobileSection!=null){
            this._mobileSection
                .select("div.list-group.list-group-root.well")
                .selectAll("a.list-group-item")
                .data(data, (d) => JSON.stringify(d.data))
                .join((enter) => {
                    const childrens = enter
                        .append("xhtml:a")
                        .attr("href", d=> {
                            return "#" + GraphUtils.slugify(d.data[0])}
                        )
                        .attr("class", "list-group-item")
                        .attr("data-toggle", "collapse")
                        .html(d => `<i class="fa fa-chevron-right icon"></i>${d.data[1][0].group}`)
                        .on('click', function() {
                            const icon = d3.select(this)
                                .select("i.icon")
                            const contains = icon.node().classList.contains("fa-chevron-right");

                            if(contains){
                                icon.classed("fa-chevron-down", true)
                                icon.classed("fa-chevron-right", false)
                            }else{
                                icon.classed("fa-chevron-down", false)
                                icon.classed("fa-chevron-right", true)
                            }
                        })

                    enter.each((d, i, n) => {
                        d3.select(n[i])
                            .insert("xhtml:div", `a[href="#${GraphUtils.slugify(d.data[0])}"] + *`)
                            .attr("class","list-group collapse")
                            .attr("id",d => GraphUtils.slugify(d.data[0]))
                            .selectAll("a.list-group-item")
                            .data(d => d.children)
                            .join(node => {
                                const a = node.append("xhmtl:a")
                                    .classed("list-group-item", true)
                                    .classed("lbh", true)
                                    .attr("href", d=>`#page.type.organizations.id.${d.data.id}`)
                                // .on('click', this._onClickNodeMobile)
                                a.filter(d => d.data.img)
                                    .append("xhtml:img")
                                    .attr("src", (d) => d.data.img)
                                    .attr("alt", (d) => d.data.label)
                                a.append("xhtml:span")
                                    .text(d => d.data.label)
                            })
                    })
                })
        }
    }

    

    async boundZoomToGroup(t, padding=30) {
        const target = "[data-group=" + GraphUtils.slugify(t) + "]"
        const currentZoom = d3.zoomTransform(this._rootSvg.node());
        this._lastZoom = currentZoom;
        this._rootSvg.call(this._zoom.transform, d3.zoomIdentity)
        const bound = this._rootG.node().getBoundingClientRect(); console.log(bound)
        const targetBound = this._rootSvg.select(target).node().getBoundingClientRect();
        console.log(targetBound)
        this._rootSvg.call(this._zoom.transform, currentZoom);

        const containerBound = this._rootSvg.node().getBoundingClientRect();

        const k1 = isFinite(containerBound.width / bound.width) ? ((containerBound.width - 50) / bound.width): 1;
        const k2 = isFinite(containerBound.height / bound.height) ? ((containerBound.height - 50) / bound.height): 1;
        const k = (k1 > k2 ? k2 : k1);

        const l1 = isFinite(containerBound.width / targetBound.width) ? ((containerBound.width - padding * 2) / targetBound.width): 1;
        const l2 = isFinite(containerBound.height / targetBound.height) ? ((containerBound.height - padding * 2) / targetBound.height): 1;
        const l = (l1 > l2 ? l2 : l1);

        const currentViewBox = this._rootSvg.node().viewBox.baseVal;

        //ADAPT TRANSFORMATION INTO VIEWBOX SCOPE
        const wRatio = currentViewBox.width / containerBound.width;
        const hRatio = currentViewBox.height / containerBound.height;

        let tx = Math.abs(containerBound.x - bound.x) * k + (containerBound.width / 2 - (bound.width / 2) * k);
        let ty = Math.abs(containerBound.y - bound.y) * k + (containerBound.height / 2 - (bound.height / 2) * k);
        tx *= wRatio;
        ty *= hRatio;
        tx += currentViewBox.x
        ty += currentViewBox.y

        let ux = (containerBound.x - targetBound.x) * l + (containerBound.width / 2 - (targetBound.width / 2) * l);
        let uy = (containerBound.y - targetBound.y) * l + (containerBound.height / 2 - (targetBound.height / 2) * l);
        ux *= wRatio;
        uy *= hRatio;
        ux += currentViewBox.x
        uy += currentViewBox.y

        return this._rootSvg.transition().duration(this._transition)
            .call(this._zoom.transform, d3.zoomIdentity.translate(tx,ty).scale(k))
            .call(this._zoom.transform, d3.zoomIdentity.translate(ux,uy).scale(l))
            .end().then(() => {
                this._focusMode = GraphUtils.slugify(t);
                this._rootSvg.on('.zoom', null)
            })
    }

    setOnZoom(callback) {
        this._onZoom = callback;
    }
    setOnClickNode(callback) {
        this._onClickNode = callback;
        if (this._isDrawed) {
            if (Array.isArray(this._leaves)) {
                for (const leaf of this._leaves) {
                    leaf.on("click", this._onClickNode);
                }
            } else {
                this._leaves.on("click", this._onClickNode);
            }
        }
    }
    setOnMouseoverNode(callback) {
        this._onMouseoverNode = callback;
        if (this._isDrawed) {
            if (Array.isArray(this._leaves)) {
                for (const leaf of this._leaves) {
                    leaf.on("mouseover", this._onMouseoverNode);
                }
            } else {
                this._leaves.on("mouseover", this._onMouseoverNode);
            }
        }
    }

    setOnMouseoutNode(callback) {
        this._onMouseoutNode = callback;
        if (this._isDrawed) {
            if (Array.isArray(this._leaves)) {
                for (const leaf of this._leaves) {
                    leaf.on("mouseout", this._onMouseoutNode);
                }
            } else {
                this._leaves.on("mouseout", this._onMouseoutNode);
            }
        }
    }

    setColor(callback) {
        this._color = callback;
        if (this._isDrawed) {
            var setColorByType = (current) => {
                if (current.node() instanceof SVGElement) {
                    current.attr("fill", (d, i, n) => this._color(d, i, n));
                    current.style("fill", (d, i, n) => this._color(d, i, n));
                } else if (current.node() instanceof HTMLElement) {
                    current.style("background-color", (d, i, n) => this._color(d, i, n));
                }
            }
            for (const color of this._colored) {
                if (color.node()) {
                    setColorByType(color);
                }else if(color.nodes()){
                    console.log(color)
                    color.each(function(d) {
                        console.log(d3.select(this))
                        setColorByType(d3.select(this))
                    })
                }
            }
        }
    }
    setBeforeDraw(callback) {
        this._beforeDraw = callback;
    }
    _preprocessData(data) {
        return data;
    }
    preprocessResults(results){
        this._lastResults = results;
        return results
    }
    _isDataEmpty(data) {
        if (Array.isArray(data) && data.length == 0) {
            return true;
        }
        return false;
    }
    addSwitcherMode(containerId){
        this._drawModeSwitcher(containerId);
        this._mobileSection = d3.select(containerId);
        this._mobileSection.append("xhtml:div")
            .attr("class", "mobile-section")
            .append("html:div")
            .attr("class", "list-group list-group-root well")
        this._rootSvg.on('click', () => {
            this.unfocus();
        })
    }

    draw(containerId) {
        this._containerId = containerId;
        d3.select(containerId)
            .attr("data-graph", this.constructor.name)
            .attr("class", "graph-panel")
            .style("overflow", "hidden")
            .selectAll("svg.graph")
            .remove();
        this._rootSvg = d3
            .select(containerId)
            .insert("svg", ":first-child")
            .attr("id", "graph")
            .attr("style", "cursor: move")
            .attr("title", "Double clique pour zoom avant");
        this._rootG = this._rootSvg.append("g");
        this.adaptViewBoxByRatio()
        this.adaptInsideContainer()
        this.drawNavigation(containerId);
    }
    adaptViewBoxByRatio(ratio = 16/7){
        this._height = GraphUtils.heightByViewportRatio(this._width, ratio);
        if(this._height < 350){
            this._height = 350;
        }
        this._rootSvg
            .attr("viewBox", [0, 0, this._width, this._height]);
    }
    adaptInsideContainer(){
        if(!this._resizeObserver){
            this._resizeObserver = new ResizeObserver(entries => {
                if(this._canResize){
                    for (const entry of entries) {
                        this.adaptViewBoxByRatio(entry.contentRect.width / entry.contentRect.height);
                    }
                    this._canResize = false;
                    this._timeoutResize = setTimeout(() => {
                        this._timeoutResize = null;
                        this._canResize = true;
                    },300)
                }
            })
            this._resizeObserver.observe(document.querySelector(this._containerId))
        }
    }
    setAfterDraw(callback) {
        this._afterDraw = () => {
            callback();
            this.initZoom();
            this._isDrawed = true;
            this.setOnMouseoutNode(this._onMouseoutNode)
            this.setOnMouseoverNode(this._onMouseoverNode)
        };
    }
    initZoom = () => {
        if(!this._rootSvg) return;
        console.log("INIT ZOOM");
        this._rootSvg.call(this._zoom.transform, d3.zoomIdentity.translate(0,0).scale(1))
    }
    _zoomBy = (value) => {
        this._zoom.scaleBy(this._rootSvg.transition(), value)
    }
    zoomIn = () => {
        this._zoomBy(this._zoomInK);
    }
    toggleMagnifyGlass = () => {
        if(document.getElementById("magnifier-glass-"+this._containerId.replace("#",""))){
            document.getElementById("magnifier-glass-"+this._containerId.replace("#","")).remove(); 
        }else{
            this.magnify(this._containerId, 3);
        }
        
    }
    zoomOut = () => {
        this._zoomBy(this._zoomOutK);
    }
    get rootG() {
        return this._rootG;
    }
    get rootSvg() {
        return this._rootSvg;
    }
    get leaves() {
        return this._leaves;
    }
    get isDrawed() {
        return this._isDrawed;
    }
    drawNavigation(containerId){
        const container = d3.select(containerId)
            .style("background-color", "white")
            .style("position", "relative");

        this._navigationNode = container
            .append("xhtml:div")
            .style("height", "auto")
            .style("width", "auto")
            .style("border-radius", "4px")
            .style("box-shadow", "rgb(0 0 0 / 50%) 0px 0px 3px -1px")
            .style("background-color", "white")
            /*.style("z-index", 20000)*/
            .style("border-color", "#acacaa")
            .style("position", "absolute")
            .style("top", "10px")
            .style("left", "10px")
            .style("display", "flex")
            .style("flex-direction", "column")
            .style("align-items", "start")
            .style("justify-content", "space-around")
            .style("font-size", "14pt")
            .style("z-index", 2)
            .classed("navigation", true);
        const zoomIn = this._navigationNode
            .append("xhtml:button")
            .style("width", "100%")
            .style("height", "100%")
            .style("background-color", "transparent")
            .style("border", "none")
            .attr("class", "text-left padding-10")
        zoomIn.append("xhtml:i")
            .attr("class", "fa fa-search-plus")
        zoomIn.append("xhtml:span")
            .text(" "+this._zoomInText);

        const zoomOut = this._navigationNode
            .append("xhtml:button")
            .style("width", "100%")
            .style("height", "100%")
            .style("background-color", "transparent")
            .style("border", "none")
            .attr("class", "text-left padding-10")
        zoomOut.append("xhtml:i")
            .attr("class", "fa fa-search-minus")
        zoomOut.append("xhtml:span")
            .text(" "+this._zoomOutText);

        const magnifyGlass = this._navigationNode
            .append("xhtml:button")
            .style("width", "100%")
            .style("height", "100%")
            .style("background-color", "transparent")
            .style("border", "none")
            .attr("class", "text-left padding-10")
        magnifyGlass.append("xhtml:i")
            .attr("class", "fa fa-search")
        magnifyGlass.append("xhtml:span")
            .text(" "+this._zoomMagnifyGlassText);

        const zoomReset = this._navigationNode
            .append("xhtml:button")
            .style("width", "100%")
            .style("height", "100%")
            .style("background-color", "transparent")
            .style("border", "none")
            .attr("class", "text-left padding-10")
        zoomReset.append("xhtml:i")
            .attr("class", "fa fa-arrows-alt")
        zoomReset.append("xhtml:span")
            .text(" "+this._zoomResetText)

        const fullscreen = this._navigationNode
            .append("xhtml:button")
            .style("width", "100%")
            .style("height", "100%")
            .style("background-color", "transparent")
            .style("border", "none")
            .attr("class", "text-left padding-10")
        fullscreen.append("xhtml:i")
            .attr("class", "fa fa-window-maximize")
        fullscreen.append("xhtml:span")
            .attr("class", "fullscreen-lbl")
            .text(" "+this._zoomFsText)

        zoomIn.on("click", this.zoomIn)
        zoomOut.on("click", this.zoomOut)
        magnifyGlass.on("click", this.toggleMagnifyGlass);
        zoomReset.on("click", this.initZoom)

        fullscreen.on("click", () => {
            if (
                document.fullscreenElement ||
                document.webkitFullscreenElement ||
                document.mozFullScreenElement ||
                document.msFullscreenElement
            ) {
                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                } else if (document.msExitFullscreen) {
                    document.msExitFullscreen();
                }
                this.adaptViewBoxByRatio()
                d3.select(this._containerId)
                    .classed("fullscreen", false)
                fullscreen.select("span").text(" Plein Ecran");
                d3.select("div.info-escap").remove();
            } else {
                let element = document.querySelector(containerId)
                if (element.requestFullscreen) {
                    element.requestFullscreen();
                } else if (element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if (element.webkitRequestFullscreen) {
                    element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                } else if (element.msRequestFullscreen) {
                    element.msRequestFullscreen();
                }
                d3.select("div.info-escap").remove();
                d3.select(this._containerId).append("xhtml:div")
                    .style("width", "90%")
                    .style("height", "auto")
                    .style("background-color", "#253C76")
                    .style("text-align", "center")
                    .style("font-size", "16pt")
                    .style("font-weight", "bold")
                    .style("margin", "0px 5% 0px 5%")
                    .style("padding", ".5em")
                    .style("position", "absolute")
                    .style("bottom", "2em")
                    .attr("class", "text-center text-white info-escap")
                    .text("Appuyez sur [ECHAP] pour quitter le mode plein écran");
                fullscreen.select("span").text(" Quitter le Mode Plein Ecran");
                this._fullscreen()
            }
        })

        document.onfullscreenchange = (e) => {
            if (
                document.fullscreenElement ||
                document.webkitFullscreenElement ||
                document.mozFullScreenElement ||
                document.msFullscreenElement
            )
            {
                this.adaptViewBoxByRatio(window.innerWidth/window.innerHeight)
            }else{
                document.querySelector("span.fullscreen-lbl").innerText = " Plein Ecran";
                d3.select("div.info-escap").remove();
            }
        }
    }

    setAuthorizedTags(authorizedTags,redraw = false, isData = false){
        this._authorizedTags = authorizedTags;
        if(redraw){
            if(isData){
                this.updateData(this._rawData);
            }else{
                this.updateData(this.preprocessResults(this._lastResults));
            }
        }
    }
    _fullscreen(){
        const container = d3.select(this._containerId)
            .classed("fullscreen", true)
        const { width, height} = container.node().getBoundingClientRect();
        this.adaptViewBoxByRatio(width / height)
    }

    _drawModeSwitcher(containerId) {
        const container = d3.select(containerId)
        this._navigationNode = container
            .append("xhtml:div")
            .style("border-radius", "4px")
            .style("box-shadow", "rgb(0 0 0 / 50%) 0px 0px 5px 0px")
            .style("background-color", "white")
            /*.style("z-index", 20000)*/
            .style("border-color", "#acacaa")
            .style("position", "absolute")
            .style("top", "10px")
            .style("right", "10px")
            .style("display", "flex")
            .style("align-items", "center")
            .style("justify-content", "space-around")
            .style("z-index", "1001")
            .classed("group-button-switcher", true)
        const iconSize = 20;
        const svgListIcon = `
          <svg style="height: ${iconSize}px; width: ${iconSize}px;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="svg-list-icon" x="0px" y="0px" viewBox="0 0 297 297" style="enable-background:new 0 0 297 297;" xml:space="preserve">
                <path d="M66.102,0H15.804C7.089,0,0,7.089,0,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804V15.804C81.907,7.089,74.817,0,66.102,0z"/>
                <path d="M173.649,0h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804V15.804C189.453,7.089,182.364,0,173.649,0z"/>
                <path d="M66.102,107.547H15.804C7.089,107.547,0,114.636,0,123.351v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C81.907,114.636,74.817,107.547,66.102,107.547z"/>
                <path d="M173.649,107.547h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C189.453,114.636,182.364,107.547,173.649,107.547z"/>
                <path d="M281.196,0h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804V15.804C297,7.089,289.911,0,281.196,0z"/>
                <path d="M281.196,107.547h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C297,114.636,289.911,107.547,281.196,107.547z"/>
                <path d="M66.102,215.093H15.804C7.089,215.093,0,222.183,0,230.898v50.298C0,289.911,7.089,297,15.804,297h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C81.907,222.183,74.817,215.093,66.102,215.093z"/>
                <path d="M173.649,215.093h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C189.453,222.183,182.364,215.093,173.649,215.093z"/>
                <path d="M281.196,215.093h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C297,222.183,289.911,215.093,281.196,215.093z"/>
          </svg>
        `
        const svgGraphIcon = `
        <svg style="height: ${iconSize}px; width: ${iconSize}px;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="svg-graph-icon" x="0px" y="0px" viewBox="0 0 349.899 349.898" style="enable-background:new 0 0 349.899 349.898;" xml:space="preserve">
          <path d="M175.522,12.235c-42.6,0-77.256,34.649-77.256,77.25c0,42.6,34.656,77.255,77.256,77.255    c42.591,0,77.257-34.656,77.257-77.255C252.779,46.895,218.113,12.235,175.522,12.235z"/>
          <path d="M77.255,337.663c42.599,0,77.255-34.641,77.255-77.251c0-42.594-34.656-77.25-77.255-77.25    C34.653,183.162,0,217.818,0,260.412C0,303.012,34.653,337.663,77.255,337.663z"/>
          <path d="M272.648,183.151c-42.603,0-77.256,34.65-77.256,77.256c0,42.604,34.653,77.25,77.256,77.25    c42.6,0,77.251-34.646,77.251-77.25C349.909,217.818,315.248,183.151,272.648,183.151z"/>
        </svg>
        `
        const svgHexagoneIcon = `
            <svg xmlns="http://www.w3.org/2000/svg" style="height: ${iconSize}px; width: ${iconSize}px" viewBox="0 0 256 256">
                <path d="M223.68,66.15,135.68,18h0a15.88,15.88,0,0,0-15.36,0l-88,48.17a16,16,0,0,0-8.32,14v95.64a16,16,0,0,0,8.32,14l88,48.17a15.88,15.88,0,0,0,15.36,0l88-48.17a16,16,0,0,0,8.32-14V80.18A16,16,0,0,0,223.68,66.15ZM216,175.82,128,224,40,175.82V80.18L128,32h0l88,48.17Z"></path>
            </svg>
        `
        this._buttonGraph = this._navigationNode.append("xhtml:div")
            .style("padding", "10px")
            .classed("button-switcher-mode", true)
            .classed("active", true)
            .style("display", "flex")
            .style("justify-content", "center")
            .style("align-items", "center")
            .style("font-size", "12pt")
            .style("font-weight", "bold")
            .style("cursor", "pointer")
            .html(svgGraphIcon+" <span class='padding-left-5'> Mode Mapping</span>")
            .on("click", () => {
                if(this._currentMode != "graph"){
                    this.switchMode("graph");
                }
            })
        this._buttonList = this._navigationNode.append("xhtml:div")
            .style("padding", "10px")
            .classed("button-switcher-mode", true)
            .style("display", "flex")
            .style("justify-content", "center")
            .style("align-items", "center")
            .style("font-size", "12pt")
            .style("font-weight", "bold")
            .style("cursor", "pointer")
            .html(svgListIcon+" <span class='padding-left-5'> Mode Liste</span>")
            .on("click", () => {
                if(this._currentMode != "list"){
                    this.switchMode("list");
                }
            })

        if(this._activeHexagone){
            this._navigationNode.append("xhtml:div")
                .style("padding", "10px")
                .classed("button-switcher-mode", true)
                .style("display", "flex")
                .style("justify-content", "center")
                .style("align-items", "center")
                .style("font-size", "12pt")
                .style("font-weight", "bold")
                .style("cursor", "pointer")
                .html(svgHexagoneIcon+" <span class='padding-left-5'> Mode Hexagone</span>")
                .on("click", () => {
                    if(this._hexagoneBtnCallback){
                        this._hexagoneBtnCallback();
                    }
                })
        }
    }

    switchMode = (mode) => {
        if (mode == "graph"){
            this._buttonGraph.classed("active", true);
            this._buttonList.classed("active", false);
            $(this._containerId+" .mobile-section").hide();
            this._currentMode = mode;
            this._transition = 750;
        }else if(mode == "list"){
            $(this._containerId+" .mobile-section").show();
            this._buttonGraph.classed("active", false);
            this._buttonList.classed("active", true);
            this._currentMode = mode;
            this._transition = 0
        }else{
            console.error("MODE UNKNOWN");
        }
    }

    toggleMode = () => {
        if(this._currentMode === "graph") {
            this.switchMode("list")
        }else{
            this.switchMode("graph")
        }
    }

    async focus(target){
        if(this._focusMode == GraphUtils.slugify(target)) return;
        this.unfocus().then(() => {
            this.boundZoomToGroup(target).then(() => {
                this._onFocusChange(target);
            })
            const container = d3.select(this._containerId);
            container.selectAll(".mobile-section > .list-group-item").each((d,i,n) => {
                n[i].classList.add("force-hide");
            })
            container.selectAll(".mobile-section > .list-group").each((d,i,n) => {
                n[i].classList.add("force-hide");
            })
            container.select(".mobile-section > .list-group-item[href='#" + GraphUtils.slugify(target) + "']")
                .classed("force-hide", false);
            container.select(".mobile-section > .list-group#" + GraphUtils.slugify(target))
                .classed("force-hide", false);
        })

    }

    unfocus = async () => {
        if(this._focusMode){
            const container = d3.select(this._containerId);
            container.selectAll(".mobile-section > .list-group-item").each((d,i,n) => {
                n[i].classList.remove("force-hide");
            })
            container.selectAll(".mobile-section > .list-group").each((d,i,n) => {
                n[i].classList.remove("force-hide");
            })
            this._onFocusChange(null);
            this._focusMode = false
            if(this._zoom){
                this._rootSvg.call(this._zoom)
                if(this._lastZoom){
                    return this._rootSvg.transition().duration(this._transition).call(this._zoom.transform, this._lastZoom).end().then(() => {
                        this._lastZoom = null;
                    })
                }else{
                    return this.initZoom();
                }
            }else{

            }
        }
    };

    magnify(elementId, zoom) {
        var elementToMagnify, glass, width, height, bw, svgNode, svgClientRect;
        elementToMagnify = document.getElementById("graph")//elementId.replace("#","graph"));
        d3.selectAll("svg#graph img").each(function(d){
            console.log("here we go", d);
            getBase64Image(document.querySelector("img[src='"+d.data.img+"'"));
        })
        
        d3.selectAll("g.divide>circle").style("fill", "transparent");
        svgNode = d3.select(elementId+" svg#graph").node();
        svgClientRect = svgNode.getBoundingClientRect();
        glass = document.createElement("div");
        $(glass).addClass("magnifier-glass");
        $(glass).attr("id", "magnifier-glass-"+elementId.replace("#",""));

        elementToMagnify.parentElement.insertBefore(glass, elementToMagnify);
        svgToImage((new XMLSerializer().serializeToString(svgNode)), function(pngDataUrl) {
            glass.style.backgroundImage = `url('${pngDataUrl}')`;
            glass.style.backgroundSize = (svgClientRect.width*zoom)+"px "+(svgClientRect.height*zoom)+"px";
        });
        
        glass.style.backgroundRepeat = "no-repeat";
        glass.style.zIndex = 1;
        
        glass.addEventListener("mousemove", moveMagnifier);
        elementToMagnify.addEventListener("mousemove", moveMagnifier);

        glass.addEventListener("touchmove", moveMagnifier);
        elementToMagnify.addEventListener("touchmove", moveMagnifier);
        
        bw = 3;
        width = glass.offsetWidth/2
        height = glass.offsetHeight/2

        function svgToImage(svgContent, callback) {
            const svgBlob = new Blob([svgContent], {type: 'image/svg+xml;charset=utf-8'});
            const url = URL.createObjectURL(svgBlob);
            callback(url);
        }

        function getBase64Image(img) {
            var canvas = document.createElement("canvas");
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0);
            //var dataURL = canvas.toDataURL("image/png");
            //return dataURL.replace(/^data:image\/?[A-z]*;base64,/);
            img.src = canvas.toDataURL("image/png");
            console.log("here we go", img.src)
          }

        function moveMagnifier(e){
            var pos, x, y;
            e.preventDefault();
            pos = getCursorPos(e);
            x=pos.x;
            y=pos.y;
            if(x > svgClientRect.width - (width / zoom)) {
                x = svgClientRect.width - (width / zoom);
            }
            if (x < width / zoom) {
                x = width / zoom;
            }
            if (y > svgClientRect.height - (height / zoom)) {
                y = svgClientRect.height - (height / zoom);
            }
            if (y < height / zoom) {
                y = height / zoom;
            }
            glass.style.left = (x-width)+"px"
            glass.style.top = (y-height)+"px"

            glass.style.backgroundPosition = "-" + ((x * zoom) - width + bw) + "px -" + ((y * zoom) - height + bw) + "px";
        }

        function getCursorPos(e){
            var a, x=0, y=0;
            e = e || window.event;
            a = svgClientRect;
            x = e.pageX - a.left;
            y = e.pageY - a.top;

            x=x-window.pageXOffset;
            y=y-window.pageYOffset;
            return {x, y};
        }
    
    }
}
window.Graph = Graph
class GraphUtils {
    static rectCollide() {
        var nodes, sizes, masses
        var size = constant([0, 0])
        var strength = 1
        var iterations = 1

        function constant(_) {
            return function() {
                return _
            }
        }

        function force() {
            var node, size, mass, xi, yi
            var i = -1
            while (++i < iterations) {
                iterate()
            }

            function iterate() {
                var j = -1
                var tree = d3.quadtree(nodes, xCenter, yCenter).visitAfter(prepare)

                while (++j < nodes.length) {
                    node = nodes[j]
                    size = sizes[j]
                    mass = masses[j]
                    xi = xCenter(node)
                    yi = yCenter(node)

                    tree.visit(apply)
                }
            }

            function apply(quad, x0, y0, x1, y1) {
                var data = quad.data
                var xSize = (size[0] + quad.size[0]) / 2
                var ySize = (size[1] + quad.size[1]) / 2
                if (data) {
                    if (data.index <= node.index) {
                        return
                    }

                    var x = xi - xCenter(data)
                    var y = yi - yCenter(data)
                    var xd = Math.abs(x) - xSize
                    var yd = Math.abs(y) - ySize

                    if (xd < 0 && yd < 0) {
                        var l = Math.sqrt(x * x + y * y)
                        var m = masses[data.index] / (mass + masses[data.index])

                        if (Math.abs(xd) < Math.abs(yd)) {
                            node.vx -= (x *= xd / l * strength) * m
                            data.vx += x * (1 - m)
                        } else {
                            node.vy -= (y *= yd / l * strength) * m
                            data.vy += y * (1 - m)
                        }
                    }
                }

                return x0 > xi + xSize || y0 > yi + ySize ||
                    x1 < xi - xSize || y1 < yi - ySize
            }

            function prepare(quad) {
                if (quad.data) {
                    quad.size = sizes[quad.data.index]
                } else {
                    quad.size = [0, 0]
                    var i = -1
                    while (++i < 4) {
                        if (quad[i] && quad[i].size) {
                            quad.size[0] = Math.max(quad.size[0], quad[i].size[0])
                            quad.size[1] = Math.max(quad.size[1], quad[i].size[1])
                        }
                    }
                }
            }
        }

        function xCenter(d) {
            return d.x + d.vx + sizes[d.index][0] / 2
        }

        function yCenter(d) {
            return d.y + d.vy + sizes[d.index][1] / 2
        }

        force.initialize = function(_) {
            sizes = (nodes = _).map(size)
            masses = sizes.map(function(d) {
                return d[0] * d[1]
            })
        }

        force.size = function(_) {
            return (arguments.length ?
                (size = typeof _ === 'function' ? _ : constant(_), force) :
                size)
        }

        force.strength = function(_) {
            return (arguments.length ? (strength = +_, force) : strength)
        }

        force.iterations = function(_) {
            return (arguments.length ? (iterations = +_, force) : iterations)
        }

        return force
    }


    /**
     *
     * @param {number} x1 x of first point
     * @param {number} y1 y of first point
     * @param {number} x2 x of second point
     * @param {number} y2 y of second point
     * @returns Eucludian distance between two points
     */
    static eucludianDistance(x1, y1, x2, y2) {
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2))
    }


    /**
     *
     * @param {string} text text to slugify
     * @returns Slugified text
     */

    static slugify(text) {
        if(!text){
            return "noslug";
        }
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(/[^\w\-]+/g, '') // Remove all non-word chars
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, ''); // Trim - from end of text
    }

    /**
     *
     * @param {string} a the text of transform
     * @returns obj with key as tranform type and value array of value
     */
    static parseTransform(a) {
        var b = {};
        for (var i in (a = a.match(/(\w+\((\-?\d+\.?\d*e?\-?\d*,?)+\))+/g))) {
            var c = a[i].match(/[\w\.\-]+/g);
            b[c.shift()] = c;
        }
        return b;
    }

    /**
     *
     * @param {string} hex Color in string hex format
     * @param {number} lum value of luminance negative if darker and positive if lighter
     * @returns new color darker or lighter
     */
    static colorLuminance(hex, lum) {

        // validate hex string
        hex = String(hex).replace(/[^0-9a-f]/gi, '');
        if (hex.length < 6) {
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
        }
        lum = lum || 0;

        // convert to decimal and change luminosity
        var rgb = "#",
            c, i;
        for (i = 0; i < 3; i++) {
            c = parseInt(hex.substr(i * 2, 2), 16);
            c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
            rgb += ("00" + c).substr(c.length);
        }

        return rgb;
    }

    /**
     *
     * @param {string} text Text to truncate
     * @param {number} max Max Caractere
     * @param {number} ellispsisText? the text to put at end of truncated text
     * @returns Truncated text
     */
    static truncate(text, max, ellispsisText = '...') {
        if(text){
            return (text.length > max) ? text.substr(0, max - 1) + ellispsisText : text;
        }else{
            return "";
        }
    }


    static splitWords(text) {
        const words = text.split(/\s+/g); // To hyphenate: /\s+|(?<=-)/
        if (!words[words.length - 1]) words.pop();
        if (!words[0]) words.shift();
        return words;
    }



    static splitLines(text) {
        const lineHeight = 12;
        const targetWidth = Math.sqrt(GraphUtils.measureWidth(text.trim()) * lineHeight)
        const words = GraphUtils.splitWords(text)
        let line;
        let lineWidth0 = Infinity;
        const lines = [];
        for (let i = 0, n = words.length; i < n; ++i) {
            let lineText1 = (line ? line.text + " " : "") + words[i];
            let lineWidth1 = GraphUtils.measureWidth(lineText1);
            if ((lineWidth0 + lineWidth1) / 2 < targetWidth) {
                line.width = lineWidth0 = lineWidth1;
                line.text = lineText1;
            } else {
                lineWidth0 = GraphUtils.measureWidth(words[i]);
                line = {
                    width: lineWidth0,
                    text: words[i]
                };
                lines.push(line);
            }
        }
        return lines;
    }

    static textRadius(lines) {
        const lineHeight = 20;
        let radius = 0;
        for (let i = 0, n = lines.length; i < n; ++i) {
            const dy = (Math.abs(i - n / 2 + 0.5) + 0.5) * lineHeight;
            const dx = lines[i].width() / 2;
            radius = Math.max(radius, Math.sqrt(dx ** 2 + dy ** 2));
        }
        return radius;
    }

    static measureWidth(text) {
        const context = document.createElement("canvas").getContext("2d");
        context.font = "1.1em Nunito";
        const textMetrics = context.measureText(text)
        const res = Math.abs(textMetrics.actualBoundingBoxLeft) +
            Math.abs(textMetrics.actualBoundingBoxRight)
        return text => res;
    }

    static squareInnerCircle(cx, cy, r, padding = 0) {
        const l = Math.sin(Math.PI / 4) * (r - padding);
        const x = cx - l;
        const y = cy - l
        const width = Math.abs(l * 2);
        const height = width;
        return {
            x,
            y,
            width,
            height
        }
    }

    static computeBoundVirtualNode(node){
        let bnd;
        let clone = node.cloneNode(true)
        clone.style.cssText = "position:fixed; top:-99999px; left: 50%; opacity:0;z-index: -1;"
        document.body.appendChild(clone);
        bnd = clone.getBoundingClientRect();
        clone.parentNode.removeChild(clone)
        return bnd;
    }

    static textfill(nodeSelector, textContainerSelector, maxFontPixels, alpha = 1){
        var fontSize = maxFontPixels;
        var ourText = $(textContainerSelector, nodeSelector);
        var maxHeight = $(nodeSelector).height();
        var maxWidth = $(nodeSelector).width();
        var textHeight;
        var textWidth;
        do {
            ourText.css('font-size', fontSize);
            textHeight = ourText.height();
            textWidth = ourText.width();
            fontSize = fontSize - alpha;
        } while ((textHeight > maxHeight || textWidth > maxWidth) && fontSize > 3);
    }
    static heightByViewportRatio(width, ratio = 16 / 7){
        return width / ratio;
    }
    static hasImage(d){
        return d.data.img != undefined && d.data.img.trim() != "";
    }
    static filterLinks(links, existingsTags = null){
        const res = []
        const set = new Set();
        for(var i = 0; i < links.length; i++){
            if(existingsTags){
                existingsTags = existingsTags.map((v) => v.toLowerCase())
                if(!existingsTags.includes(links[i].source) || !existingsTags.includes(links[i].target)){
                    continue;
                }
            }
            const normal = links[i].source + "|||" + links[i].target;
            const inverted = links[i].target + "|||" + links[i].source;
            if(set.has(normal) || set.has(inverted)){
                continue;
            }
            set.add(normal);
            set.add(inverted);
            res.push(links[i]);
        }
        return res;
    }
    static wrap(text, width) {
        text.each(function() {
            var text = d3.select(this),
                words = text.text().split(/\s+/).reverse(),
                word,
                line = [],
                lineNumber = 0,
                lineHeight = 1.4, // ems
                y = text.attr("y"),
                x = text.attr("x"),
                dy = parseFloat(text.attr("dy")),
                tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy + "em");

            while (word = words.pop()) {
                line.push(word);
                tspan.text(line.join(" "));
                if (tspan.node().getComputedTextLength() > width) {
                    line.pop();
                    tspan.text(line.join(" "));
                    line = [word];
                    tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
                }
            }
        });
    }//wrap
}
window.GraphUtils = GraphUtils
class GraphTooltip {
    _defaultImage = null;
    _last = null;
    _node = null;
    _titleContainer = null;
    _image = null;
    _anchor = null;
    _description = null;
    _parent = null;
    tooltip = null;

    constructor(containerId) {
        this._parent = containerId;
        const container = d3.select(containerId)
            .style("position", "relative")
            .style("overflow", "hidden");
        if(!container.select("div#graph-tooltip").node()){
            this.tooltip = container
                .append("xhtml:div")
                .attr("id", "graph-tooltip")
                .style("width", "350px")
                .style("display", "none")
                .html(`
            <i class="fa fa-close" style="position: absolute;top: 10px;font-size: 16px;right: 10px; cursor:pointer; z-index:1;" id="graph-tooltip-close"> </i>
            <div class="container-fluid">
            <div class="row">
            <div class="col-xs-12" style="display: flex; align-items: center;">
                <div style="margin-right: 10px;">
                <div id="bulle-image" style="height: 50px; width: 50px; display: block; border-radius: 50%; background-size: contain; background-repeat: no-repeat; background-position: center center; box-shadow: 0px 0px 5px -2px rgb(0 0 0 / 50%);">
                </div>
                
                </div>
                <h3 style="display: block; font-size: 0.9em" id="bulle-title">Titre</h3>
                </div>
                </div>
                <div class="row" id="bulle-description">
                <div class="col-xs-12">
                <span style="display: block; border-bottom: 1px solid #455a64; font-size: 1.1em; margin-bottom: 10px;">Description</span>
                </div>
                <div class="col-xs-12">
                <p class="description" style="font-size: 0.8em; text-align: justify;">
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Veniam quos, odio id suscipit qui tempore.
                </p>
                </div>
                </div>
                <div class="row">
                <div class="col-xs-12" style="text-align: center;">
                <a class="btn btn-more" id="bulle-anchor" href="#page.type.organizations.id.60631a86c7f7b859ca733385">
                En savoir plus
                </a>
                </div>
                </div>
                </div>
                <div class="bulle-arrow"></div>`)
        }else{
            this.tooltip = container.select("div#graph-tooltip")
        }
        this.tooltip.select("#graph-tooltip-close").on('click', () => this.hide());
        this._title = this.tooltip.select("#bulle-title");
        this._image = this.tooltip.select("#bulle-image");
        this._anchor = this.tooltip.select("#bulle-anchor");
        this._anchor.on('click', () => this.hide())
        this._description = this.tooltip.select("#bulle-description");
    }

    set defaultImage(img) {
        this._defaultImage = img;
    }
    set node(n) {
        this._node = n;
        this.goToNode()
    }
    goToNode() {
        if (this._node != null && this._node.node()) {

            const n = $(this._node.node())
            var childPos = n.offset();
            const bnd = this._node.node().getBoundingClientRect();
            var parentPos = $(this._parent).offset();
            const style = {
                top: childPos.top - parentPos.top,
                left: childPos.left - parentPos.left
            }
            this.tooltip
                .style("top", style.top + "px")
                .style("left", (style.left + bnd.width / 2) + "px")
                .style("transform", `translate(-50%,calc(-100% - 20px ))`)
        }
    }
    setContent(data, modePreview = true) {
        if (data == this._last)
            return
        const d = data.data;
        if (d.img) {
            this._image.style("background-image", `url(${d.img})`)
        } else {
            this._image.style("background-image", `url(${this.defaultImage})`)
        }
        this._title.text(d.label)

        if (d.description) {
            this._description.style("display", 'block')
                .select("p.description")
                .text(GraphUtils.truncate(d.description.replace(/(<([^>]+)>)/gi, ""), 150))
        } else {
            this._description.style("display", 'none')
        }
        var collection = 'organizations';
        if(data.collection){
            collection = data.collection;
        }else if(d.collection){
            collection = d.collection;
        }
        this._anchor
            .attr("href", `#page.type.${collection}.id.${d.id}`)
        if(modePreview){
            this._anchor.classed("lbh-preview-element", true)
        }else{
            this._anchor.classed("lbh", true)
        }
        this._last = data;
    }
    show() {
        this.tooltip.style("display", "block")
    }
    hide() {
        this.tooltip.style("display", "none")
    }
}
window.GraphTooltip = GraphTooltip
class CircleGraph extends Graph {
    _id = Math.random()*1000;
    _textColored = [];
    _circlePadding = 30;
    _splitRegex = /(?=[A-Z][a-z])|\s+/g;
    _padding = this._circlePadding - 10;
    _globalMaxX = -Infinity;
    _globalMaxY = -Infinity;
    _nodes = [];
    _size = null;
    _funcGroup = null;
    /**
     *
     * @param {*} data array of obj {img?: url, text?: string, id: string | number}
     * @param {*} funcGroup function to indicate which obj key to group
     */
    constructor(data, funcGroup, authorizedTags = []) {
        super();
        this._authorizedTags = authorizedTags;
        this._funcGroup = funcGroup;
        this._data = this._preprocessData(data);
    }
    // setModeMobile(value){
    //     super.setModeMobile(value);
    //     this.initZoom();
    // }
    preprocessResults(results){
        super.preprocessResults(results);
        const res = []
        var tags = new Set();
        for (const [id, value] of Object.entries(results)) {
            if (value.tags) {

                for (const tag of value.tags) {
                    if(this._authorizedTags && this._authorizedTags.length > 0){
                        if(!this._authorizedTags.includes(tag)){
                            continue;
                        }
                    }
                    const row = {
                        id
                    }
                    row.label = value.name
                    row.description = value.description
                    row.img = value.profilMediumImageUrl
                    row.group = tag
                    tags.add(tag);
                    res.push(row);
                }
            }
        }
        this.tags =[...tags];
        if(this._authorizedTags.length > 0){
            this.tags = this._authorizedTags;
        }
        return res
    }
    initZoom = () => {
        if (!this._rootSvg) return;
        const currentZoom = d3.zoomTransform(this._rootSvg.node());

        this._rootSvg.call(this._zoom.transform, d3.zoomIdentity);
        const bound = this._rootG.node().getBoundingClientRect();
        console.log(bound);
        this._rootSvg.call(this._zoom.transform, currentZoom);

        const containerBound = this._rootSvg.node().getBoundingClientRect();

        const k1 = isFinite(containerBound.width / bound.width)
            ? (containerBound.width - 50) / bound.width
            : 1;
        const k2 = isFinite(containerBound.height / bound.height)
            ? (containerBound.height - 50) / bound.height
            : 1;
        const k = k1 > k2 ? k2 : k1;

        const currentViewBox = this._rootSvg.node().viewBox.baseVal;

        //ADAPT TRANSFORMATION INTO VIEWBOX SCOPE
        const wRatio = currentViewBox.width / containerBound.width;
        const hRatio = currentViewBox.height / containerBound.height;
        let tx = containerBound.width / 2 - (bound.width / 2) * k + Math.abs(containerBound.x - bound.x) * k;
        let ty = containerBound.height / 2 - (bound.height / 2) * k + Math.abs(containerBound.y - bound.y) * k;
        tx *= wRatio;
        ty *= hRatio;
        this._rootSvg
            .transition()
            .call(this._zoom.transform, d3.zoomIdentity.translate(tx, ty).scale(k));
    }
    _preprocessData(data) {
        this._beforeUpdate()
        this._nodes = [];

        const d = this._group(data, this._funcGroup);
        this._dfs(d);

        const packed = d3.packSiblings(this._nodes);

        let minY = Infinity;
        let minX = Infinity;
        for (let i = 0; i < packed.length; i++) {
            const x = packed[i].x - packed[i].r;
            const y = packed[i].y - packed[i].r;
            if (x < minX) minX = x;
            if (y < minY) minY = y;
            const curr_x = packed[i]["r"] + packed[i]["dr"] + packed[i]["x"];
            const curr_y = packed[i]["r"] + packed[i]["dr"] + packed[i]["y"];
            if (this._globalMaxX < curr_x) this._globalMaxX = curr_x;
            if (this._globalMaxY < curr_y) this._globalMaxY = curr_y;
        }
        if (minX != 0) {
            if (minX < 0) {
                for (let i = 0; i < packed.length; i++) {
                    const x = (packed[i]["x"] += Math.abs(minX));
                }
            } else {
                for (let i = 0; i < packed.length; i++) {
                    packed[i]["x"] -= Math.abs(minX);
                }
            }
        }
        if (minY != 0) {
            if (minY < 0) {
                for (let i = 0; i < packed.length; i++) {
                    const y = (packed[i]["y"] += Math.abs(minY));
                }
            } else {
                for (let i = 0; i < packed.length; i++) {
                    packed[i]["y"] -= Math.abs(minY);
                }
            }
        }
        const enclose = d3.packEnclose(packed);
        if (this._isDataEmpty(data)) {
            return packed;
        }
        // CALCUL the X and Y SIZE fitting the entire graph
        this._width = enclose.r * 2;
        return packed;
    }

    /**
     *
     * @param {*} data
     * @param {*} funcGroup
     */
    _group(data, funcGroup) {
        const group = d3.group(data, funcGroup);
        return d3
            .hierarchy(group)
            .sum((d) => d.size)
            .sort((a, b) => b.value - a.value);
    }

    set circlePadding(value) {
        this._circlePadding = value;
        this._padding = this._circlePadding - 10;
    }

    _dfs(parent) {
        console.log(parent);
        if (parent.children) {
            if (parent.children[0].children) {
                // not leaf
                for (let i = 0; i < parent.children.length; i++) {
                    this._dfs(parent.children[i]);
                }
            } else {
                // leaf nodes

                for (let i = 0; i < parent.children.length; i++) {
                    parent.children[i]["x"] = Math.random() * 100 - 50;
                    parent.children[i]["y"] = Math.random() * 100 - 50;
                    if (parent.children[i]["data"].img) {
                        parent.children[i]["width"] = 100;
                        parent.children[i]["bw"] = parent.children[i]["width"];
                        parent.children[i]["height"] = 100;
                        parent.children[i]["bh"] = parent.children[i]["height"];
                    } else {
                        if (!parent.children[i]["textParts"]) {
                            const textParts = parent.children[i]["data"].label.split(
                                this._splitRegex
                            );
                            let maxWLen = -Infinity;
                            for (const parts of textParts) {
                                if (maxWLen < parts.length) {
                                    maxWLen = parts.length;
                                }
                            }
                            maxWLen *= 14;
                            const len = textParts.length;
                            parent.children[i]["textParts"] = textParts;
                            parent.children[i]["maxWidthText"] = maxWLen;
                        }
                        const container = document.createElement("div");
                        const div = document.createElement("div");
                        container.appendChild(div)
                        div.innerHTML = parent.children[i]["data"].label;
                        div.setAttribute("style", `width: ${parent.children[i]["maxWidthText"]}px; padding: 20px;`)
                        const {width, height} = GraphUtils.computeBoundVirtualNode(container);

                        parent.children[i]["width"] = width;
                        parent.children[i]["bw"] = width;
                        parent.children[i]["height"] = height;
                        parent.children[i]["bh"] = height;
                    }
                    if(parent.children.length <= 2){
                        parent.children[i]["x"] = - parent.children[i]["width"] / 2;
                        parent.children[i]["y"] = - parent.children[i]["height"] / 2;
                    }
                }
                //POSITION CALCUL HERE
                const simulation = d3
                    .forceSimulation()
                    .force("center", d3.forceCenter(0, 0))
                    .force("charge", d3.forceManyBody())
                    .force(
                        "collide",
                        GraphUtils.rectCollide().size((d) => {
                            return [d.bw, d.bh];
                        })
                    )
                    .nodes(parent.children)
                    .stop();
                const n = Math.ceil(
                    Math.log(simulation.alphaMin()) /
                    Math.log(1 - simulation.alphaDecay())
                );
                for (var i = 0; i < n; ++i) {
                    simulation.tick();
                }

                // parent.children["simulation"] = simulation;

                let minX = Infinity;
                let maxX = -Infinity;
                let minY = Infinity;
                let maxY = -Infinity;
                for (let i = 0; i < parent.children.length; i++) {
                    const x = Number(parent.children[i]["x"]);
                    const y = Number(parent.children[i]["y"]);
                    if (x < minX) minX = x;
                    if (y < minY) minY = y;
                    if (maxX < x) maxX = x;
                    if (maxY < y) maxY = y;
                }
                const minR = 0; // compute r for placing items (nodes)
                let r = minR;
                for (let i = 0; i < parent.children.length; i++) {
                    const x = parent.children[i]["x"];
                    const y = parent.children[i]["y"];
                    const width = parent.children[i]["bw"];
                    const height = parent.children[i]["bh"];
                    const hyp = Math.sqrt(width ** 2 + height ** 2);
                    const currR = GraphUtils.eucludianDistance(minR, minR, x, y) + hyp;
                    if (r < currR) r = currR;
                }

                parent["r"] = r + this._circlePadding; // SUBSTRACT CIRCLE PADDING AT DRAWING
                parent["dr"] = parent["r"] - minR;
                this._nodes.push(parent);
            }
            // calcul couple le plus loin
        }
    }

    _correctTextParentSize() {
        const svg = this._rootG;
        const [x, y, w, h] = this._rootSvg.attr("viewBox").split(",");
        const dimension = svg.node().getBoundingClientRect();
        let k = Math.max(w, h) / Math.max(dimension.width, dimension.height);
        if (k > 2.5) {
            k = 2.5;
        }
        if(k < 1){
            k = 1;
        }
        const parentTexts = this._rootSvg.selectAll("textPath.parent-text");
        parentTexts.style("font-size", `${50 * k}px`);
    }

    draw(containerId) {
        super.draw(containerId);
        //super.addSwitcherMode(containerId);
        this._rootG
            .attr("id", "circle-root")
        this._zoom = d3.zoom().on("zoom", (e) => {
            this._rootG.attr("transform", e.transform);
            this._correctTextParentSize();
            this._onZoom(e);
        });
        this._rootSvg.call(this._zoom);
        const filter_ombre = this._rootSvg
            .append("defs")
            .append("filter")
            .attr("id", "ombre" + this._id)
            .append("feDropShadow")
            .attr("flood-opacity", 0.3)
            .attr("dx", 0)
            .attr("dy", 1);
        this._update(this._data);
        this._afterDraw();
        this._rootSvg.call(this._zoom.transform, d3.zoomIdentity.translate(0,50));
    }

    _update(data) {
        super._update(data);
        this._leaves = [];
        this._colored = [];
        this._textColored = [];
        this._rootG
            .selectAll("g")
            .data(data, (d) => JSON.stringify(d.data) + d.x + d.y + d.r)
            .join((enter) => {
                const parent_g = enter.append("g");
                parent_g.classed("divide", true)
                    .attr("data-group", d => GraphUtils.slugify(d.data[0]));
                const path = parent_g
                    .append("path")
                    .attr("stroke", "none")
                    .attr("fill", "none")
                    .attr("id", (d) => `path-${GraphUtils.slugify(d["data"][0])}-${this._id}`)
                    .attr(
                        "d",
                        (d) =>
                            `M ${d.x} ${d.y + d.r - this._padding} A 1 1 0 1 1 ${d.x} ${
                                d.y - d.r + this._padding
                            } M ${d.x} ${d.y - d.r + this._padding} A 1 1 0 1 1 ${d.x} ${
                                d.y + d.r - this._padding
                            } `
                    );
                const text = parent_g
                    .append("text")
                    .append("textPath")
                    .style("font-size", "100px")
                    .classed("svg-text", true)
                    .classed("parent-text", true)
                    .attr(
                        "xlink:href",
                        (d) => `#path-${GraphUtils.slugify(d["data"][0])}-${this._id}`
                    )
                    .text((d) => d.children[0].data.group)
                    .attr("fill", (d, i) =>
                        GraphUtils.colorLuminance(this._color(d, i), -0.2)
                    )
                    .attr("text-anchor", "middle")
                    .attr("startOffset", "50%");
                this._textColored.push(text);
                const circle_parent = parent_g
                    .append("circle").attr("cursor", "cursor: zoom-in")
                    .attr("cx", (d) => d.x)
                    .attr("cy", (d) => d.y)
                    .attr("r", (d) => {return (d.r - (this._circlePadding+(d.r*0.3)))})
                    .attr("stroke", "none")
                    .attr("fill", (d, i) => this._color(d, i))
                    .attr("filter", "url(#ombre" + this._id + ")");
                circle_parent.on("click", (e, d) => {
                    //e.stopPropagation()
                    this.focus(d.data[0]);
                    if(e.target.style.cursor=="zoom-in"){
                        e.target.style.cursor = "zoom-out";
                    }else{
                        e.target.style.cursor = "zoom-in";
                    }
                })
                this._colored.push(circle_parent);
                const leaf_svg = parent_g
                    .append("g")
                    .style("overflow", "visible")
                    .classed("leaf-svg", true)
                    .attr("transform", d => `translate(${d.x - d.r + d.dr}, ${d.y - d.r + d.dr})`)
                // .attr("x", (d) => d.x)
                // .attr("y", (d) => d.y);
            });

        this._rootSvg.selectAll("g.leaf-svg")
            .selectAll("g")
            .data(
                (d) => d.children,
                (d) => {
                    return JSON.stringify(d.data);
                }
            )
            .join(
                (enter) => {
                    const leaf_svg_g = enter
                        .append("g")
                        .style("cursor", "pointer")
                        .classed("leaf-group", true)
                        .on("click", this._onClickNode);

                    this._leaves.push(leaf_svg_g);



                    const foreign = leaf_svg_g
                        .append("foreignObject")
                        .style("overflow", "visible")
                        .attr("width", (d) => d.width)
                        .attr("height", (d) => d.height)
                        .attr("x", (d) => d.x)
                        .attr("y", (d) => d.y)
                        .style("transform-box", "fill-box")
                        .style("transform", "translate(-50%, -50%)")

                    foreign.filter((d) => !d.data.img)
                        .append("xhtml:div")
                        .style("overflow", "hidden")
                        .style("text-align", "center")
                        .style("padding", "10px")
                        .style("display", "flex")
                        .style("justify-content", "center")
                        .style("align-items", "center")
                        .style("background-color", "transparent")
                        .style("color", "#455a64")
                        .style("border", "2px solid rgba(69, 90, 100, 0.5)")
                        .style("border-radius", "5px")
                        .text(d => d.data.label)
                        .on("click", this._onClickNode)

                    foreign
                        .filter((d) => d.data.img)
                        .append("xhtml:div")
                        .style("overflow", "hidden")
                        .style("max-width", "100%")
                        .style("max-height", "100%")
                        .style("font-size", "6px")
                        .append("xhtml:img")
                        .attr("src", (d) => d.data.img)
                        .attr("alt", (d) => d.data.label)
                        .style("width", "100%")
                        .style("height", "auto")


                    // const imgs = leaf_svg_g
                    //     .filter((d) => d.data.img)
                    //     .append("image")
                    //     .attr("xlink:href", (d) => d.data.img)



                    this._leaves.push(foreign);

                });
        this._correctTextParentSize();
        this._afterUpdate();
    }
    setColor(callback) {
        super.setColor(callback);
        for (const text of this._textColored) {
            text.attr("fill", (d, i) =>
                GraphUtils.colorLuminance(this._color(d, i), -0.2)
            );
        }
    }
}
window.CircleGraph = CircleGraph
class RelationGraph extends Graph {
    _iconClass = "fa fa-users";
    _links = [];
    _linksNode = [];
    _groups = [];
    _groupsNode = [];
    _defaultColorGroup = d3.scaleOrdinal(d3.schemeCategory10);
    _pathPadding = 30;
    _clicked = false;
    _radius = 75;
    _groupRadius = 100;
    _marginCollide = 35;
    _coloredGroup = [];
    _minX = 0;
    _minY = 0;
    _onClickGroup = () => {};
    _colorGroup = (d, i, n) => {
        return this._defaultColorGroup(i);
    };

    get iconClass() {
        return this._iconClass;
    }
    set iconClass(icon) {
        this._iconClass = icon;
        this._groupsNode.selectAll("i").attr("class", this._iconClass);
    }

    setRadius(radius) {
        this._radius = radius;
    }
    setGroupRadius(radius) {
        this._groupRadius = radius;
    }
    setColorGroup(callback) {
        this._colorGroup = callback;
        for (const coloredGroup of this._coloredGroup) {
            coloredGroup.attr("fill", (d, i, n) => this._colorGroup(d, i, n));
        }
    }
    constructor(rawData,authorizedTags = []) {
        super();
        const {
            data,
            links,
            groups
        } = this._preprocessData(rawData);
        this._authorizedTags = authorizedTags;
        this._links = links;
        this._data = data;
        this._groups = groups;
    }

    preprocessResults(results){
        super.preprocessResults(results);
        const res = []
        var tagsSet = new Set();
        for (const [id, value] of Object.entries(results)) {
            var tags = value.tags;
            if(!tags){
                continue;
            }
            var groups = tags;
            if(this._authorizedTags && this._authorizedTags.length > 0){
                groups = tags.filter(x => this._authorizedTags.indexOf(x) !== -1);
            }
            if(!groups || !groups.length > 0){
                continue;
            }
            for (const group of groups) {
                tagsSet.add(group);
            }
            res.push({...value, id, groups, label: value.name ? value.name : "", img: value.profilMediumImageUrl})
        }
        this.tags = [...tagsSet];
        if(this._authorizedTags.length > 0){
            this.tags = this._authorizedTags;
        }
        return res
    }

    _preprocessData(dataRaws) {
        let data = [],
            links = [],
            groups = [];
        for (const dataRaw of dataRaws) {
            data.push({
                data: dataRaw,
            });
        }
        const tmpSets = new Set();
        for (let i = 0; i < data.length; i++) {
            const el = data[i];
            if (el.data.groups) {
                if (Array.isArray(el.data.groups)) {
                    for (const group of el.data.groups) {
                        tmpSets.add(group);
                        const source = el.data.id;
                        const target = group;
                        links.push({
                            source,
                            target,
                        });
                    }
                } else {}
            }
        }
        groups = Array.from(tmpSets);
        for (const group of groups) {
            data.push({
                data: {
                    id: group,
                    type: "group",
                },
            });
        }
        for (const d of data) {
            d.x = Math.random() * 100;
            d.y = Math.random() * 100;
        }
        var simulation = d3
            .forceSimulation(data)
            .force(
                "link",
                d3
                    .forceLink(links)
                    .id((d) => d.data.id)
                    .distance(200)
                    .strength(1)
            )
            // .force("center", d3.forceCenter().x(width * .5).y(height * .5))
            .force("charge", d3.forceManyBody().strength(-1000))
            .force("x", d3.forceX())
            .force(
                "collide",
                d3
                    .forceCollide()
                    .radius(
                        (d) =>
                            (d.data.type == "group" ? this._groupRadius : this._radius) +
                            this._marginCollide
                    )
                    .iterations(2)
            )
            .force("y", d3.forceY())
            .stop();
        const n = Math.ceil(
            Math.log(simulation.alphaMin()) / Math.log(1 - simulation.alphaDecay())
        );
        for (var i = 0; i < n; ++i) {
            simulation.tick();
        }
        return {
            data,
            links,
            groups,
        };
    }

    draw(containerId) {
        super.draw(containerId);
        this._rootG.append("g").attr("id", "links-group");
        this._update();
        this._afterDraw()
    }

    adaptViewBoxByRatio(ratio = 16/7){
        this._height = GraphUtils.heightByViewportRatio(this._width,ratio);
        this._rootSvg
            .attr("viewBox", [0, 0,this._width,this._height]);
    }

    updateData(rawData, draw = true) {
        const {
            data,
            links,
            groups
        } = this._preprocessData(rawData);
        this._links = links;
        this._data = data;
        this._groups = groups;
        if(draw){
            this._update();
        }
    }
    _update(data) {
        //super._update(data); to do
        this._beforeDraw();
        this._rootG
            .select("g#links-group")
            .selectAll("line")
            .data(this._links, (d) => {
                return JSON.stringify(d);
            })
            .join((enter) => {
                this._linksNode = enter
                    .append("line")
                    .attr("stroke-width", 1.1)
                    .attr("stroke-opacity", 0.3)
                    .attr("stroke", "#999")
                    .attr("x1", (d) => d.source.x)
                    .attr("y1", (d) => d.source.y)
                    .attr("x2", (d) => d.target.x)
                    .attr("y2", (d) => d.target.y);
            });

        this._rootG
            .selectAll("g.leaf")
            .data(this._data, (d) => {
                // return JSON.stringify(d.data)
                return JSON.stringify(d);
            })
            .join((enter) => {
                const nodeSvg = enter
                    .append("g")
                    .classed("leaf", true)
                    .style("overflow", "visible")
                    .style("cursor", "pointer");
                const node = nodeSvg
                this._groupsNode = node.filter(
                    (d) => d.data.type && d.data.type == "group"
                );
                const path = this._groupsNode
                    .append("path")
                    .attr("stroke", "none")
                    .attr("fill", "none")
                    .attr("id", (d, i) => `group-${i}`)
                    .attr(
                        "d",
                        (d) =>
                            `M 0 ${this._groupRadius - this._pathPadding} A 1 1 0 1 1 0 ${
                                -this._groupRadius + this._pathPadding
                            } M 0 ${-this._groupRadius + this._pathPadding} A 1 1 0 1 1 0 ${
                                this._groupRadius - this._pathPadding
                            } `
                    );

                this._leaves = node.filter(
                    (d) => !d.data.type || d.data.type != "group"
                );
                const circle = this._leaves
                    .append("circle")
                    .attr("r", this._radius)
                    .attr("fill", (d, i) => (d.color = this._color(d, i)))
                this.leaves
                    .each(d => {
                        d.innerSquare = GraphUtils.squareInnerCircle(0,0,this._radius, 10);
                    })
                this._leaves
                    .append("foreignObject")
                    .attr("x", d => d.innerSquare.x)
                    .attr("y", d => d.innerSquare.y)
                    .attr("width", d => d.innerSquare.width)
                    .attr("height", d => d.innerSquare.height)

                this._leaves.on("click", this._onClickNode);
                this._zoom = d3.zoom().on("zoom", (e) => {
                    this._rootG.attr("transform", e.transform);
                    if (this._clicked) {
                        this._addMouseEvent(this._leaves, this._groupsNode);
                        this._clicked = false;
                    }
                    this._onZoom();
                });
                this._rootSvg.call(this._zoom);
                const circleGroup = this._groupsNode
                    .append("circle")
                    .attr("r", this._groupRadius)
                    .attr("fill", (d, i) => (d.color = this._colorGroup(d, i)));
                this._coloredGroup.push(circleGroup);
                this._groupsNode
                    .append("text")
                    .append("textPath")
                    .attr("text-anchor", "middle")
                    .style("fill", "white")
                    .style("font-size", "100px")
                    .attr("startOffset", "50%")
                    .attr("xlink:href", (d, i) => `#group-${i}`)
                    .text((d) => d.data.id);
                const fontSize = this._groupRadius * (2 / 3);
                this._groupsNode
                    .append("foreignObject")
                    .attr("x", -50)
                    .attr("y", -50)
                    .attr("width", 100)
                    .attr("height", 100)
                    .append("xhtml:div")
                    .style("width", "100%")
                    .style("height", "100%")
                    .style("display", "flex")
                    .style("justify-content", "center")
                    .style("align-items", "center")
                    .append("xhtml:i")
                    .style("font-size", fontSize + "px")
                    .style("color", "white")
                    .attr("class", (d) => this._iconClass);

                this._groupsNode.on("click", this._focusOnGroup);


                this._colored.push(circle);
                const div = this._leaves
                    .selectAll("foreignObject")
                    .append("xhtml:div")
                    .style("width", "100%")
                    .style("height", "100%")
                    .style("display", "flex")
                    .style("justify-content", "center")
                    .style("align-items", "center")
                div
                    .filter((d) => d.data.img)
                    .append("xhtml:img")
                    .attr("src", (d) => d.data.img)
                    .style("width", "100%")
                    .style("height", "auto")
                const texts = div
                    .filter((d) => !d.data.img)

                texts.append("xhtml:span")
                    .style("text-align", "center")
                    .text(d => d.data.label)

                texts.each((d,i,n) => {
                    GraphUtils.textfill(n[i], 'span', 20);
                })
                nodeSvg.attr("transform", (d) => `translate(${d.x}, ${d.y})`);
                this._addMouseEvent(this._leaves, this._groupsNode);
            });
        this._afterDraw();
    }

    _removeAllMouseEvent() {
        this._leaves.on("mouseover", null);
        this._leaves.on("mouseout", null);
        this._groupsNode.on("mouseover", null);
        this._groupsNode.on("mouseout", null);
        this._groupsNode.on("click", (e) => {
            e.stopPropagation();
        });
    }
    _leafMouseOver(e, data) {
        this._rootSvg.select("g#top-container").remove();
        this._leaves.each((d,i,n) => {
            d3.select(n[i]).classed("svg-blur", true)
        });
        this._groupsNode.each((d,i,n) => {
            d3.select(n[i]).classed("svg-blur", true)
        });

        this._linksNode.classed("svg-blur", true)
        d3.select(e.currentTarget)
            .attr("opacity", "1")
            .classed("svg-blur", false)
            .attr("id", "node-active");

        const top_g = this._rootG
            .insert("g", "#node-active")
            .attr("id", "top-container");

        const activeLink = this._linksNode
            .filter((d) => d.source.data.id == data.data.id)
            .attr("stroke-opacity", 1)
            .attr("stroke-width", 3)
            .classed("svg-blur", false)
            .attr("id", (_, i, node) => {
                return "link-active-" + i;
            });

        activeLink.each((l, i, node) => {
            top_g.append("use").attr("xlink:href", "#link-active-" + i);
        });
        const activeGroup = this._groupsNode
            .filter((d) => data.data.groups.includes(d.data.id))

        activeGroup.each((l, i, node) => {
            d3.select(node[i]).classed("svg-blur", false).attr("id", (_, i) => "group-active-" + i);
            top_g.append("use").attr("xlink:href", "#group-active-" + i);
        });
    }
    _leafMouseOut(d, i) {
        // groups.attr("fill", d => d.data.color)
        // circles.attr("fill", d => d.data.color)
        this._leaves.each((d,i,n) => {
            d3.select(n[i]).classed("svg-blur", false).attr("id", null);
        });
        this._groupsNode.each((d,i,n) => {
            d3.select(n[i]).classed("svg-blur", false).attr("id", null);
        });
        this._linksNode.attr("stroke-width", 1.1)
            .classed("active", false)
            .classed("svg-blur", false)
            .attr("id", null);

        const top = d3.select("g#top-container");
        if (top) {
            top.remove();
        }
    }
    _groupMouseOver(e, data) {
        this._rootSvg.select("g#top-container").remove();
        this._groupsNode.each((d,i,n) => {
            d3.select(n[i]).classed("svg-blur", true)
        });
        this._linksNode.classed("svg-blur", true)

        d3.select(e.currentTarget)
            .classed("svg-blur", false)
            .attr("opacity", "1")
            .attr("id", "node-active");

        const top_g = this._rootG
            .insert("g", "*")
            .attr("id", "top-container");
        const activeLink = this._linksNode
            .filter((e) => e.target.data.id == data.data.id)
            .attr("stroke-opacity", 1)
            .attr("stroke-width", 3)
            .classed("svg-blur", false)
            .attr("id", (_, i, node) => {
                return "link-active-" + i;
            });
        activeLink.each((l, i, node) => {
            top_g.append("use").attr("xlink:href", "#link-active-" + i);
        });
        const activeLeaf = this._leaves
            .filter((d) => d.data.groups.includes(data.data.id))
            .attr("opacity", "1")

        activeLeaf.each((l, i, node) => {
            d3.select(node[i]).attr("id", (_, i) => "leaf-active-" + i);
        });
        this._toggleBlurNotActiveLeaf(activeLeaf);
        activeLink.classed("svg-blur", false);
    }
    _groupMouseOut(d, i) {
        this._toggleBlurNotActiveLeaf(false);
        this._leaves.each((d,i,n) => {
            d3.select(n[i]).attr("opacity", "1").attr("id", null);
        });
        this._groupsNode.each((d,i,n) => {
            d3.select(n[i]).attr("opacity", "1")
                .classed("svg-blur", false)
                .attr("id", null);
        });

        this._linksNode
            .attr("stroke-opacity", 0.3)
            .attr("stroke-width", 1.1)
            .classed("active", false)
            .attr("id", null);
        const top = this._rootSvg.select("g#top-container");
        if (top) {
            top.remove();
        }
    }
    _focusOnGroup(event, data) {
        this._onClickGroup();
        event.stopPropagation();
        this._removeAllMouseEvent();
        this._boundZoomToGroup(this._radius).finally(() => {
            this._clicked = true;
            this._rootSvg.select("#content").on("click", (e) => {
                this._clicked = false;
                e.stopPropagation();
                this._removeAllMouseEvent();
                this._addMouseEvent(this._leaves, group);
                this._leafMouseOut();
                this._groupMouseOut();
                this._rootSvg.select("#content").on("click", null);
            });
        });
    }
    async _boundZoomToGroup(padding = 0) {
        const currentZoom = d3.zoomTransform(this._rootSvg.node());

        this._rootSvg.call(this._zoom.transform, d3.zoomIdentity)
        const bound = this._rootG.node().getBoundingClientRect(); console.log(bound)
        const targetBound = this._rootSvg.select("g#top-container").node().getBoundingClientRect();
        console.log(targetBound)
        this._rootSvg.call(this._zoom.transform, currentZoom);

        const containerBound = this._rootSvg.node().getBoundingClientRect();

        const k1 = isFinite(containerBound.width / bound.width) ? ((containerBound.width - 50) / bound.width): 1;
        const k2 = isFinite(containerBound.height / bound.height) ? ((containerBound.height - 50) / bound.height): 1;
        const k = (k1 > k2 ? k2 : k1);

        const l1 = isFinite(containerBound.width / targetBound.width) ? ((containerBound.width - padding * 2) / targetBound.width): 1;
        const l2 = isFinite(containerBound.height / targetBound.height) ? ((containerBound.height - padding * 2) / targetBound.height): 1;
        const l = (l1 > l2 ? l2 : l1);

        const currentViewBox = this._rootSvg.node().viewBox.baseVal;

        //ADAPT TRANSFORMATION INTO VIEWBOX SCOPE
        const wRatio = currentViewBox.width / containerBound.width;
        const hRatio = currentViewBox.height / containerBound.height;

        let tx = Math.abs(containerBound.x - bound.x) * k + (containerBound.width / 2 - (bound.width / 2) * k);
        let ty = Math.abs(containerBound.y - bound.y) * k + (containerBound.height / 2 - (bound.height / 2) * k);
        tx *= wRatio;
        ty *= hRatio;
        tx += currentViewBox.x
        ty += currentViewBox.y

        let ux = (containerBound.x - targetBound.x) * l + (containerBound.width / 2 - (targetBound.width / 2) * l);
        let uy = (containerBound.y - targetBound.y) * l + (containerBound.height / 2 - (targetBound.height / 2) * l);
        ux *= wRatio;
        uy *= hRatio;
        ux += currentViewBox.x
        uy += currentViewBox.y

        return this._rootSvg.transition().duration(750)
            .call(this._zoom.transform, d3.zoomIdentity.translate(tx,ty).scale(k))
            .call(this._zoom.transform, d3.zoomIdentity.translate(ux,uy).scale(l))
            .end()
    }
    _toggleBlurNotActiveLeaf(activeLeaf) {
        if (activeLeaf) {
            this._leaves
                .on("click", (e) => e.stopPropagation())
                .each((d,i,n) => {
                    d3.select(n[i]).classed("svg-blur", true)
                });
            activeLeaf.on("click", this._onClickNode)
            activeLeaf.each((l, i, node) => {
                d3.select(node[i]).classed("svg-blur", false);
            });
        } else {
            this._leaves
                .on("click", (e) => e.stopPropagation())
                .each((d,i,n) => {
                    d3.select(n[i]).classed("svg-blur", false)
                });
        }
    }

    _addMouseEvent() {
        this.leaves.on("mouseover", (e, d) => this._leafMouseOver(e, d));
        this.leaves.on("mouseout", (e, d) => this._leafMouseOut(e, d));
        this._groupsNode.on("mouseover", (e, d) => this._groupMouseOver(e, d));
        this._groupsNode.on("mouseout", (e, d) => this._groupMouseOut(e));
        this._groupsNode.on("click", (e, d) => this._focusOnGroup(e, d));
    }
    initZoom = () => {
        if(!this._rootSvg) return;
        const currentZoom = d3.zoomTransform(this._rootSvg.node());

        this._rootSvg.call(this._zoom.transform, d3.zoomIdentity)
        const bound = this._rootG.node().getBoundingClientRect(); console.log(bound)
        this._rootSvg.call(this._zoom.transform, currentZoom);

        const containerBound = this._rootSvg.node().getBoundingClientRect();

        const k1 = isFinite(containerBound.width / bound.width) ? ((containerBound.width - 50) / bound.width): 1;
        const k2 = isFinite(containerBound.height / bound.height) ? ((containerBound.height - 50) / bound.height): 1;
        const k = (k1 > k2 ? k2 : k1);

        const currentViewBox = this._rootSvg.node().viewBox.baseVal;

        //ADAPT TRANSFORMATION INTO VIEWBOX SCOPE
        const wRatio = currentViewBox.width / containerBound.width;
        const hRatio = currentViewBox.height / containerBound.height;

        let tx = Math.abs(containerBound.x - bound.x) * k + (containerBound.width / 2 - (bound.width / 2) * k);
        let ty = Math.abs(containerBound.y - bound.y) * k + (containerBound.height / 2 - (bound.height / 2) * k);
        tx *= wRatio;
        ty *= hRatio;
        tx += currentViewBox.x
        ty += currentViewBox.y
        return this._rootSvg.transition().call(this._zoom.transform, d3.zoomIdentity.translate(tx,ty).scale(k))
    }
}
window.RelationGraph = RelationGraph
class MindmapGraph extends Graph {
    _modeAdd = false;
    _rootObj = {
        id: "root",
        label: "SEARCH"
    };
    _duration = 750;
    _nodePadding = {
        top: 5,
        right: 10,
        bottom: 5,
        left: 10,
    };
    _defaultColor = d3.scaleOrdinal(d3.schemePastel2)
    _margin = {
        top: 20,
        right: 180,
        bottom: 30,
        left: 90,
    };
    _theme = null;
    _collapsed = [];

    _source = null;
    _i = 0;

    _nodes = [];
    _links = [];
    _treemap = null;

    _depth = null;

    constructor(data, depthToCollapse = null, rootObj = null , modeAdd = false) {
        super()
        this._modeAdd = modeAdd;
        this._data = this._preprocessData(data);
        if(rootObj){
            this._rootObj = rootObj;
        }
        this._depth = depthToCollapse;
    }

    setTheme(value){
        this._theme = value;
    }

    _preprocessResultsByTheme(results){
        let countTag = 0;
        let res = this._rootObj;
        let raw = []
        const tags = new Set()
        for (const [id, value] of Object.entries(results)) {
            const row = {
                id,
                ...value
            }
            row.label = value.name
            row.description = value.description
            row.img = value.profilMediumImageUrl
            let hasTheme = false;
            if(row.tags){
                for (const tag of row.tags) {
                    for (const [theme, themeTagsObj] of Object.entries(this._theme)) {
                        for (const themeTag of Object.values(themeTagsObj)) {
                            if(tag == themeTag){
                                hasTheme = true;
                                raw.push({...row, theme})
                                tags.add(tag);
                            }
                        }
                    }
                }
            }
        }
        // for (const tag of [...tags]) {
        //     for (const [theme, themeTagsObj] of Object.entries(this._theme)) {
        //         for (const themeTag of Object.values(themeTagsObj)) {
        //             if(tag == themeTag){
        //                 raw.push({theme, collection: 'tags', id: tag, label: tag})
        //             }
        //         }
        //     }
        // }
        this.tags = [...tags];
        if(this._authorizedTags.length > 0){
            this.tags = this._authorizedTags;
        }
        console.log(raw);
        const typesGroup = d3.group(raw, d => d.theme, d => d.collection, d => d.type);
        let children = parcours(typesGroup);
        function parcours(map) {
            let children = []
            if(map instanceof Map){
                for (const [key,value] of map) {
                    if(key){
                        children.push({id: key, label: key, children: parcours(value)})
                    }else{
                        children = parcours(value);
                    }
                }
            }else{
                return map;
            }
            return children;
        }
        res.children = children
        return res
    }

    _preprocessResultsDefault(results){
        let countTag = 0;
        let res = this._rootObj;
        let raw = []
        const tags = new Set()
        for (const [id, value] of Object.entries(results)) {
            const row = {
                id,
                ...value
            }
            row.label = value.name
            row.description = value.description
            row.img = value.profilMediumImageUrl
            raw.push(row);
            if(row.tags){
                for (const tag of row.tags) {
                    tags.add(tag);
                }
            }
        }
        this.tags = [...tags];
        // for (const tag of [...tags]) {
        //     raw.push({
        //             label: tag,
        //             id : tag,
        //             group : 'tags',
        //             collection : 'tags',
        //     })
        // }
        if(this._authorizedTags.length > 0){
            this.tags = this._authorizedTags;
        }
        const typesGroup = d3.group(raw, d => d.collection, d => d.type);
        let children = parcours(typesGroup);
        function parcours(map) {
            let children = []
            if(map instanceof Map){
                for (const [key,value] of map) {
                    if(key){
                        children.push({id: key, label: key, children: parcours(value)})
                    }else{
                        children = parcours(value);
                    }
                }
            }else{
                return map;
            }
            return children;
        }
        res.children = children
        return res
    }

    preprocessResults(results){
        super.preprocessResults(results);
        if(this._theme){
            return this._preprocessResultsByTheme(results);
        }else{
            return this._preprocessResultsDefault(results);
        }
    }
    _addPlusNode(rawData, depth){
        console.log(depth, rawData);
        if(rawData.children && rawData.children.length > 0){
            for (let i = 0; i < rawData.children.length; i++) {
                rawData.children[i] = this._addPlusNode(rawData.children[i], depth + 1);
            }
            rawData.children.push({id: "plus", name: "+", label: "+"})
        }else{
            rawData.children = [{id: "plus", name: "+", label: "+"}];
        }
        return rawData;
    }
    _preprocessData(rawData) {
        if(this._modeAdd){
            rawData = this._addPlusNode(rawData, 0);
        }
        console.log(rawData);
        rawData = d3.hierarchy(rawData);
        const w = this._width - this._margin.left - this._margin.right;
        const h = GraphUtils.heightByViewportRatio(w);
        rawData.x0 = h / 2
        rawData.y0 = 0

        this._source = rawData;

        this._treemap = d3
            .tree()
            .size([w, h])
            .nodeSize([50, 240])
            .separation((a, b) => {
                if (a.parent == b.parent) return 1.5;
                else return 2;
            });
        for (const d of rawData.descendants()) {
            if (this._collapsed.includes(d.data.id)) {
                if (d.children) {
                    d._children = d.children;
                    d.children = null;
                } else {
                    d.children = d._children;
                    d._children = null;
                }
            }
        }
        return rawData;
    }
    draw(containerId) {
        this._beforeDraw()
        super.draw(containerId)
        this._zoom = d3.zoom().on("zoom", (e) => {
            const transform = e.transform;
            if(!(isNaN(transform.k) || isNaN(transform.x) || isNaN(transform.k))){
                this._rootG.attr("transform", e.transform);
                this._onZoom(e);
            }
        });
        this._rootSvg.call(this._zoom);
        if(this._depth != null && this._depth >= 0){
            this.collapseAll(this._data, this._depth);
        }
        this._update(this._data);
        // this._afterDraw()
        // this._rootSvg.call(this._zoom.transform, d3.zoomIdentity.translate(this._margin.left, (this._margin.top + this._height / 2)))
        // setTimeout(() => {
        //     this.initZoom()
        // },1500)
    }

    adaptViewBoxByRatio(ratio = 16/7){
        const w = this._width + this._margin.right + this._margin.left
        const h =  GraphUtils.heightByViewportRatio(w,ratio);
        this._rootSvg
            .attr("viewBox", [0,0,w,h])
    }

    initZoom = () => {
        if(!this._rootSvg) return;
        const currentZoom = d3.zoomTransform(this._rootSvg.node());

        this._rootSvg.call(this._zoom.transform, d3.zoomIdentity);
        const bound = this._rootG.node().getBoundingClientRect(); console.log(bound)
        this._rootSvg.call(this._zoom.transform, currentZoom);

        const containerBound = this._rootSvg.node().getBoundingClientRect();
        const k1 = isFinite(containerBound.width / bound.width) ? ((containerBound.width - 50) / bound.width): 1;
        const k2 = isFinite(containerBound.height / bound.height) ? ((containerBound.height - 50) / bound.height): 1;
        const k = (k1 > k2 ? k2 : k1);

        const currentViewBox = this._rootSvg.node().viewBox.baseVal;

        //ADAPT TRANSFORMATION INTO VIEWBOX SCOPE
        const wRatio = currentViewBox.width / containerBound.width;
        const hRatio = currentViewBox.height / containerBound.height;
        let tx = (containerBound.width / 2) - (bound.width / 2) * k;
        let ty = (containerBound.height / 2) - (bound.height / 2) * k + Math.abs(containerBound.y - bound.y) * k ;
        tx *= wRatio;
        ty *= hRatio;
        console.log("ZOOM", tx,ty, k)
        this._rootSvg.transition().call(this._zoom.transform, d3.zoomIdentity.translate(tx,ty).scale(k))
    }
    setColor(callback) {
        this._color = callback;
        if (this._isDrawed) {
            for (const color of this._colored) {
                if (color.node()) {
                    if (color.node() instanceof SVGElement) {
                        color.attr("fill", (d, i, n) => this._color(d, i, n))
                    } else if (color.node() instanceof HTMLElement) {
                        color.style("background-color", (d, i, n) => this._color(d, d.depth, n));
                    }
                }
            }
        }
    }
    updateData(data, draw = true) {
        this._nodes
        this._data = this._preprocessData(data);
        const tmp = this._duration;
        this._duration = 0
        if(this._depth != null && this._depth >= 0){
            this.collapseAll(this._data, this._depth);
        }
        if(draw){
            this._update(this._data)
        }
        this._duration = tmp;
    }
    _update(data) {
        // super._update(data); to do
        this._beforeUpdate()
        var treeData = this._treemap(data);
        this._nodes = treeData.descendants();
        this._links = treeData.descendants().slice(1);
        var node_g;
        var node = this._rootG
            .selectAll("g.node")
            .data(this._nodes, (d) => d.id || (d.id = ++this._i));
        node.join(
            (enter) => {
                node_g = enter
                    .append("g")
                    .attr("class", "node")
                    .on("click", (e, d) => {
                        this._click(e, d);
                    })
                    .attr(
                        "transform",
                        (d) => "translate(" + this._source.y0 + "," + this._source.x0 + ")"
                    )
                const rect = node_g.append("foreignObject")
                    .style("cursor", "pointer")
                    .attr("width", (d) =>
                    {
                        const span = document.createElement("span")
                        span.style.cssText = "font-size: 15px";
                        span.innerText = GraphUtils.truncate(this._labelFunc(d), 20);
                        d.w = GraphUtils.computeBoundVirtualNode(span).width + this._nodePadding.left + this._nodePadding.right;
                        return d.w;
                    })
                    .attr("height",(d) =>(d.h = 15 + this._nodePadding.top + this._nodePadding.bottom))
                    .attr("x", 0)
                    .attr("y", d => - d.h / 2)
                    .append("xhtml:div")
                    .style("height", "100%")
                    .style("width", "100%")
                    .style("display", "flex")
                    .style("justify-content", "center")
                    .style("align-items", "center")
                    .style("border-radius", "10px")
                    .style("background-color", (d, i, n) => this._color(d, d.depth, n))
                    .text(d => GraphUtils.truncate(this._labelFunc(d), 20))
                    .style("color", "#455a64")
                    .on("mouseover", (e,d) => {
                        const g_parent = d3.select(e.target.parentNode.parentNode)
                        const span = document.createElement("span")
                        span.innerText = this._labelFunc(d);
                        g_parent.select("foreignObject")
                            .attr("width",
                                GraphUtils.computeBoundVirtualNode(span).width + this._nodePadding.left + this._nodePadding.right
                            );
                        g_parent.select("div").text(this._labelFunc)
                        this._onMouseoverNode(e,d)
                    })
                    .on("mouseout", (e,d) => {
                        const g_parent = d3.select(e.target.parentNode.parentNode)
                        g_parent.select("foreignObject").attr("width", d.w)
                        g_parent.select("div").text(d => GraphUtils.truncate(this._labelFunc(d), 20))
                        this._onMouseoutNode(e,d)
                    })
                this._colored.push(rect)
                this._leaves.push(node_g);
            },
            async (update) => {
                Promise.all([
                    node_g
                        .transition()
                        .duration(this._duration)
                        .attr("transform", (d) => {
                            return "translate(" + d.y + "," + d.x + ")";
                        }).end(),
                    update
                        .transition()
                        .duration(this._duration)
                        .attr("transform", (d) => {
                            return "translate(" + d.y + "," + d.x + ")";
                        }).end()
                ]).then(() => {
                    // alert("MIUP");
                    if(!this._isDrawed){
                        this._afterDraw();
                        this.initZoom()
                    }
                }).catch(console.log)
            },
            (exit) => {
                exit
                    .transition()
                    .duration(this._duration)
                    .attr("transform", (d) => {
                        return "translate(" + this._source.y + "," + this._source.x + ")";
                    })
                    .style("opacity", 0)
                    .remove();

                exit.select("text").style("fill-opacity", 1e-6);
            }
        );
        var link = this._rootG
            .selectAll("path.link")
            .data(this._links, (d) => d.id)
            .style("stroke-width", 1);
        var linkEnter;
        link.join(
            (enter) => {
                linkEnter = enter
                    .insert("path", "g")
                    .attr("class", "link")
                    .attr("d", (d) => {
                        var o = {
                            x: this._source.x0,
                            y: this._source.y0,
                        };
                        return this._diagonal(o, o);
                    })
                    .style("stroke-width", 1);
            },
            (update) => {
                linkEnter
                    .transition()
                    .duration(this._duration)
                    .attr("stroke", "#929292")
                    .attr("fill", "none")
                    .attr("d", (d) => {
                        return this._diagonal(d, d.parent);
                    });
                update
                    .transition()
                    .duration(this._duration)
                    .attr("stroke", "#929292")
                    .attr("fill", "none")
                    .attr("d", (d) => {
                        return this._diagonal(d, d.parent);
                    });
            },
            (exit) => {
                exit
                    .transition()
                    .duration(this._duration)
                    .attr("d", (d) => {
                        var o = {
                            x: this._source.x,
                            y: this._source.y,
                        };
                        return this._diagonal(o, o);
                    })
                    .style("stroke-width", 1)
                    .remove();
            }
        );
        this._nodes.forEach((d) => {
            d.x0 = d.x;
            d.y0 = d.y;
        });
        this._afterUpdate()
    }

    _diagonal(s, d) {
        let path
        let sy
        let dy
        if (s != d) {
            sy = s.y;
            dy = d.y;
            dy += d.w;
            path = `M ${sy} ${s.x}
            C ${(sy + dy) / 2} ${s.x},
            ${(sy + dy) / 2} ${d.x},
            ${dy} ${d.x}`;
        } else {
            path = `M ${s.y} ${s.x}
            C ${(s.y + d.y) / 2} ${s.x},
            ${(s.y + d.y) / 2} ${d.x},
            ${d.y} ${d.x}`;
        }
        return path;
    }
    collapseAll(data, depth){
        if(data.depth < depth){
            if(data.children){
                for (const child of Object.values(data.children)) {
                    this.collapseAll(child, depth);
                }
            }
        }else if(data.depth > depth){
            if(data.children){
                for (const child of Object.values(data.children)) {
                    this.collapseAll(child, depth + 1);
                }
            }
            return;
        }else{
            if(data.children){
                for (const child of Object.values(data.children)) {
                    this.collapseAll(child, depth + 1);
                }
            }
            data._children = data.children
            data.children = null;
        }
    }
    _click(e, d) {
        if (d.children) {
            d._children = d.children;
            d.children = null;
            this._collapsed.push(d.data.id);
        } else {
            if (d._children) {
                d.children = d._children;
                d._children = null;
                this._collapsed = this._collapsed.filter(e => e != d.data.id)
            } else {
                this._onClickNode(e, d)
            }
        }
        this._source = d
        this._update(this._data);
    }
}
window.MindmapGraph = MindmapGraph
class BadgeGraph extends Graph {
    _modeAdd = false;
    _rootObj = {
        id: "root",
        label: "SEARCH"
    };
    _onLabelClick = (e, d) => {
        e.stopPropagation();
        e.preventDefault();
        console.log(d)
    };
    _duration = 750;
    _nodePadding = {
        top: 5,
        right: 10,
        bottom: 5,
        left: 10,
    };
    _defaultColor = d3.scaleOrdinal(d3.schemePastel2)
    _margin = {
        top: 20,
        right: 180,
        bottom: 30,
        left: 90,
    };
    _theme = null;
    _collapsed = [];

    _source = null;
    _i = 0;

    _nodes = [];
    _links = [];
    _treemap = null;

    _depth = null;
    _onDeleteClicked = () => {};

    constructor(data, depthToCollapse = null, rootObj = null , modeAdd = false) {
        super()
        this._modeAdd = modeAdd;
        this._data = this._preprocessData(data);
        if(rootObj){
            this._rootObj = rootObj;
        }
        this._depth = depthToCollapse;
    }

    setTheme(value){
        this._theme = value;
    }
    setOnDeleteClicked(callback) {
        this._onDeleteClicked = callback;
    }

    setOnLabelClick(callback) {
        this._onLabelClick = callback;
    }

    _preprocessResultsByTheme(results){
        let countTag = 0;
        let res = this._rootObj;
        let raw = []
        const tags = new Set()
        for (const [id, value] of Object.entries(results)) {
            const row = {
                id,
                ...value
            }
            row.label = value.name
            row.description = value.description
            row.img = value.profilMediumImageUrl
            let hasTheme = false;
            if(row.tags){
                for (const tag of row.tags) {
                    for (const [theme, themeTagsObj] of Object.entries(this._theme)) {
                        for (const themeTag of Object.values(themeTagsObj)) {
                            if(tag == themeTag){
                                hasTheme = true;
                                raw.push({...row, theme})
                                tags.add(tag);
                            }
                        }
                    }
                }
            }
        }
        // for (const tag of [...tags]) {
        //     for (const [theme, themeTagsObj] of Object.entries(this._theme)) {
        //         for (const themeTag of Object.values(themeTagsObj)) {
        //             if(tag == themeTag){
        //                 raw.push({theme, collection: 'tags', id: tag, label: tag})
        //             }
        //         }
        //     }
        // }
        this.tags = [...tags];
        if(this._authorizedTags.length > 0){
            this.tags = this._authorizedTags;
        }
        console.log(raw);
        const typesGroup = d3.group(raw, d => d.theme, d => d.collection, d => d.type);
        let children = parcours(typesGroup);
        function parcours(map) {
            let children = []
            if(map instanceof Map){
                for (const [key,value] of map) {
                    if(key){
                        children.push({id: key, label: key, children: parcours(value)})
                    }else{
                        children = parcours(value);
                    }
                }
            }else{
                return map;
            }
            return children;
        }
        res.children = children
        return res
    }

    _preprocessResultsDefault(results){
        let countTag = 0;
        let res = this._rootObj;
        let raw = []
        const tags = new Set()
        for (const [id, value] of Object.entries(results)) {
            const row = {
                id,
                ...value
            }
            row.label = value.name
            row.description = value.description
            row.img = value.profilMediumImageUrl
            raw.push(row);
            if(row.tags){
                for (const tag of row.tags) {
                    tags.add(tag);
                }
            }
        }
        this.tags = [...tags];
        // for (const tag of [...tags]) {
        //     raw.push({
        //             label: tag,
        //             id : tag,
        //             group : 'tags',
        //             collection : 'tags',
        //     })
        // }
        if(this._authorizedTags.length > 0){
            this.tags = this._authorizedTags;
        }
        const typesGroup = d3.group(raw, d => d.collection, d => d.type);
        let children = parcours(typesGroup);
        function parcours(map) {
            let children = []
            if(map instanceof Map){
                for (const [key,value] of map) {
                    if(key){
                        children.push({id: key, label: key, children: parcours(value)})
                    }else{
                        children = parcours(value);
                    }
                }
            }else{
                return map;
            }
            return children;
        }
        res.children = children
        return res
    }

    preprocessResults(results){
        super.preprocessResults(results);
        if(this._theme){
            return this._preprocessResultsByTheme(results);
        }else{
            return this._preprocessResultsDefault(results);
        }
    }
    _addPlusNode(rawData, depth){
        console.log(depth, rawData);
        if(rawData.children && rawData.children.length > 0){
            for (let i = 0; i < rawData.children.length; i++) {
                rawData.children[i] = this._addPlusNode(rawData.children[i], depth + 1);
            }
            rawData.children.push({id: "plus", name: "+", label: "+"})
        }else{
            rawData.children = [{id: "plus", name: "+", label: "+"}];
        }
        return rawData;
    }
    _preprocessData(rawData) {
        if(this._modeAdd){
            rawData = this._addPlusNode(rawData, 0);
        }
        console.log(rawData);
        rawData = d3.hierarchy(rawData);
        const w = this._width - this._margin.left - this._margin.right;
        const h = GraphUtils.heightByViewportRatio(w);
        rawData.x0 = h / 2
        rawData.y0 = 0

        this._source = rawData;

        this._treemap = d3
            .tree()
            .size([w, h])
            .nodeSize([75, 240])
            .separation((a, b) => {
                if (a.parent == b.parent) return 1.5;
                else return 2;
            });
        for (const d of rawData.descendants()) {
            if (this._collapsed.includes(d.data.id)) {
                if (d.children) {
                    d._children = d.children;
                    d.children = null;
                } else {
                    d.children = d._children;
                    d._children = null;
                }
            }
        }
        return rawData;
    }
    draw(containerId) {
        this._beforeDraw()
        super.draw(containerId)
        this._zoom = d3.zoom().on("zoom", (e) => {
            this._rootG.attr("transform", e.transform);
            this._onZoom(e);
        });
        this._rootSvg.call(this._zoom);
        if(this._depth != null && this._depth >= 0){
            this.collapseAll(this._data, this._depth);
        }
        this._update(this._data);
        this._afterDraw()
        this._rootSvg.call(this._zoom.transform, d3.zoomIdentity.translate(this._margin.left, (this._margin.top + this._height / 2)))
    }

    adaptViewBoxByRatio(ratio = 16/7){
        const w = this._width + this._margin.right + this._margin.left
        const h =  GraphUtils.heightByViewportRatio(w,ratio);
        this._rootSvg
            .attr("viewBox", [0,0,w,h])
    }

    initZoom = () => {
        if(!this._rootSvg) return;
        const currentZoom = d3.zoomTransform(this._rootSvg.node());

        this._rootSvg.call(this._zoom.transform, d3.zoomIdentity);
        const bound = this._rootG.node().getBoundingClientRect(); console.log(bound)
        this._rootSvg.call(this._zoom.transform, currentZoom);

        const containerBound = this._rootSvg.node().getBoundingClientRect();
        const k1 = isFinite(containerBound.width / bound.width) ? ((containerBound.width - 50) / bound.width): 1;
        const k2 = isFinite(containerBound.height / bound.height) ? ((containerBound.height - 50) / bound.height): 1;
        const k = (k1 > k2 ? k2 : k1);

        const currentViewBox = this._rootSvg.node().viewBox.baseVal;

        //ADAPT TRANSFORMATION INTO VIEWBOX SCOPE
        const wRatio = currentViewBox.width / containerBound.width;
        const hRatio = currentViewBox.height / containerBound.height;
        let tx = (containerBound.width / 2) - (bound.width / 2) * k;
        let ty = (containerBound.height / 2) - (bound.height / 2) * k + Math.abs(containerBound.y - bound.y) * k ;
        tx *= wRatio;
        ty *= hRatio;
        this._rootSvg.transition().call(this._zoom.transform, d3.zoomIdentity.translate(tx,ty).scale(k))
    }
    setColor(callback) {
        this._color = callback;
        if (this._isDrawed) {
            for (const color of this._colored) {
                if (color.node()) {
                    if (color.node() instanceof SVGElement) {
                        color.attr("fill", (d, i, n) => this._color(d, i, n))
                    } else if (color.node() instanceof HTMLElement) {
                        color.style("background-color", (d, i, n) => this._color(d, d.depth, n));
                    }
                }
            }
        }
    }
    updateData(data, draw = true) {
        this._nodes
        this._data = this._preprocessData(data);
        const tmp = this._duration;
        this._duration = 0
        if(this._depth != null && this._depth >= 0){
            this.collapseAll(this._data, this._depth);
        }
        if(draw){
            this._update(this._data)
        }
        this._duration = tmp;
    }
    _update(data) {
        super._update(data);
        this._beforeUpdate()
        var treeData = this._treemap(data);
        this._nodes = treeData.descendants();
        this._links = treeData.descendants().slice(1);
        var node_g;
        var node = this._rootG
            .selectAll("g.node")
            .data(this._nodes, (d) => d.id || (d.id = ++this._i));
        node.join(
            (enter) => {
                node_g = enter
                    .append("g")
                    .attr("class", "node")
                    .on("click", (e, d) => {
                        this._click(e, d);
                    })
                    .attr(
                        "transform",
                        (d) => "translate(" + this._source.y0 + "," + this._source.x0 + ")"
                    )
                const foreign = node_g.append("foreignObject")
                    .style("cursor", "pointer")
                    .attr("width", d => d.w = d.data.id != "plus" ? 100 : 40)
                    .attr("height",d => d.h = d.data.id != "plus" ? 100 : 40)
                    .attr("x", 0)
                    .style("overflow", "visible")
                    .attr("y", d => - d.h / 2)

                const div = foreign
                    .append("xhtml:div")
                    .style("height", "100%")
                    .style("width", "100%")
                    .style("display", "flex")
                    .style("justify-content", "center")
                    .style("align-items", "center")
                    .style("border-radius", d => d.data.id != "plus" ? "5px" : "50%")
                    .style("background-color", "white")
                    .style("border", "1px solid #455a64")
                    .style("position", "relative")
                div.filter(d => d.data.id == "plus")
                    .text(d => d.data.label)
                const divNode = div.filter(d => d.data.id != "plus")
                const imageNode = divNode.filter(d => d.data.profilMediumImageUrl)
                imageNode.append("xhtml:img")
                    .attr("src", d => d.data.profilMediumImageUrl)
                    .style("height", "90%")
                    .style("width", "90%")
                    .style("pointer-events", "none")
                imageNode.append("xhtml:div")
                    .attr("class", "badge-graph-label")
                    .style("position", "absolute")
                    .style("top", "100%")
                    .style("width", "100%")
                    .style("text-align", "center")
                    .text(d => d.data.name ? d.data.name : "badge")
                    .on('click', (e, d) => this._onLabelClick(e, d));
                const textNode = divNode.filter(d => !d.data.profilMediumImageUrl)
                textNode.append("xhtml:div")
                    .style("max-height", "90%")
                    .style("max-width", "90%")
                    .style("overflow-wrap", "break-word")
                    .style("overflow", "hidden")
                    .style("text-align", "center")
                    .attr("class", "badge-graph-label")
                    .text(d => d.data.name ? d.data.name : "Badge")
                    .on('click', (e, d) => this._onLabelClick(e, d));
                div.filter(d => d.data.locked)
                    .append("xhtml:div")
                    .style("height", "90%")
                    .style("width", "90%")
                    .style("background-color", "rgba(0,0,0,0.7)")
                    .style("position", "absolute")
                    .style("top", "50%")
                    .style("left", "50%")
                    .style("transform", "translate(-50%, -50%)")
                    .style("display", "flex")
                    .style("justify-content", "center")
                    .style("align-items", "center")
                    .append("xhtml:i")
                    .attr("class", "fa fa-2x fa-lock")
                    .style("color", "white")


                const divNodeButtons = divNode
                    .filter(d => d.parent)
                    .append("xhtml:button")
                    .attr("class","button-delete-badge-parcours btn btn-danger")
                    .style("position", "absolute")
                    .style("top", "-10px")
                    .style("display", "none")
                    .style("height", "30px")
                    .style("width", "30px")
                    .style("padding", "3px 5px")
                    .on('click', (event, d) => {
                        event.stopPropagation();
                        this._onDeleteClicked(event, d);
                    })

                divNodeButtons.append("xhtml:i")
                    .attr("class", "fa fa-trash")
                    .style("pointer-events", "none")

                divNode.on("mouseover", (event, d) => {
                    if(this._modeAdd){
                        d3.select(event.target)
                            .select("button")
                            .style("display", "inline")
                    }
                })
                divNode.on("mouseout", (event, d) => {
                    if(this._modeAdd){
                        divNodeButtons.style("display", "none")
                    }
                })
                this._colored.push(div)
                this._leaves.push(node_g);
            },
            (update) => {
                node_g
                    .transition()
                    .duration(this._duration)
                    .attr("transform", (d) => {
                        return "translate(" + d.y + "," + d.x + ")";
                    });
                update
                    .transition()
                    .duration(this._duration)
                    .attr("transform", (d) => {
                        return "translate(" + d.y + "," + d.x + ")";
                    });
            },
            (exit) => {
                exit
                    .transition()
                    .duration(this._duration)
                    .attr("transform", (d) => {
                        return "translate(" + this._source.y + "," + this._source.x + ")";
                    })
                    .style("opacity", 0)
                    .remove();

                exit.select("text").style("fill-opacity", 1e-6);
            }
        );
        var link = this._rootG
            .selectAll("path.link")
            .data(this._links, (d) => d.id)
            .style("stroke-width", 1);
        var linkEnter;
        link.join(
            (enter) => {
                linkEnter = enter
                    .insert("path", "g")
                    .attr("class", "link")
                    .attr("d", (d) => {
                        var o = {
                            x: this._source.x0,
                            y: this._source.y0,
                        };
                        return this._diagonal(o, o);
                    })
                    .style("stroke-width", 1);
            },
            (update) => {
                linkEnter
                    .transition()
                    .duration(this._duration)
                    .attr("stroke", "#929292")
                    .attr("fill", "none")
                    .attr("d", (d) => {
                        return this._diagonal(d, d.parent);
                    });
                update
                    .transition()
                    .duration(this._duration)
                    .attr("stroke", "#929292")
                    .attr("fill", "none")
                    .attr("d", (d) => {
                        return this._diagonal(d, d.parent);
                    });
            },
            (exit) => {
                exit
                    .transition()
                    .duration(this._duration)
                    .attr("d", (d) => {
                        var o = {
                            x: this._source.x,
                            y: this._source.y,
                        };
                        return this._diagonal(o, o);
                    })
                    .style("stroke-width", 1)
                    .remove();
            }
        );
        this._nodes.forEach((d) => {
            d.x0 = d.x;
            d.y0 = d.y;
        });
        this._afterUpdate()
    }

    _diagonal(s, d) {
        let path
        let sy
        let dy
        if (s != d) {
            sy = s.y;
            dy = d.y;
            dy += d.w;
            path = `M ${sy} ${s.x}
            C ${(sy + dy) / 2} ${s.x},
            ${(sy + dy) / 2} ${d.x},
            ${dy} ${d.x}`;
        } else {
            path = `M ${s.y} ${s.x}
            C ${(s.y + d.y) / 2} ${s.x},
            ${(s.y + d.y) / 2} ${d.x},
            ${d.y} ${d.x}`;
        }
        return path;
    }
    collapseAll(data, depth){
        if(data.depth < depth){
            if(data.children){
                for (const child of Object.values(data.children)) {
                    this.collapseAll(child, depth);
                }
            }
        }else if(data.depth > depth){
            if(data.children){
                for (const child of Object.values(data.children)) {
                    this.collapseAll(child, depth + 1);
                }
            }
            return;
        }else{
            if(data.children){
                for (const child of Object.values(data.children)) {
                    this.collapseAll(child, depth + 1);
                }
            }
            data._children = data.children
            data.children = null;
        }
    }
    _click(e, d) {
        if (d.children) {
            d._children = d.children;
            d.children = null;
            this._collapsed.push(d.data.id)
        } else {
            if (d._children) {
                d.children = d._children;
                d._children = null;
                this._collapsed = this._collapsed.filter(e => e != d.data.id)
            } else {
                this._onClickNode(e, d)
            }
        }
        this._source = d
        this._update(this._data);
    }
}
window.BadgeGraph = BadgeGraph
class NetworkGraph extends Graph {
    _root = {};
    _defaultIcon = "fa fa-home";
    _defaultRootIcon = "fa fa-home";
    _simulation = null;
    _linksNode = null;
    _nodes = null;
    _circlesNode = null;
    _groupNode = null;
    _isEmpty = false;
    _funcGroup = d => d.data.type;
    _onTick = () => {}
    _groupIcons = () => {
        return this._defaultIcon;
    }
    _first = true;
    setGroupIcon(callback){
        this._groupIcons = callback;
        if(this._groupNode){
            this._groupNode.selectAll("i").attr("class", this._groupIcons)
        }
    }

    setOnTick(callback){
        this._onTick = callback;
    }
    setCircleSize(callback) {
        this._circleSize = (d,i,n) => {
            const r = callback(d,i,n);
            this._afterCicleSize(d,r);
            return r;
        };
        if(this._circlesNode){
            this._circlesNode.attr("r",(d,i,n) => this._circleSize(d,i,n));
        }
    }

    _color = (d, i, n) => {
        if (d.data.type == "group") {
            return "#c62f80";
        }
        if (d.data.group == "root") {
            return "black";
        }
        if (d.data.group == "TAGS") {
            return "steelblue";
        }
        if (d.data.group == "PROJECTS") {
            return "purple";
        }
        return this._defaultColor(i);
    };
    constructor(rawData, funcGroup = d => d.data.type) {
        super();
        this._funcGroup = funcGroup;
        this._data = this._preprocessData(rawData);
    }

    preprocessResults(results){
        super.preprocessResults(results);
        let countTag = 0;
        let res = [{
            label: "SEARCH",
            type: "root",
            group: "root"
        }]
        for (const [id, value] of Object.entries(results)) {
            const row = {
                id,
                ...value
            }
            row.label = value.name
            row.description = value.description
            row.img = value.profilMediumImageUrl
            res.push(row);
        }
        return res
    }

    _preprocessData(rawData) {
        let rootCount = 0;
        var root = null;
        const filteredData = []
        const tags = new Set();
        for (const row of rawData) {
            if(row.group == "root"){
                rootCount++;
                if(rootCount > 1){
                    throw new Error("Pas de donnée pour root ou plusieur root");
                }
                root = {}
                root.data = row;
                console.log(root)
            }else{
                filteredData.push({data: row})
            }
            if(row.tags){
                for (const tag of row.tags) {
                    tags.add(tag);
                }
            }
        }
        for (const tag of [...tags]) {
            filteredData.push({
                data : {
                    label: tag,
                    id : tag,
                    group : 'tags',
                    type : 'tags',
                }
            })
        }
        this.tags = [...tags];
        if(this._authorizedTags.length > 0){
            this.tags = this._authorizedTags;
        }
        const dataByLinks = d3.group(filteredData, this._funcGroup);
        console.log(dataByLinks);
        const links = [];
        const groups = [];
        for (const [group, children] of dataByLinks.entries()) {
            groups.push({data: {
                    id: group,
                    label: group,
                    type: "group",
                    group: "group",
                }});
            links.push({
                target: group + ".group",
                source: root.data.id + ".root",
            });
            for (const child of children) {
                links.push({
                    source: group + ".group",
                    target: child.data.id + "." + this._funcGroup(child),
                });
            }
        }



        const res = {
            root,
            nodes: [root, ...groups, ...filteredData],
            links,
            groups,
        };
        this._isEmpty = !(res.links.length > 0);
        if(!this._isEmpty){
            this._simulation = d3
                .forceSimulation(res.nodes)
                .force("charge_force", d3.forceManyBody().strength(d => {
                    if(d.data.id == "tags" && d.data.type == "group" && d.data.group == "group"){
                        return -10000
                    }
                    return -50
                }))
                .force("center_force", d3.forceCenter(this._width / 2, this._width / 2).strength(0.095))
                .force("collide", d3.forceCollide(50).iterations(10).strength(0.2))
                .force(
                    "links",
                    d3
                        .forceLink(res.links)
                        .id((d) => d.data.id + "." + this._funcGroup(d))
                        .strength((d) => 1)
                        .distance((d) => {
                            if(d.source.data.type == "root"){
                                return 200;
                            }
                            return 50;
                        })
                ).on("end", () => {
                    if(this._first){
                        this._first = false;
                        this.initZoom();
                    }
                })
                .stop();
        }
        return res;
    }
    draw(containerId) {
        super.draw(containerId);
        this._zoom = d3.zoom().on("zoom", (e, d) => {
            this._rootG.attr("transform", e.transform);
            this._onZoom(e,d);
        })
        this._rootSvg.call(this._zoom);
        this._update();
        this._afterDraw()
    }
    _circleSize(d, i, n) {
        var r = 30;
        if (this._funcGroup(d) == "root" || d.data.type == "group") r = 20;
        if (d.data.type == "tags") r = 10;
        this._afterCicleSize(d,r);
        return r;
    }
    _afterCicleSize(d,r){
        d.innerSquare = GraphUtils.squareInnerCircle(0, 0, r, 5);
        this._nodes
            .selectAll("foreignObject")
            .attr("x", (d) => d.innerSquare.x)
            .attr("y", (d) => d.innerSquare.y)
            .attr("width", (d) => d.innerSquare.width)
            .attr("height", (d) => d.innerSquare.height);
    }
    _update() {
        if(!this._isEmpty){
            this._simulation.restart()
            this._simulation.on("tick", () => this._tickActions());
            if (!this._rootG.select("g.links").node()) {
                this._rootG.append("g").attr("class", "links");
            }
            this._rootG
                .select("g.links")
                .selectAll("line")
                .data(this._data.links, d => d.source.id + " " + d.target.id)
                .join(
                    (enter) => {
                        this._linksNode = enter
                            .append("line")
                            .classed("links-line", true)
                            .attr("stroke-width", 5)
                            .style("stroke", "rgba(51,51,51,0.6)");
                    },
                    (update) => {
                        return update;
                    },
                    (exit) => {
                        exit.remove();
                    }
                );
            if (!this._rootG.select("g.nodes").node()) {
                this._rootG.append("g").attr("class", "nodes");
            }
            const nodes = this._rootG
                .select("g.nodes")
                .selectAll("g.node")
                .data(this._data.nodes, d => JSON.stringify(d.data))
                .join((enter) => {
                        this._nodes = enter
                            .append("g")
                            .style("overflow", "visible")
                            .style("cursor", "pointer")
                            .classed("node", true)
                        this._nodes.append("g")
                            .on("click", (d, i, n) => this._onClickNode(d, i, n))
                            .on("mouseover", (e,d) => {
                                d3.select(e.currentTarget).select("text").text(this._labelFunc(d))
                            })
                            .on("mouseout", (e,d) => {
                                d3.select(e.currentTarget).select("text").text(d => GraphUtils.truncate(this._labelFunc(d), 20))
                            })

                        this._circlesNode = this._nodes
                            .select("g")
                            .append("circle")
                            .attr("r", (d,i,n) => this._circleSize(d,i,n))
                            .attr("fill", (d, i, n) => this._color(d, i, n))
                            .call(
                                d3
                                    .drag()
                                    .on("start", (e, d) => this._dragStart(e, d))
                                    .on("drag", (e, d) => this._dragDrag(e, d))
                                    .on("end", (e, d) => this._dragEnd(e, d))
                            );
                        this._colored.push(this._circlesNode)

                        const foreign = this._nodes
                            .select("g")
                            .append("foreignObject")
                            .attr("x", (d) => d.innerSquare.x)
                            .attr("y", (d) => d.innerSquare.y)
                            .attr("width", (d) => d.innerSquare.width)
                            .attr("height", (d) => d.innerSquare.height)
                            .call(
                                d3
                                    .drag()
                                    .on("start", (e, d) => this._dragStart(e, d))
                                    .on("drag", (e, d) => this._dragDrag(e, d))
                                    .on("end", (e, d) => this._dragEnd(e, d))
                            );
                        const group = foreign
                            .filter((d) => d.data.type == "group" || this._funcGroup(d) == "root")
                            .append("xhtml:div")
                            .style("width", "100%")
                            .style("height", "100%")
                            .style("display", "flex")
                            .style("justify-content", "center")
                            .style("align-items", "center")
                        this._groupNode = group;
                        group.append("xhtml:i")
                            .style("color", "white")
                            .attr("class", this._groupIcons);

                        foreign
                            .filter((d, i) => d.data.img != undefined && d.data.img.trim() != "")
                            .append("xhtml:img")
                            .attr("src", (d) => d.data.img)
                            .style("width", "100%")
                            .style("height", "100%")
                            .on("click", (e, d) => this._onClickNode(e, d));
                        this._nodes
                            .filter(d => !GraphUtils.hasImage(d))
                            .select("g")
                            .append("text")
                            .text((d) => GraphUtils.truncate(this._labelFunc(d), 20))
                            .attr("font-size", 20)
                            .attr("x", 15)
                            .attr("y", 4);
                    }, update => {
                    },
                    exit => exit.remove());
            this._leaves.push(nodes);
        }
    }
    _dragStart(event, d) {
        event.sourceEvent.stopPropagation();
        if (!event.active) this._simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
    }

    _dragDrag(event, d) {
        d.fx = event.x;
        d.fy = event.y;
    }

    _dragEnd(event, d) {
        if (!event.active) this._simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
    }
    _tickActions() {
        this._onTick()
        this._rootSvg.selectAll("g.node")
            .attr("transform", d=> `translate(${d.x}, ${d.y})`);

        //update link positions
        this._rootSvg.selectAll(".links-line")
            .attr("x1", function(d) {
                return d.source.x;
            })
            .attr("y1", function(d) {
                return d.source.y;
            })
            .attr("x2", function(d) {
                return d.target.x;
            })
            .attr("y2", function(d) {
                return d.target.y;
            });
    }
    initZoom = () => {
        if(!this._rootSvg) return;
        const currentZoom = d3.zoomTransform(this._rootSvg.node());

        this._rootSvg.call(this._zoom.transform, d3.zoomIdentity);
        const bound = this._rootG.node().getBoundingClientRect();
        this._rootSvg.call(this._zoom.transform, currentZoom);

        const containerBound = this._rootSvg.node().getBoundingClientRect();

        const k1 = isFinite(containerBound.width / bound.width) ? ((containerBound.width - 50) / bound.width): 1;
        const k2 = isFinite(containerBound.height / bound.height) ? ((containerBound.height - 50) / bound.height): 1;
        const k = (k1 > k2 ? k2 : k1);

        const currentViewBox = this._rootSvg.node().viewBox.baseVal;

        const wRatio = currentViewBox.width / containerBound.width;
        const hRatio = currentViewBox.height / containerBound.height;
        let tx = (containerBound.width / 2) - (bound.width / 2) * k + Math.abs(containerBound.x - bound.x) * k ;
        let ty = (containerBound.height / 2) - (bound.height / 2) * k + Math.abs(containerBound.y - bound.y) * k ;
        tx *= wRatio;
        ty *= hRatio;

        this._rootSvg.transition().call(this._zoom.transform, d3.zoomIdentity.translate(tx,ty).scale(k,k))
    }
}
window.NetworkGraph = NetworkGraph
class DendoGraph extends Graph {

}
window.DendoGraph = DendoGraph
class CircleRelationGraph extends Graph {
    _id = Math.random() * 1000;
    _textColored = [];
    _circlePadding = 30;
    _splitRegex = /(?=[A-Z][a-z])|\s+/g;
    _padding = this._circlePadding - 10;
    _globalMaxX = -Infinity;
    _globalMaxY = -Infinity;
    _titleMarginBottom = 100;
    _nodes = [];
    _size = null;
    _funcGroup = null;
    _externalCircleMargin = 30;
    _textPathPadding = this._externalCircleMargin - 50;
    _links = [];
    _savedLinks = [];
    _draggable = null;
    _transition = 750;
    _color = () => "white";
    _relationSimulation = null;
    _onTickEnd = () => {};
    _onDragEnd = () => {};
    _modeListing = false;
    _beforeDrag = () => {};
    _initPosition = null;

    _pathGenerator = (d) =>
        `M ${d.x} ${d.y + d.r - this._textPathPadding} A 1 1 0 1 1 ${d.x} ${
            d.y - d.r + this._textPathPadding
        } M ${d.x} ${d.y - d.r + this._textPathPadding} A 1 1 0 1 1 ${d.x} ${
            d.y + d.r - this._textPathPadding
        } `;
    _onClickNodeMobile = console.log
    /**
     *
     * @param {*} data array of obj {img?: url, text?: string, id: string | number}
     * @param {*} funcGroup function to indicate which obj key to group
     */
    constructor(data, funcGroup, authorizedTags = [],links = [], draggable = true) {
        super();
        this._authorizedTags = authorizedTags;
        this._funcGroup = funcGroup;
        this._data = this._preprocessData(data);
        this._draggable = draggable;
        this._links = links;
        this._savedLinks = links;
    }
    setModeMobile(value){
        if(value){
            this._links = [];
        }else{
            this._links = this._savedLinks;
        }
        super.setModeMobile(value);
    }
    preprocessResults(results) {
        super.preprocessResults(results);
        const res = [];
        console.log("Index tag", this._authorizedTags);
        var tags = new Set();
        for (const [id, value] of Object.entries(results)) {
            if (value.tags) {
                for (const tag of value.tags) {
                    if (this._authorizedTags && this._authorizedTags.length > 0) {
                        const exist = this._authorizedTags.findIndex(function(tg){
                            return tg.toLowerCase() == tag.toLowerCase();
                        })
                        //if (!this._authorizedTags.includes(tag)) {
                        console.log("Index tag", exist)
                        if (exist == -1) {
                            continue;
                        }
                    }
                    const row = {
                        id,
                    };
                    row.label = value.name;
                    row.description = value.description;
                    row.img = value.profilMediumImageUrl;
                    row.group = tag.toLowerCase();
                    tags.add(tag);
                    res.push(row);
                }
            }
        }
        this.tags = [...tags];
        if (this._authorizedTags.length > 0) {
            this.tags = this._authorizedTags;
        }
        return res;
    }
    initZoom = () => {
        if (!this._rootSvg) return;
        const currentZoom = d3.zoomTransform(this._rootSvg.node());

        this._rootSvg.call(this._zoom.transform, d3.zoomIdentity);
        const bound = this._rootG.node().getBoundingClientRect();
        console.log(bound);
        this._rootSvg.call(this._zoom.transform, currentZoom);

        const containerBound = this._rootSvg.node().getBoundingClientRect();

        const k1 = isFinite(containerBound.width / bound.width)
            ? (containerBound.width - 50) / bound.width
            : 1;
        const k2 = isFinite(containerBound.height / bound.height)
            ? (containerBound.height - 50) / bound.height
            : 1;
        const k = k1 > k2 ? k2 : k1;

        const currentViewBox = this._rootSvg.node().viewBox.baseVal;

        //ADAPT TRANSFORMATION INTO VIEWBOX SCOPE
        const wRatio = currentViewBox.width / containerBound.width;
        const hRatio = currentViewBox.height / containerBound.height;
        let tx = containerBound.width / 2 - (bound.width / 2) * k + Math.abs(containerBound.x - bound.x) * k;
        let ty = containerBound.height / 2 - (bound.height / 2) * k + Math.abs(containerBound.y - bound.y) * k;
        tx *= wRatio;
        ty *= hRatio;
        this._rootSvg
            .transition()
            .call(this._zoom.transform, d3.zoomIdentity.translate(tx, ty).scale(k));
    };
    _preprocessData(data) {
        this._beforeUpdate();
        this._nodes = [];

        const d = this._group(data, this._funcGroup);
        this._dfs(d);

        const packed = d3.packSiblings(this._nodes);

        let minY = Infinity;
        let minX = Infinity;
        for (let i = 0; i < packed.length; i++) {
            const x = packed[i].x - packed[i].r;
            const y = packed[i].y - packed[i].r;
            if (x < minX) minX = x;
            if (y < minY) minY = y;
            const curr_x = packed[i]["r"] + packed[i]["dr"] + packed[i]["x"];
            const curr_y = packed[i]["r"] + packed[i]["dr"] + packed[i]["y"];
            if (this._globalMaxX < curr_x) this._globalMaxX = curr_x;
            if (this._globalMaxY < curr_y) this._globalMaxY = curr_y;
        }
        if (minX != 0) {
            if (minX < 0) {
                for (let i = 0; i < packed.length; i++) {
                    const x = (packed[i]["x"] += Math.abs(minX));
                }
            } else {
                for (let i = 0; i < packed.length; i++) {
                    packed[i]["x"] -= Math.abs(minX);
                }
            }
        }
        if (minY != 0) {
            if (minY < 0) {
                for (let i = 0; i < packed.length; i++) {
                    const y = (packed[i]["y"] += Math.abs(minY));
                }
            } else {
                for (let i = 0; i < packed.length; i++) {
                    packed[i]["y"] -= Math.abs(minY);
                }
            }
        }
        // const enclose = d3.packEnclose(packed);
        // if (this._isDataEmpty(data)) {
        //     return packed;
        // }
        // CALCUL the X and Y SIZE fitting the entire graph
        // this._width = enclose.r * 2;
        return packed;
    }

    /**
     *
     * @param {*} data
     * @param {*} funcGroup
     */
    _group(data, funcGroup) {
        const group = d3.group(data, funcGroup);
        return d3
            .hierarchy(group)
            .sum((d) => d.size)
            .sort((a, b) => b.value - a.value);
    }

    set circlePadding(value) {
        this._circlePadding = value;
        this._padding = this._circlePadding - 10;
    }

    _dfs(parent) {
        if (parent.children) {
            if (parent.children[0].children) {
                // not leaf
                for (let i = 0; i < parent.children.length; i++) {
                    this._dfs(parent.children[i]);
                }
            } else {
                // leaf nodes

                for (let i = 0; i < parent.children.length; i++) {
                    parent.children[i]["x"] = Math.random() * 100 - 50;
                    parent.children[i]["y"] = Math.random() * 100 - 50;
                    if (parent.children[i]["data"].img) {
                        parent.children[i]["width"] = 100;
                        parent.children[i]["bw"] = parent.children[i]["width"];
                        parent.children[i]["height"] = 100;
                        parent.children[i]["bh"] = parent.children[i]["height"];
                    } else {
                        if (!parent.children[i]["textParts"]) {
                            const textParts = parent.children[i]["data"].label.split(
                                this._splitRegex
                            );
                            let maxWLen = -Infinity;
                            for (const parts of textParts) {
                                if (maxWLen < parts.length) {
                                    maxWLen = parts.length;
                                }
                            }
                            maxWLen *= 14;
                            const len = textParts.length;
                            parent.children[i]["textParts"] = textParts;
                            parent.children[i]["maxWidthText"] = maxWLen;
                        }
                        const container = document.createElement("div");
                        const div = document.createElement("div");
                        container.appendChild(div);
                        div.innerHTML = parent.children[i]["data"].label;
                        div.setAttribute(
                            "style",
                            `width: ${parent.children[i]["maxWidthText"]}px; padding: 20px;`
                        );
                        const { width, height } =
                            GraphUtils.computeBoundVirtualNode(container);

                        parent.children[i]["width"] = width;
                        parent.children[i]["bw"] = width;
                        parent.children[i]["height"] = height;
                        parent.children[i]["bh"] = height;
                    }
                    if (parent.children.length <= 2) {
                        parent.children[i]["x"] = -parent.children[i]["width"] / 2;
                        parent.children[i]["y"] = -parent.children[i]["height"] / 2;
                    }
                }
                //POSITION CALCUL HERE
                const simulation = d3
                    .forceSimulation()
                    .force("center", d3.forceCenter(0, 0))
                    .force("charge", d3.forceManyBody())
                    .force(
                        "collide",
                        GraphUtils.rectCollide().size((d) => {
                            return [d.bw, d.bh];
                        })
                    )
                    .nodes(parent.children)
                    .stop();
                const n = Math.ceil(
                    Math.log(simulation.alphaMin()) /
                    Math.log(1 - simulation.alphaDecay())
                );
                for (var i = 0; i < n; ++i) {
                    simulation.tick();
                }

                // parent.children["simulation"] = simulation;

                let minX = Infinity;
                let maxX = -Infinity;
                let minY = Infinity;
                let maxY = -Infinity;
                for (let i = 0; i < parent.children.length; i++) {
                    const x = Number(parent.children[i]["x"]);
                    const y = Number(parent.children[i]["y"]);
                    if (x < minX) minX = x;
                    if (y < minY) minY = y;
                    if (maxX < x) maxX = x;
                    if (maxY < y) maxY = y;
                }
                const minR = 0; // compute r for placing items (nodes)
                let r = minR;
                for (let i = 0; i < parent.children.length; i++) {
                    const x = parent.children[i]["x"];
                    const y = parent.children[i]["y"];
                    const width = parent.children[i]["bw"];
                    const height = parent.children[i]["bh"];
                    const hyp = Math.sqrt(width ** 2 + height ** 2);
                    const currR = GraphUtils.eucludianDistance(minR, minR, x, y) + hyp;
                    if (r < currR) r = currR;
                }

                parent["r"] = r + this._circlePadding; // SUBSTRACT CIRCLE PADDING AT DRAWING
                parent["dr"] = parent["r"] - minR;
                this._nodes.push(parent);
            }
            // calcul couple le plus loin
        }
    }

    _correctTextParentSize() {
        const svg = this._rootG;
        const [x, y, w, h] = this._rootSvg.attr("viewBox").split(",");
        const dimension = svg.node().getBoundingClientRect();
        let k = Math.max(w, h) / Math.max(dimension.width, dimension.height);
        if (k > 2.5) {
            k = 2.5;
        }
        if (k < 1) {
            k = 1;
        }
        const parentTexts = this._rootSvg.selectAll("textPath.parent-text");
        parentTexts.style("font-size", `${80 * k}px`);
    }

    draw(containerId) {
        super.draw(containerId);
        super.addSwitcherMode(containerId);
        this._rootG.attr("id", "circle-root");
        this._zoom = d3.zoom().on("zoom", (e) => {
            //if(!this._focusMode){
            this._rootG.attr("transform", e.transform);
            this._correctTextParentSize();
            this._onZoom(e);
            //}
        });
        this._rootSvg.call(this._zoom);
        const filter_ombre = this._rootSvg
            .append("defs")
            .append("filter")
            .attr("id", "ombre" + this._id)
            .append("feDropShadow")
            .attr("flood-opacity", 0.3)
            .attr("dx", 0)
            .attr("dy", 1);
    }

    _update(data) {
        super._update(data);
        this._leaves = [];
        this._colored = [];
        this._textColored = [];

        this._rootG
            .selectAll("g")
            .data(data, (d) => JSON.stringify(d.data) + d.x + d.y + d.r)
            .join((enter) => {
                const parent_g = enter.append("g");
                parent_g.classed("divide", true)
                    .attr("data-group", d => GraphUtils.slugify(d.data[0]))
                const circle_parent = parent_g
                    .append("circle")
                    .attr("style", "cursor:zoom-in;")
                    .attr("cx", (d) => d.x)
                    .attr("cy", (d) => d.y)
                    .attr("r", (d) =>{return (d.r - (this._circlePadding+d.r))})
                    .attr("stroke", "none")
                    .attr("fill", (d, i) => this._color(d, i))
                    .attr("filter", "url(#ombre" + this._id + ")");
                circle_parent.on("click", (e, d) => {
                    //e.stopPropagation()
                    this._draggable = true;
                    this.focus(d.data[0]);
                    if(e.target.style.cursor=="zoom-in"){
                        e.target.style.cursor = "zoom-out";
                    }else{
                        e.target.style.cursor = "zoom-in";
                    }
                })
                circle_parent.on("mouseover", (e, d) => {

                    /*e.stopPropagation();
                    if(!this._draggable){
                        this.focus(d.data[0])
                    }*/
                });
                circle_parent.call(
                    d3
                        .drag()
                        .on("start", (e, d) => {
                            this._beforeDrag();
                            if(this._draggable){
                                if (!e.active){
                                    this._restartSimulation()
                                }
                            }
                        })
                        .on("drag", (e, d) => {
                            if(this._draggable){
                                d.x = e.x;
                                d.y = e.y;
                            }
                        })
                        .on("end", (e, d) => {
                            if(this._draggable){
                                if (!e.active) this._relationSimulation.alphaTarget(0);
                                this._onDragEnd()
                            }
                        })
                );
                this._colored.push(circle_parent);
                const leaf_svg = parent_g
                    .append("g")
                    .style("overflow", "visible")
                    .classed("leaf-svg", true)
                    .attr(
                        "transform",
                        (d) => `translate(${d.x - d.r + d.dr}, ${d.y - d.r + d.dr})`
                    );
                // .attr("x", (d) => d.x)
                // .attr("y", (d) => d.y);
                parent_g
                    .append("g")
                    .classed("external-cirlcles", true)
                    .append("circle")
                    .attr("cx", (d) => d.x)
                    .attr("cy", (d) => d.y)
                    .attr(
                        "r",
                        (d) => d.r - this._circlePadding + this._externalCircleMargin
                    )
                    .attr("fill", "none")
                    .attr("stroke-width", 10)
                    .attr("stroke-dasharray", "10 14")
                    .style("stroke", (d, i) => this._color(d, i));
                parent_g
                    .append("path")
                    .classed("text-path", true)
                    .attr("stroke", "none")
                    .attr("fill", "none")
                    .attr("id", (d) => `path-${GraphUtils.slugify(d["data"][0])}-${this._id}`)
                    .attr("d",this._pathGenerator);
                const text = parent_g
                    .append("text")
                    .append("textPath")
                    .style("font-size", "100px")
                    .classed("svg-text", true)
                    .classed("parent-text", true)
                    .attr(
                        "xlink:href",
                        (d) => `#path-${GraphUtils.slugify(d["data"][0])}-${this._id}`
                    )
                    .text((d) => d.children[0].data.group)
                    .attr("fill", this._color)
                    .attr("text-anchor", "middle")
                    .attr("startOffset", "50%");
            });
        this._rootG
            .selectAll("g.links")
            .data(this._links)
            .join((enter) => {
                enter
                    .insert("g", ":first-child")
                    .classed("links", true)
                    .append("line")
                    .classed("links-line", true)
                    .attr("stroke-width", 4)
                    .attr("stroke-dasharray", "10 14")
                    .style("stroke", "white");
            });

        this._rootSvg
            .selectAll("g.leaf-svg")
            .selectAll("g")
            .data(
                (d) => d.children,
                (d) => {
                    return JSON.stringify(d.data);
                }
            )
            .join((enter) => {
                const leaf_svg_g = enter
                    .append("g")
                    .style("cursor", "pointer")
                    .classed("leaf-group", true)
                    .on("click", this._onClickNode);

                this._leaves.push(leaf_svg_g);

                const foreign = leaf_svg_g
                    .append("foreignObject")
                    .classed("nodes-container", true)
                    .style("overflow", "visible")
                    .attr("width", (d) => Number(d.width))
                    .attr("height", (d) => Number(d.height))
                    .attr("x", (d) => Number(d.x))
                    .attr("y", (d) => Number(d.y))
                    .style("transform-box", "fill-box")
                    .style("transform", "translate(-50%, -50%)");

                foreign
                    .filter((d) => !d.data.img)
                    .append("xhtml:div")
                    .style("overflow", "hidden")
                    .style("text-align", "center")
                    .style("padding", "10px")
                    .style("display", "flex")
                    .style("justify-content", "center")
                    .style("align-items", "center")
                    .style("background-color", "transparent")
                    .style("color", "#455a64")
                    .style("border", "2px solid rgba(69, 90, 100, 0.5)")
                    .style("border-radius", "5px")
                    .text((d) => d.data.label)
                    .on("click", (...args) => {
                        args[0].stopPropagation();
                        this._onClickNode(...args);
                    });

                foreign
                    .filter((d) => d.data.img)
                    .append("xhtml:div")
                    .style("overflow", "hidden")
                    .style("max-width", "100%")
                    .style("max-height", "100%")
                    .style("font-size", "6pt")
                    .append("xhtml:img")
                    .attr("src", (d) => d.data.img)
                    .attr("alt", (d) => d.data.label)
                    .style("width", "100%")
                    .style("height", "auto");

                // const imgs = leaf_svg_g
                //     .filter((d) => d.data.img)
                //     .append("image")
                //     .attr("xlink:href", (d) => d.data.img)

                this._leaves.push(foreign);
            });
        this._correctTextParentSize();
        if (!this._isDataEmpty(data)) {
            this._links = GraphUtils.filterLinks(this._links, data.map((v) => v.data[0]))
            if(this._initPosition){
                this._rootG.selectAll("g.divide").each((d) => {
                    if(this._initPosition[d.data[0]]){
                        d.x = Number(this._initPosition[d.data[0]].x)
                        if(isNaN(d.x)){
                            d.x = 0;
                        }
                        d.y = Number(this._initPosition[d.data[0]].y)
                        if(isNaN(d.y)){
                            d.y = 0;
                        }
                    }
                })
            }
            var i = 0;
            this._relationSimulation = d3
                .forceSimulation(data)
                .force(
                    "collide",
                    d3.forceCollide().radius((d) => d.r + this._externalCircleMargin + 30)
                )
                .force(
                    "link",
                    d3
                        .forceLink(this._links)
                        .id((d) => d.data[0].toLowerCase())
                        .strength(0)
                )
                .on("tick", () => {
                    i++;
                    this._rootG
                        .selectAll("circle")
                        .attr("cx", (d) => d.x)
                        .attr("cy", (d) => d.y);
                    this._rootG
                        .selectAll("path.text-path")
                        .attr("d",this._pathGenerator);
                    this._rootG
                        .selectAll("g.leaf-svg")
                        .attr(
                            "transform",
                            (d) => `translate(${d.x - d.r + d.dr}, ${d.y - d.r + d.dr})`
                        );
                    //update link positions
                    const lines = this._rootG
                        .selectAll(".links-line")
                    lines.each((data, i, nodes) => {
                        if(isNaN(Number(data.source.x))){
                            d3.select(nodes[i]).remove();
                        }else{
                            d3.select(nodes[i]).attr("x1", (d) => {
                                var res = Number(d.source.x);
                                var h = GraphUtils.eucludianDistance(
                                    d.source.x,
                                    d.source.y,
                                    d.target.x,
                                    d.target.y
                                );
                                const dx =
                                    ((d.source.x - d.target.x) *
                                        (d.source.r +
                                            this._externalCircleMargin -
                                            this._circlePadding +
                                            15)) /
                                    h;
                                res -= dx;
                                return res;
                            })
                                .attr("y1", (d) => {
                                    var res = Number(d.source.y);
                                    var h = GraphUtils.eucludianDistance(
                                        d.source.x,
                                        d.source.y,
                                        d.target.x,
                                        d.target.y
                                    );
                                    const dx =
                                        ((d.source.y - d.target.y) *
                                            (d.source.r +
                                                this._externalCircleMargin -
                                                this._circlePadding +
                                                15)) /
                                        h;
                                    res -= dx;
                                    return res;
                                })
                                .attr("x2", (d) => {
                                    var res = Number(d.target.x);
                                    var h = GraphUtils.eucludianDistance(
                                        d.source.x,
                                        d.source.y,
                                        d.target.x,
                                        d.target.y
                                    );
                                    const dx =
                                        ((d.source.x - d.target.x) *
                                            (d.target.r +
                                                this._externalCircleMargin -
                                                this._circlePadding +
                                                15)) /
                                        h;
                                    res += dx;
                                    return res;
                                })
                                .attr("y2", (d) => {
                                    var res = Number(d.target.y);
                                    var h = GraphUtils.eucludianDistance(
                                        d.source.x,
                                        d.source.y,
                                        d.target.x,
                                        d.target.y
                                    );
                                    const dx =
                                        ((d.source.y - d.target.y) *
                                            (d.target.r +
                                                this._externalCircleMargin -
                                                this._circlePadding +
                                                15)) /
                                        h;
                                    res += dx;
                                    return res;
                                });
                        }
                    });
                    if(i == 20){
                        this.initZoom();
                    }
                })
                .on("end", () => {
                    this._onTickEnd();
                    i = 0;
                })
        }
        this._afterUpdate();
    }
    _restartSimulation(){
        this._relationSimulation.alphaTarget(0.3).restart();
    }
    setColor(callback) {
        super.setColor(callback);
        for (const text of this._textColored) {
            text.attr("fill", (d, i) =>
                GraphUtils.colorLuminance(this._color(d, i), -0.2)
            );
        }
    }
    setOnTickEnd(callback){
        this._onTickEnd = callback;
    }
    setOnDragEnd(callback){
        this._onDragEnd = callback;
    }
    setBeforeDrag(callback){
        this._beforeDrag = callback;
    }
    setDraggable(value){
        this._draggable = !!value;
    }
    setInitPosition(value){
        this._initPosition = value;
    }
    setOnClickNodeChildren(callback){
        this._onClickNodeMobile = callback;
    }
}

window.CircleRelationGraph = CircleRelationGraph
class VennGraph extends Graph {
    _sets = {}
    _minLeaf = 3;
    _maxCanDraw = 6;
    _canDraw = true;
    _splitRegex = /(?=[A-Z][a-z])|\s+/g;
    _padding = -10;
    constructor(rawData,authorizedTags = []) {
        super();
        if(Array.isArray(rawData) && rawData.length > 0){
            this._data = this._preprocessData(rawData);
            this._data = venn.layout(this._data);
        }else{
            this._data = [];
        }

        this._authorizedTags = authorizedTags;
    }

    preprocessResults(results){
        super.preprocessResults(results);
        const res = []
        var tagsSet = new Set();
        for (const [id, value] of Object.entries(results)) {
            var tags = value.tags;
            if(!tags){
                continue;
            }
            var groups = tags;
            if(this._authorizedTags && this._authorizedTags.length > 0){
                groups = tags.filter(x => this._authorizedTags.indexOf(x) !== -1);
            }
            if(!groups || !groups.length > 0){
                continue;
            }
            for (const group of groups) {
                tagsSet.add(group);
            }
            res.push({...value, id, groups, label: value.name ? value.name : "", img: value.profilMediumImageUrl})
        }
        this.tags = [...tagsSet];
        if(this._authorizedTags.length > 0){
            this.tags = this._authorizedTags;
        }
        return res
    }

    _preprocessData(dataRaws) {
        let data = {},
            existingMix = new Set(),
            allSets = {};

        for (const dataRaw of dataRaws) {
            const sets = dataRaw.groups
            sets.sort()
            const mix = sets.join(",");
            if(!Object.keys(data).includes(mix)){
                data[mix] = {
                    data: [],
                    sets: sets
                }
            }
            data[mix].data.push({
                data: dataRaw,
                sets
            });
            if(!Object.keys(allSets).includes(mix)){
                allSets[mix] = 1
            }else{
                allSets[mix]++
            }
            if(sets.length > 1){
                for (const set of sets) {
                    if(!Object.keys(allSets).includes(set)){
                        allSets[set] = 1
                    }else{
                        allSets[set]++
                    }
                }
            }
            existingMix.add(mix);
        }
        let cleanedAllSets = {}
        if(!this.authorizedTags){
            let setCounter = 0;
            for(const [k,v] of Object.entries(allSets)) {
                if(v >= this._minLeaf){
                    if(k.split(",").length == 1){
                        cleanedAllSets[k] = v;
                        setCounter++;
                    }
                }
            }
            function check(items, sets) {
                if(items.length > sets.length){
                    return false;
                }else{
                    for (const item of items) {
                        if(!sets.includes(item)){
                            return false;
                        }
                    }
                }
                return true;
            }
            const sets = Object.keys(cleanedAllSets);
            //SETS misy anle atome
            for(const [k,v] of Object.entries(data)){
                const items = k.split(",");
                if(items.length > 1){
                    if(check(items, sets)){
                        cleanedAllSets[k] = allSets[k];
                    }
                }
            }
        }
        const result = {};
        if(this._canDraw){
            for (const [k,v] of Object.entries(cleanedAllSets)) {
                if(!Object.keys(data).includes(k)){
                    result[k] = {}
                    result[k].data = []
                    result[k].sets = [k]
                    result[k].size = cleanedAllSets[k]
                }else{
                    result[k] = data[k];
                    result[k].size = cleanedAllSets[k]
                    for (let i = 0; i < result[k].data.length; i++) {
                        result[k].data[i]["x"] = Math.random() * 100 - 50;
                        result[k].data[i]["y"] = Math.random() * 100 - 50;
                        if (result[k].data[i]["data"].img) {
                            result[k].data[i]["width"] = 100;
                            result[k].data[i]["bw"] = result[k].data[i]["width"];
                            result[k].data[i]["height"] = 100;
                            result[k].data[i]["bh"] = result[k].data[i]["height"];
                        } else {
                            if (!result[k].data[i]["textParts"]) {
                                const textParts = result[k].data[i]["data"].label.split(
                                    this._splitRegex
                                );
                                let maxWLen = -Infinity;
                                for (const parts of textParts) {
                                    if (maxWLen < parts.length) {
                                        maxWLen = parts.length;
                                    }
                                }
                                maxWLen *= 14;
                                const len = textParts.length;
                                result[k].data[i]["textParts"] = textParts;
                                result[k].data[i]["maxWidthText"] = maxWLen;
                            }
                            const container = document.createElement("div");
                            const div = document.createElement("div");
                            container.appendChild(div)
                            div.innerHTML = result[k].data[i]["data"].label;
                            div.setAttribute("style", `width: ${result[k].data[i]["maxWidthText"]}px; padding: 20px;`)
                            const {width, height} = GraphUtils.computeBoundVirtualNode(container);

                            result[k].data[i]["width"] = width;
                            result[k].data[i]["bw"] = width;
                            result[k].data[i]["height"] = height;
                            result[k].data[i]["bh"] = height;
                        }
                        if(result[k].data.length <= 2){
                            result[k].data[i]["x"] = - result[k].data[i]["width"] / 2;
                            result[k].data[i]["y"] = - result[k].data[i]["height"] / 2;
                        }
                    }
                    const simulation = d3
                        .forceSimulation()
                        .force("center", d3.forceCenter(0, 0))
                        .force("charge", d3.forceManyBody())
                        .force(
                            "collide",
                            GraphUtils.rectCollide().size((d) => {
                                return [d.bw, d.bh];
                            })
                        )
                        .nodes(result[k].data)
                        .stop();
                    const n = Math.ceil(
                        Math.log(simulation.alphaMin()) /
                        Math.log(1 - simulation.alphaDecay())
                    );
                    for (var i = 0; i < n; ++i) {
                        simulation.tick();
                    }
                }
            }
        }else{
            throw "CANNOT DRAW TOO MUCH DATA"
        }
        return Object.values(result);
    }

    draw(containerId) {
        super.draw(containerId);
        this._rootG.append("g").attr("id", "venn-group");
        this._zoom = d3.zoom().on("zoom", (e) => {
            this._rootG.attr("transform", e.transform);
            this._correctTextParentSize();
            this._onZoom(e);
        });
        this._rootSvg.call(this._zoom);
        this._update();
        this._afterDraw()
    }

    adaptViewBoxByRatio(ratio = 16/7){
        this._height = GraphUtils.heightByViewportRatio(this._width,ratio);
        this._rootSvg
            .attr("viewBox", [0, 0,this._width,this._height]);
    }

    updateData(rawData, draw = true) {
        this._data = this._preprocessData(rawData);
        this._data = venn.layout(this._data);
        if(draw){
            this._update();
        }
    }
    _update(data) {
        super._update(data);
        this._beforeDraw();
        const g = this._rootG
            .selectAll('g.groups')
            .data(this._data)
            .join((enter) => {
                const g = enter.filter(d => d.distinctPath.length > 6).append('g').classed("groups", true)
                g.append("path").classed("paths", true);
                g.append('circle');
                g.append("g").classed("nodes-group", true)
                const main_group = g.filter(d => d.circles.length == 1)
                main_group
                    .append("path")
                    .attr("stroke", "none")
                    .attr("fill", "none")
                    .attr("id", (d) => `path-${GraphUtils.slugify(d.circles[0].set)}-${this._id}`)
                    .each(d => console.log(d))
                    .attr(
                        "d",
                        (d) =>
                            `M ${d.circles[0].x} ${d.circles[0].y + d.circles[0].radius - this._padding} A 1 1 0 1 1 ${d.circles[0].x} ${
                                d.circles[0].y - d.circles[0].radius + this._padding
                            } M ${d.circles[0].x} ${d.circles[0].y - d.circles[0].radius + this._padding} A 1 1 0 1 1 ${d.circles[0].x} ${
                                d.circles[0].y + d.circles[0].radius - this._padding
                            } `
                    );
                const text = main_group
                    .append("text")
                    .append("textPath")
                    .style("font-size", "100px")
                    .classed("svg-text", true)
                    .classed("parent-text", true)
                    .attr(
                        "xlink:href",
                        (d) => `#path-${GraphUtils.slugify(d.circles[0].set)}-${this._id}`
                    )
                    .text((d) => d.circles[0].set)
                    .attr("fill", (d, i) =>
                        GraphUtils.colorLuminance(this._color(d, i), -0.2)
                    )
                    .attr("text-anchor", "middle")
                    .attr("startOffset", "50%");
                return g;
            });
        g.select('circle')
            .style("fill", "#cdc8c868")
            .style("stroke", "#cdc8c868")
            .attr('r', d => {
                let r = d.innerCircle.radius;
                if(r < 0){
                    return 0
                }
                if(r > 30){
                    r -= 5
                }
                return r
            })
            .attr('cx',(d) => d.innerCircle.x)
            .attr('cy',(d) => d.innerCircle.y);
        const paths = g.select('path')
            .attr('d', (d) => d.distinctPath)
            .style('fill', this._color);
        if(!this._colored){
            this._colored = []
        }
        this._colored.push(this._rootG.selectAll("path.paths"))
        this._rootG.selectAll("g.nodes-group")
            .selectAll("g.nodes")
            .data((d) => d.data.data,(d) => {
                return JSON.stringify(d);
            })
            .join(
                (enter) => {
                    const leaf_svg_g = enter
                        .append("g")
                        .style("cursor", "pointer")
                        .classed("nodes", true)
                    // .on("click", this._onClickNode);

                    this._leaves.push(leaf_svg_g);
                    const foreign = leaf_svg_g
                        .append("foreignObject")
                        .style("overflow", "visible")
                        .attr("width", (d) => d.width)
                        .attr("height", (d) => d.height)
                        .attr("x", (d) => d.x)
                        .attr("y", (d) => d.y)
                        .style("transform-box", "fill-box")
                        .style("transform", "translate(-50%, -50%)")

                    foreign.filter((d) => !d.data.img)
                        .append("xhtml:div")
                        .style("overflow", "hidden")
                        .style("text-align", "center")
                        .style("padding", "10px")
                        .style("display", "flex")
                        .style("justify-content", "center")
                        .style("align-items", "center")
                        .style("background-color", "transparent")
                        .style("color", "#455a64")
                        .style("border", "2px solid rgba(69, 90, 100, 0.5)")
                        .style("border-radius", "5px")
                        .text(d => d.data.label)
                        .on("click", this._onClickNode)

                    foreign
                        .filter((d) => d.data.img)
                        .append("xhtml:div")
                        .style("overflow", "hidden")
                        .style("max-width", "100%")
                        .style("max-height", "100%")
                        .style("font-size", "6px")
                        .append("xhtml:img")
                        .attr("src", (d) => d.data.img)
                        .attr("alt", (d) => d.data.label)
                        .style("width", "100%")
                        .style("height", "auto")

                    this._leaves.push(foreign);

                });
        this._rootG
            .selectAll("g.nodes-group")
            .attr("transform", d => `translate(${d.innerCircle.x}, ${d.innerCircle.y}) scale(1)`)
            .each((d, i, n) => {
                const bound = n[i].getBoundingClientRect();
                const max = bound.width > bound.height ? bound.width : bound.height;
                const k = (d.innerCircle.radius * 2) / max
                if(!isFinite(k) || k < 0){
                    d.scale = 1
                }else{
                    d.scale = k
                }
                console.log(d.scale);
            })
            .attr("transform", d => `translate(${d.innerCircle.x}, ${d.innerCircle.y}) scale(${d.scale})`)
        this._correctTextParentSize();
        this._afterDraw();

    }
    setColorMap(map){
        var colorFunction = (d, i, n) => {
            const key = d.data.sets.join(",")
            if(Object.keys(map).includes(key)){
                return map[key]
            }
            return this._defaultColor(i);
        };
        this.setColor(colorFunction)
    }
    _correctTextParentSize() {
        const svg = this._rootG;
        const [x, y, w, h] = this._rootSvg.attr("viewBox").split(",");
        const dimension = svg.node().getBoundingClientRect();
        let k = Math.max(w, h) / Math.max(dimension.width, dimension.height);
        if (k > 2.5) {
            k = 2.5;
        }
        if(k < 1){
            k = 1;
        }
        const parentTexts = this._rootSvg.selectAll("textPath.parent-text");
        parentTexts.style("font-size", `${20 * k}px`);
    }
}
window.VennGraph = VennGraph
class CircularBarplotGraph extends Graph{
    _margin = {top: 100, right: 0, bottom: 0, left: 0};
    _width = 800 - this._margin.left - this._margin.right;
    _height = 800 - this._margin.top - this._margin.bottom;
    _innerRadius = 90
    _outerRadius = Math.min(this._width, this._height) / 2;
    _max = 0;
    _defaultColor = d3.scaleOrdinal([
        "#69b3a2"
    ]);
    constructor(authorizedTags = []){
        super();
        super.setAuthorizedTags(authorizedTags);
    }
    draw(containerId){
        super.draw(containerId);
        this._zoom = d3.zoom().on("zoom", (e) => {
            this._rootG.attr("transform", e.transform);
            this._onZoom(e);
        });
        this._rootSvg.call(this._zoom);
    }
    initZoom = () => {
        if(!this._rootSvg) return;
        const currentZoom = d3.zoomTransform(this._rootSvg.node());

        this._rootSvg.call(this._zoom.transform, d3.zoomIdentity);
        const bound = this._rootG.node().getBoundingClientRect();
        this._rootSvg.call(this._zoom.transform, currentZoom);

        const containerBound = this._rootSvg.node().getBoundingClientRect();

        const k1 = isFinite(containerBound.width / bound.width) ? ((containerBound.width - 50) / bound.width): 1;
        const k2 = isFinite(containerBound.height / bound.height) ? ((containerBound.height - 50) / bound.height): 1;
        const k = (k1 > k2 ? k2 : k1);

        const currentViewBox = this._rootSvg.node().viewBox.baseVal;

        const wRatio = currentViewBox.width / containerBound.width;
        const hRatio = currentViewBox.height / containerBound.height;
        let tx = (containerBound.width / 2) - (bound.width / 2) * k + Math.abs(containerBound.x - bound.x) * k ;
        let ty = (containerBound.height / 2) - (bound.height / 2) * k + Math.abs(containerBound.y - bound.y) * k ;
        tx *= wRatio;
        ty *= hRatio;

        this._rootSvg.transition().call(this._zoom.transform, d3.zoomIdentity.translate(tx,ty).scale(k,k))
    }
    preprocessResults(result){
        var result = super.preprocessResults(result);
        var data = {};
        var maxCount = 0;
        for (const [id,row] of Object.entries(result)) {
            if(row.tags){
                for(const tag of row.tags){
                    if(this._authorizedTags && Array.isArray(this._authorizedTags) && this._authorizedTags.length > 0){
                        if(this._authorizedTags.includes(tag)){
                            if(!Object.keys(data).includes(tag)){
                                data[tag] = 0;
                            }
                            data[tag]++;
                            if(maxCount < data[tag]){
                                maxCount = data[tag];
                            }
                        }
                    }else{
                        if(!Object.keys(data).includes(tag)){
                            data[tag] = 0;
                        }
                        data[tag]++;
                        if(maxCount < data[tag]){
                            maxCount = data[tag];
                        }
                    }
                }
            }
        }
        if(this._max > 0 && this._max < Object.keys(data).length){
            const entries = Object.entries(data);
            entries.sort((a,b) => b[1] - a[1]);
            const cleanedData = {}
            for (let i = 0; i < this._max; i++) {
                cleanedData[entries[i][0]] = entries[i][1]
            }
            data = cleanedData;
        }
        return data;
    }
    setMax(value){
        this._max = isNaN(Number(value)) ? this._max : Number(value);
    }
    _update(data){
        var x = d3.scaleBand()
            .range([0, 2 * Math.PI])
            .align(0)
            .domain(Object.keys(data));
        var y = d3.scaleRadial()
            .range([this._innerRadius, this._outerRadius])
            .domain([0, this._max * 10]);
        this._rootG.append("g")
            .selectAll("g.items")
            .data(Object.entries(data))
            .join(enter => {
                const mainG = enter.append("g").classed("items", true)
                mainG.append("path")
                    .attr("fill", this._color)
                    .attr("d", d3.arc()     // imagine your doing a part of a donut plot
                        .innerRadius(this._innerRadius)
                        .outerRadius(function(d) { return y(d[1]); })
                        .startAngle(function(d) { return x(d[0]); })
                        .endAngle(function(d) { return x(d[0]) + x.bandwidth(); })
                        .padAngle(0.01)
                        .padRadius(this._innerRadius))
                mainG.append("g")
                    .attr("text-anchor", function(d) { return (x(d[0]) + x.bandwidth() / 2 + Math.PI) % (2 * Math.PI) < Math.PI ? "end" : "start"; })
                    .attr("transform", function(d) { return "rotate(" + ((x(d[0]) + x.bandwidth() / 2) * 180 / Math.PI - 90) + ")"+"translate(" + (y(d[1])+10) + ",0)"; })
                    .append("text")
                    .text(function(d){return(d[0])})
                    .attr("transform", function(d) { return (x(d[0]) + x.bandwidth() / 2 + Math.PI) % (2 * Math.PI) < Math.PI ? "rotate(180)" : "rotate(0)"; })
                    .style("font-size", "9px")
                    .attr("alignment-baseline", "middle")
            })
        this.initZoom();
    }
}
window.CircularBarplotGraph = CircularBarplotGraph
class MultiGraph extends Graph{
    _margin = {top: 100, right: 0, bottom: 0, left: 0};
    _width = 800 - this._margin.left - this._margin.right;
    _height = 800 - this._margin.top - this._margin.bottom;
    _innerRadius = 0
    _outerRadius = Math.min(this._width, this._height) / 2;
    _maxInternal = 0;
    _maxExternal = 0;
    //Percentage of margin
    _maxInternalMargin = 0.05;
    _internalTitleBackgroundColor = "#00723F"
    _externalTitleBackgroundColor = "#00723F"
    _internalBackgroundColor = "#FF0014"
    _internalBorderColor = "#ffffff"
    _internalBorderWidth = 1
    _internalCircleRadius = 100
    _titleCircleWidth = 25
    _labelCircleWidth = 45
    _labelCircleColor = "#77B82A"
    _barplotCircleRadius = 250
    _defaultInternalColor = "#E5E5E5"
    _labelFontSize = 10
    _labelInternalMargin = 11;
    _labelExternalMargin = 7;
    _internalColor = (d,i,n) => {
        return d3.scaleOrdinal([this._defaultInternalColor])(d,i,n)
    };
    _externalColor = d3.scaleOrdinal(d3.schemeSet3)
    constructor(authorizedTags = []){
        super();
        super.setAuthorizedTags(authorizedTags);
    }
    draw(containerId){
        super.draw(containerId);
        this._zoom = d3.zoom().on("zoom", (e) => {
            this._rootG.attr("transform", e.transform);
            this._onZoom(e);
        });
        this._rootSvg.call(this._zoom);
    }
    initZoom = () => {
        if(!this._rootSvg) return;
        const currentZoom = d3.zoomTransform(this._rootSvg.node());

        this._rootSvg.call(this._zoom.transform, d3.zoomIdentity);
        const bound = this._rootG.node().getBoundingClientRect();
        this._rootSvg.call(this._zoom.transform, currentZoom);

        const containerBound = this._rootSvg.node().getBoundingClientRect();

        const k1 = isFinite(containerBound.width / bound.width) ? ((containerBound.width - 50) / bound.width): 1;
        const k2 = isFinite(containerBound.height / bound.height) ? ((containerBound.height - 50) / bound.height): 1;
        const k = (k1 > k2 ? k2 : k1);

        const currentViewBox = this._rootSvg.node().viewBox.baseVal;

        const wRatio = currentViewBox.width / containerBound.width;
        const hRatio = currentViewBox.height / containerBound.height;
        let tx = (containerBound.width / 2) - (bound.width / 2) * k + Math.abs(containerBound.x - bound.x) * k ;
        let ty = (containerBound.height / 2) - (bound.height / 2) * k + Math.abs(containerBound.y - bound.y) * k ;
        tx *= wRatio;
        ty *= hRatio;

        this._rootSvg.transition().call(this._zoom.transform, d3.zoomIdentity.translate(tx,ty).scale(k,k))
    }
    preprocessResults(result){
        var result = super.preprocessResults(result);
        var data = result;
        if(this._maxInternal <= 0){
            for(const currentInternalData of data.internalData){
                currentInternalData.value = (currentInternalData.value ?? "").split(";").map((d) => Number(d)).filter(d => !isNaN(d))
                for(const v of currentInternalData.value){
                    if(v > this._maxInternal){
                        this._maxInternal = v
                    }
                }
            }
        }
        if(this._maxExternal <= 0){
            for(const {value} of data.externalData){
                if(value > this._maxExternal){
                    this._maxExternal = value
                }
            }
        }
        this._maxInternal += this._maxInternal * this._maxInternalMargin
        return data;
    }
    setMaxInternal(value){
        this._maxInternal = isNaN(Number(value)) ? this._maxInternal : Number(value);
        this._maxInternal += this._maxInternal * this._maxInternalMargin
    }
    setMaxExternal(value){
        this._maxExternal = isNaN(Number(value)) ? this._maxExternal : Number(value);
    }
    setInternalTitleBackgroundColor(color) {
        if(color){
            this._internalTitleBackgroundColor = color
        }
    }
    setInternalBackgroundColor(color) {
        if(color){
            this._internalBackgroundColor = color
        }
    }
    setExternalTitleBackgroundColor(color) {
        if(color){
            this._externalTitleBackgroundColor = color
        }
    }
    setLabelCircleColor(color) {
        if(color){
            this._labelCircleColor = color
        }
    }
    _generateTextPath = (x,y,r) =>
        `M ${x} ${y + r} A 1 1 0 1 1 ${x} ${
            y - r
        } M ${x} ${y - r} A 1 1 0 1 1 ${x} ${
            y + r
        } `
    _updateInternal(data, title = "title internal"){
        var x = d3.scaleBand()
            .range([0, 2 * Math.PI])
            .align(0)
            .domain(data.map(d => d.label));
        const internalGroup = this._rootG.append("g")
            .classed("internal",true)

        //Label 
        //circle
        internalGroup.append("g")
            .classed("label", true)
            .append("circle")
            .attr("r", this._titleCircleWidth + this._internalCircleRadius + this._labelCircleWidth)
            .attr("fill", this._labelCircleColor)
            .style("filter", "drop-shadow(0px 0px 5px rgb(0 0 0 / 0.2))")

        //TITLE 
        //circle
        internalGroup.append("g")
            .classed("title", true)
            .append("circle")
            .attr("r", this._titleCircleWidth + this._internalCircleRadius)
            .attr("fill", this._internalTitleBackgroundColor)
            .style("filter", "drop-shadow(0px 0px 5px rgb(0 0 0 / 0.2))")
        const r = this._internalCircleRadius + 3
        //path
        internalGroup
            .append("path")
            .attr("stroke", "none")
            .attr("fill", "none")
            .attr("id", "title-path-internal")
            .attr(
                "d",
                this._generateTextPath(0,0,r)
            );
        //text
        internalGroup
            .append("text")
            .append("textPath")
            .style("font-size", "20px")
            .classed("svg-text", true)
            .classed("parent-text", true)
            .attr(
                "xlink:href",
                "#title-path-internal"
            )
            .text(title)
            .attr("fill", "white")
            .attr("text-anchor", "middle")
            .attr("startOffset", "50%");


        //CIRCULAR INTERIOR
        //RED PART
        internalGroup
            .append("g")
            .classed("background", true)
            .append("circle")
            .attr("cx", 0)
            .attr("cy", 0)
            .attr("r", this._internalCircleRadius)
            .attr("fill", this._internalBackgroundColor)


        // DATA PART
        internalGroup.append("g")
            .classed("data", true)
            .selectAll("g.items")
            .data(data)
            .join((enter) => {
                const mainG = enter.append("g").classed("items", true)
                const textGroup = mainG.append("g")
                    .classed("texts", true)
                // PATH TEXT
                textGroup.append("path")
                    .attr("id", d=> `path-${GraphUtils.slugify(d.label)}`)
                    .attr("d", (d,i,n) => {
                            let cosAngle = Math.cos(x(d.label) + Math.PI)
                            let cosNextAngle = Math.cos(x(d.label) + x.bandwidth() + Math.PI)
                            const isSouth = cosAngle > 0 && cosNextAngle > 0;
                            const finalTarget = this._internalCircleRadius + this._titleCircleWidth + this._labelInternalMargin;
                            let pathValue
                            if(isSouth){
                                pathValue = d3.arc()
                                    .innerRadius(finalTarget + this._labelFontSize / 2)
                                    .outerRadius(finalTarget + this._labelFontSize + 1)
                                    .startAngle(function(d, i) { return x(d.label)})
                                    .endAngle(function(d, i) { return x(d.label) + x.bandwidth() })(d,i,n)
                            }else{
                                pathValue = d3.arc()
                                    .innerRadius(0)
                                    .outerRadius(finalTarget)
                                    .startAngle(function(d, i) { return x(d.label)})
                                    .endAngle(function(d, i) { return x(d.label) + x.bandwidth() })(d,i,n)
                            }
                            let res = ""
                            if(isSouth){
                                res = "M" + pathValue.substring(pathValue.lastIndexOf("L") + 1, pathValue.length - 1)
                            }else{
                                res = pathValue.substring(0, pathValue.lastIndexOf("L"))
                            }
                            return res
                        }
                    )
                    .attr("stroke", "none")
                    .attr("fill", "none")
                //TEXT
                textGroup
                    .append("text")
                    .append("textPath")
                    .style("font-size", this._labelFontSize + "px")
                    .classed("svg-text", true)
                    .classed("parent-text", true)
                    .attr(
                        "xlink:href",
                        d => `#path-${GraphUtils.slugify(d.label)}`
                    )
                    .text(d => d.label)
                    .attr("fill", "white")
                    .attr("text-anchor", "middle")
                    .attr("startOffset", "50%");
                mainG
                    .append("g")
                    .classed("arc-data", true)
                    .selectAll("path")
                    .data(d => d.value)
                    .join((pathEnter) => {
                        pathEnter
                            .append("path")
                            .attr("d", (d,i,n) => {
                                    const parentData = d3.select(n[0].parentNode).datum();
                                    const current = parentData.label
                                    const {value} = parentData
                                    return d3.arc()
                                        .innerRadius(this._innerRadius)
                                        .outerRadius((d) => d * this._internalCircleRadius / this._maxInternal)
                                        .startAngle(function(d, i) { return x(current) + (x.bandwidth() / value.length) * i; })
                                        .endAngle(function(d, i) { return x(current) + (x.bandwidth() / value.length) * (i + 1); })(d,i,n)
                                }
                            )
                            .attr("fill", (d,i,n) => {
                                const parentData = d3.select(n[0].parentNode).datum();
                                const { color} = parentData
                                return color ?? this._internalColor(d,i,n)
                            })
                            .attr("stroke", (d,i,n) => {
                                const parentData = d3.select(n[0].parentNode).datum();
                                const { color} = parentData
                                return color ?? this._internalColor(d,i,n)
                            })
                    })
            })

        //BORDER
        internalGroup.append("g")
            .classed("borders", true)
            .selectAll("line")
            .data(data)
            .join((enter) => {
                enter.append("line")
                    .attr("x1", 0)
                    .attr("y1", 0)
                    .attr("x2", (d) => Math.sin(x(d.label) + Math.PI) * this._internalCircleRadius)
                    .attr("y2", (d) => Math.cos(x(d.label) + Math.PI) * this._internalCircleRadius)
                    .attr("stroke", this._internalBorderColor)
                    .attr("stroke-width", this._internalBorderWidth)
            })
    }
    _updateExternal(data, title = "external title"){
        const internalRadius = this._internalCircleRadius + this._titleCircleWidth * 2 + this._labelCircleWidth
        const externalGroup = this._rootG.append("g")
            .classed("external", true)

        //TITLE 
        //circle
        externalGroup.append("g")
            .classed("title", true)
            .append("circle")
            .attr("r", internalRadius)
            .attr("fill", this._externalTitleBackgroundColor)
            .style("filter", "drop-shadow(0px 0px 5px rgb(0 0 0 / 0.2))")
        const r = this._internalCircleRadius + this._titleCircleWidth + this._labelCircleWidth + 3
        //path
        const path = externalGroup
            .append("path")
            .attr("stroke", "none")
            .attr("fill", "none")
            .attr("id", "title-path-external")
            .attr(
                "d",
                this._generateTextPath(0,0,r)
            );
        //text
        const text = externalGroup
            .append("text")
            .append("textPath")
            .style("font-size", "20px")
            .classed("svg-text", true)
            .classed("parent-text", true)
            .attr(
                "xlink:href",
                "#title-path-external"
            )
            .text(title)
            .attr("fill", "white")
            .attr("text-anchor", "middle")
            .attr("startOffset", "50%");




        var x = d3.scaleBand()
            .range([0, 2 * Math.PI])
            .align(0)
            .domain(data.map(d => d.label));
        var y = d3.scaleRadial()
            .range([internalRadius, this._barplotCircleRadius])
            .domain([0, this._maxExternal]);

        externalGroup.selectAll("g.items")
            .data(data)
            .join(enter => {
                const mainG = enter.append("g").classed("items", true)

                mainG.append("path")
                    .attr("fill", this._externalColor)
                    .style("fill", d => d.color)
                    .attr("d", d3.arc()
                        .innerRadius(internalRadius)
                        .outerRadius(function(d) { return y(d.value); })
                        .startAngle(function(d) { return x(d.label); })
                        .endAngle(function(d) { return x(d.label) + x.bandwidth(); })
                        .padAngle(0.01)
                        .padRadius(internalRadius))

                const textGroup = mainG.append("g")
                    .classed("texts", true)
                // PATH TEXT
                textGroup.append("path")
                    .attr("id", d=> `path-${GraphUtils.slugify(d.label)}`)
                    .attr("d", (d,i,n) => {
                            let cosAngle = Math.cos(x(d.label) + Math.PI)
                            let cosNextAngle = Math.cos(x(d.label) + x.bandwidth() + Math.PI)
                            const isSouth = cosAngle > 0 && cosNextAngle > 0;
                            const finalTarget = y(d.value) + this._labelExternalMargin
                            let pathValue
                            if(isSouth){
                                pathValue = d3.arc()
                                    .innerRadius(finalTarget + this._labelFontSize / 2)
                                    .outerRadius(finalTarget + this._labelFontSize + 1)
                                    .startAngle(function(d, i) { return x(d.label)})
                                    .endAngle(function(d, i) { return x(d.label) + x.bandwidth() })(d,i,n)
                            }else{
                                pathValue = d3.arc()
                                    .innerRadius(0)
                                    .outerRadius(finalTarget)
                                    .startAngle(function(d, i) { return x(d.label)})
                                    .endAngle(function(d, i) { return x(d.label) + x.bandwidth() })(d,i,n)
                            }
                            let res = ""
                            if(isSouth){
                                res = "M" + pathValue.substring(pathValue.lastIndexOf("L") + 1, pathValue.length - 1)
                            }else{
                                res = pathValue.substring(0, pathValue.lastIndexOf("L"))
                            }
                            return res
                        }
                    )
                    .attr("stroke", "none")
                    .attr("fill", "none")
                //TEXT
                textGroup
                    .append("text")
                    .append("textPath")
                    .style("font-size", this._labelFontSize + "px")
                    .classed("svg-text", true)
                    .classed("parent-text", true)
                    .attr(
                        "xlink:href",
                        d => `#path-${GraphUtils.slugify(d.label)}`
                    )
                    .text(d => d.label)
                    .attr("fill", "black")
                    .attr("text-anchor", "middle")
                    .attr("startOffset", "50%");
            })
    }
    _update(data){
        this._updateExternal(data.externalData, data.externalTitle)
        this._updateInternal(data.internalData, data.internalTitle)
        this.initZoom();
    }
}
window.MultiGraph = MultiGraph
class RadialGraph extends Graph{
    _margin = {top: 100, right: 0, bottom: 0, left: 0};
    _width = 800 - this._margin.left - this._margin.right;
    _height = 800 - this._margin.top - this._margin.bottom;
    _radius = Math.min(this._width, this._height) / 2;
    _labelFontSize = 10
    _levels = 4			//How many levels or inner circles should there be drawn
    _maxValue = 0 			//What is the value that the biggest circle will represent
    _labelFactor = 1.25 	//How much farther than the radius of the outer circle should the labels be placed
    _wrapWidth = 80 		//The number of pixels after which a label needs to be given a new line
    _opacityArea = 0.35 	//The opacity of the area of the blob
    _dotRadius = 6 			//The size of the colored circles of each blog
    _opacityCircles = 0.1 	//The opacity of the circles of each blob
    _strokeWidth = 3 		//The width of the stroke around each blob
    _roundStrokes = true	//If true the area and stroke will follow a round path (cardinal-closed)
    _color = null

    constructor(authorizedTags = []){
        super();
        super.setAuthorizedTags(authorizedTags);
    }
    setLevels(levels){
        levels = Number(levels);
        if(!isNaN(levels)){
            this._levels = levels
        }
    }
    draw(containerId){
        this._color = this._defaultColor
        super.draw(containerId);
        this._zoom = d3.zoom().on("zoom", (e) => {
            this._rootG.attr("transform", e.transform);
            this._onZoom(e);
        });
        this._rootSvg.call(this._zoom);
        this._addSVGFilters()
    }
    initZoom = () => {
        if(!this._rootSvg) return;
        const currentZoom = d3.zoomTransform(this._rootSvg.node());

        this._rootSvg.call(this._zoom.transform, d3.zoomIdentity);
        const bound = this._rootG.node().getBoundingClientRect();
        this._rootSvg.call(this._zoom.transform, currentZoom);

        const containerBound = this._rootSvg.node().getBoundingClientRect();

        const k1 = isFinite(containerBound.width / bound.width) ? ((containerBound.width - 50) / bound.width): 1;
        const k2 = isFinite(containerBound.height / bound.height) ? ((containerBound.height - 50) / bound.height): 1;
        const k = (k1 > k2 ? k2 : k1);

        const currentViewBox = this._rootSvg.node().viewBox.baseVal;

        const wRatio = currentViewBox.width / containerBound.width;
        const hRatio = currentViewBox.height / containerBound.height;
        let tx = (containerBound.width / 2) - (bound.width / 2) * k + Math.abs(containerBound.x - bound.x) * k ;
        let ty = (containerBound.height / 2) - (bound.height / 2) * k + Math.abs(containerBound.y - bound.y) * k ;
        tx *= wRatio;
        ty *= hRatio;

        this._rootSvg.transition().call(this._zoom.transform, d3.zoomIdentity.translate(tx,ty).scale(k,k))
    }
    preprocessResults(result){
        const names = new Set()
        const categoryNames = {}
        for(const [categoryName, categoryValues] of Object.entries(result)){
            categoryNames[categoryName] = new Set()
            const obj = {}
            result[categoryName].length = categoryValues.length
            for(const item of categoryValues){
                names.add(item.name)
                categoryNames[categoryName].add(item.name)
                obj[item.name] = item
            }
            result[categoryName] = obj
        }
        for(const categoryName of Object.keys(categoryNames)){
            if(names.size !== result[categoryName].length){
                for(const name of [...names]){
                    if(!result[categoryName].hasOwnProperty(name)){
                        result[categoryName][name] = {name, value: 0}
                    }
                }
            }
        }
        for(const categoryName of Object.keys(result)){
            result[categoryName] = [...names].map((name) => result[categoryName][name])
        }
        return result;
    }
    _computeMaxValue(data){
        this._maxValue = Math.max(this._maxValue, d3.max(Object.values(data), datum => d3.max(datum.map(item => item.value))));
    }
    _addSVGFilters(){
        //Filter for the outside glow
        let filter = this._rootSvg.append('defs').append('filter').attr('id','glow'),
            feGaussianBlur = filter.append('feGaussianBlur').attr('stdDeviation','2.5').attr('result','coloredBlur'),
            feMerge = filter.append('feMerge'),
            feMergeNode_1 = feMerge.append('feMergeNode').attr('in','coloredBlur'),
            feMergeNode_2 = feMerge.append('feMergeNode').attr('in','SourceGraphic');
    }
    _drawGrid(){
        //Draw the background circles
        this._axisGrid.selectAll(".levels")
            .data(d3.range(1,(this._levels+1)).reverse())
            .enter()
            .append("circle")
            .attr("class", "gridCircle")
            .attr("r", (d, i) => this._radius/this._levels*d)
            .style("fill", "#CDCDCD")
            .style("stroke", "#CDCDCD")
            .style("fill-opacity", this._opacityCircles)
            .style("filter" , "url(#glow)");

        //Text indicating at what % each level is
        this._axisGrid.selectAll(".axisLabel")
            .data(d3.range(1,(this._levels+1)).reverse())
            .enter().append("text")
            .attr("class", "axisLabel")
            .attr("x", 4)
            .attr("y", d=> -d*this._radius/this._levels)
            .attr("dy", "0.4em")
            .style("font-size", "15px")
            .attr("fill", "#737373")
            .text((d,i) => this._maxValue * d/this._levels);
    }
    _drawAxis(allAxis, angleSlice, rScale){
        //Create the straight lines radiating outward from the center
        var axis = this._axisGrid.selectAll(".axis")
            .data(allAxis)
            .enter()
            .append("g")
            .attr("class", "axis");
        //Append the lines
        axis.append("line")
            .attr("x1", 0)
            .attr("y1", 0)
            .attr("x2", (d, i) => rScale(this._maxValue*1.1) * Math.cos(angleSlice*i - Math.PI/2))
            .attr("y2", (d, i) => rScale(this._maxValue*1.1) * Math.sin(angleSlice*i - Math.PI/2))
            .attr("class", "line")
            .style("stroke", "white")
            .style("stroke-width", "2px");

        //Append the labels at each axis
        axis.append("text")
            .attr("class", "legend")
            .style("font-size", "18px")
            .attr("text-anchor", "middle")
            .attr("dy", "0.35em")
            .attr("x", (d, i) => rScale(this._maxValue * this._labelFactor) * Math.cos(angleSlice*i - Math.PI/2))
            .attr("y", (d, i) => rScale(this._maxValue * this._labelFactor) * Math.sin(angleSlice*i - Math.PI/2))
            .text(function(d){return d})
            .call(GraphUtils.wrap, this._wrapWidth);
    }
    _getAllAxis(data){
        const uniqueAxis = new Set()
        for(const datum of Object.values(data)){
            for(const item of datum){
                uniqueAxis.add(item.name)
            }
        }
        return [...uniqueAxis]
    }
    _update(data){
        this._computeMaxValue(data)
        let allAxis = this._getAllAxis(data),	//Names of each axis
            total = allAxis.length,     			//The number of different axes
            Format = d3.format('%'),			 	//Percentage formatting
            angleSlice = Math.PI * 2 / total;
        //Scale for the radius
        const rScale = d3.scaleLinear()
            .range([0, this._radius])
            .domain([0, this._maxValue]);

        //Wrapper for the grid & axes
        this._axisGrid = this._rootG.append("g").attr("class", "axisWrapper");
        this._drawGrid()
        this._drawAxis(allAxis, angleSlice, rScale)

        //The radial line function
        var radarLine = d3.lineRadial()
            .curve(d3.curveLinearClosed)
            .radius(function(d) { return rScale(d.value); })
            .angle(function(d,i) {	return i*angleSlice; });

        if(this._roundStrokes) {
            radarLine.curve(d3.curveCardinalClosed);
        }

        //Create a wrapper for the blobs	
        this._rootG.selectAll(".radarWrapper").remove()
        var blobWrapper =
            this._rootG.append("g")
                .attr("class", "radarWrapper")
        //Append the backgrounds
        blobWrapper
            .selectAll("path")
            .data(Object.values(data))
            .join((enter) => {
                enter
                    .append("path")
                    .attr("class", "radarArea")
                    .attr("d", (d,i) => { return radarLine(d); })
                    .style("stroke", (d,i) => this._color(i))
                    .style("stroke-width", this._strokeWidth + "px")
                    .style("fill", "none")
                    .style("filter" , "url(#glow)");
                const groupCircle = enter.append("g")
                    .attr("data-index", (d,i) => i)
                groupCircle.selectAll(".radarCircle")
                    .data(function(d,i) { return d; })
                    .enter().append("circle")
                    .attr("class", "radarCircle")
                    .attr("r", this._dotRadius)
                    .attr("cx", function(d,i){ return rScale(d.value) * Math.cos(angleSlice*i - Math.PI/2); })
                    .attr("cy", function(d,i){ return rScale(d.value) * Math.sin(angleSlice*i - Math.PI/2); })
                    .style("fill", (d,i,j) => {
                        const index = +j[0].parentNode.getAttribute("data-index");
                        return this._color(index)
                    })
                    .style("fill-opacity", 0.8);
            })
        this.initZoom();
    }
}
window.RadialGraph = RadialGraph

class CommunityNodeGraph extends Graph{
    _root = {};
    _simulation = null;
    _nodes = null;
    _links = null;
    _onTick = () => {};
    _isEmpty = false;
    _funcGroup = d => d.group;
    _first = true;
    _dataTransformed = null;
    _buttonGraph = null;
    _currentMode = "graph";
    _buttonList = null;
    _breadElement = null;
    constructor(data, breadElement = null, funcGroup = d => d.group){
        super();
        this._breadElement = breadElement ? d3.select(breadElement) : null;
        this._funcGroup = funcGroup;
        this._data = data;
        this._addBreadcrumbs(this._data);
        this._dataTransformed = this._preprocessData(data);
    }

    _drawModeSwitcher(containerId) {
        const container = d3.select(containerId)
        this._navigationNode = container
            .append("xhtml:div")
            .style("border-radius", "4px")
            .style("box-shadow", "rgb(0 0 0 / 50%) 0px 0px 5px 0px")
            .style("background-color", "white")
            /*.style("z-index", 20000)*/
            .style("border-color", "#acacaa")
            .style("position", "absolute")
            .style("top", "10px")
            .style("right", "10px")
            .style("display", "flex")
            .style("align-items", "center")
            .style("justify-content", "space-around")
            .style("z-index", "1001")
            .classed("group-button-switcher", true)
        const iconSize = 20;
        const svgListIcon = `
          <svg style="height: ${iconSize}px; width: ${iconSize}px;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="svg-list-icon" x="0px" y="0px" viewBox="0 0 297 297" style="enable-background:new 0 0 297 297;" xml:space="preserve">
                <path d="M66.102,0H15.804C7.089,0,0,7.089,0,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804V15.804C81.907,7.089,74.817,0,66.102,0z"/>
                <path d="M173.649,0h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804V15.804C189.453,7.089,182.364,0,173.649,0z"/>
                <path d="M66.102,107.547H15.804C7.089,107.547,0,114.636,0,123.351v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C81.907,114.636,74.817,107.547,66.102,107.547z"/>
                <path d="M173.649,107.547h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C189.453,114.636,182.364,107.547,173.649,107.547z"/>
                <path d="M281.196,0h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804V15.804C297,7.089,289.911,0,281.196,0z"/>
                <path d="M281.196,107.547h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C297,114.636,289.911,107.547,281.196,107.547z"/>
                <path d="M66.102,215.093H15.804C7.089,215.093,0,222.183,0,230.898v50.298C0,289.911,7.089,297,15.804,297h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C81.907,222.183,74.817,215.093,66.102,215.093z"/>
                <path d="M173.649,215.093h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C189.453,222.183,182.364,215.093,173.649,215.093z"/>
                <path d="M281.196,215.093h-50.298c-8.715,0-15.804,7.089-15.804,15.804v50.298c0,8.715,7.089,15.804,15.804,15.804h50.298     c8.715,0,15.804-7.089,15.804-15.804v-50.298C297,222.183,289.911,215.093,281.196,215.093z"/>
          </svg>
        `
        const svgGraphIcon = `
        <svg style="height: ${iconSize}px; width: ${iconSize}px;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="svg-graph-icon" x="0px" y="0px" viewBox="0 0 349.899 349.898" style="enable-background:new 0 0 349.899 349.898;" xml:space="preserve">
          <path d="M175.522,12.235c-42.6,0-77.256,34.649-77.256,77.25c0,42.6,34.656,77.255,77.256,77.255    c42.591,0,77.257-34.656,77.257-77.255C252.779,46.895,218.113,12.235,175.522,12.235z"/>
          <path d="M77.255,337.663c42.599,0,77.255-34.641,77.255-77.251c0-42.594-34.656-77.25-77.255-77.25    C34.653,183.162,0,217.818,0,260.412C0,303.012,34.653,337.663,77.255,337.663z"/>
          <path d="M272.648,183.151c-42.603,0-77.256,34.65-77.256,77.256c0,42.604,34.653,77.25,77.256,77.25    c42.6,0,77.251-34.646,77.251-77.25C349.909,217.818,315.248,183.151,272.648,183.151z"/>
        </svg>
        `
        this._buttonGraph = this._navigationNode.append("xhtml:div")
            .style("padding", "10px")
            .classed("button-switcher-mode", true)
            .classed("active", true)
            .style("display", "flex")
            .style("justify-content", "center")
            .style("align-items", "center")
            .style("font-size", "12pt")
            .style("font-weight", "bold")
            .style("cursor", "pointer")
            .html(svgGraphIcon+" <span class='padding-left-5'> Mode Mapping</span>")
            .on("click", () => {
                if(this._currentMode != "graph"){
                    this.switchMode("graph");
                }
            })
        this._buttonList = this._navigationNode.append("xhtml:div")
            .style("padding", "10px")
            .classed("button-switcher-mode", true)
            .style("display", "flex")
            .style("justify-content", "center")
            .style("align-items", "center")
            .style("font-size", "12pt")
            .style("font-weight", "bold")
            .style("cursor", "pointer")
            .html(svgListIcon+" <span class='padding-left-5'> Mode Liste</span>")
            .on("click", () => {
                if(this._currentMode != "list"){
                    this.switchMode("list");
                }
            })
    }

    switchMode = (mode) => {
        if (mode == "graph"){
            this._buttonGraph.classed("active", true);
            this._buttonList.classed("active", false);
            $(this._containerId+" .mobile-section").hide();
            this._currentMode = mode;
            this._transition = 750;
        }else if(mode == "list"){
            $(this._containerId+" .mobile-section").show();
            this._buttonGraph.classed("active", false);
            this._buttonList.classed("active", true);
            this._currentMode = mode;
            this._transition = 0
        }else{
            console.error("MODE UNKNOWN");
        }
    }
    setOnTick(callback) {
        this._onTick = callBack;
    }

    _preprocessData(data) {
        var nodes = {};
        var links = [];
        function recurse(node, parent){
            if(node.children){
                node.children.forEach((elt) => {
                    links.push({source: node.id, target: elt.id})
                    recurse(elt, node);
                })

            }
            node.parent = parent;
            nodes[node.id] = node;
        }
        recurse(data, null);

        this._isEmpty = !(Object.keys(nodes).length > 0);
        if(!this._isEmpty){
            this._simulation = d3
                .forceSimulation(Object.values(nodes))
                .force("link",
                    d3
                        .forceLink(links)
                        .id((d) => d.id)
                        .strength(0.3).distance((d) => d.group == "root" ? 100 : 50)
                )
                .force("charge", d3.forceManyBody())
                .force("collide", d3.forceCollide(30).iterations(4))
                .force("x", d3.forceX(this._width/2))
                .force("y", d3.forceY(this._height/2).strength(0.1*this._width/this._height))
                // .force("center", d3.forceCenter(this._width / 2, this._width / 4))

        }
        mylog.log('Object data', {
            nodes: Object.values(nodes),
            links: links
        });
        return {
            nodes: Object.values(nodes),
            links: links
        };
    }
    _color = {
        "citoyens": "#F5E740",
        "organizations": "#93C020",
        "contributors": "#8C5AA1",
        "projects": "#8C5AA1",
        "forms": "#e33551",
        "answers": "#e33551",
    };
    draw(containerId) {
        super.draw(containerId);
        super.addSwitcherMode(containerId);
        this._zoom = d3.zoom().on("zoom", (e, d) => {
            this._rootG.attr("transform", e.transform);
            this._onZoom(e, d);
        })
        this._rootSvg.call(this._zoom);
        this._rootSvg
            .attr("viewBox", [0, 0, this._width, this._height]);
        this._update();
    }
    _update(){
        if(!this._isEmpty){
            this._addMobileSection();
            this._rootG.selectAll("g").remove()
            this._simulation.restart()
            this._simulation.on("tick", () => this._tickActions());
            if (!this._rootG.select("g.links").node()) {
                this._rootG.append("g").attr("class", "links");
            }
            this._rootG
                .select("g.links")
                .selectAll("line")
                .data(this._dataTransformed.links, d => d.source+" "+ d.target)
                .join(
                    (enter) => {
                        this._linksNode = enter
                            .append("line")
                            .classed("links-line", true)
                            .attr("stroke-width", 2)
                            .attr("stroke", "#00000080");
                    }
                );
            if (!this._rootG.select("g.nodes").node()) {
                this._rootG.append("g").attr("class", "nodes");
            }
            const nodes = this._rootG
                .select("g.nodes")
                .selectAll("g.node")
                .data(this._dataTransformed.nodes, d => JSON.stringify(d, () => {
                const seen = new WeakSet();
                return (key, value) => {
                    if (typeof value === "object" && value !== null) {
                        if (seen.has(value)) {
                            return;
                        }
                        seen.add(value);
                    }
                    return value;
                };
            }))
                .join(
                    (enter) => {
                        this._nodes = enter
                            .append("g")
                            .style("overflow", "visible")
                            .style("cursor", "pointer")
                            .classed("node", true).on("click", (d, i,n) => this._clickNode(d,i,n))
                        this._nodes.append("circle")
                            .attr("r", d => {
                                return d.group == "root" ? 15 : (typeof d.size != "undefined" ? (d.size < 10 ? 10 : (d.size > 30 ? 30 : d.size)) : 10);
                            })
                            .attr("fill", d => {
                                if(d.group == "root"){
                                    return "green";
                                }else if(typeof this._color[d.group] == "undefined" && typeof d.type != "undefined"){
                                    return this._color[d.type];
                                }else {
                                    return this._color[d.group];
                                }
                            });
                            this._nodes.append("text").attr("x", d =>  d.group == "root" ? 15 : (typeof d.size != "undefined" ? (d.size < 10 ? 10 : (d.size > 30 ? 30 : d.size)) : 10)).attr("y", 5).text(function(d){
                                return d.group == "root" ? "" : (typeof d.profil != "undefined" && d.profil != null ? "" : d.name);
                            })
                            this._nodes.append("title").text(function(d){
                                return d.name;
                            })
                        this._nodes.append("image").attr("xlink:href", d => {
                            return typeof d.profil != "undefined" ? d.profil : "";
                        })
                            .attr("x", d => d.group == "root" ? "-15px" : (typeof d.size != "undefined" ? (d.size < 10 ? "-10px" : (d.size > 30 ? "-30px" : `-${d.size}px`)) : "-10px"))
                            .attr("clip-path",'inset(0 0 0 0 round 50%)')
                            .attr("y", d => d.group == "root" ? "-15px" : (typeof d.size != "undefined" ? (d.size < 10 ? "-10px" : (d.size > 30 ? "-30px" : `-${d.size}px`)) : "-10px"))
                            .attr("width", d => d.group == "root" ? "30px" : (typeof d.size != "undefined" ? (d.size < 10 ? "20px" : (d.size > 30 ? "60px" : `${d.size*2}px`)) : "20px"))
                            .attr("height", d => d.group == "root" ? "30px" : (typeof d.size != "undefined" ? (d.size < 10 ? "20px" : (d.size > 30 ? "60px" : `${d.size*2}px`)) : "20px"))
                    }
                );
            nodes.call(
                d3
                    .drag()
                    .on("start", (e, d) => this._dragStart(e, d))
                    .on("drag", (e, d) => this._dragDrag(e, d))
                    .on("end", (e, d) => this._dragEnd(e, d))
            );
        }
    }

    _addMobileSection(){
        if(this._mobileSection!=null){
            this._mobileSection.selectAll("div.list-group.list-group-root.well").remove();
            this._mobileSection
                .select("div.mobile-section")
                .append("div").attr("class", "list-group list-group-root well")
                .selectAll("a.list-group-item")
                .data(this._data.children ?? this._data._children, (d) => JSON.stringify(d, () => {
                    const seen = new WeakSet();
                    return (key, value) => {
                        if (typeof value === "object" && value !== null) {
                            if (seen.has(value)) {
                                return;
                            }
                            seen.add(value);
                        }
                        return value;
                    };
                }))
                .join((enter) => {
                    const childrens = enter
                        .append("xhtml:a")
                        .attr("href", d=> {
                            return "#"+ GraphUtils.slugify(d.id)
                        })
                        .attr("class", "list-group-item text-left")
                        .attr("data-toggle", "collapse")
                        .html(d => `<i class="fa fa-chevron-right icon"></i> ${d.name}`)
                        .on('click', function() {
                            const icon = d3.select(this)
                                .select("i.icon")
                            const contains = icon.node().classList.contains("fa-chevron-right");

                            if(contains){
                                icon.classed("fa-chevron-down", true)
                                icon.classed("fa-chevron-right", false)
                            }else{
                                icon.classed("fa-chevron-down", false)
                                icon.classed("fa-chevron-right", true)
                            }
                        })
                    enter.each((d, i, n) => {
                        d3.select(n[i])
                            .insert("xhtml:div", `a[href="#${GraphUtils.slugify(d.id)}"] + *`)
                            .attr("class","list-group collapse")
                            .attr("id",d => GraphUtils.slugify(d.id))
                            .selectAll("a.list-group-item")
                            .data(d => {
                                return typeof d.children != "undefined" && d.children ? d.children : (typeof d._children != "undefined" && d._children ? d._children : [])
                            })
                            .join(node => {
                                this._addElement(node);
                            })
                    })
                })
        }
    }

    _addBreadcrumbs(element){
        if(this._breadElement){
            this._breadElement.selectAll("li").remove();
            var breadData = [];
            function getBreadData(data){
                if(data.parent){
                    getBreadData(data.parent);
                }
                breadData.push(data.name);
            }
            getBreadData(element);
            breadData.forEach((element, index) => {
                this._breadElement.insert("xhtml:li").attr("class", index === (breadData.length - 1 )? "active" : '').html(`${element}`);
            });
        }
    }

    _addElement(node){
        node.each((d, i,n) => {
            node.selectAll("a.list-group-item"+GraphUtils.slugify(d.group)).remove();
            if((typeof d.children != "undefined" && d.children) || (typeof d._children != "undefined" && d._children)){
                const childrens = node
                        .append("xhtml:a")
                        .attr("href", d=> {
                            return "#"+ GraphUtils.slugify(d.id)
                        })
                        .attr("class", "list-group-item text-left list-group-item"+GraphUtils.slugify(d.group))
                        .attr("data-toggle", "collapse")
                        .html(d => `<i class="fa fa-chevron-right icon"></i> ${d.name}`)
                        .on('click', function() {
                            const icon = d3.select(this)
                                .select("i.icon")
                            const contains = icon.node().classList.contains("fa-chevron-right");

                            if(contains){
                                icon.classed("fa-chevron-down", true)
                                icon.classed("fa-chevron-right", false)
                            }else{
                                icon.classed("fa-chevron-down", false)
                                icon.classed("fa-chevron-right", true)
                            }
                        })
                    childrens
                        .insert("xhtml:div", `a[href="#${GraphUtils.slugify(d.id)}"] + *`)
                        .attr("class","list-group collapse")
                        .attr("id",d => GraphUtils.slugify(d.id))
                        .selectAll("a.list-group-item")
                        .data(d => {
                            return typeof d.children != "undefined" && d.children ? d.children : (typeof d._children != "undefined" && d._children ? d._children : [])
                        })
                        .join(node => {
                            this._addElement(node);
                        })
            }else{
                const a = node.append("xhmtl:a")
                    .classed("list-group-item text-left", true)
                    .classed("list-group-item"+GraphUtils.slugify(d.group), true)
                    //.classed("lbh-preview-element", true)
                    .attr("href", d =>{
                        if(["projects", "organizations", "citoyens"].includes(d.group)){
                            return `#page.type.${d.group}.id.${d.id}`
                        }
                    })
                // .on('click', this._onClickNodeMobile)
                a.filter(d => d.profil)
                    .append("xhtml:img")
                    .attr("src", (d) => d.profil)
                    .attr("alt", (d) => d.name)
                a.append("xhtml:span")
                    .text(d => d.name)
            }
        })
    }
    _dragStart(event, d) {
        event.sourceEvent.stopPropagation();
        if (!event.active) this._simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
    }

    _dragDrag(event, d) {
        d.fx = event.x;
        d.fy = event.y;
    }

    _dragEnd(event, d) {
        if (!event.active) this._simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
    }
    _clickNode = (event, d, n) => {
        if(d.children){
            d._children = d.children;
            d.children = null;
            this._addBreadcrumbs(d.group == "root" ? d : d.parent);
            this.update();
        } else if(d._children) {
            d.children = d._children;
            d._children = null;
            this._addBreadcrumbs(d);
            this.update();
        }else if (d.id.match(/^[0-9a-fA-F]{24}$/)) {
            getAPIData(d.group, d.id, d);
        }
    }
    update = () => {
        this._dataTransformed = this._preprocessData(this._data);
        // this._simulation.nodes(this._dataTransformed.nodes).force('links').links(this._dataTransformed.links);
        this._update();
    }
    _tickActions() {
        this._onTick()
        this._rootSvg.selectAll("g.node")
            .attr("transform", d=> `translate(${d.x}, ${d.y})`);

        //update link positions
        this._rootSvg.selectAll(".links-line")
            .attr("x1", function(d) {
                return d.source.x;
            })
            .attr("y1", function(d) {
                return d.source.y;
            })
            .attr("x2", function(d) {
                return d.target.x;
            })
            .attr("y2", function(d) {
                return d.target.y;
            });
    }
    initZoom = () => {
        if(!this._rootSvg) return;
        const currentZoom = d3.zoomTransform(this._rootSvg.node());

        this._rootSvg.call(this._zoom.transform, d3.zoomIdentity);
        const bound = this._rootG.node().getBoundingClientRect();
        this._rootSvg.call(this._zoom.transform, currentZoom);

        const containerBound = this._rootSvg.node().getBoundingClientRect();

        const k1 = isFinite(containerBound.width / bound.width) ? ((containerBound.width - 50) / bound.width): 1;
        const k2 = isFinite(containerBound.height / bound.height) ? ((containerBound.height - 50) / bound.height): 1;
        const k = (k1 > k2 ? k2 : k1);

        const currentViewBox = this._rootSvg.node().viewBox.baseVal;

        const wRatio = currentViewBox.width / containerBound.width;
        const hRatio = currentViewBox.height / containerBound.height;
        let tx = (containerBound.width / 2) - (bound.width / 2) * k + Math.abs(containerBound.x - bound.x) * k ;
        let ty = (containerBound.height / 2) - (bound.height / 2) * k + Math.abs(containerBound.y - bound.y) * k ;
        tx *= wRatio;
        ty *= hRatio;

        this._rootSvg.transition().call(this._zoom.transform, d3.zoomIdentity.translate(tx,ty).scale(k,k))
    }
    addChild = (parent, child) => {
        if(this._data._children){
            this._data.children = this._data._children;
            this._data._children = null;
        }

        this._data.children.forEach((element) => {
            if(element.id == parent){
                if(typeof element._children != "undefined" && element._children){
                    element.children = element._children;
                    element._children = null;
                }
                element.children.push(child)
            }
        })
        this.update();
    }
    removeChild = (parent, child) => {
        if(this._data._children){
            this._data.children = this._data._children;
            this._data._children = null;
        }

        this._data.children.forEach((element) => {
            if(element.id == parent){
                if(typeof element._children != "undefined" && element._children){
                    element.children = element._children;
                    element._children = null;
                }
                element.children = element.children.filter(item => item.id != child);
            }
        })
        this.update();
    }
}
window.CommunityNodeGraph = CommunityNodeGraph