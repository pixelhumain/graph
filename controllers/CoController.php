<?php


namespace PixelHumain\PixelHumain\modules\graph\controllers;

use CommunecterController;

/**
 * CoController.php
 *
 * Cocontroller always works with the PH base
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CoController extends CommunecterController
{


	public function beforeAction($action)
	{
		//parent::initPage();
		return parent::beforeAction($action);
	}

	public function actions()
	{
		return array(
			'getdata'  => \PixelHumain\PixelHumain\modules\graph\controllers\actions\GetDataAction::class,
			'fluid'    => \PixelHumain\PixelHumain\modules\graph\controllers\actions\FluidAction::class,
			'fludy'    => \PixelHumain\PixelHumain\modules\graph\controllers\actions\FludyAction::class,
			'finder'   => \PixelHumain\PixelHumain\modules\graph\controllers\actions\FinderAction::class,
			//'viewer'   => \PixelHumain\PixelHumain\modules\graph\controllers\actions\ViewerAction::class,
            'd3'  	   => \PixelHumain\PixelHumain\modules\graph\controllers\actions\D3Action::class,
            'community'  	   => \PixelHumain\PixelHumain\modules\graph\controllers\actions\CommunityAction::class,
            'communitydata'  	   => \PixelHumain\PixelHumain\modules\graph\controllers\actions\GetCommunityDataAction::class,
			'search'   => \PixelHumain\PixelHumain\modules\graph\controllers\actions\SearchAction::class,
			'menu'     => \PixelHumain\PixelHumain\modules\graph\controllers\actions\MenuAction::class,
			'time'     => \PixelHumain\PixelHumain\modules\graph\controllers\actions\TimeAction::class,
			'narative' => \PixelHumain\PixelHumain\modules\graph\controllers\actions\NarativeAction::class,
			'linear'   => \PixelHumain\PixelHumain\modules\graph\controllers\actions\LinearAction::class,
			'lisearch' => \PixelHumain\PixelHumain\modules\graph\controllers\actions\LisearchAction::class,
			'dash'     => \PixelHumain\PixelHumain\modules\graph\controllers\actions\DashAction::class,
			'chart'    => \PixelHumain\PixelHumain\modules\graph\controllers\actions\ChartAction::class,
			'circle'     => \PixelHumain\PixelHumain\modules\graph\controllers\actions\CircleAction::class,
			'organization'     => \PixelHumain\PixelHumain\modules\graph\controllers\actions\OrganizationAction::class,
		);
	}
}
