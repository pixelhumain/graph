<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers;

use CommunecterController;
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CteController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
            'all' => \PixelHumain\PixelHumain\modules\graph\controllers\actions\ctenat\AllAction::class,
            'badge' => \PixelHumain\PixelHumain\modules\graph\controllers\actions\ctenat\BadgeAction::class,
            'finder'   => \PixelHumain\PixelHumain\modules\graph\controllers\actions\FinderAction::class,
	    );
	}
}
