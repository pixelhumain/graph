<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers;

use CommunecterController;

/**
 * CoController.php
 *
 * Cocontroller always works with the PH base
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class ApiController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
            'children' => \PixelHumain\PixelHumain\modules\graph\controllers\actions\ChildrenAction::class,
            
	    );
	}
}
