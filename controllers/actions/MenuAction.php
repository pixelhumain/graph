<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers\actions;

use CAction, OpenData, Yii, Rest;
use PixelHumain\PixelHumain\components\ThemeHelper;

class MenuAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type=null,$view=null)
    {
        $controller=$this->getController();
        $menu = OpenData::$categ;

        $root = array( "id" => "root", "group" => 0,  "label" => "Home", "level" => 0 );
        $data = array($root);
        $links = array();
        $tags = array();
        $strength = 0.08;

        foreach ($menu as $key => $value)
        {
            array_push($data, array( "id" => "lvl1".$key, "group" => 1,  "label" => $value["name"], "level" => 2,"type"=>"lvl1" ));
            array_push($links, array( "target" => "lvl1".$key, "source" => "root",  "strength" => $strength ) );
                
    		foreach ($value["tags"] as $ix => $tag) 
            {
                array_push($data, array( "id" => "lvl1".$key."lvl2".$ix, "group" => 1,  "label" => $tag, "level" => 2,"type"=>"tag" ));
                array_push($links, array( "target" => "lvl1".$key."lvl2".$ix, "source" => "lvl1".$key,  "strength" => $strength ) );
    		}
    	}
        

        $params = array( 
            'data' => $data, 
            'links' => $links,
            'item' => $menu,
            'tags' => $tags,
            "typeMap" => $type,
            "title" => "Menu : ".$type
            );

        ThemeHelper::setWebsiteTheme(ThemeHelper::EMPTY);
        Yii::app()->session["theme"] = "empty";

        if($view)
            return Rest::json($data);
        else{
            if(Yii::app()->request->isAjaxRequest)
                return $controller->renderPartial('d3', $params);
            else{
                ThemeHelper::setWebsiteTheme(ThemeHelper::EMPTY);
               return $controller->render('d3', $params);
            }
        }
    }
}
