<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers\actions;
use CAction;
use MongoId;
use Person;
use PHDB;
use Rest;
use Yii;

class GetDataAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $type=null)
    {
    	if(!@$id && isset(Yii::app()->session["userId"])){
    		$id = Yii::app()->session["userId"];
    		$type = Person::COLLECTION;
    	}
        $item = PHDB::findOne( $type ,array("_id"=>new MongoId($id)));
  		return Rest::json($item);
    }
}