<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers\actions;
use CAction;
use Event;
use Organization;
use Person;
use PixelHumain\PixelHumain\components\ThemeHelper;
use Project;
use Rest;
use SearchNew;
use Yii;

class SearchAction extends \PixelHumain\PixelHumain\components\Action
{
    const COLORS = [
        Person::COLLECTION => "#FFC600",
        Organization::COLLECTION => "#93C020",
        Project::COLLECTION => "purple",
        Event::COLLECTION => "#FFA200",
        'middle' => "#c62f80",
        'root' => "black",
        'tag' => 'steelblue',
        'default' => '#cccccc'
    ];
    public function run($q = null, $tag = null, $type = null, $view = null, $geo = null)
    {

        if (!@$id && isset(Yii::app()->session["userId"])) {
            $id = Yii::app()->session["userId"];
            $type = Person::COLLECTION;
        }
        $controller = $this->getController();

        $links = array();
        $tags = array();
        $strength = 0.085;
        $hasOrga = false;
        $hasKnows = false;
        $hasEvents = false;
        $hasProjects = false;
        $hasMembersO = false;
        $hasMembersP = false;

        $searchCrit = array(
            "searchType" => array(Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION, Person::COLLECTION),
            "countType" => array(Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION, Person::COLLECTION),
            "indexMin" => 0,
            "indexStep" => 300,
            "count" => true
        );
        $crit = "";
        $icon = "";
        $link = "";
        if (@$tag) {
            $searchCrit["searchTags"] = array($tag);
            $crit = "TAG : " . $tag;
            $icon = "<i class='fa fa-tag'></i> ";
            $link = "";
        } else if (@$q) {
            $searchCrit["name"] = $q;
            $crit = "SEARCH : '" . $q . "'";
            $icon = "<i class='fa fa-search'></i> ";
            $link = "";
        } else if (@$geo) {
            $searchCrit["locality"] = array(array("type" => "country", "countryCode" => $geo));
            $crit = "GEO : " . $geo . "";
            $icon = "<i class='fa fa-map-marker'></i> ";
            $link = "";
        } else if (@$type) {
            $searchCrit["searchType"] = array($type);
            $crit = "TYPE : " . $type;
        }


        $root = array("id" => "search", "group" => 0,  "label" => $crit, "level" => 0, 'type' => 'root');
        $data = array($root);

        /*if(@$geo){
            $persons = PHDB::find(Person::COLLECTION,$searchGeo);
            $orgas = PHDB::find(Organization::COLLECTION,$searchGeo);
            $events = PHDB::find(Event::COLLECTION,$searchGeo);
            $projects = PHDB::find(Project::COLLECTION,$searchGeo);
            $list = array( "results" => array_merge($persons,$orgas,$events,$projects));
            var_dump($list);
        } else
            */
        $list = SearchNew::globalAutoComplete($searchCrit);
        if (isset($list) && @$list["results"]) {
            foreach ($list["results"] as $key => $value) {
                $types = array(
                    Organization::COLLECTION,
                    Organization::TYPE_BUSINESS,
                    Organization::TYPE_NGO,
                    Organization::TYPE_GROUP,
                    Organization::TYPE_GOV,
                    Organization::TYPE_COOP,
                    Project::COLLECTION,
                    Event::COLLECTION,
                    Person::COLLECTION
                );

                if (@$value['collection'] && in_array($value['collection'], $types)) {

                    if (in_array($value['collection'], array(Organization::COLLECTION, Organization::TYPE_BUSINESS, Organization::TYPE_NGO, Organization::TYPE_GROUP, Organization::TYPE_GOV, Organization::TYPE_COOP))) {
                        if (!$hasOrga) {
                            array_push($data, array("id" => "orgas", "group" => 1, "type" => "middle",  "label" => "ORGANIZATIONS", "level" => 1));
                            array_push($links, array("target" => "orgas", "source" => "search",  "strength" => $strength));
                            $hasOrga = true;
                        }
                        array_push($data, array("id" => $key, "group" => 2,  "label" => @$value["name"], "level" => 2, "type" => Organization::COLLECTION, "tags" => @$value["tags"], "linkSize" => count(@$value["links"], COUNT_RECURSIVE)));
                        array_push($links, array("target" => $key, "source" => "orgas",  "strength" => $strength));
                    } else if ($value['collection'] == Person::COLLECTION) {
                        if (!$hasKnows) {
                            array_push($data, array("id" => "people", "group" => 1, "type" => "middle",  "label" => "PEOPLE", "level" => 1));
                            array_push($links, array("target" => "people", "source" => "search",  "strength" => $strength));
                            $hasKnows = true;
                        }
                        array_push($data, array("id" => $key, "group" => 1,  "label" => @$value["name"], "level" => 2, "tags" => @$value["tags"], "type" => Person::COLLECTION, "linkSize" => count(@$value["links"], COUNT_RECURSIVE)));
                        array_push($links, array("target" => $key, "source" => "people",  "strength" => $strength, "tags" => @$value["tags"]));
                    } else if ($value['collection'] == Event::COLLECTION) {
                        if (!$hasEvents) {
                            array_push($data, array("id" => "events", "group" => 1, "type" => "middle",  "label" => "EVENTS", "level" => 1));
                            array_push($links, array("target" => "events", "source" => "search",  "strength" => $strength));
                            $hasEvents = true;
                        }
                        array_push($data, array("id" => $key, "group" => 4,  "label" => @$value["name"], "level" => 2, "type" => Event::COLLECTION, "tags" => @$value["tags"], "linkSize" => count(@$value["links"], COUNT_RECURSIVE)));
                        array_push($links, array("target" => $key, "source" => "events",  "strength" => $strength));
                    } else if ($value['collection'] == Project::COLLECTION) {
                        if (!$hasProjects) {
                            array_push($data, array("id" => "projects", "group" => 1, "type" => "middle",  "label" => "PROJECTS", "level" => 1));
                            array_push($links, array("target" => "projects", "source" => "search",  "strength" => $strength));
                            $hasProjects = true;
                        }
                        array_push($data, array("id" => $key, "group" => 3,  "label" => @$value["name"], "level" => 2, "type" => Project::COLLECTION, "tags" => @$value["tags"], "linkSize" => count(@$value["links"], COUNT_RECURSIVE)));
                        array_push($links, array("target" => $key, "source" => "projects",  "strength" => $strength));
                    }


                    if (@$value["tags"]) {
                        foreach (@$value["tags"] as $ix => $tag) {
                            if (!in_array($tag, $tags))
                                $tags[] = $tag;
                        }
                    }
                }
            }
        }
        if (isset($list["count"])) {
            $countRes = 0;
            foreach ($list["count"] as $v) {
                $countRes += $v;
            }
        } else
            $countRes = count($data);
        $crit .= " (" . $countRes . ")";
        $params = array(
            'data' => $data,
            'links' => $links,
            'list' => $list,
            'tags' => $tags,
            "title" => $icon . $crit,
            "colors" => self::COLORS
        );

        if ($view) {
            //$data["crit"] = $searchCrit;
            return Rest::json($data);
        } else {
            if (Yii::app()->request->isAjaxRequest)
                return $controller->renderPartial('d3', $params);
            else {
                ThemeHelper::setWebsiteTheme(ThemeHelper::EMPTY);
               return $controller->render('d3', $params);
            }
        }
    }
}
