<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers\actions;

use CAction, Person, Yii, Organization, Project, Event, PHDB, MongoId, Rest, Link;
class OrganizationAction extends \PixelHumain\PixelHumain\components\Action
{

    public function __construct($controller, $id)
    {
        parent::__construct($controller, $id);
        $this->images = [
            Person::class => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-citoyens.png",
            Organization::class => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-organizations.png",
            Project::class => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-projects.png",
            Event::class => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-events.png",
            Person::COLLECTION => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-citoyens.png",
            Organization::COLLECTION => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-organizations.png",
            Project::COLLECTION => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-projects.png",
            Event::COLLECTION => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-events.png",
        ];
    }

    private $images;

    const LABELS = [
        Link::organization2personOrga => 'ORGANIZER',
        Link::project2person => 'CONTRIBUTORS people',
        Link::person2organization => 'MEMBER OF',
        Link::person2person => 'FRIENDS',
        Link::person2events => 'EVENTS',
        Link::event2person => 'ATTENDEES',
        Link::person2projects => 'PROJECTS',
        Link::organization2person => 'MEMBERS people',
        Person::COLLECTION => 'PERSONS',
        Project::COLLECTION => 'PROJETS',
        Organization::COLLECTION => 'ORGANIZATIONS',
        Event::COLLECTION => 'EVENTS',
    ];

    const ICONS = [
        Link::organization2personOrga => 'f007', //User
        Link::event2person => 'f133', //calendar
        Link::project2person => 'f0eb', //lightbulb
        Link::person2organization => 'f0c0', // users
        Link::person2person => 'f007', //user
        Link::person2events => 'f133', //calendar
        Link::person2projects => 'f0eb', //lightbulb
        Link::organization2person => 'f007', //user
        "TAGS" => 'f02c', //tags
        "tags" => 'f02c', //tags
        "TAG" => 'f02b', //tags
        "tag" => 'f02b', //tags
        "HOME" => 'f015',
    ];
    const SIZE = [
        "default" => 100,
        "tag" => 50
    ];
    const COLORS = [
        Person::COLLECTION => "#FFC600",
        Organization::COLLECTION => "#93C020",
        Project::COLLECTION => "purple",
        Event::COLLECTION => "#FFA200",
        'middle' => "#c62f80",
        'root' => "black",
        'tag' => 'steelblue',
        'tags' => 'steelblue',
        'default' => '#cccccc'
    ];


    public function runCircle($id = null, $view = null, $slug = null, $ctx = null, $primary = null, $secondary = 'true', $type = null)
    {

        $this->getController()->layout = "//layouts/empty";
        $item = PHDB::findOne(Organization::COLLECTION, ['_id' => new MongoId($id)]);
        $itemType = Organization::COLLECTION;
        $root = ['id' => (string) $item['_id'], 'label' => $item['name'], 'icon' => self::ICONS["HOME"], 'type' => $itemType];
        $data = [];
        $byGroup = [];
        $tags = [];
        $secondaryTags = [];
        if (isset($item["tags"])) {
            foreach ($item["tags"] as $key => $tag) {
                if (!array_key_exists($tag, $data)) {
                    $data[$tag] = [];
                    $tags[] = $tag;
                }
                $list = PHDB::find(Organization::COLLECTION, ['tags' => $tag], ['name' => 1, 'profilMediumImageUrl' => 1, 'description' => 1,  'tags' => 1]);
                $byGroup = array_merge($byGroup, $list);
                foreach ($list as $idOrga => $obj) {
                    $curr_data = ['id' => (string) $idOrga, 'label' => @$obj['name'], 'size' => self::SIZE["default"], 'type' => Organization::COLLECTION, 'group' => $tag, 'description' => @$obj["description"]];
                    if (isset($obj["profilMediumImageUrl"]) && $obj["profilMediumImageUrl"] != "") {
                        $curr_data['img'] = ($obj["profilMediumImageUrl"]);
                    }
                    if (isset($obj["tags"])) {
                        $secondaryTags = array_merge($secondaryTags, $obj['tags']);
                    }
                    array_push($data[$tag], $curr_data);
                }
                $secondaryTags = array_unique($secondaryTags);
                $secondaryTags = array_diff($secondaryTags, $item["tags"]);
            }
            $secondaryTags = array_combine(range(0, count($secondaryTags) - 1), $secondaryTags);
            if (!$primary && $secondary && $secondaryTags) {
                $secondaryData = [];
                $list = PHDB::find(Organization::COLLECTION, ['tags' => ['$in' => $secondaryTags]], ['name' => 1, 'description' => 1, 'profilMediumImageUrl' => 1, 'tags' => ['$elemMatch' => ['$in' => $secondaryTags]]]);

                $byGroup = array_merge($byGroup, PHDB::find(Organization::COLLECTION, ['tags' => ['$in' => $secondaryTags]], ['name' => 1, 'description' => 1, 'profilMediumImageUrl' => 1, 'tags' => ['$elemMatch' => ['$in' => array_merge($secondaryTags, $item["tags"])]]]));
                foreach ($list as $idOrga => $obj) {
                    foreach ($obj['tags'] as $tag) {
                        if (!array_key_exists($tag, $secondaryData)) {
                            $secondaryData[$tag] = [];
                        }
                        $curr_data = ['id' => (string) $idOrga, 'label' => @$obj['name'], 'size' => self::SIZE["default"], 'type' => Organization::COLLECTION, 'group' => $tag, 'description' => @$obj["description"]];
                        if (isset($obj["profilMediumImageUrl"]) && $obj["profilMediumImageUrl"] != "") {
                            $curr_data['img'] = ($obj["profilMediumImageUrl"]);
                            $labels[$curr_data['label']] = mb_strtoupper($curr_data['label']);
                        }
                        array_push($secondaryData[$tag], $curr_data);
                    }
                }


                if ((string) $secondary === (string) intval($secondary)) {
                    arsort($secondaryData);
                    $secondaryData = array_slice($secondaryData, 0, intval($secondary));
                }
                $tags = array_merge($tags, $secondaryTags);
                $data = array_merge($data, $secondaryData);
            }
        }
        $res = [];
        $labels = array_merge(['Autres' => 'AUTRES'], self::LABELS);
        foreach ($data as $key => $d) {
            $res = array_merge($res, $d);
            $labels[$key] = mb_strtoupper($key);
        }
        $has = [];
        $resGroup = [];
        foreach ($byGroup as $idOrga => $obj) {
            if (!in_array($idOrga, $has)) {
                $curr_data = ['id' => (string) $idOrga, 'label' => @$obj['name'], 'size' => self::SIZE["default"], 'type' => Organization::COLLECTION, 'set' => $obj['tags'], 'description' => @$obj["description"]];
                array_push($resGroup, $curr_data);
                array_push($has, (string) $idOrga);
            }
        }
        $params = [
            'byGroup' => $resGroup,
            'defaultImage' => $this->images[Organization::class],
            'colors' => self::COLORS,
            'icons' => self::ICONS,
            'labels' => $labels,
            'data' => $res,
            'root' => $root,
            'item' => $item,
            'tags' => array_unique($tags),
            'type' => Organization::COLLECTION,
            'title' => Organization::COLLECTION . ' : ' . $item['name'],
            'isAjax' => Yii::app()->request->isAjaxRequest,
        ];
        $controller = $this->getController();
        if ($view) {
            return Rest::json($params);
        } else {
            if ($type == 'venn') {
                if (Yii::app()->request->isAjaxRequest) {
                    return $controller->renderPartial('venn-organisation', $params);
                } else {
                   return $controller->render('venn-organisation', $params);
                }
            } else {
                if (Yii::app()->request->isAjaxRequest) {
                    return $controller->renderPartial('circle-organisation', $params);
                } else {
                   return $controller->render('circle-organisation', $params);
                }
            }
        }
    }


    public function runRelation($id = null, $view = null, $slug = null, $ctx = null, $primary = null, $secondary = 'true', $type = null)
    {

        $this->getController()->layout = "//layouts/empty";
        $item = PHDB::findOne(Organization::COLLECTION, ['_id' => new MongoId($id)]);
        $itemType = Organization::COLLECTION;
        $root = ['id' => (string) $item['_id'], 'label' => $item['name'], 'icon' => self::ICONS["HOME"], 'type' => $itemType];
        $data = [];
        $tags = [];
        $secondaryTags = [];
        $added = [$item['_id']];

        if (isset($item["tags"])) {
            foreach ($item["tags"] as $key => $tag) {
                $list = PHDB::find(Organization::COLLECTION, ['tags' => $tag], ['name' => 1, 'profilMediumImageUrl' => 1, 'description' => 1,  'tags' => 1]);
                foreach ($list as $idOrga => $obj) {
                    if (in_array($idOrga, $added)) {
                        continue;
                    } else {

                        $curr_data = [
                            'id' => (string) $idOrga, 'label' => @$obj['name'], 'size' => self::SIZE["default"], 'type' => Organization::COLLECTION, 'description' => @$obj["description"],
                            'set' => $obj['tags']
                        ];
                        if (isset($obj["profilMediumImageUrl"]) && $obj["profilMediumImageUrl"] != "") {
                            $curr_data['img'] = ($obj["profilMediumImageUrl"]);
                        }
                        if (isset($obj["tags"])) {
                            $secondaryTags = array_merge($secondaryTags, $obj['tags']);
                        }
                        array_push($data, $curr_data);
                        $added[] = $idOrga;
                    }
                }
                $secondaryTags = array_unique($secondaryTags);
                $secondaryTags = array_diff($secondaryTags, $item["tags"]);
            }
            $secondaryTags = array_combine(range(0, count($secondaryTags) - 1), $secondaryTags);
            $tags = array_merge($item["tags"], $tags);
            $tags = array_merge($secondaryTags, $tags);
            if (!$primary && $secondary && $secondaryTags) {
                $secondaryData = [];
                $list = PHDB::find(Organization::COLLECTION, ['tags' => ['$in' => $secondaryTags]], ['name' => 1, 'description' => 1, 'profilMediumImageUrl' => 1, 'tags' => ['$elemMatch' => ['$in' => array_merge($secondaryTags, $item["tags"])]]]);
                foreach ($list as $idOrga => $obj) {
                    if (in_array($idOrga, $added)) {
                        continue;
                    } else {
                        $curr_data = ['id' => (string) $idOrga, 'label' => @$obj['name'], 'size' => self::SIZE["default"], 'type' => Organization::COLLECTION, 'description' => @$obj["description"], 'set' => $obj['tags']];
                        if (isset($obj["profilMediumImageUrl"]) && $obj["profilMediumImageUrl"] != "") {
                            $curr_data['img'] = ($obj["profilMediumImageUrl"]);
                        }
                        array_push($secondaryData, $curr_data);
                        $labels[$curr_data['label']] = mb_strtoupper($curr_data['label']);
                        $tags = array_merge($obj['tags'], $tags);
                        $added[] = $idOrga;
                    }
                }

                if ((string) $secondary === (string) intval($secondary)) {
                    arsort($secondaryData);
                    $secondaryData = array_slice($secondaryData, 0, intval($secondary));
                }
                $tags = array_merge($tags, $secondaryTags);
                $data = array_merge($data, $secondaryData);
            }
        }
        $labels = array_merge(['Autres' => 'AUTRES'], self::LABELS);

        $tags = array_unique($tags);
        $tags = array_combine(range(0, count($tags) - 1), $tags);

        $params = [
            'data' => $data,
            'defaultImage' => $this->images[Organization::class],
            'colors' => self::COLORS,
            'icons' => self::ICONS,
            'labels' => $labels,
            'root' => $root,
            'item' => $item,
            'tags' => $tags,
            'type' => Organization::COLLECTION,
            'title' => Organization::COLLECTION . ' : ' . $item['name'],
            'isAjax' => Yii::app()->request->isAjaxRequest,
        ];
        $controller = $this->getController();
        if ($view) {
            return Rest::json($params);
        } else {
            if (Yii::app()->request->isAjaxRequest) {
                return $controller->renderPartial('relation-organisation', $params);
            } else {
               return $controller->render('relation-organisation', $params);
            }
        }
    }
    public function run($id = null, $view = null, $slug = null, $ctx = null, $primary = null, $secondary = 'true', $type = null)
    {
        if ($type == 'relation') {
            $this->runRelation($id, $view, $slug, $ctx, $primary, $secondary, $type);
        } else {
            $this->runCircle($id, $view, $slug, $ctx, $primary, $secondary, $type);
        }
    }
}
