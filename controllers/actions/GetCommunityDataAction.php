<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers\actions;

use Form;
use MongoId;
use Organization;
use Person;
use PHDB;
use Project;
use Rest;

class GetCommunityDataAction extends \PixelHumain\PixelHumain\components\Action
{
    private $typeAndClass = [
        "person" => Person::COLLECTION,
        "organizations" => Organization::COLLECTION
    ];
    public function run(){
        if(isset($_POST["id"]) && isset($_POST["type"])){
            if($_POST["type"] == "forms"){
                $answers = PHDB::find(Form::ANSWER_COLLECTION, ['form' => $_POST["id"], 'draft' => ['$exists' => false], "answers.aapStep1.titre" => ['$exists' => true]]);
                $data = array_map(function($answer){
                    return ["id" => (string) $answer["_id"], "name" => $answer["answers"]['aapStep1']["titre"], "group" => "answers"];
                } ,array_values($answers));
                return Rest::json(array("children" => $data));
            }else if($_POST["type"] == "answers"){
                $data = [];
                $answers = PHDB::findOneById(Form::ANSWER_COLLECTION, $_POST["id"], ["project", "answers", "links"]);
                if(isset($answers["project"]['id'])){
                    $data["projects"] = ["id" => "projects".$_POST["id"], "name" => "Projets", "group" => "projects".$_POST["id"]];
                    $projects = PHDB::findOneById(Project::COLLECTION, $answers["project"]['id'], ["name", "profilThumbImageUrl", "links"]);
                    $data["projects"]["children"][] = ["id" => (string) $projects["_id"], "name" => $projects["name"], "group" => "projects".$_POST["id"], "profil" => @$projects["profilThumbImageUrl"]];
                    if(isset($projects["links"]["contributors"])){
                        $data["projects"][0]["size"] = count($projects["links"]["contributors"]);
                    }
                }
                if(isset($answers["answers"]["aapStep1"]["depense"])){
                    $data["financement"] = ["id" => "financement".$_POST["id"], "name" => "Financements", "group" => "financement".$_POST["id"]];
                    foreach ($answers["answers"]["aapStep1"]["depense"] as $key => $value){
                       if($value["poste"]){
                            $depense = ["id" => "financement".$_POST["id"].$key, "name" => $value["poste"], "group" => "financement".$_POST["id"]];
                            /*if(isset($value["financer"]) && count($value["financer"]) > 0){

                            }*/
                            $data["financement"]["children"][] = $depense;
                        }
                    }
                }
                return Rest::json(["children" => array_values($data)]);
            }else if($_POST["type"] == "projects"){
                $id = $_POST["id"];
                $projects = PHDB::findOneById(Project::COLLECTION, $id, ["name", "profilThumbImageUrl", "links"]);
                $data = [
                    "children" => [
                        "contributors" => ["id" => "contributors".$_POST["id"], "name" => "Contributeurs", "group" => "contributors".$_POST["id"]]
                    ]
                ];
                foreach ($projects["links"] as $key => $value){
                    if($key == "contributors"){
                        $organizations = PHDB::findByIds(Organization::COLLECTION, array_keys($value), ["name", "profilThumbImageUrl"]);
                         $children = array_map(function($element){
                            return ["id" => (string) $element["_id"], "name" => $element["name"], "group" => "contributors".$_POST["id"], "type" => "organizations", "profil" => @$element["profilThumbImageUrl"]];
                        }, array_values($organizations));
                        $citoyens = PHDB::findByIds(Person::COLLECTION, array_keys($value), ["name", "profilThumbImageUrl"]);
                        $childrenCitoyens = array_map(function($element){
                            return ["id" => (string) $element["_id"], "name" => $element["name"], "group" => "contributors".$_POST["id"],"type" => "citoyens", "profil" => @$element["profilThumbImageUrl"]];
                        }, array_values($citoyens));
                        $data["children"]["contributors"]["children"] = array_merge($children, $childrenCitoyens);
                    }
                }
                return Rest::json(["children" => array_values($data["children"])]);
            }
        }
    }
}