<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers\actions;

use CAction, Yii;
class ChartAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        //$g == pie , bar, 
        if(Yii::app()->request->isAjaxRequest)
            return $this->getController()->renderPartial( $_POST["g"] , $_POST);
        else {
    		$this->getController()->layout = "//layouts/empty";
    		return $this->getController()->render( $_POST["g"] ,$_POST);
        }
    }
}