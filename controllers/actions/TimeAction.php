<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers\actions;

use CAction;
class TimeAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $type=null,$data=null)
    {
    	
        //ex :http://127.0.0.1/ph/graph/co/time   	
    	if( @$data )
    		return $this->getController()->renderPartial('timeData');
    	else {
    		$this->getController()->layout = "//layouts/empty";
    		return $this->getController()->render('time');
    	}
    }
}