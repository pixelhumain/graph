<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers\actions;
use CAction;
use Yii;
use function ctype_digit;

class DashAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($g, $type=null, $id=null, $data=null,$size="S", $path=null, $title=null)
    {
        $controller = $this->getController();
        //$g == pie , bar
        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial( $g , ["type"=>$type,"size"=>$size,"id"=>$id, "title"=>$title, "path"=>$path], true);
        else {
    		$this->getController()->layout = "//layouts/empty";
    		return $controller->render( $g ,["type"=>$type,"size"=>$size,"id"=>$id, "title"=>$title, "path"=>$path]);
        }
    }
}
