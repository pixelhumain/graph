<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers\actions;

use Answer;
use CAction, Yii, Person, Slug, PHDB, Organization, Event, Project, SearchNew, Rest, MongoId;
use Form;
use PhpParser\Node\Expr\Cast\String_;

class ChildrenAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run ($id=null, $type=null,$slug=null, $tag=null, $coformid=null, $answerid=null, $ocecoformid=null, $parentformoceco=null, $answerocecoid=null) {

        if(!@$id && isset(Yii::app()->session["userId"])){
            $id = Yii::app()->session["userId"];
            $type = Person::COLLECTION;
        }
        $controller=$this->getController();
        
        $cfollower = array();
        $people = array();
        $corg = array();
        $cproj = array();
        $cevents = array();
        $ctags = array();
        $cpropo = array();
        $cacti = array();
        $clife = array();
        $cform = array();
        $ocecoform = array();
        $cformanswer = array();

        if(isset($slug))
        {
            $el = Slug::getElementBySlug($slug);
            $item = $el["el"];
            $itemType = $el["type"];
        }
        if(isset($tag) && $tag != "") {
            $searchCrit = array(
                "searchType"=> array(Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION, Person::COLLECTION),
                "indexMin"=>0,
                "indexStep"=>300
            );
            $searchCrit["searchTags"]= array($tag);
            $list = SearchNew::globalAutoComplete( $searchCrit );
            if(isset($list) && @$list["results"]) {
                foreach ($list["results"] as $key => $value){
                    $types = array( 
                        Organization::COLLECTION, 
                        Organization::TYPE_BUSINESS , 
                        Organization::TYPE_NGO, 
                        Organization::TYPE_GROUP, 
                        Organization::TYPE_GOV, 
                        Organization::TYPE_COOP,
                        Project::COLLECTION, 
                        Event::COLLECTION, 
                        Person::COLLECTION);
    
                    if( @$value['collection'] && in_array( $value['collection'], $types) ) {    
    
                        if(  in_array($value['collection'], array( Organization::COLLECTION,Organization::TYPE_BUSINESS , Organization::TYPE_NGO, Organization::TYPE_GROUP, Organization::TYPE_GOV, Organization::TYPE_COOP))  ){
                            array_push($corg, array( "id" => $key,  "label" => @$value["name"] , "slug" => @$value["slug"], "open"=>"column", "icon"=>"fa-circle-o", "type"=>Organization::COLLECTION ) );
    
                        }
    
                        if ($value['collection'] == Event::COLLECTION ){
                            array_push($cevents, array( "id" => $key,  "label" => @$value["name"], "slug" => @$value["slug"], "open"=>"column", "icon"=>"fa-calendar", "type"=>Event::COLLECTION) );
                        }
               
                        if ($value['collection'] == Project::COLLECTION ){
                            array_push($cproj, array( "id" => $key,  "label" => @$value["name"], "slug" => @$value["slug"], "open"=>"column", "icon"=>"fa-lightbulb-o", "type"=>Project::COLLECTION ));
                        }

                        if ($value['collection'] == Person::COLLECTION ){
                            array_push($people, array( "id" => $key,  "label" => @$value["name"], "slug" => @$value["slug"], "open"=>"column", "icon"=>"fa-user", "type"=>Person::COLLECTION ));
                        }
                    }
                }
            }

            $root = array();

            if(count($people) > 0){
                $root[] = array( "id" => "citoyen",  "label" => "COMMUNAUTÉ (".count($people).")" ,"icon"=>"fa-group", "children" =>$people);
            }

            if(count($corg) > 0){
                $root[] = array("id" => "organization", "label" => "ORGANISATIONS (".count($corg).")" , "icon"=>"fa-circle-o", "children" =>$corg);
            }
            
            if(count($cevents)> 0){
                $root[] = array( "id" => "event", "label" => "EVENNEMENTS (".count($cevents).")" , "icon"=>"fa-calendar", "children" =>$cevents);
            }
            
            if(count($cproj)> 0){
                $root[] = array( "id" => "project",  "label" => "PROJETS (".count($cproj).")" ,"icon"=>"fa-lightbulb-o", "children" =>$cproj);
            }
                
            return Rest::json($root);
        }
        if(isset($answerid)) {
            $elements = PHDB::findOneById(Answer::COLLECTION, $answerid);
            // foreach ($elements as $i => $c) {
                    
            //         if(isset($c[array_keys($c)[0]])){
            //             $u = PHDB::findOneById(Person::COLLECTION, $c["user"], array("id", "name", "username", "slug"));
            //             $cformanswer[] = array("label"=>$u['name'],"open"=>"column-new", "id"=>$i,"icon"=>"fa-wpforms", "type"=>Answer::COLLECTION);
            //         }
            //         // if(isset($c["tags"]))
            //         //     $elemTags = array_merge($elemTags,$c["tags"]);
                
            // }
            $root = array();
            if(isset($elements['user'])){
                // $u = PHDB::findOneById(Person::COLLECTION, $$elements["user"], array("id", "name", "username", "slug"));
                $root[] = array( "id" => (string)$elements['_id'],  "label" => "Reponse", 'open' => "column-new", "icon"=>"fa-wpforms" , "type" =>Answer::COLLECTION);
            }

            return Rest::json($root);
        }
        if(isset($coformid)) {
            $elements = PHDB::findOneById(Answer::COLLECTION, $answerid);
            $root = array();
            $root[] = array( "id" => $coformid,  "label" => "Liste des réponses", 'open' => "column-answers", "icon"=>"fa-wpforms" , "type" =>Form::COLLECTION);

            return Rest::json($root);
        }

        if(isset($ocecoformid) && isset($parentformoceco)) {
            $root = array();
            // $oceco = PHDB::findOneById(Form::COLLECTION, $ocecoformid, array("name","slug", "type", "parent"));
            // return Rest::json($oceco);
            $root[] = array("label"=> "Liste projets", "open"=>"column-ocecoform-answers", "id"=>$ocecoformid, "icon"=>"fa-wpforms", "type"=>Form::COLLECTION, "parent_form" => $parentformoceco);
            return Rest::json($root);
        }

        if(isset($answerocecoid)) {
            $root[] = array("label"=> "Detail", "open"=>"column-oceco-answer", "id"=>$answerocecoid, "icon"=>"fa-wpforms", "type"=>Form::ANSWER_COLLECTION);
            return Rest::json($root);
        }
            
        $tags = array();
        $elemTags = array();
        if(isset($item["tags"]))
            $elemTags = array_merge($elemTags,$item["tags"]);

        $link = "#page.type.".$itemType.".id.".$id;

        if(isset($item) && isset($item["links"]) ){
            

             if(isset($item["links"]["members"]) && $item["links"]["members"] && is_array($item["links"]["members"]))
            {
                $elements = PHDB::findByIds( Person::COLLECTION, array_keys($item["links"]["members"]), array("name","username", "slug") );
                foreach ($elements as $i => $c) {
                    
                        if(isset($c["name"]))
                            $people[] = array("label"=>$c["name"],"open"=>"column", "id"=>$c["_id"],"icon"=>"fa-user", "type"=>Person::COLLECTION,"slug"=>@$c["slug"]);
                         if(isset($c["tags"]))
                            $elemTags = array_merge($elemTags,$c["tags"]);
                    
                }

            }
            if(isset($item["links"]["contributors"]) && $item["links"]["contributors"] && is_array($item["links"]["contributors"]))
            {
                $elements = PHDB::findByIds( Person::COLLECTION, array_keys($item["links"]["contributors"]), array("name","username", "slug") );
                foreach ($elements as $i => $c) {
                    
                        if(isset($c["name"]))
                            $people[] = array("label"=>$c["name"],"open"=>"column", "id"=>$c["_id"],"icon"=>"fa-user", "type"=>Person::COLLECTION,"slug"=>@$c["slug"]);
                         if(isset($c["tags"]))
                            $elemTags = array_merge($elemTags,$c["tags"]);
                    
                }

            }

            if(isset($item["links"]["memberOf"]) && $item["links"]["memberOf"] && is_array($item["links"]["memberOf"]))
            {
                $elements = PHDB::findByIds( Organization::COLLECTION, array_keys($item["links"]["memberOf"]), array("name","slug") );
                //var_dump($elements);exit;
                foreach ($elements as $i => $c) {
                    
                        if(isset($c["name"]))
                            $corg[] = array("label"=>$c["name"],"open"=>"column", "id"=>$c["_id"],"icon"=>"fa-circle-o", "type"=>Organization::COLLECTION,"slug"=>@$c["slug"]);
                         if(isset($c["tags"]))
                            $elemTags = array_merge($elemTags,$c["tags"]);
                    
                }

            }

            if(isset($item["links"]["events"]) && $item["links"]["events"] && is_array($item["links"]["events"]))
            {
                $elements = PHDB::findByIds( Event::COLLECTION, array_keys($item["links"]["events"]), array("name","slug","tags") );
                //var_dump($elements);exit;
                foreach ($elements as $i => $c) {
                        if(isset($c["name"]))
                            $cevents[] = array("label"=>$c["name"],"open"=>"column", "id"=>$i,"icon"=>"fa-calendar", "type"=>Event::COLLECTION,"slug"=>@$c["slug"]);
                        if(isset($c["tags"]))
                            $elemTags = array_merge($elemTags,$c["tags"]);
                    
                }

            }

            if(isset($item["links"]["projects"]) && $item["links"]["projects"] && is_array($item["links"]["projects"]))
            {
                $elements = PHDB::findByIds( Project::COLLECTION, array_keys($item["links"]["projects"]), array("name","slug","tags") );
                //var_dump($elements);exit;
                foreach ($elements as $i => $c) {
                    
                        if(isset($c["name"]))
                            $cproj[] = array("label"=>$c["name"],"open"=>"column", "id"=>$i,"icon"=>"fa-lightbulb-o", "type"=>Project::COLLECTION,"slug"=>@$c["slug"]);
                        if(isset($c["tags"]))
                            $elemTags = array_merge($elemTags,$c["tags"]);
                    
                }

            }
            if(isset($item["links"]["followers"]) && $item["links"]["followers"] && is_array($item["links"]["followers"]))
            {
                $elements = PHDB::findByIds( Person::COLLECTION, array_keys($item["links"]["followers"]), array("name","slug") );
                //var_dump($elements);exit;
                foreach ($elements as $i => $c) {
                    
                        if(isset($c["name"]))
                            $cfollower[] = array("label"=>$c["name"],"open"=>"column", "id"=>$i,"icon"=>"fa-user", "type"=>Person::COLLECTION,"slug"=>@$c["slug"]);
                        if(isset($c["tags"]))
                            $elemTags = array_merge($elemTags,$c["tags"]);
                    
                }

            }
            if($itemType == Organization::COLLECTION || $itemType == Project::COLLECTION) {
                // $elements = PHDB::find( Form::COLLECTION, array('parent.'. (string)$item['_id'] => array('$exists' => true)), array("name","slug", "type", "parent") );
                // foreach ($elements as $i => $c) {
                        
                //         if(isset($c["name"])) {
                //             if(isset($c["type"])) {
                //                 if($c["type"] == "aap") {
                //                     $ocecoAnswers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => (string)$i, "answers.aapStep1.titre" => ['$exists' => true]), array("user", "answers", "form", "context"));
                //                     $allAnswersOceco = array();
                //                     foreach ($ocecoAnswers as $key => $value) {
                //                         if($value['answers'])
                //                             $allAnswersOceco[] = array("label" => $value['answers']["aapStep1"]['titre'], "id" => $key, "icon" => "fa-wpforms", "type" => Form::ANSWER_COLLECTION, "form" => $value['form'], "context" => $value['context'], "open" => "column-oceco-answer");
                //                     }
                //                     if(count($allAnswersOceco) > 0) {
                //                         $ocecoform[] = array("label"=>$c["name"].' ('.count($allAnswersOceco).')',"children"=>$allAnswersOceco, "id"=>$i,"icon"=>"fa-wpforms", "type"=>Form::COLLECTION, "parent_form" => array_keys($c['parent'])[0]);
                //                     }
                //                 }
                //             } else 
                //                 $cform[] = array("label"=>$c["name"],"open"=>"column-answers", "id"=>$i,"icon"=>"fa-wpforms", "type"=>Form::COLLECTION);
                //         }
                //         if(isset($c["tags"]))
                //             $elemTags = array_merge($elemTags,$c["tags"]);
                    
                // }
            }
                    
                    
            
            if( count($elemTags) )
            {
                foreach (@$elemTags as $ix => $tag) {
                    if(!in_array($tag, $tags)){
                        $ctags[]= array( "label" => $tag,"icon"=>"fa-tag", "open" => "finder-tag");
                    }
                }
            }
            
            
            /////////////////////FOREACH EXPERIENCES
            if(isset($item["curiculum"]["lifepath"])){
                foreach ( $item["curiculum"]["lifepath"] as $kle => $valere) {


                         //var_dump($item["curiculum"]["lifepath"]);
                         $clife[] = array( "label" => $valere["title"], "parent" => $life);
                        //var_dump($valere["title"]);
                    
                }
            }
            
            ////////FOREACH SONDAGES
            // $proposals = PHDB::find( $sondType );
            // if(isset($proposals)){
            //     $proposals = PHDB::findByIds( Proposal::COLLECTION, array_keys($proposals), array("name") );
            //     foreach ($proposals as $key => $obj) {
            //         $cpropo[] =  array("id" => (string)@$obj["_id"], "label" => @$obj["title"]);
            //     }
            // }


            ///////////FOREACH ACTIONS
            // $actions = PHDB::find( $actionType );
            // if(isset($actions)){   
            //     $actions = PHDB::findByIds( Actions::COLLECTION, array_keys($actions), array("name") );
            //     foreach ($actions as $key => $value) {
            //         $cacti[]= array("id" => (string)@$obj["_id"], "label" => @$obj["name"], "parent" => $acti,"tags" => @$obj["tags"]);
            //     }
            // }


            ///////Nodes pour rajouter PROJECTS
            // $cproj[]=array( "id" => "addproj", "label" => "AJOUTER...","parent" => $proj);
            // $corg[]=array( "id" => "addorg", "label" => "AJOUTER...","parent" => $org);
            // $cevents[]=array( "id" => "addevents", "label" => "AJOUTER...","parent" => $events);
            
            //$clife[]=array( "id" => "addlife", "label" => "AJOUTER..." , "parent" => $life);
            //$cpropo[]=array( "id" => "addpropo", "label" => "AJOUTER...", "parent" => $propo);
            //$cacti[]=array( "id" => "addaction", "label" => "AJOUTER...", "parent" => $acti);

            
            /////////REQUETE
            $root = array(
                //array( "id" => (string)$item["_id"], "label" => $item["name"], "parent" => null)
            );
            $apId = "";
            if($item["_id"]) $apId = $item["_id"];
            // $apropo[]= array("label"=>"Afficher detail","open"=>"column", "id"=>$apId,"icon"=>"fa-info", "type"=>$itemType,"slug"=>$item["slug"]);
            // $root[] = array("id" => "about", "label" => "À PROPOS (".count($apropo).")","icon"=>"fa-info" , "children" =>$apropo);
            $root[] = array("id" => $apId, "label" => "À PROPOS","icon"=>"fa-info" , "open"=>"column", "type"=>$itemType,"slug"=>$item["slug"]);
            //$root["children"] = array();
                if(count($people) ){
                    $root[] = array("id" => "citoyen", "label" => "COMMUNAUTÉ (".count($people).")","icon"=>"fa-group" , "children" =>$people);
                }

                if(count($corg) ){
                    $root[] = array("id" => "organization", "label" => "ORGANISATIONS (".count($corg).")","icon"=>"fa-circle-o" , "children" =>$corg);
                }
                
                if(count($cevents)){
                    $root[] = array( "id" => "event", "label" => "ÉVENNEMENTS (".count($cevents).")","icon"=>"fa-calendar" , "children" =>$cevents);
                }
                
                if(count($cproj)){
                    $root[] = array( "id" => "project",  "label" => "PROJETS (".count($cproj).")","icon"=>"fa-lightbulb-o", "children" =>$cproj);
                    // $root[] = array( "id" => "plus", "parent" => $cproj);
                    
                }
                
                if(count($ctags)){
                     $root[] = array( "id" => "tags",  "label" => "TAGS (".count($ctags).")","icon"=>"fa-tags" ,  "children" =>$ctags, "data" => $item, "type" => $itemType, array('parent.'. (string)$item['_id'] => array('$exists' => true)));
                    }

                 if(count($cfollower)){
                     $root[] = array( "id" => "follower",  "label" => "SUIVEURS (".count($cfollower).")","icon"=>"fa-link" , "children" =>$cfollower);
                    }


                if(count($clife)){
                     $root[] = array( "id" => "lifepath",  "label" => "EXPERIENCE (".count($clife).")","icon"=>"fa-heart" , "children" =>$clife);
                    }

                if(count($cpropo)){
                     $root[] = array( "id" => "proposal",  "label" => "SONDAGES (".count($cpropo).")","icon"=>"fa-gavel" , "children" =>$cpropo);
                 }

                if(count($cacti)){
                     $root[] = array( "id" => "actions",  "label" => "ACTIONS (".count($cacti).")","icon"=>"fa-cogs" , "children" =>$cacti);
                 }
                if(count($cform)){
                    $root[] = array( "id" => "forms",  "label" => "COFORM (".count($cform).")","icon"=>"fa-wpforms" , "children" =>$cform);
                }
                if(count($ocecoform)){
                    $root[] = array( "id" => "forms",  "label" => "OCECOFORM (".count($ocecoform).")","icon"=>"fa-wpforms" , "children" =>$ocecoform);
                }

            return Rest::json($root);
        }
    }
}
