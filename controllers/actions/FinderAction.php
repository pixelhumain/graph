<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers\actions;

use CAction, Yii;
class FinderAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run( $slug=null, $open=null, $tag=null )
    {
        $ctrl=$this->getController();
        $params = array( 'action' => ($open) ? $open : "all",
                         "slug"   => $slug,
                         "tag" => str_replace("#", " ", $tag)
                         );
       
        if(Yii::app()->request->isAjaxRequest)
            return $ctrl->renderPartial('graph.views.co.finder', $params);
        else {
            $ctrl->layout = "//layouts/empty";
            return $ctrl->render('graph.views.co.finder', $params);
        }
    }
}
