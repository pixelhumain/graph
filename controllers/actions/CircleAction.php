<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers\actions;

use CAction, Person, Yii, Organization, Project, Event, PHDB, Rest, SearchNew, Slug, MongoId, Link;
use PixelHumain\PixelHumain\components\ThemeHelper;

class CircleAction extends \PixelHumain\PixelHumain\components\Action
{

    public function __construct($controller, $id)
    {
        parent::__construct($controller, $id);
        $this->images = [
            Person::class => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-citoyens.png",
            Organization::class => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-organizations.png",
            Project::class => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-projects.png",
            Event::class => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-events.png",
            Person::COLLECTION => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-citoyens.png",
            Organization::COLLECTION => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-organizations.png",
            Project::COLLECTION => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-projects.png",
            Event::COLLECTION => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-events.png",
        ];
    }

    private $images;

    const LABELS = [
        Link::organization2personOrga => 'ORGANIZER',
        Link::project2person => 'CONTRIBUTORS people',
        Link::person2organization => 'MEMBER OF',
        Link::person2person => 'FRIENDS',
        Link::person2events => 'EVENTS',
        Link::event2person => 'ATTENDEES',
        Link::person2projects => 'PROJECTS',
        Link::organization2person => 'MEMBERS people',
        Person::COLLECTION => 'PERSONS',
        Project::COLLECTION => 'PROJETS',
        Organization::COLLECTION => 'ORGANIZATIONS',
        Event::COLLECTION => 'EVENTS',
    ];

    const ICONS = [
        Link::organization2personOrga => 'f007', //User
        Link::event2person => 'f133', //calendar
        Link::project2person => 'f0eb', //lightbulb
        Link::person2organization => 'f0c0', // users
        Link::person2person => 'f007', //user
        Link::person2events => 'f133', //calendar
        Link::person2projects => 'f0eb', //lightbulb
        Link::organization2person => 'f007', //user
        "TAGS" => 'f02c', //tags
        "tags" => 'f02c', //tags
        "TAG" => 'f02b', //tags
        "tag" => 'f02b', //tags
        "HOME" => 'f015',
    ];
    const SIZE = [
        "default" => 100,
        "tag" => 50
    ];
    const COLORS = [
        Person::COLLECTION => "#FFC600",
        Organization::COLLECTION => "#93C020",
        Project::COLLECTION => "purple",
        Event::COLLECTION => "#FFA200",
        'middle' => "#c62f80",
        'root' => "black",
        'tag' => 'steelblue',
        'tags' => 'steelblue',
        'default' => '#cccccc'
    ];

    public function run($id = null, $type = null, $view = null, $slug = null, $by = 'tags', $ctx = null, $primary = null, $secondary = 5)
    {
        ThemeHelper::setWebsiteTheme( ThemeHelper::EMPTY);
        Yii::app()->layout = 'empty';
        Yii::app()->session['theme'] = 'empty';


        $this->setDefaultTypeIfTypeIsNull($id, $type);


        if ($type != 'tags' && $type != 'organizations') {
            $url = "/graph/co/d3";
            if (isset($id)) {
                $url .= "/id/" . $id;
            }
            if (isset($type)) {
                $url .= "/type/" . $type;
            }
            $this->getController()->redirect($url);
        } else if ($type == 'organizations') {
            if ($by == 'tags') {
                $item  = $this->getRootItemAndClass($id, $slug, $type);
                $itemType = $item[1]::COLLECTION;
                $item = $item[0];
                $root = ['id' => (string) $item['_id'], 'label' => $item['name'], 'icon' => self::ICONS["HOME"], 'type' => $itemType];
                $data = [];
                $tags = [];
                $ids = [];
                $secondaryTags = [];
                if (isset($item["tags"])) {
                    foreach ($item["tags"] as $key => $tag) {
                        if (!array_key_exists($tag, $data)) {
                            $data[$tag] = [];
                            $tags[] = $tag;
                        }
                        $list = PHDB::find(Organization::COLLECTION, ['tags' => $tag], ['name' => 1, 'profilMediumImageUrl' => 1, 'tags' => 1]);
                        foreach ($list as $idOrga => $obj) {
                            $curr_data = ['id' => (string) $idOrga, 'label' => @$obj['name'], 'size' => self::SIZE["default"], 'type' => Organization::COLLECTION, 'group' => $tag, 'img' => (isset($obj["profilMediumImageUrl"]) && $obj["profilMediumImageUrl"] != ""  ? $obj["profilMediumImageUrl"] : $this->images[Organization::class])];
                            if (isset($obj["tags"])) {
                                $secondaryTags = array_merge($secondaryTags, $obj['tags']);
                            }
                            array_push($data[$tag], $curr_data);
                        }
                        $secondaryTags = array_unique($secondaryTags);
                        $secondaryTags = array_diff($secondaryTags, $item["tags"]);
                    }
                    $secondaryTags = array_combine(range(0, count($secondaryTags) - 1), $secondaryTags);
                    if (!$primary && $secondary && $secondaryTags) {

                        $secondaryData = [];
                        $list = PHDB::find(Organization::COLLECTION, ['tags' => ['$in' => $secondaryTags]], ['name' => 1, 'profilMediumImageUrl' => 1, 'tags' => ['$elemMatch' => ['$in' => $secondaryTags]]]);

                        foreach ($list as $idOrga => $obj) {
                            foreach ($obj['tags'] as $tag) {
                                if (!array_key_exists($tag, $secondaryData)) {
                                    $secondaryData[$tag] = [];
                                }
                                $curr_data = ['id' => (string) $idOrga, 'label' => @$obj['name'], 'size' => self::SIZE["default"], 'type' => Organization::COLLECTION, 'group' => $tag, 'img' => (isset($obj["profilMediumImageUrl"]) && $obj["profilMediumImageUrl"] != ""  ? $obj["profilMediumImageUrl"] : $this->images[Organization::class])];
                                array_push($secondaryData[$tag], $curr_data);
                            }
                        }

                        if ((string) $secondary == (string) intval($secondary)) {
                            arsort($secondaryData);
                            $secondaryData = array_slice($secondaryData, 0, intval($secondary));
                        }
                        $tags = array_merge($tags, $secondaryTags);
                        $data = array_merge($data, $secondaryData);
                    }
                }
                $res = [];
                $labels = array_merge(['Autres' => 'AUTRES'], self::LABELS);
                $beg = microtime(true);
                foreach ($data as $key => $d) {
                    $res = array_merge($res, $d);
                    $labels[$key] = mb_strtoupper($key);
                }
                $end = microtime(true);
                var_dump($end - $beg);
                $params = [
                    // 'raw' => $data,
                    'colors' => self::COLORS,
                    'icons' => self::ICONS,
                    'labels' => $labels,
                    'data' => $res,
                    'root' => $root,
                    'item' => $item,
                    'tags' => array_unique($tags),
                    'type' => $type,
                    'title' => $type . ' : ' . $item['name'],
                    'isAjax' => Yii::app()->request->isAjaxRequest,
                ];
            } else if ($by == 'roles') {
                $item  = $this->getRootItemAndClass($id, $slug, $type);
                $itemType = $item[1]::COLLECTION;
                $item = $item[0];
                $root = ['id' => (string) $item['_id'], 'label' => $item['name'], 'icon' => self::ICONS["HOME"], 'type' => $itemType];
                $data = [];
                $tags = [];
                $has = [];
                $linkDisabled = [ "subEvent", "members"];
                if (isset($item, $item['links'])) {
                    foreach ($item['links'] as $linkType => $value) {
                        foreach ($value as $k => $v) {
                            $obj = null;
                            switch ($linkType) {
                                case Link::person2projects:
                                    $obj = $this->pushNodeByRoles($data,  $k, $v, $has,   $linkType,   Project::class);
                                    break;
                                case Link::person2events:
                                    $obj = $this->pushNodeByRoles($data,  $k, $v, $has,   $linkType,  Event::class);
                                    break;
                                case Link::event2person:
                                case Link::person2person:
                                    $obj = $this->pushNodeByRoles($data,  $k, $v, $has,   $linkType,   Person::class);
                                    break;
                                case Link::person2organization:
                                    if (isset($v['type'])) {
                                        if (0 == strcmp($v['type'], Organization::COLLECTION)) {
                                            $obj = $this->pushNodeByRoles($data,  $k, $v, $has,   Link::person2organization,   Organization::class);
                                        }
                                    }
                                    break;
                                case Link::organization2personOrga:
                                    if (0 == strcmp("projects", $v["type"])) {
                                        $obj =   $this->pushNodeByRoles($data,  $k, $v, $has,   $linkType,   Project::class);
                                    } else {
                                        $obj =  $this->pushNodeByRoles($data,  $k, $v, $has,   $linkType,   Organization::class);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                $link = '#page.type.' . $itemType . '.id.' . (string) $item['_id'];
                arsort($data);
                $res = [];
                $labels = array_merge(['Autres' => 'AUTRES'], self::LABELS);
                foreach ($data as $key => $d) {
                    $res = array_merge($res, $d);
                    $labels[$key] = mb_strtoupper($key);
                }

                $params = [
                    'colors' => self::COLORS,
                    'icons' => self::ICONS,
                    'labels' => $labels,
                    'root' => $root,
                    'data' => $res,
                    'item' => $item,
                    'tags' => $tags,
                    'type' => $type,
                    'title' => $type . ' : ' . $item['name'],
                    'link' => $link,
                    'isAjax' => Yii::app()->request->isAjaxRequest,
                ];
            }
            $controller = $this->getController();
            if ($view) {
                return Rest::json($params);
            } else {
                if (Yii::app()->request->isAjaxRequest) {
                    return $controller->renderPartial('circle-organisation', $params);
                } else {
                    ThemeHelper::setWebsiteTheme( ThemeHelper::EMPTY);
                   return $controller->render('circle-organisation', $params);
                }
            }
        } else if ($type == 'tags') {
            $root = ['id' => $id, 'label' => "#" . $id];
            $data = [];
            $tags = [];
            $searchCrit = array(
                "searchType" => array(Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION, Person::COLLECTION),
                "countType" => array(Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION, Person::COLLECTION),
                "indexMin" => 0,
                "indexStep" => 300,
                "count" => true,
                "searchTags" => array($id)
            );
            $list = SearchNew::globalAutoComplete($searchCrit)["results"];
            $labels = array_merge(['Autres' => 'Autres'], self::LABELS);
            foreach ($list as $key => $value) {
                array_push($data, ['id' => $key, 'group' => $value["collection"], "label" => @$value["name"], 'img' => (isset($value["profilThumbImageUrl"]) && $value["profilThumbImageUrl"] != ""  ? $value["profilThumbImageUrl"] : $this->images[$value["collection"]]), 'size' => 100, 'type' => $value["collection"], 'value' => $value]);
            }
            $params = [
                'colors' => self::COLORS,
                'icons' => self::ICONS,
                'labels' => $labels,
                'root' => $root,
                'data' => $data,
                'type' => $type,
                'tags' => $tags,
                'isAjax' => Yii::app()->request->isAjaxRequest,
            ];
            $controller = $this->getController();
            if ($view) {
                return Rest::json($params);
            } else {
                if (Yii::app()->request->isAjaxRequest) {
                    return $controller->renderPartial('circle', $params);
                } else {
                    ThemeHelper::setWebsiteTheme(ThemeHelper::EMPTY);
                   return $controller->render('circle', $params);
                }
            }
        }
    }

    private function setDefaultTypeIfTypeIsNull(&$id = null, &$type)
    {
        if (!@$id && isset(Yii::app()->session['userId'])) {
            $id = Yii::app()->session['userId'];
            $type = Person::COLLECTION;
        }
    }

    private function getRootItemAndClass($id, $slug, $type)
    {
        if (isset($slug)) {
            $el = Slug::getElementBySlug($slug);
            $item = $el['el'];
            $clazz = $el['type'];
        } else {
            $clazz = Person::class;
            if ('organizations' == $type) {
                $clazz = Organization::class;
            } elseif ('events' == $type) {
                $clazz = Event::class;
            } elseif ('projects' == $type) {
                $clazz = Project::class;
            }

            $item = PHDB::findOne($clazz::COLLECTION, ['_id' => new MongoId($id)]);
        }

        return array($item, $clazz);
    }

    private function pushNodeByRoles(&$data, $k, $v, &$has, $linkType,  $clazz)
    {
        $obj = $clazz::getById($k);
        if (!@$obj['_id'] || !@$obj['name']) {
            return;
        }
        $id = uniqid($obj['_id'], true);
        if (isset($v['roles'])) {
            foreach ($v['roles'] as $tag) {
                if (!array_key_exists($tag, $data)) {
                    $data[$tag] = [];
                }
                if (array_key_exists($tag, $data)) {
                    $curr_data = ['id' => (string) $id, 'label' => @$obj['name'], 'size' => self::SIZE["default"], 'type' => $clazz::COLLECTION, 'linkType' => $linkType, 'group' => $tag, 'tags' => @$obj['tags'], 'img' => (isset($obj["profilThumbImageUrl"]) && $obj["profilThumbImageUrl"] != ""  ? $obj["profilThumbImageUrl"] : $this->images[$clazz])];
                    $data[$tag][] = $curr_data;
                }
            }
        }
        return $obj;
    }
}
