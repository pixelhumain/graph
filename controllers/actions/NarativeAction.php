<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers\actions;

use CAction;
class NarativeAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $type=null,$data=null)
    {
    	
    	
    	if( @$data )
    		return $this->getController()->renderPartial('narativeData');
    	else {
    		$this->getController()->layout = "//layouts/empty";
    		return $this->getController()->render('narative');
    	}
    }
}