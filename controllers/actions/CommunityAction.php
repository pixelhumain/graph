<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers\actions;

use Element;
use Form;
use Organization;
use Person;
use PHDB;
use Project;
use Rest;

class CommunityAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id, $type, $slug=null){
        $preview = isset($_POST["preview"]) ? true : false;
        $elt = Element::getByTypeAndId($type, $id, ["name", "profilThumbImageUrl", "links", "collection"]);
        $data = ["id" => (string)$elt["_id"], "name" => $elt["name"], "profil" => @$elt["profilThumbImageUrl"], "group" => "root"];
        $forms = PHDB::find(Form::COLLECTION, array("type" => "aap", "parent.$id" => array('$exists' => true)), array("name"));
        if($forms && count($forms) > 0){
            $data["children"]["forms"] = ["id" => "forms", "name" => "Formulaire AAP", "bread" => "Formulaire AAP","group" => "forms", "_children" => array_map(function($element){
                return ["id" => (string) $element["_id"], "name" => $element["name"], "bread" => $element["name"], "group" => "forms"];
            }, array_values($forms))];
        }
        foreach ($elt["links"] as $key => $value){
            if($key == "projects"){
                $projects = PHDB::findByIds(Project::COLLECTION, array_keys($value), ["name", "profilThumbImageUrl", "links"]);
                $data["children"]["projects"] = ["id" => "projects", "name" => "Projets", "bread" => "Projets", "group" => "projects", "_children" => array_map(function($element){
                    $data = ["id" => (string) $element["_id"], "name" => $element["name"], "bread" => $element["name"], "group" => "projects", "profil" => @$element["profilThumbImageUrl"]];
                    if(isset($element["links"]["contributors"])){
                        $data["size"] = count($element["links"]["contributors"]);
                    }
                    return $data;
                }, array_values($projects))];
            }else if($key === "members"){
                $organizations = PHDB::findByIds(Organization::COLLECTION, array_keys($value), ["name", "profilThumbImageUrl"]);
                $data["children"]["organizations"] = array_merge(["id" => "organizations", "name" => "Organisations", "bread" => "Organisations", "group" => "organizations", "_children" => array_map(function($element){
                        return ["id" => (string) $element["_id"], "name" => $element["name"], "bread" => $element["name"], "group" => "organizations", "profil" => @$element["profilThumbImageUrl"]];
                    }, array_values($organizations))], ($data["children"]["organizations"] ?? [] ));
                $citoyens = PHDB::findByIds(Person::COLLECTION, array_keys($value), ["name", "profilThumbImageUrl"]);
                $data["children"]["citoyens"] = ["id" => "citoyens", "name" => "Personnes", "bread" => "Personnes", "group" => "citoyens", "_children" => array_map(function($element){
                    return ["id" => (string) $element["_id"], "name" => $element["name"], "bread" => $element["name"], "group" => "citoyens", "profil" => @$element["profilThumbImageUrl"]];
                }, array_values($citoyens))];
            }else if($key === "memberOf"){
                $organizations = PHDB::findByIds(Organization::COLLECTION, array_keys($value), ["name", "profilThumbImageUrl"]);
                $data["children"]["organizations"] = array_merge(["id" => "organizations", "name" => "Organisations","bread" => "Organisations", "group" => "organizations", "_children" => array_map(function($element){
                    return ["id" => (string) $element["_id"], "name" => $element["name"],"bread" => $element["name"], "group" => "organizations", "profil" => @$element["profilThumbImageUrl"]];
                }, array_values($organizations))], ($data["children"]["organizations"] ?? [] ));
            }else if($key == "contributors"){
                $data["children"]["contributors"] = ["id" => "contributors".$id, "name" => "Contributeurs", "group" => "contributors".$id,"type" => "contributors"];
                $organizations = PHDB::findByIds(Organization::COLLECTION, array_keys($value), ["name", "profilThumbImageUrl"]);
                 $children = array_map(function($element) use ($id){
                    return ["id" => (string) $element["_id"], "name" => $element["name"], "group" => "contributors".$id, "type" => "organizations", "profil" => @$element["profilThumbImageUrl"]];
                }, array_values($organizations));
                $citoyens = PHDB::findByIds(Person::COLLECTION, array_keys($value), ["name", "profilThumbImageUrl"]);
                $childrenCitoyens = array_map(function($element) use ($id){
                    return ["id" => (string) $element["_id"], "name" => $element["name"], "group" => "contributors".$id,"type" => "citoyens", "profil" => @$element["profilThumbImageUrl"]];
                }, array_values($citoyens));
                $data["children"]["contributors"]["children"] = array_merge($children, $childrenCitoyens);
            }
        }
        $data["children"] = array_values($data["children"]);
        $controller = $this->getController();
        return $controller->renderPartial('community', ["element" => $data, "title" => $elt, "preview" => $preview]);
    }
}