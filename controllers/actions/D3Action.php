<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers\actions;

use CAction, Person, Yii, Organization, Project, Event, Rest, Slug, PHDB, MongoId, Link;
use PixelHumain\PixelHumain\components\ThemeHelper;
use function PHPSTORM_META\type;

class D3Action extends \PixelHumain\PixelHumain\components\Action
{

    public function __construct($controller, $id)
    {
        parent::__construct($controller, $id);
        $this->images = [
            Person::class => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-citoyens.png",
            Organization::class => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-organizations.png",
            Project::class => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-projects.png",
            Event::class => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/thumbnail-events.png",
        ];
    }

    private $images;

    public function run($id = null, $type = null, $view = null, $slug = null, $ctx = null)
    {
        $this->setDefaultTypeIfTypeIsNull($id, $type);
        $item  = $this->getRootItemAndClass($id, $slug, $type);
        $itemType = $item[1]::COLLECTION;
        $item = $item[0];
        $root = ['id' => (string) $item['_id'], 'group' => 'root', 'type' => 'root', 'label' => $item['name'], 'level' => 0, 'tags' => @$item['tags']];

        $data = [$root];
        $links = [];
        $tags = [];
        $strength = 0.095;
        $middleStrength = 0.095;

        $has = [];


        //Ajout des tags du root
        if (@$item['tags']) {
            foreach (@$item['tags'] as $ix => $tag) {
                if (!in_array($tag, $tags)) {
                    $tags[] = $tag;
                    if (!isset($has['tags'])) {
                        array_push($links, ['target' => 'tags', 'source' => $root['id'],  'strength' => $middleStrength]);
                        $has['tags'] = true;
                    }
                    array_push($data, ['id' => 'tag' . (count($tags)), 'group' => 'TAGS',  'label' => $tag, 'level' => 2, 'type' => 'tag']);
                    array_push($links, ['target' => 'tag' . (count($tags)), 'source' => 'tags',  'strength' => $strength]);
                }
            }
        }
        $linkDisabled = [
            "subEvent",
            "members",
        ];
        $linkEnabled = [
            Link::person2projects,
            Link::person2events,
            Link::event2person,
            Link::person2person,
            Link::person2organization,
            Link::organization2personOrga,
        ];
        if (isset($item, $item['links'])) {
            foreach ($item['links'] as $linkType => $value) {
                foreach ($value as $k => $v) {
                    $obj = null;

                    switch ($linkType) {


                        case Link::person2projects:
                            $obj = $this->pushNodeAndLink($data, $links, $k, $linkType, $strength, 3, 2, Project::class);
                            break;
                        case Link::person2events:
                        // case Link::event2subevent:
                            $obj = $this->pushNodeAndLink($data, $links, $k, $linkType, $strength, 4, 2, Event::class);
                            break;
                        case Link::event2person:
                        case Link::person2person:
                            $obj = $this->pushNodeAndLink($data, $links, $k, $linkType, $strength, 1, 2, Person::class);
                            // $obj = $this->pushNodeAndLink($data, $links, $k, Link::organization2person, $strength, 1, 2, Person::class);
                            break;
                        case Link::person2organization:
                            if (isset($v['type'])) {
                                if (0 == strcmp($v['type'], Organization::COLLECTION)) {
                                    $obj = $this->pushNodeAndLink($data, $links, $k, Link::person2organization, $strength, 2, 2, Organization::class);
                                }
                            }
                            break;
                        case Link::organization2personOrga:
                            if (0 == strcmp("projects", $v["type"])) {
                                $obj =   $this->pushNodeAndLink($data, $links, $k, $linkType, $strength, 3, 2, Project::class);
                            } else {
                                $obj =  $this->pushNodeAndLink($data, $links, $k, $linkType, $strength, 2, 2, Organization::class);
                            }
                            break;
                        default:
                            break;
                    }
                    $this->pushTags($data, $links, $has, $tags, $root, $middleStrength, $obj);
                }
            }
        }
        $link = '#page.type.' . $itemType . '.id.' . (string) $item['_id'];
        $params = [
            'data' => $data,
            'links' => $links,
            'item' => $item,
            'tags' => $tags,
            'type' => $type,
            'title' => $type . ' : ' . $item['name'],
            'link' => $link,
            'isAjax' => Yii::app()->request->isAjaxRequest,
        ];

        ThemeHelper::setWebsiteTheme(ThemeHelper::EMPTY);
        Yii::app()->layout = 'empty';
        Yii::app()->session['theme'] = 'empty';

        $controller = $this->getController();
        if ($view) {
            return Rest::json($params);
        } else {
            if (Yii::app()->request->isAjaxRequest) {
                return $controller->renderPartial('d3', $params);
            } else {
                ThemeHelper::setWebsiteTheme(ThemeHelper::EMPTY);
               return $controller->render('d3', $params);
            }
        }
    }

    private function setDefaultTypeIfTypeIsNull(&$id = null, &$type)
    {
        if (!@$id && isset(Yii::app()->session['userId'])) {
            $id = Yii::app()->session['userId'];
            $type = Person::COLLECTION;
        }
    }

    private function getRootItemAndClass($id, $slug, $type)
    {
        if (isset($slug)) {
            $el = Slug::getElementBySlug($slug);
            $item = $el['el'];
            $clazz = $el['type'];
        } else {
            $clazz = Person::class;
            if ('organizations' == $type) {
                $clazz = Organization::class;
            } elseif ('events' == $type) {
                $clazz = Event::class;
            } elseif ('projects' == $type) {
                $clazz = Project::class;
            }

            $item = PHDB::findOne($clazz::COLLECTION, ['_id' => new MongoId($id)]);
        }

        return array($item, $clazz);
    }

    private function pushNodeAndLink(&$data, &$links, $k, $linkType, $strength,  $group, $level, $clazz)
    {
        $obj = $clazz::getById($k);
        if (!@$obj['_id'] || !@$obj['name']) {
            return;
        }
        $id = $obj['_id'];

        $curr_data = ['id' => (string) $id, 'group' => $linkType,  'label' => @$obj['name'], 'level' => $level, 'type' => $clazz::COLLECTION, 'tags' => @$obj['tags'], 'linkSize' => count(@$obj['links'], COUNT_RECURSIVE), 'img' => (isset($obj["profilThumbImageUrl"]) && $obj["profilThumbImageUrl"] != ""  ? $obj["profilThumbImageUrl"] : $this->images[$clazz])];

        array_push($data, $curr_data);
        array_push($links, ['target' => (string) $id, 'source' => $linkType,  'strength' => $strength]);
        return $obj;
    }

    private function pushTags(&$data, &$links, &$has, &$tags, $root, $strength, $obj)
    {
        if (@$obj['tags']) {
            foreach (@$obj['tags'] as $ix => $tag) {
                if (!in_array($tag, $tags)) {
                    $tags[] = $tag;
                    if (!isset($has['tags'])) {
                        array_push($links, ['target' => 'tags', 'source' => $root['id'],  'strength' => $strength]);
                        $has['tags'] = true;
                    }
                    array_push($data, ['id' => 'tag' . (count($tags)), 'group' => 'TAGS',  'label' => $tag, 'level' => 2, 'type' => 'tag']);
                    array_push($links, ['target' => 'tag' . (count($tags)), 'source' => 'tags',  'strength' => $strength]);
                }
            }
        }
    }
}
