<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers\actions\ctenat;

use CAction, Yii, Person, Slug, Organization, Event, Project, PHDB, Rest, Link;
class AllAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $type=null,$slug=null)
    {
        if(!@$id && isset(Yii::app()->session["userId"])){
            $id = Yii::app()->session["userId"];
            $type = Person::COLLECTION;
        }
        $controller=$this->getController();

        if(isset($slug))
        {
            $el = Slug::getElementBySlug($slug);
            $item = $el["el"];
            $itemType = $el["type"];
        }
        else 
        {
            $itemType = Person::COLLECTION;
            if($type == "organization"){
                $itemType = Organization::COLLECTION;
            }else if($type == "event"){
                $itemType = Event::COLLECTION;
            }else if($type == "project"){
                $itemType = Project::COLLECTION;
            }
        }
        
        $root = array( 
                       "label" => $item["name"], 
                       "children" => array());

        
        $tags = array();

        $link = "#page.type.".$itemType.".id.".(string)$item["_id"];

        if(@$item["tags"])
            foreach (@$item["tags"] as $ix => $tag) {
                if( !in_array( $tag, $tags ) )
                    $tags[] = array("label"=>$tag);
            }

/*
element
    name
    organizations
        communauté
    projects
        communauté
    events
        communauté
    doc
      menu poi
    forum
      elements
    agenda
      events
    dashboards
      titre affichage
    tags
*/
        $data = array($root);
        if(isset($item))
        {
           
            $cterrs = PHDB::find( Project::COLLECTION , array("source.key" => "ctenat") );
            $cterrL = array();
            $cterrLC = array();
            $cterrLL = array();
            $cterrLS = array();
            foreach ($cterrs as $id => $c){
                $cterrChildren = array(
                    array("label" => $c["name"], "icon"=>"fa-map-marker")
                );

                if(isset($c["source"]["status"]["ctenat"]))
                    $cterrChildren[] = array("label" => @$c["source"]["status"]["ctenat"]);

                if(isset($c["links"]["contributors"]) && $c["links"]["contributors"] && is_array($c["links"]["contributors"])){
                    $linksEl = Link::groupFind( $c["links"]["contributors"] , array("name","slug"));
                    //var_dump($linksEl);exit;
                    $cLinks = array();
                    foreach ($linksEl as $type => $L) {
                        foreach ($L as $i => $el) {
                            if(isset($el["name"]))
                                $cLinks[] = array("label"=>$el["name"], "id"=>$i, "type"=>$type,"slug"=>$c["slug"],
                                    "children" => array(
                                        array("label" => "VOIR", "id"=>$i, "type"=>$type,"slug"=>$c["slug"], "color"=>"red", "icon"=>"fa-eye","url"=>"@".$c["slug"]),
                                        array("label" => "FINDER", "id"=>$i, "type"=>$type,"slug"=>$c["slug"], "color"=>"red", "icon"=>"fa-search","url"=>"@".$c["slug"])));
                        }
                    }
                    //var_dump($cLinks);exit;
                    $cterrChildren[] = array("label" => "COMMUNAUTÉ (".count( @$c["links"]["contributors"] ).")", "icon"=>"fa-group", "children"=>$cLinks );
                }
                
                if(isset($c["links"]["projects"]) && count($c["links"]["projects"]) && is_array($c["links"]["projects"])){
                    $linksEl = Link::groupFind( $c["links"]["projects"] , array("name","slug"));
                    //var_dump($linksEl);exit;
                    $cLinks = array();
                    foreach ($linksEl as $type => $L) {
                        foreach ($L as $i => $el) {
                            if(isset($el["name"]))
                                $cLinks[] = array("label"=>$el["name"], "id"=>$i, "type"=>$type,
                                    "children" => array(
                                        array("label" => "VOIR", "color"=>"red", "icon"=>"fa-eye","url"=>"@".$c["slug"]),
                                        array("label" => "FINDER", "color"=>"red", "icon"=>"fa-search","url"=>"@".$c["slug"])));
                        }
                    }
                    //var_dump($cLinks);exit;
                    $cterrChildren[] = array("label" => "ACTIONS (".count( @$c["links"]["projects"] ).")", "icon"=>"fa-lightbulb-o", "children"=>$cLinks );
                }
                
                if(isset($c["links"]["events"]) && $c["links"]["events"] && is_array($c["links"]["events"]))
                {
                    $linksEl = Link::groupFind( $c["links"]["events"] , array("name","slug"));
                    //var_dump($linksEl);exit;
                    $cLinks = array();
                    foreach ($linksEl as $type => $L) {
                        foreach ($L as $i => $el) {
                            if(isset($el["name"]))
                                $cLinks[] = array("label"=>$el["name"], "id"=>$i, "type"=>$type,
                                    "children" => array(
                                        array("label" => "VOIR", "color"=>"red", "icon"=>"fa-eye","url"=>"@".$c["slug"]),
                                        array("label" => "FINDER", "color"=>"red", "icon"=>"fa-search","url"=>"@".$c["slug"])));
                        }
                    }
                    //var_dump($cLinks);exit;
                    $cterrChildren[] = array("label" => "EVENNEMENTS (".count( @$c["links"]["events"] ).")", "icon"=>"fa-calendar", "children"=>$cLinks );
                }
                
                if(isset($c["tags"]))
                {
                    $ctags = array();
                    foreach ($c["tags"] as $i => $t) {
                        $ctags[] = array("label"=>$t);
                    }
                    $cterrChildren[] = array("label" => "TAGS TODO", "icon"=>"fa-tags", "children"=>$ctags);
                }
                $cterrChildren[] = array("label" => "VOIR", "color"=>"red", "icon"=>"fa-eye","url"=>"@".$c["slug"]);
                $cterrChildren[] = array("label" => "FINDER", "color"=>"red", "icon"=>"fa-search","url"=>"@".$c["slug"]);

                $cterrL[] = array ( "label" => $c["name"], "icon"=>"fa-map-marker",
                                    "status" => @$c["source"]["status"]["ctenat"], 
                                    "children" => $cterrChildren );

                if(isset($c["source"]["status"]["ctenat"]))
                {
                    if( $c["source"]["status"]["ctenat"] == "Territoire Candidat" )
                        $cterrLC[] = array ( "label" => $c["name"], "children" => $cterrChildren );
                    if( $c["source"]["status"]["ctenat"] == "Territoire lauréat" )
                        $cterrLL[] = array ( "label" => $c["name"], "children" => $cterrChildren );
                    if( $c["source"]["status"]["ctenat"] == "CTE Signé" )
                        $cterrLS[] = array ( "label" => $c["name"], "children" => $cterrChildren );
                }

            }

            //rajouté au 1er etage
            $data[] = array( "label" => "TERRITOIRES", "icon"=>"fa-map-marker", "children" => $cterrL ) ;
            $data[] = array( "label" => "CANDIDATS","icon"=>" fa-mortar-board", "children" => $cterrLC ) ;
            $data[] = array( "label" => "LAURÉATS","icon"=>"fa-trophy", "children" => $cterrLL ) ;
            $data[] = array( "label" => "SIGNÉS","icon"=>"fa-pencil-square-o", "children" => $cterrLS ) ;
            $data[] = array( "label" => "FICHES ACTIONS","icon"=>"fa-lightbulb-o", "children" => array() ) ;
            $data[] = array( "label" => "ACTIONS séléctionné","icon"=>"fa-thumbs-o-up", "children" => array() ) ;
            $data[] = array( "label" => "AGENDA","icon"=>"fa-calendar", "children" => array( array("label" => "ALL EVENTS")) );
            $data[] = array( "label" => "DASHBOARD","icon"=>"fa-tachometer", "children" => array( array("label" => "ALL GRAPHS")) );
            $data[] = array( "label" => "DOCUMENTATION","icon"=>"fa-book", "children" => array( array("label" => "ALL DOCS")) );
            $data[] = array( "label" => "FORUM","icon"=>"fa-comments", "children" => array( array("label" => "ALL FAQs")) );
            $data[] = array( "label" => "TAGS","icon"=>"fa-tags", "children" => $tags );
        }
        

        $params = array(
            'data'  => $data,
            'item'  => $item,
            'slug'  => $slug,
            'tags'  => $tags,
            "title" => $type." : ".$item["name"],
        );
        
        return Rest::json($data);
        
    }
}
