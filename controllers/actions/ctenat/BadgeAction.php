<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers\actions\ctenat;

use CAction, PHDB, Badge, Rest;
class BadgeAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run ($id=null, $type=null,$slug=null) {
        $controller=$this->getController();

        
        $bagdes = PHDB::find(Badge::COLLECTION, array("source.key" => "ctenat", "parent" => array('$exists' => false)));
        $rootA = array(
            //array( "id" => (string)$item["_id"], "label" => $item["name"], "parent" => null)
        );
        $rootB = array(
            //array( "id" => (string)$item["_id"], "label" => $item["name"], "parent" => null)
        );
        //$root["children"] = array();
        if(count($bagdes) ){
            foreach ($bagdes as $key => $value){
                $bChildren = PHDB::find(Badge::COLLECTION, array("source.key" => "ctenat", "parent.".$key => array('$exists' => true) ) );
                $child = array();

                if(count($bChildren) ){
                    foreach ($bChildren as $keyC => $valC) {
                        $child[] = array(   "label" => $valC["name"],
                                            "img" => ( !empty($valC["profilThumbImageUrl"]) ? $valC["profilThumbImageUrl"] : "" ),
                                            "icon"=>"fa-group" , 
                                            "children" => array() );
                    }
                }
                if($value["category"] == "domainAction"){
                    $rootA[] = array("label" => $value["name"],
                                "img" => ( !empty($value["profilThumbImageUrl"]) ? $value["profilThumbImageUrl"] : "" ),
                                "icon"=>"fa-group" , 
                                "children" =>$child);
                }else{
                    $rootB[] = array("label" => $value["name"],
                                "img" => ( !empty($value["profilThumbImageUrl"]) ? $value["profilThumbImageUrl"] : "" ),
                                "icon"=>"fa-group" , 
                                "children" =>$child);
                }
                
            }
        }
        
         $root[] = array("label" => "objectif",
                                "icon"=>"fa-group" , 
                                "children" =>$rootA);
         $root[] = array("label" => "cible",
                                "icon"=>"fa-group" , 
                                "children" =>$rootB);


        return Rest::json($root);
    }
}
