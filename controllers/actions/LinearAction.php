<?php

namespace PixelHumain\PixelHumain\modules\graph\controllers\actions;

use CAction, Yii, Person, Organization, Event, Project, PHDB, MongoId, Proposal, Actions, Rest;
use PixelHumain\PixelHumain\components\ThemeHelper;

class LinearAction extends \PixelHumain\PixelHumain\components\Action

{

    public function run ($id=null, $type=null,$view=null) {

        ThemeHelper::setWebsiteTheme(ThemeHelper::CO2);
        Yii::app()->session["theme"] = ThemeHelper::CO2;
        $ctrl = $this->getController();
        $ctrl->layout = "//layouts/empty";

        if( !@$id && isset(Yii::app()->session["userId"]) ){
                $id = Yii::app()->session["userId"];
                $type = Person::COLLECTION;
            }
            $controller=$this->getController();

            //$condAction = array("idUserAuthor" => $id);
            $itemType = Person::COLLECTION;
            if($type == "organization"){
                //$condAction = array("parentId" => $id, "parentType" => "organization");
                $itemType = Organization::COLLECTION;
            }else if($type == "event"){
                $itemType = Event::COLLECTION;
            }else if($type == "project"){
                $itemType = Project::COLLECTION;
            }

        ////Nom de chaque nodes principales
        $org = "ORGANIZATIONS";
        $proj= "PROJETS";
        $events = "EVENTS";
        $tage = "TAGS";
        $propo = "SONDAGES";
        $acti = "ACTIONS";
        $life = "EXPERIENCES";
        $follower = "FOLLOWERS";
        ///////followers
        $cfollower = array();
        $countid = array();
        
        //Tableaux dans lesquelson va ajouter les données
        $corg = array();
        $cproj = array();
        $cevents = array();
        $ctags = array();
        $cpropo = array();
        $cacti = array();
        $clife = array();
            
        $item = PHDB::findOne( $itemType , array("_id"=>new MongoId($id)) );
        
        $item ["type"] = $itemType;
        $item ["id"] = $id;
        //Récupère données collection
        $sondType = Proposal::COLLECTION;
        $actionType = Actions::COLLECTION;
        
        $sond = PHDB::find( $sondType );
        $action = PHDB::find( $actionType );

        // $sond = PHDB::find( Proposal::COLLECTION, $condAction );
        // $action = PHDB::find( Actions::COLLECTION, $condAction );

        // $root = array( "id" => (string)$item["_id"], "label" => $item["name"], "parent" => null , "children" => 
        //             array( array("id" => "orgas", "label" => $org, "parent" => $item["name"], "children" => []),
        //                         array( "id" => "projects", "label" => $proj, "parent" => $item["name"]),
        //                         array( "id" => "events",  "label" => $events, "parent" => $item["name"]),
        //                         array( "id" => "tags",  "label" => $tags, "parent" => $item["name"]))
        //             );
        // $data = array($root); //tableau "$data" qui récupère les données de root
        

        $links = array();
        $tags = array();
        


        $link = "#page.type.".$itemType.".id.".$id;

        if(@$item["tags"])
            foreach (@$item["tags"] as $ix => $tag) {if(!in_array($tag, $tags))$tags[] = $tag;}

        if(isset($item) && isset($item["links"]) /*isset($item["curiculum"]["lifepath"]) && isset($item["Katana"])*/ ){
            /* if(@$item["parentId"] || @$item["organizerId"]  ){
                if(@$item["organizerId"]){
                    $item["parentId"] = $item["organizerId"];
                    $item["parentType"] = $item["organizerType"];
                }
             
                array_push($data, array( "id" => "parent", "group" => 1,  "label" => "Parent", "level" => 0 ) );
                array_push($links, array( "target" => "parent", "source" => $root["id"],  "strength" => $strength ) );
                $parent = PHDB::findOne( $item["parentType"] , array("_id"=>new MongoId( $item["parentId"] )) );
                $grp = array_search($item["parentType"], array("citoyens", "organizations", "projects", "events"))+1;
                array_push($data, array( "id" => $item["parentId"], "group" => $grp,  "label" => @$parent["name"], "level" => 2,"type"=>$item["parentType"],"tags" => @$parent["tags"], "linkSize" => count(@$parent["links"], COUNT_RECURSIVE),"img"=>@$parent["profilThumbImageUrl"] ) );
                array_push($links, array( "target" => $item["parentId"], "source" => "parent",  "strength" => $strength  ) );
            }
            */

            //Parcours links
            foreach ($item["links"] as $key => $value){
                foreach ($value as $k => $v) {
                    // 
                    if(strcmp($key, "memberOf") == 0 || strcmp($key, "organizer") == 0){
                        $obj = Organization::getById($k);
                        if(!@$obj["_id"] || !@$obj["name"])continue;
                        
                          array_push($corg, array( "id" => (string)@$obj["_id"], "label" => @$obj["name"],"type"=>Organization::CONTROLLER, "parent" => $org,"tags" => @$obj["tags"]));  
                    }
                    //
                    else if (strcmp($key, "events") == 0){
                        $obj = Event::getById($k);
                        if(!@$obj["_id"] || !@$obj["name"])continue;
                       
                            array_push($cevents, array("id" => (string)@$obj["_id"], "label" => @$obj["name"],"type"=>Event::CONTROLLER, "parent" => $events,"tags" => @$obj["tags"] ));
                    }
                    //
                    else if (strcmp($key, "projects") == 0){
                        $obj = Project::getById($k);
                        if(!@$obj["_id"] || !@$obj["name"])continue;
                        
                            array_push($cproj, array( "id" => (string)@$obj["_id"], "label" => @$obj["name"],"type"=>Project::CONTROLLER, "parent" => $proj,"tags" => @$obj["tags"] )); 

                    }

                    else if(strcmp($key, "followers") == 0){
                            
                            $find = PHDB::find(Person::COLLECTION, array("_id"=>new MongoId((String)$k)));

                            array_push($countid, $k);
                            
                            array_push($cfollower, array( "id" => $k, "label" => $find[$k]["name"], "parent" => $follower));
                            //var_dump($k);
                             
                    }
                    
                    
                    //
                    if(@$obj["tags"]){
                        foreach (@$obj["tags"] as $ix => $tag) {
                            if(!in_array($tag, $tags)){
                                $tags[] = $tag;
      
                                array_push($ctags, array( "id" => "tag".(count($tags)),  "label" => $tag, "parent" => $tage));
                            }
                        }
                    }//fin if tag
                }//fin 2eme foreach

            }//fin 1er foreach

            
            /////////////////////FOREACH EXPERIENCES
            if(isset($item["curiculum"]["lifepath"])){
                foreach ( $item["curiculum"]["lifepath"] as $kle => $valere) {


                         //var_dump($item["curiculum"]["lifepath"]);
                         array_push($clife, array( "label" => $valere["title"], "parent" => $life));
                        //var_dump($valere["title"]);
                    
                }
            }
            
             
            //////////FOREACH DONNÉES CRÉER
             $cnom = array();
             if (isset($item["graph"]["linear"])){
                
                foreach ($item["graph"]["linear"]["label"] as $key => $value) {

                    $nom = $value["title"];
                    //var_dump($nom);
                }

                foreach ( $item["graph"]["linear"]["data"] as $key => $value) {

                        array_push($cnom, array( "label" => $value["name"], "parent" => $nom));
                           
                }
            }
            ////////FOREACH SONDAGES
            if(isset($sond)){
                foreach ($sond as $key => $value) {
                    //
                    $obj = Proposal::getById($key);
                    if(!@$obj["_id"] || !@$obj["title"])continue;
                    if( $id == @$obj["creator"]){
                        
                        array_push($cpropo, array("id" => (string)@$obj["_id"], "label" => @$obj["title"], "parent" => $propo));
                    }
                }
            }
            ///////////FOREACH ACTIONS
            if(isset($action)){   
                foreach ($action as $key => $value) {
                    //
                    $obj = Actions::getById($key);
                    if(!@$obj["_id"] || !@$obj["name"])continue;
                    if( $id == @$obj["idUserAuthor"]){
                        
                        array_push($cacti, array("id" => (string)@$obj["_id"], "label" => @$obj["name"], "parent" => $acti,"tags" => @$obj["tags"]));
                    }
                }
            }


            ///////Nodes pour rajouter PROJECTS
            array_push($cproj, array( "id" => "addproj", "label" => "AJOUTER...","parent" => $proj));
            array_push($corg, array( "id" => "addorg", "label" => "AJOUTER...","parent" => $org));
            array_push($cevents, array( "id" => "addevents", "label" => "AJOUTER...","parent" => $events));
            
            array_push($clife, array( "id" => "addlife", "label" => "AJOUTER..." , "parent" => $life));
            array_push($cpropo, array("id" => "addpropo", "label" => "AJOUTER...", "parent" => $propo));
            array_push($cacti, array("id" => "addaction", "label" => "AJOUTER...", "parent" => $acti));

            
            /////////REQUETE
            $root = array( "id" => (string)$item["_id"], "label" => $item["name"], "parent" => null);
            $root["children"] = array();

                if(count($corg) >= 1){
                    $root["children"][] = array("id" => "organization", "label" => $org." (".count($corg).")" , "parent" => $item["name"], "children" =>$corg);
                }
                
                if(count($cevents)>= 1){
                    $root["children"][] = array( "id" => "event", "label" => $events." (".count($cevents).")" , "parent" => $item["name"], "children" =>$cevents);
                }
                
                if(count($cproj)>= 1){
                    $root["children"][] = array( "id" => "project",  "label" => $proj." (".count($cproj).")" , "parent" => $item["name"], "children" =>$cproj);
                    // $root["children"][] = array( "id" => "plus", "parent" => $cproj);
                    
                }
                
                if(count($ctags)>= 1){
                     $root["children"][] = array( "id" => "tags",  "label" => $tage." (".count($ctags).")" , "parent" => $item["name"], "children" =>$ctags);
                    }

                 if(count($cfollower)>= 1){
                     $root["children"][] = array( "id" => "follower",  "label" => $follower." (".count($cfollower).")" , "parent" => $item["name"], "children" =>$cfollower);
                    }


                if(count($clife)>= 1){
                     $root["children"][] = array( "id" => "lifepath",  "label" => $life." (".count($clife).")" , "parent" => $item["name"], "children" =>$clife);
                    }

                if(count($cpropo)>= 1){
                     $root["children"][] = array( "id" => "proposal",  "label" => $propo." (".count($cpropo).")" , "parent" => $item["name"], "children" =>$cpropo);
                 }

                if(count($cacti)>= 1){
                     $root["children"][] = array( "id" => "actions",  "label" => $acti." (".count($cacti).")" , "parent" => $item["name"], "children" =>$cacti);
                 }

                if(count($cnom)>= 1){
                     $root["children"][] = array( "id" => "nom",  "label" => $nom." (".count($cnom).")", "parent" => $item["name"], "children" =>$cnom );
                 }

                    
            // $root = array( "id" => (string)$item["_id"], "label" => $item["name"], "parent" => null , "children" =>
            //             array( 
            //                 array("id" => "organization", "label" => $org." (".count($corg).")", "parent" => $item["name"], "children" => $corg),
            //                 array( "id" => "project", "label" => $proj." (".count($cproj).")", "parent" => $item["name"], "children" => $cproj),
            //                 array( "id" => "event",  "label" => $events." (".count($cevents).")", "parent" => $item["name"], "children" => $cevents),
            //                 array( "id" => "tags",  "label" => $tage." (".count($ctags).")", "parent" => $item["name"], "children" => $ctags)
            //             )
            //         );

                
            $data =array($root);
     
            $params = array(
                'data' => $data,
                'links' => $links,
                'item' => $item,
                'tags' => $tags,
                "title" => $type." : ".$item["name"],
                "link" => $link
                );
            
            ThemeHelper::setWebsiteTheme(ThemeHelper::EMPTY);
            Yii::app()->session["theme"] = "empty";

             if($view)
                return Rest::json($data);
            else{
            if(Yii::app()->request->isAjaxRequest)
                    return $controller->renderPartial('linear', $params);
                else{
                    ThemeHelper::setWebsiteTheme(ThemeHelper::EMPTY);
                   return $controller->render('linear', $params);
                }
            }

        }
    }
}