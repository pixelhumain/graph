<meta charset="utf-8">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Baloo+2:wght@600&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@600&display=swap" rel="stylesheet">
<?php if (!$isAjax) { ?>
    <?php
    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile(Yii::app()->getModule("graph")->getAssetsUrl() . '/plugins/d3/d3.v6.min.js');
    $cs->registerScriptFile(Yii::app()->getModule("graph")->getAssetsUrl() . '/plugins/list-js/list.min.js');
    $cs->registerScriptFile(Yii::app()->getModule("graph")->getAssetsUrl() . '/plugins/svg-text/svg-text.js');
    $cs->registerScriptFile(Yii::app()->getModule("graph")->getAssetsUrl() . '/js/rect-collide.js');
    $cs->registerScriptFile(Yii::app()->getModule("graph")->getAssetsUrl() . '/js/graph.js');
    $cs->registerScriptFile(Yii::app()->request->baseUrl . '/plugins/jQuery/jquery-2.1.1.min.js');
    $cssJs = array(
        '/js/api.js',
        '/plugins/jquery.dynForm.js',
        '/plugins/bootstrap/css/bootstrap.min.css',
        '/plugins/bootstrap/js/bootstrap.min.js',
        '/plugins/bootbox/bootbox.min.js',
        '/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css',
        '/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js',
        '/plugins/blockUI/jquery.blockUI.js',
        '/plugins/jquery-cookie/jquery.cookie.js',
        '/plugins/jquery-validation/dist/jquery.validate.min.js',
        '/plugins/font-awesome/css/font-awesome.min.css',

    );
    HtmlHelper::registerCssAndScriptsFiles($cssJs, Yii::app()->request->baseUrl);
    HtmlHelper::registerCssAndScriptsFiles(['/css/graph.css'], Yii::app()->getModule("graph")->getAssetsUrl());

    $cssAnsScriptFilesModule = array(
        '/js/default/search.js',
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule("co2")->getAssetsUrl());

    ?>

    <style>
        #mainNav {
            display: none !important;
        }

        .svg-text,
        text,
        textPath {
            pointer-events: none;
            fill: #455a64;
            font-size: 1.1em;
        }

        tspan {
            font-family: 'Nunito', sans-serif;
        }

        .drag-mode {
            cursor: grabbing;
        }

        textPath {
            font-family: 'Baloo 2', cursive;
        }

        #graphtags {
            padding-top: 20px;
            background-color: #fff;
            float: left;
            height: 600px;
            width: 20%;
            display: none;
        }

        #graphtags a {
            color: #333;
            text-decoration: none;
        }

        #sectionList a {
            color: red;
            text-decoration: none;
        }

        #search {
            float: right;
            margin-right: 100px;
        }

        #title {
            background-color: #eee;
            height: 80px;
            font-size: 2em;
            padding: 15px;
        }
    </style>
<?php } ?>
</style>

<style type="text/css">
    body {
        font-family: 'Nunito', Helvetica, Arial, sans-serif;
        font-size: 18px;
        font-weight: 300;
        line-height: 140%;
        margin: 0;
        min-height: 100vh;
        padding: 0;
        width: 100%;
        overflow-x: hidden;
    }


    h1.page_title {
        color: #fff;
    }

    h4.what_to_do {
        color: #fff;
    }

    h2.menu-title {
        color: #9fbd38;
    }

    .page_title,
    .what_to_do {
        font-weight: 300;
        line-height: 120%;
        text-align: center;
        text-shadow: 0 1px 5px rgba(0, 0, 0, .8);
        text-transform: uppercase;
    }

    /* PEN STYLES ========== */
    a,
    .side_menu {
        -webkit-transition: all 300ms ease-in-out;
        transition: all 300ms ease-in-out;
    }

    /* MENU CONTAINER ----- */
    .side_menu {
        box-shadow: -2px 4px 10px 0px rgb(0 0 0 / 50%);
        background-color: white;
        height: 100vh;
        position: fixed;
        top: 0;
        right: -300px;
        width: 300px;
        z-index: 10;
    }

    .side_menu .container {
        padding: 0 1em;
    }

    /* HAMBURGER STYLES ----- */
    .burger_box {
        display: block;
        float: left;
        margin-left: -45px;
    }

    .burger_box a.menu-icon {
        display: inline-block;
        float: none;
        height: 43px;
        padding: 10px;
        opacity: .5;
        width: 45px;
        z-index: 100;
    }

    .burger_box a.menu-icon:hover,
    .burger_box a.menu-icon.opened {
        opacity: 1;
    }

    .burger_box a.menu-icon.opened {
        background: #9fbd38;
    }

    .burger_box .menu-icon_box {
        display: inline-block;
        height: 25px;
        position: relative;
        text-align: left;
        width: 25px;
    }

    .burger_box .menu-icon_line {
        background: #455a64;
        border-radius: 2px;
        display: inline-block;
        height: 3px;
        position: absolute;
        width: 100%;
    }

    .burger_box .menu-icon_line--1 {
        top: 2px;
    }

    .burger_box .menu-icon_line--2 {
        top: 10px;
    }

    .burger_box .menu-icon_line--3 {
        top: 18px;
    }

    .burger_box .menu-icon_line--1 {
        transition: top 200ms 250ms, transform 200ms;
        -webkit-transition: top 200ms 250ms, -webkit-transform 200ms;
    }

    .burger_box .menu-icon_line--2 {
        transition: opacity 0ms 300ms;
        -webkit-transition: opacity 0ms 300ms;
    }

    .burger_box .menu-icon_line--3 {
        transition: top 100ms 300ms, transform 200ms;
        -webkit-transition: top 100ms 300ms, -webkit-transform 200ms;
    }

    .burger_box .menu-icon.opened .menu-icon_box {
        transform: scale3d(0.9, 0.9, 0.9);
        -webkit-transform: scale3d(0.9, 0.9, 0.9);
    }

    .burger_box .menu-icon.opened .menu-icon_line {
        top: 10px;
        background-color: #fff;
    }

    .burger_box .menu-icon.opened .menu-icon_line--1 {
        transform: rotate3d(0, 0, 1, 45deg);
        -webkit-transform: rotate3d(0, 0, 1, 45deg);
        transition: top 100ms, transform 200ms 250ms;
        -webkit-transition: top 100ms, -webkit-transform 200ms 250ms;
    }

    .burger_box .menu-icon.opened .menu-icon_line--2 {
        opacity: 0;
        transition: opacity 200ms;
        -webkit-transition: opacity 200ms;
    }

    .burger_box .menu-icon.opened .menu-icon_line--3 {
        transform: rotate3d(0, 0, 1, -45deg);
        -webkit-transform: rotate3d(0, 0, 1, -45deg);
        transition: top 200ms, transform 200ms 250ms;
        -webkit-transition: top 200ms, -webkit-transform 200ms 250ms;
    }

    /* STAGGER LIST ----- */
    .list_load {
        display: none;
        list-style: none;
        padding: 0;
    }

    .list_item {
        margin-left: -20px;
        opacity: 0;
        -webkit-transition: all 200ms ease-in-out;
        transition: all 200ms ease-in-out;
    }

    .list_item a {
        color: #fff;
        display: block;
        padding: 5px 10px;
        text-decoration: none;
    }

    .list_item a:hover {
        background: rgba(255, 255, 255, .2);
    }

    #accordion {
        overflow-x: hidden;
        overflow-y: auto;
        padding: 10px;
    }

    #accordion::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        background-color: #F5F5F5;
    }

    #accordion::-webkit-scrollbar {
        width: 6px;
        background-color: #F5F5F5;
    }

    #accordion::-webkit-scrollbar-thumb {
        background-color: #455a64;
    }

    .panel-heading {
        position: sticky;
        top: -10px;
        z-index: 100;
    }

    .h-100 {
        height: 100%;
    }

    .w-100 {
        width: 100%;
    }

    #search-clear {
        position: absolute;
        right: 10px;
        top: 50%;
        transform: translateY(-50%);
        font-size: 1.1em;
        cursor: pointer;
        color: #ccc;
    }

    #search-clear:hover {
        color: #455a64;
    }

    #search-container {
        display: none;
        padding: 10px;
    }

    #submenu-container {

        padding: 10px;
        text-align: end;
    }

    .submenu-icon {
        color: #ccc;
        margin-left: 10px;
        margin-right: 10px;
    }

    .submenu-icon:hover {
        color: #455a64;
    }
</style>


<div id="content">
    <div id="side_menu" class="side_menu">
        <div class="burger_box">
            <div class="menu-icon-container">
                <a href="#" class="menu-icon js-menu_toggle closed">
                    <span class="menu-icon_box">
                        <span class="menu-icon_line menu-icon_line--1"></span>
                        <span class="menu-icon_line menu-icon_line--2"></span>
                        <span class="menu-icon_line menu-icon_line--3"></span>
                    </span>
                </a>
            </div>
        </div>
        <div id="submenu-container" style="display: block;">
            <span id="btn-toggle-search" class="fa fa-search submenu-icon"></span>
            <span id="btn-toggle-sort" class="fa submenu-icon fa-sort-alpha-asc"></span>
        </div>
        <div id="search-container" style="display: none;">
            <div class="btn-group w-100">
                <input id="search-input" type="search" class="form-control w-100">
                <span id="search-clear" class="fa fa-close"></span>
            </div>
        </div>
        <div class="h-100">
            <div class="panel-group h-100" id="accordion">
            </div>
        </div>
    </div>



    <div id="graph-container">
        <svg id="graph"></svg>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            // Requires jQuery
            $(document).on('click', '.js-menu_toggle.closed', function(e) {
                e.preventDefault();
                $('.list_load, .list_item').stop();
                $(this).removeClass('closed').addClass('opened');

                $('.side_menu').css({
                    'right': '0px'
                });

                var count = $('.list_item').length;
                $('.list_load').slideDown((count * .6) * 100);
                $('.list_item').each(function(i) {
                    var thisLI = $(this);
                    timeOut = 100 * i;
                    setTimeout(function() {
                        thisLI.css({
                            'opacity': '1',
                            'margin-left': '0'
                        });
                    }, 100 * i);
                });
            });

            $(document).on('click', '.js-menu_toggle.opened', function(e) {
                e.preventDefault();
                $('.list_load, .list_item').stop();
                $(this).removeClass('opened').addClass('closed');

                $('.side_menu').css({
                    'right': '-300px'
                });

                var count = $('.list_item').length;
                $('.list_item').css({
                    'opacity': '0',
                    'margin-left': '-20px'
                });
                $('.list_load').slideUp(300);
            });
        });
    </script>


    <script>
        activeModuleId = "graph";
        var debug = false;

        function toggleSideGraph(id) {
            if ($(id).css('display') == "none") {
                $(id).css('display', 'block');
                $('#graph').css('display', 'none');
            } else {
                $(id).css('display', 'none');
                $('#graph').css('display', 'block');
            }
        }

        function runScript(e) {
            if (e.keyCode == 13) {
                s = document.getElementById("search").value;
                if (s.indexOf("#") == 0)
                    openUrl("graph/co/search/tag/" + s.substring(1));
                else if (s.indexOf(".") == 0)
                    openUrl("graph/co/search/geo/" + s.substring(1));
                else if (s.indexOf(">") == 0)
                    openUrl("graph/co/search/type/" + s.substring(1));
                else
                    openUrl("graph/co/search/q/" + s);
            }
        }

        const width = 800;
        const height = 800;


        var baseUrl = "<?php echo Yii::app()->getRequest()->getBaseUrl(true); ?>";

        const data = <?= json_encode($data) ?>;

        var tooltip = new GraphTooltip("graph-container");
        tooltip.defaultImage = "<?php echo $defaultImage ?>";
        var circleGraph = new CircleGraph(data, d => GraphUtils.slugify(d.group))
        circleGraph.setOnClickNode(selectNode)
        circleGraph.setOnZoom(correctSize)
        circleGraph.setAfterDraw(() => {
            correctSize()
        })
        circleGraph.draw("graph-container")


        function selectNode(event, dataItem) {
            tooltip.node = d3.select(event.target);
            tooltip.setContent(dataItem);
            tooltip.goToNode()
            tooltip.show()
        }

        function selectNodeUsingMenu(id) {
            const target = (d3.select(id));
            tooltip.node = target
            tooltip.setContent(target.node().__data__);
            tooltip.show()
        }

        function openMenu(baseLink, id, type) {
            if (!baseLink)
                return;
            let url = baseLink;
            if (id)
                url += "/id/" + id
            if (type)
                url += "/type/" + type
            window.location.href = baseUrl + "/" + url
        }

        window.onresize = () => {
            correctSize();
        }

        function correctSize() {
            tooltip.goToNode();
        }

        function openUrl(url) {
            // const fullUrl = baseUrl + '/' + url;
            // if (typeof $ != "undefined")
            //     ajaxPost("#content", fullUrl, null, (data) => {
            //         history.pushState(null, null, "/" + url);
            //     });
            // else
            // REMETTRE "/ph/"+url; en prod
            window.location.href = fullUrl;
        }

        window.onpopstate = function(event) {
            if (typeof $ != "undefined")
                ajaxPost("#content", document.location.href);
            else
                // REMETTRE "/ph/"+url; en prod
                window.location.href = fullUrl;
        };
    </script>

    <script>
        // let htmlContent = "";
        // let count = 0;
        // for (const parents of hierarchy.children) {
        //     // const arrays = raw[1];
        //     const label = labels[parents.children[0].data.group]
        //     htmlContent += `
        // <div class="panel panel-default">
        // <div class="panel-heading">
        // <h4 class="panel-title">
        // <a class="anchor-collapse" data-toggle="collapse" data-parent="#accordion" href="#collapse-${count}">${label}</a>
        // </h4>
        // </div>
        // <div id="collapse-${count}" class="panel-collapse collapse">
        // <div class="panel-body">
        // <table class="table">
        // <tbody class="list">
        // `;
        //     let htmlItemsContent = "";
        //     let j = 0;
        //     for (const raw of parents.children) {
        //         const obj = raw.data
        //         htmlItemsContent += `
        //     <tr>
        //     <td>
        //     <a class="name" href="javascript:" onclick="selectNodeUsingMenu('#id${count}-${j++}');">${obj.label}</a>
        //     </td>
        //     </tr>
        //     `;
        //     }

        //     htmlContent += `${htmlItemsContent}
        //     </tbody>
        // </table>
        // </div>
        // </div>
        // </div>`;
        //     count++;
        // }
        // d3.select("#accordion").html(htmlContent);
        // correctSize();
    </script>

    <script>
        // const options = {
        //     valueNames: ['name']
        // };
        // const lists = [];
        // for (let i = 0; i < count; i++) {
        //     const list = new List('collapse-' + i, options);
        //     lists.push(list)
        // }

        // function doSearch() {
        //     for (let index = 0; index < lists.length; index++) {
        //         const list = lists[index];
        //         const res = list.search($('#search-input').val())
        //         if (res.length > 0) {
        //             console.log($('#collapse-' + index).parent().css("display", "block"))

        //         } else {
        //             console.log($('#collapse-' + index).parent().css("display", "none"))
        //         }
        //     }
        // }

        // function doSort(asc) {
        //     for (const list of lists) {
        //         list.sort('name', {
        //             order: asc ? "asc" : "desc"
        //         });
        //     }
        //     $('#btn-toggle-sort')
        //         .removeClass("fa-sort-alpha-asc")
        //         .removeClass("fa-sort-alpha-desc")
        //     if (asc) {
        //         $('#btn-toggle-sort').addClass("fa-sort-alpha-desc")
        //     } else {
        //         $('#btn-toggle-sort').addClass("fa-sort-alpha-asc")
        //     }
        // }

        // $('#search-input').on('keyup', () => {
        //     doSearch();
        // })
        // if ($('#search-input').val() != "") {
        //     doSearch();
        // }
        // $('#btn-toggle-search').click(() => {
        //     $("#submenu-container").css("display", "none")
        //     $("#search-container").css("display", "block")
        // })
        // $('#search-clear').click(() => {
        //     $("#submenu-container").css("display", "block")
        //     $("#search-container").css("display", "none")
        //     $('#search-input').val('')
        //     doSearch()
        // })
        // $('#btn-toggle-sort').click(() => {
        //     doSort($('#btn-toggle-sort').hasClass("fa-sort-alpha-asc"))
        // })

        // doSort(true);
    </script>
</div>
</div>