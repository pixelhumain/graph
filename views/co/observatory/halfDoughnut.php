<?php 
	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
	var_dump(get_defined_vars());
?>

<div id="container ocecoouter" style="margin:0px auto;width:100%">
	<canvas id="canvas-bar<?php echo $id?>"></canvas>
	<p class="ocecopercent">
    	79%
    </p>
</div>
<script>
	window.chartColors = {
		red: 'rgb(255, 99, 132)',
		orange: 'rgb(255, 159, 64)',
		yellow: 'rgb(255, 205, 86)',
		green: 'rgb(75, 192, 192)',
		blue: 'rgb(54, 162, 235)',
		purple: 'rgb(153, 102, 255)',
		grey: 'rgb(201, 203, 207)'
	};


	var materialcolor = [
		window.chartColors.red , window.chartColors.orange , window.chartColors.yellow, window.chartColors.green, window.chartColors.blue, window.chartColors.purple, window.chartColors.grey
	];

	var COLORS = [
		"#0A2F62",
		"#0B2D66",
		"#064191",
		"#2C97D0",
		"#16A9B1",
		"#0AA178",
		"#74B976",
		"#0AA178",
		"#16A9B1",
		"#2A99D1",
		"#064191",
		"#0B2E68"
	];

	var randomScalingFactor = function() {
		return Math.round(Math.random() * 100);
	};

	jQuery(document).ready(function() {
	
	//Data
	mylog.log("render","/modules/graph/views/co/halfDoughnut.php");


	var hDChartData = [{
		label: '# of Votes',
        data: [59, 20, 29],
        backgroundColor: [
            'rgba(46, 204, 113, 1)',
            'rgba(224, 224, 0, 1)',
            'rgba(221, 221, 221, 1)'
        ],
        borderColor: [
            'rgba(46, 204, 113, 1)',
            'rgba(224, 224, 0, 1)',
            'rgba(221, 221, 221, 1)'
        ],
        borderWidth: 7
	}];
	
	var hDChartOptions =  {
		rotation: 1 * Math.PI,
        circumference: 1 * Math.PI,
        legend: {
            display: false
        },
        tooltip: {
            enabled: false
        },
        cutoutPercentage: 95,
        responsive: true,
        scales: {
        x: {
            grid: {
              offset: true
            }
        }
    }
	}

	var ctx = document.getElementById('canvas-bar<?php echo $id?>').getContext('2d');
	window.myBar = new Chart(ctx, {
		type: 'doughnut',
		data: {
			labels: ["Done", "P", "UnDone"],
			datasets: hDChartData
		},
		options: hDChartOptions
		
	});

		
});
</script>
