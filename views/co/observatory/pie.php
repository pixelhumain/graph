<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

?>

<script type="text/javascript">
window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};

var materialcolor = [
	window.chartColors.red , window.chartColors.orange , window.chartColors.yellow, window.chartColors.green, window.chartColors.blue, window.chartColors.purple, window.chartColors.grey
]


	var MONTHS = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];

	var COLORS = [
	"#0A2F62",
	"#0B2D66",
	"#064191",
	"#2C97D0",
	"#16A9B1",
	"#0AA178",
	"#74B976",
	"#0AA178",
	"#16A9B1",
	"#2A99D1",
	"#064191",
	"#0B2E68"
	];
</script>


		<div id="canvas-holder" style="margin:25px auto;width:100%">
				<canvas id="chart-area<?php echo $id?>"></canvas>
		</div>
	<script>
	
		var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};
jQuery(document).ready(function() {
	mylog.log("render","/modules/graph/views/co/pie.php");
		
		if(typeof <?php echo $id?>Data == "undefined" || <?php echo $id?>Data.label.length == 0){
			var config = {
				type: 'pie',
	    		data: {
					datasets: [{
						data: [
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor(),
							randomScalingFactor()
						],
						backgroundColor: COLORS,
						label: 'Dataset 1'
					}],
					labels: [
						'One fdgsdfg fds gsdfgsdf',
						'Two sfdgs fdg fdsgsdf',
						'Three sfg fdsgfdsgsdf',
						'Four sfdg fdsgsfdg',
						'Five sfgs fdgsfdgsd',
						'One sfdg dsfgsfd',
						'Two sfdg dsf gsfd',
						'Three sgf dsfgsd',
						'Four fgs fdgsdf',
						'Five sdfg fdsgdsf',
						"Eleven sfgs fdgsfd"
					]
				},
				options: {
					responsive: true,
					legend : {
						<?php if($size=="S") { ?>display: false<?php } ?>
					},
					title: {
						display: true,
						text: 'Camembert écologique'
					}
				}
			};
		}

		else {

			var config = {
				type: 'pie',
	    		data: {
					datasets: [{
						data: <?php echo $id?>Data.dataset,
						backgroundColor: materialcolor
					}],
					labels: <?php echo $id?>Data.label
				},
				options: {
					responsive: true,
					title: {
						display: true,
						text: 'Camembert'
					}
				}
			};

		}



			var ctx = document.getElementById('chart-area<?php echo $id?>').getContext('2d');
			window.myPie = new Chart(ctx, config);
		
	});
	</script>