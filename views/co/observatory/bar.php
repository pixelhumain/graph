<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>

<script type="text/javascript">

	window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};


var materialcolor = [
	window.chartColors.red , window.chartColors.orange , window.chartColors.yellow, window.chartColors.green, window.chartColors.blue, window.chartColors.purple, window.chartColors.grey
]

	var COLORS = [
	"#0A2F62",
	"#0B2D66",
	"#064191",
	"#2C97D0",
	"#16A9B1",
	"#0AA178",
	"#74B976",
	"#0AA178",
	"#16A9B1",
	"#2A99D1",
	"#064191",
	"#0B2E68"
	];
</script>


		<div id="container" style="margin:0px auto;width:100%">
			<canvas id="canvas-bar<?php echo $id?>"></canvas>
		</div>
<script>
		var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};
	jQuery(document).ready(function() {
	if(typeof <?php echo $id?>Data == "undefined" || <?php echo $id?>Data.label.length == 0){
		

			mylog.log("render","/modules/graph/views/co/bar.php");
			var barChartData = {
				labels: ["Developpement",
							"Chef de projet",	
							"Formation",
							"Commercial",	
							"Graphiste",	
							"Designer",	
							"Créatif"],
				datasets: [{
					label : "Tibor",
					backgroundColor: window.chartColors.red,
					borderColor: window.chartColors.red,
					borderWidth: 1,
					data: [78,43,40,29,40,59,103]
				},
				{
					label : "Rapha",
					backgroundColor: window.chartColors.blue,
					borderColor: window.chartColors.blue,
					borderWidth: 1,
					data: [28,23,20,9,20,29,203]
				},
				{
					label : "Bouboule",
					backgroundColor: window.chartColors.orange,
					borderColor: window.chartColors.orange,
					borderWidth: 1,
					data: [8,5,2,98,200,299,3]
				}]

			};

		

	} else {
		var barChartData<?php echo $id?> = {
			labels : <?php echo $id?>Data.label,
			datasets: [
				{
					backgroundColor: materialcolor,
					borderColor: materialcolor,
					borderWidth: 1,
					data: <?php echo $id?>Data.dataset
				}
			]

		}

	}

			var ctx = document.getElementById('canvas-bar<?php echo $id?>').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData<?php echo $id?>,
				options: {
					title: {
						display: true,
						text: 'Chart.js Bar Chart - Stacked'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					}
				}
			});

		
});
	</script>
