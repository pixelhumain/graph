<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

?>

<script type="text/javascript">
window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};


	var MONTHS = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];

	var COLORS = [
	"#0A2F62",
	"#0B2D66",
	"#064191",
	"#2C97D0",
	"#16A9B1",
	"#0AA178",
	"#74B976",
	"#0AA178",
	"#16A9B1",
	"#2A99D1",
	"#064191",
	"#0B2E68"
	];
</script>


		<div id="canvas-holder" style="margin:0px auto;width:80%">
			<canvas id="chart-area"></canvas>
		</div>

	<script>
		var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};
jQuery(document).ready(function() {
	mylog.log("render","/modules/graph/views/co/polar.php");
		var chartColors = window.chartColors;
		var config = {
			data: {
				datasets: [{
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
					],
					backgroundColor: COLORS,
					label: 'My dataset' // for legend
				}],
				labels: [
					'One fdgsdfg fds gsdfgsdf',
					'Two sfdgs fdg fdsgsdf',
					'Three sfg fdsgfdsgsdf',
					'Four sfdg fdsgsfdg',
					'Five sfgs fdgsfdgsd',
					'One sfdg dsfgsfd',
					'Two sfdg dsf gsfd',
					'Three sgf dsfgsd',
					'Four fgs fdgsdf',
					'Five sdfg fdsgdsf',
					"Eleven sfgs fdgsfd"
				]
			},
			options: {
				responsive: true,
				legend: {
					position: 'right',
				},
				title: {
					display: true,
					text: 'Chart.js Polar Area Chart'
				},
				scale: {
					ticks: {
						beginAtZero: true
					},
					reverse: false
				},
				animation: {
					animateRotate: false,
					animateScale: true
				}
			}
		};

			var ctx = document.getElementById('chart-area');
			window.myPolarArea = Chart.PolarArea(ctx, config);
		

	});
	</script>
