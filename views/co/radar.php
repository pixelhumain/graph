<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>
<script type="text/javascript">
window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};


	var MONTHS = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];

	var COLORS = [
	"#0A2F62",
	"#0B2D66",
	"#064191",
	"#2C97D0",
	"#16A9B1",
	"#0AA178",
	"#74B976",
	"#0AA178",
	"#16A9B1",
	"#2A99D1",
	"#064191",
	"#0B2E68"
	];

	function getRandomColor() {
		var letters = '0123456789ABCDEF';
		var color = '#';
		for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	}

	function randomRgbaString () {
  		let r = Math.floor(Math.random() * 255);
  		let g = Math.floor(Math.random() * 255);
  		let b = Math.floor(Math.random() * 255);
  			return r+","+g+","+b+",";
		}


</script>

		<div style="margin:25px auto; width:100%">
			<canvas id="canvas"></canvas>
		</div>
		
	<script>
		
	jQuery(document).ready(function() {
		if(typeof <?php echo $id?>Data == "undefined")
			alert("<?php echo $id?>Data is undefined");
		if(!id)
			alert("<?php echo $id?> id cannot be null");


		var <?php echo $id?>radarChartData = {
			labels: <?php echo $id?>Data.labels ,
			datasets: [{
				backgroundColor: <?php echo json_encode( Ctenat::$COLORS )  ?>,
				borderWidth: 1,
				data: <?php echo $id?>Data.datasets[0].data 
			}]

		};


		var op = [];
		var len = <?php echo $id?>radarChartData.labels[0].length;

		for(i = 0; i < len  ; i++){
			let ccc = randomRgbaString();
			op.push(
				{
					label : <?php echo $id?>radarChartData.labels[0][i],
					backgroundColor: 'rgba('+ccc+'0.5)',
					borderColor: 'rgba('+ccc+'1)',
					pointBackgroundColor: 'rgba('+ccc+'1)',
					data: <?php echo $id?>Data.datasets[0].data[i]
				}
			);
		}
		
		
		
		mylog.log("render","/modules/graph/views/co/radar.php");
		var color = Chart.helpers.color;
		var config = {
			type: 'radar',
			data: {
				labels: <?php echo $id?>radarChartData.labels[1],
				datasets: op
			},
			options: {
				legend: {
					position: 'top'
					<?php if($size=="S") { ?>,display: false<?php } ?>
				},
				title: {
					display: true,
					text: 'Smoothie Environnemental'
				},
				scale: {
					ticks: {
						beginAtZero: true
					}
				}
			}
		};

		window.myRadar = new Chart(document.getElementById('canvas'), config);

	});
	</script>