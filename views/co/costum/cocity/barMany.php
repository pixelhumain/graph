<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>


<div>
	
</div>
<div id="container"  style="margin:0px auto;width:<?php echo ($size == "S") ? "90" : "100";?>%;">
	<canvas id="<?php echo $id?>-canvas-bar"></canvas>
</div>

<script>
	var  colors = [
        "#93C020",
        "#8C5AA1",
        "#F9B21A",
        "#2C97D0",
        "#F5E740",
        "#0AA178",
        "#74B976",
        "#0AA178",
        "#16A9B1",
        "#2A99D1",
        "#064191",
        "#0B2E68"
    ];
	var randomScalingFactor = function() {
		return Math.round(Math.random() * 100);
	};
	jQuery(document).ready(function() {
		if(typeof <?php echo $id?>Data == "undefined")
			alert("<?php echo $id?>Data is undefined");
		if(!id)
			alert("<?php echo $id?> id cannot be null");

		var <?php echo $id?>barChartData = {
			labels: <?php echo $id?>Data.labels ,
			datasets: [{
				backgroundColor: colors,
				borderWidth: 1,
				data: <?php echo $id?>Data.datasets[0].data 
			}]

		};
		
		mylog.log("render","/modules/costum/views/custom/ctenat/graph/barMany.php", "<?php echo $id?>barChartData",<?php echo $id?>barChartData);
		mylog.log("gagagaga : :", <?php echo $id?>barChartData.datasets[0].data);
		var ctxContainer = document.getElementById('<?php echo $id?>-canvas-bar');
		var ctx = ctxContainer.getContext('2d');
		var valeur_maximale = Math.max.apply(null, <?php echo $id?>barChartData.datasets[0].data);
		var interval = 1;
		if(valeur_maximale > 20 && valeur_maximale < 200)
			interval = 10;
		else if(valeur_maximale >= 200 && valeur_maximale < 600)
			interval = 50;
		else if(valeur_maximale >= 600)
			interval = 100;
		window.myBar<?php echo $id?> = new Chart(ctx, {
			type: 'bar',
			data: <?php echo $id?>barChartData,
			options: {
				responsive: true,
				legend : {display:false},
				scales: {
				        yAxes: [{
				            ticks: {
				                beginAtZero: true,
				                stepSize : interval
				            }, 
				            scaleLabel: {
						        display: true
						      }
				        }]
				    }
			}
		});

		if (typeof <?php echo $id?>Data.yAxesLabel != "undefined") {
			myBar<?php echo $id?>.options.scales.yAxes[0].scaleLabel.display = true;
		    myBar<?php echo $id?>.options.scales.yAxes[0].scaleLabel.labelString = <?php echo $id?>Data.yAxesLabel;
		    myBar<?php echo $id?>.update();
		}

		ctxContainer.onclick = function(evt) {
	      var activePoints = myBar<?php echo $id?>.getElementsAtEvent(evt);
	      if (activePoints[0]) {
	        var chartData = activePoints[0]['_chart'].config.data;
	        var idx = activePoints[0]['_index'];

	        var label = chartData.labels[idx];
	        var value = chartData.datasets[0].data[idx];

	        var url = "label=" + label + "&value=" + value;
	        
	        
	        var classSel = slugify( label ).toLowerCase();
	        $(".dashElem").removeClass("hide");
	        $(".dashElem:not(."+classSel+")").addClass("hide");
	        $("#dashElemTitle").html("Projets "+label +" ("+$("."+classSel).length+")");
	        //alert(label+"||"+slugify( label ).toLowerCase() +"||"+classSel);
	      }
	    };

	});
</script>