<meta charset="utf-8">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Baloo+2:wght@600&display=swap" rel="stylesheet">
<?php if (!$isAjax) { ?>


    <?php
    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile(Yii::app()->getModule("graph")->getAssetsUrl() . '/plugins/d3/d3.v6.min.js');
    $cs->registerScriptFile(Yii::app()->getModule("graph")->getAssetsUrl() . '/plugins/svg-text/svg-text.js');
    $cs->registerScriptFile(Yii::app()->getModule("graph")->getAssetsUrl() . '/js/rect-collide.js');
    $cs->registerScriptFile(Yii::app()->request->baseUrl . '/plugins/jQuery/jquery-2.1.1.min.js');

    $cssJs = array(
        '/js/api.js',

        '/plugins/bootstrap/css/bootstrap.min.css',
        '/plugins/bootstrap/js/bootstrap.min.js',
        '/plugins/bootbox/bootbox.min.js',
        '/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css',
        '/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js',
        '/plugins/blockUI/jquery.blockUI.js',
        '/plugins/jquery-cookie/jquery.cookie.js',
        '/plugins/jquery-validation/dist/jquery.validate.min.js',
        '/plugins/font-awesome/css/font-awesome.min.css',

    );
    HtmlHelper::registerCssAndScriptsFiles($cssJs, Yii::app()->request->baseUrl);
    ?>

<?php } ?>
<style>
    .svg-text,
    text,
    textPath {
        pointer-events: none;
    }

    textPath {
        font-family: 'Baloo 2', cursive;
    }

    #graph-container {
        width: 500px;
        height: 500px;
    }

    #graphtags {
        padding-top: 20px;
        background-color: #fff;
        float: left;
        height: 600px;
        width: 20%;
        display: none;
    }

    #graphtags a {
        color: #333;
        text-decoration: none;
    }

    #sectionList a {
        color: red;
        text-decoration: none;
    }

    #search {
        float: right;
        margin-right: 100px;
    }

    #title {
        background-color: #eee;
        height: 80px;
        font-size: 2em;
        padding: 15px;
    }
</style>


<div id="content">
    <div id='title'>

        <?php
        $l = $title;
        if (!empty(@$link))
            $l = '<a class="lbh" data-dismiss="modal" href="/' . $link . '">' . $title . ' <i class="fa fa-link"></i></a>';
        ?>

        <div class='pull-left'>
            <div class="dropdown" style="display: inline-block;">
                <a href="#" class="dropdown-toggle padding-5" style="padding-top: 7px !important;" data-toggle="dropdown" role="button">
                    <i class="fa fa-bars"></i></a>
                <ul class="dropdown-menu arrow_box dropdown-languages-nouser" role="menu" style="">
                    <li class="bold">View</li>
                    <li><a href="javascript:;" onclick="openMenu('graph/co/d3', '<?= (string) $item['_id'] ?>', '<?= $type ?>')">Graph</a></li>
                    <li class="active"><a href="javascript:;">Circle Packing</a></li>
                    <li><a href="javascript:;" onclick="alert('todo')">MindMap</a></li>
                    <li><a href="javascript:;" onclick="alert('todo')">Finder</a></li>
                    <li class="bold">Filters</li>
                    <li><a href="javascript:;" onclick="toggleSideGraph('#graphtags')">tag List</a></li>
                    <li><a href="javascript:;" onclick="alert('todo')">community</a></li>
                    <li><a href="javascript:;" onclick="alert('todo')">projects</a></li>
                    <li class="bold">Graph By</li>
                    <li><a href="javascript:;" onclick="alert('todo')">links by projects</a></li>
                    <li><a href="javascript:;" onclick="alert('todo')">activity</a></li>
                </ul>
            </div>
            <?php echo $l ?>
        </div>

        <input id='search' type='text' placeholder='#tag, free search, >types' onkeypress='return runScript(event)' />
    </div>

    <div id="graphtags">
        <a href="javascript:;" class="pull-right " onclick=""><i class="fa fa-times"></i></a>
        <div id="sectionList"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <svg id="graph"></svg>
            </div>
        </div>
    </div>
    <?php
    $d3 = Yii::app()->getModule("graph")->getAssetsUrl() . '/plugins/d3/d3.v6.min.js';
    $rect = (Yii::app()->getModule("graph")->getAssetsUrl() . '/js/rect-collide.js');
    ?>
    <script id="worker1" type="javascript/worker">
        importScripts(location.origin + '<?= $d3 ?>');
        importScripts(location.origin + '<?= $rect ?>');
        self.onmessage = function(e) {
            let data = e.data.data;
            const node = e.data.node;
            const id = e.data.id;
            const x_rect = e.data.x_rect;
            const y_rect = e.data.y_rect;
            const w = e.data.w;
            const h = e.data.h;
            const simulation = d3.forceSimulation()
                .force("center", d3.forceCenter(node.x, node.y))
                .force("charge", d3.forceManyBody())
                .force("collide", rectCollide().size(d => [d.bw, d.bh]))
                .nodes(data)
                .stop()
                // Default n = 300 = Math.ceil(Math.log(simulation.alphaMin()) / Math.log(1 - simulation.alphaDecay()))
            for (var i = 0, n = 100; i < n; ++i) {
                simulation.tick();
            }
            self.postMessage({data, id, x_rect, y_rect, h, w});
        };
    </script>


    <script>
        activeModuleId = "graph";
        var debug = false;

        function toggleSideGraph(id) {
            if ($(id).css('display') == "none") {
                $(id).css('display', 'block');
                $('#graph').css('display', 'none');
            } else {
                $(id).css('display', 'none');
                $('#graph').css('display', 'block');
            }
        }

        function runScript(e) {
            if (e.keyCode == 13) {
                s = document.getElementById("search").value;
                if (s.indexOf("#") == 0)
                    openUrl("graph/co/search/tag/" + s.substring(1));
                else if (s.indexOf(".") == 0)
                    openUrl("graph/co/search/geo/" + s.substring(1));
                else if (s.indexOf(">") == 0)
                    openUrl("graph/co/search/type/" + s.substring(1));
                else
                    openUrl("graph/co/search/q/" + s);
            }
        }
        const width = 800;
        const height = 800;


        var baseUrl = "<?php echo Yii::app()->getRequest()->getBaseUrl(true); ?>";

        const data = <?= json_encode($data) ?>;
        const root = <?= json_encode($root) ?>;
        const icons = <?= json_encode($icons) ?>;
        const colors = <?= json_encode($colors) ?>;
        const labels = <?= json_encode($labels) ?>;
        const tags = <?= json_encode($tags) ?>;
        const type = '<?= $type ?>';
        console.log(type)
        console.log(data);
        console.log(tags);




        const root_svg = d3.select("svg")
            .attr("viewBox", `0 0 ${width+20} ${height+20}`)
            .attr("text-anchor", "middle")
        const svg = root_svg
            .append("g")
            .attr("transform", "translate(10,10)")
        const filter_ombre = root_svg
            .append("defs")
            .append("filter")
            .attr("id", "ombre")
            .append("feDropShadow")
            .attr("flood-opacity", 0.3)
            .attr("dx", 0)
            .attr("dy", 1);
        const padding = 11;

        if (data.length == 0) {
            svg.append("circle")
                .attr
            const x = width / 2;
            const y = height / 2;
            const r = Math.min(x, y);
            const parent_circle = svg
                .append("circle")
                .attr("r", r)
                .attr("cx", x)
                .attr("cy", y)
                .attr("stroke", "none")
                .attr("fill", "white")
                .attr("filter", "url(#ombre)")
            const text = svg
                .append("text")
                .append("textPath")
                .style("font-size", "0.75em")
                .classed("svg-text", true)
                .attr("xlink:href", d => `#path-root`)
                .text(root.label)
                .style("fill", "black")
                .attr("text-anchor", "middle")
                .attr("startOffset", "50%")
            const path = svg
                .append("path")
                .attr("stroke", "none")
                .attr("fill", "none")
                .attr("id", `path-root`)
                .attr('d', `M ${x} ${y + r - padding} A 1 1 0 1 1 ${x} ${y - r + padding} M ${x} ${y - r + padding} A 1 1 0 1 1 ${x} ${y + r - padding} `)

        } else {

            const group = d3.group(data, d => slugify(d.group));

            const hierarchy = d3.hierarchy(group).sum(d => d.size).sort((a, b) => b.value - a.value)
            const packed = d3.pack(hierarchy)
                .size([width, height])
                .padding(20)(hierarchy)


            const blob = new Blob([
                document.querySelector('#worker1').textContent
            ], {
                type: "text/javascript"
            })
            // Note: window.webkitURL.createObjectURL() in Chrome 10+.
            const worker = new Worker(window.URL.createObjectURL(blob));
            worker.onmessage = function(e) {
                const x_rect = e.data.x_rect;
                const y_rect = e.data.y_rect;
                const w = e.data.w;
                const h = e.data.h;
                const svg = d3.select(`#${e.data.id}`);
                svg.selectAll("*").remove();
                const svg_node = svg.selectAll("g")
                    .data(e.data.data)
                    // .enter()
                    .join("g")
                const imgs = svg_node
                    .append("image")
                    .attr("xlink:href", d => d.img)
                    .attr("width", d => (d.width))
                    .attr("height", d => (d.height))
                    //positioner l'image dans le rectangle
                    .attr("x", d => d.x)
                    .attr("y", d => d.y)
                    .on('click', (evt, d) => selectNode(d));

                // imgs
                // .attr("x", d => d.x)
                // .attr("y", d => d.y);

                let minX = Infinity;
                let maxX = -Infinity;
                let minY = Infinity;
                let maxY = -Infinity;
                for (const node of svg_node.nodes()) {
                    const bound = node.getBBox()
                    const x = bound.x
                    const y = bound.y
                    if (x < minX) minX = x;
                    if (x > maxX) maxX = x;
                    if (y < minY) minY = y;
                    if (y > maxY) maxY = y;
                }


                if (minX != x_rect) {
                    if (minX < x_rect) {
                        imgs.attr("x", d => (d.x = d.x + Math.abs(minX - x_rect)))
                    } else {
                        imgs.attr("x", d => (d.x = d.x - Math.abs(minX - x_rect)))
                    }
                }
                if (minY != y_rect) {
                    if (minY < y_rect) {

                        imgs.attr("y", d => (d.y = d.y + Math.abs(minY - y_rect)))
                    } else {
                        imgs.attr("y", d => (d.y = d.y - Math.abs(minY - y_rect)))
                    }
                }
                const texts = svg_node
                    .append("text")
                    .attr("y", d => (d.y + d.height + 15))
                    .on('click', (evt, d) => selectNode(d));

                const textSpans = texts.selectAll("tspan")
                    .data(d => d.label.split(/(?=[A-Z][a-z])|\s+/g))
                    .join("tspan")
                    .attr("x", (_, i, j) => {
                        const d = j[i].parentNode.__data__;
                        const x = (d.x + d.width / 2)
                        return x;
                    })
                    .attr("dy", (d, i, j) => i != 0 ? 15 : 0)
                    .text(d => d + " ")
                const bound = svg.node().getBBox();
                const k = Math.min(w, h) / (Math.max(bound.width, bound.height));
                svg.style("transform-origin", "10px 0%")
                svg.style("transform-box", "fill-box")
                svg.attr("transform", `scale(${k})`)
            }
            var myColor = d3.scaleOrdinal().domain(tags)
                .range(d3.schemeTableau10);

            function parcoursProfondeur(node, currSvg) {
                if (node.children) {
                    const suffix_id = slugify(node.children[0].data.group ?? (node.parent != null ? "tags" : "root"));
                    if (node.parent != null) {

                        root_svg
                            .select("defs")
                            .append("clipPath")
                            .attr("id", `clip-${suffix_id}`)
                            .append("circle")
                            .attr("r", node.r - 13)
                            .attr("cx", node.x)
                            .attr("cy", node.y)
                        const path = currSvg
                            .append("path")
                            .attr("stroke", "none")
                            .attr("fill", "none")
                            .attr("id", `path-${suffix_id}`)
                            .attr('d', `M ${node.x} ${node.y + node.r - padding} A 1 1 0 1 1 ${node.x} ${node.y - node.r + padding} M ${node.x} ${node.y - node.r + padding} A 1 1 0 1 1 ${node.x} ${node.y + node.r - padding} `)
                    }

                    const parent_circle = currSvg
                        .append("circle")
                        .attr("r", node.r)
                        .attr("cx", node.x)
                        .attr("cy", node.y)
                        .attr("stroke", "none")
                        .attr("fill", !colors[node.children[0].data.type] ? "white" : type != 'tags' ? myColor(node.children[0].data[0] ?? node.children[0].data.group) : colors[node.children[0].data.type])
                        .attr("filter", "url(#ombre)")
                        .on('click', e => zoom(e, node))

                    const textLen = (labels[node.children[0].data[0] ?? node.children[0].data.group] ?? "TAGS").length;
                    const text = currSvg
                        .append("text")
                        .append("textPath")
                        .style("font-size", "0.75em")
                        .classed("svg-text", true)
                        .attr("xlink:href", d => `#path-${suffix_id}`)
                        .text(labels[node.children[0].data[0] ?? node.children[0].data.group] ?? "TAGS")
                        .style("fill", "white")
                        .attr("text-anchor", "middle")
                        .attr("startOffset", "50%")

                    currSvg
                        .append("title")
                        .text(labels[node.children[0].data[0] ?? node.children[0].data.group] ?? "TAGS")

                    const clip_group = currSvg
                        .append("g")
                        .classed("clip-group", true)
                        .attr("clip-path", `url(#clip-${suffix_id})`)

                    if (node.children[0].children) {
                        const children_group = clip_group
                            .append("g")
                            .classed("children-group", true)
                        for (let i = 0; i < node.children.length; i++) {
                            parcoursProfondeur(node.children[i], children_group.append("g"))
                        }
                    } else {
                        const x_dist = Math.cos(Math.PI / 4) * (node.r)
                        const y_dist = x_dist; // cos(pi/4) = sin(pi/4)
                        const x_rect = node.x - x_dist;
                        const y_rect = node.y - y_dist;
                        const w = x_dist * 2;
                        const h = y_dist * 2;

                        if (node.children[0].data.type != "tags") {
                            const zoom_group = clip_group
                                .append("g")
                                .classed("zoom-group", true)
                            const uniq = 'id' + (new Date()).getTime();
                            const svg = zoom_group.
                            append("g")
                                .attr("id", uniq)
                                .classed("children-group", true);
                            // clip_group.append("rect")
                            //     .attr("x", x_rect)
                            //     .attr("y", y_rect)
                            //     .attr("width", w)
                            //     .attr("height", h)
                            //     .attr("stroke", "red")
                            //     .attr("fill", "none")
                            const svg_node = svg.selectAll("g")
                                .data(node.data[1])
                                // .enter()
                                .join("g")

                            const imgs = svg_node
                                .append("image")
                                .attr("xlink:href", d => d.img)
                                .attr("width", d => (d.width = 50))
                                .attr("height", d => (d.height = 50))
                                //positioner l'image dans le rectangle
                                .attr("x", d => (d.x = x_rect + w / 2 + Math.random() * 10 - 5))
                                .attr("y", d => (d.y = y_rect + h / 2 + Math.random() * 10 - 5))

                                .on('click', (evt, d) => selectNode(d));
                            const texts = svg_node
                                .append("text")

                                .attr("x", d => x_rect + w / 2)
                                .attr("y", d => y_rect + h / 2 + d.height / 2 + 15)
                                .on('click', (evt, d) => selectNode(d));

                            const textSpans = texts.selectAll("tspan")
                                .data(d => d.label.split(/(?=[A-Z][a-z])|\s+/g))
                                .join("tspan")
                                .attr("x", (_, i, j) => {
                                    const d = j[i].parentNode.__data__;
                                    const x = (d.x + d.width / 2)
                                    return x;
                                })
                                .attr("dy", (d, i, j) => i != 0 ? 15 : 0)
                                .text(d => d + " ")

                            svg_node
                                .attr("data-size", d => {
                                    const txtBound = svg_node.select("text").node().getBBox();
                                    const imgBound = svg_node.select("image").node().getBBox();
                                    d.bw = txtBound.width < imgBound.width ? imgBound.width : txtBound.width;
                                    d.bh = imgBound.height + 15 + txtBound.height;
                                    return `${d.bw} ${d.bh}`;
                                })
                            svg_node.style("display", "none")
                            // const simulation = d3.forceSimulation()
                            //     .force("center", d3.forceCenter(node.x, node.y))
                            //     .force("charge", d3.forceManyBody())
                            //     .force("collide", rectCollide().size(d => [d.bw, d.bh]))
                            //     .nodes(node.data[1])
                            //     .stop()
                            // for (var i = 0, n = Math.ceil(Math.log(simulation.alphaMin()) / Math.log(1 - simulation.alphaDecay())); i < n; ++i) {
                            //     simulation.tick();
                            // }



                            worker.postMessage({
                                "data": node.data[1],
                                node,
                                'id': uniq,
                                x_rect,
                                y_rect,
                                w,
                                h
                            });
                        }
                    }
                }
            }
            parcoursProfondeur(packed, svg);
            d3.select("circle")
                .attr("id", "root-circle")
            let view;
            let focus = packed;
            const root_coord = [packed.x, packed.y, packed.r * 2];
            zoomTo(root_coord);

            function zoomTo(v) {
                view = v;
                const k = Math.min(width, height) / (v[2] + 20);
                const d = focus;
                svg.attr("transform", `translate(${width/2 - v[0] * k},${height/2 - v[1] * k}) scale(${k})`);
            }
            let zoomEvent = d3.zoom();

            function zoom(event, d) {
                if (zoomEvent) zoomEvent.on("zoom", null);
                const focus0 = focus;
                focus = d;
                const target_coord = [focus.x, focus.y, focus.r * 2];
                const target = d3.select(event.target)
                const transition = svg.transition()
                    .duration(event.altKey ? 7500 : 750)
                    .tween("zoom", d => {
                        const i = d3.interpolateZoom(view, target_coord);
                        return t => zoomTo(i(t));
                    });
                if (target.attr("id") != "root-circle") {
                    const curr_g = d3.select(target.node().parentNode)
                        .select(".zoom-group")
                    zoomEvent.on("zoom", e => {
                        curr_g.attr("transform", (transform = e.transform));
                    })
                    const parent_g = d3.select(target.node().parentNode);
                    parent_g.call(zoomEvent)
                }
            }


            const text_parent = svg
                .select("g")
                .append("text")
                .append("textPath")
                .style("font-size", "1em")
                .attr("xlink:href", d => "#root")
                .attr('dx', packed.x)
                .attr('dy', packed.y - packed.r - 13)
                .attr("text-anchor", "middle")
                .attr("startOffset", "50%")
                .text(root.label)
            const path = svg
                .append("path")
                .attr("stroke", "none")
                .attr("fill", "none")
                .attr("id", "root")
                .attr('d', `M ${packed.x - packed.r + padding} ${packed.y} A 1 1 0 1 1 ${packed.x + packed.r - padding} ${packed.y}`)
            const titleRoot = svg.select("title").text(root.label)
            // svg.select("circle")
            // .attr("fill", colors[root.type])

            function selectNode(dataItem) {
                let selectedNode = dataItem;
                const id = selectedNode.id.substr(0, 24);
                if (id.length > 20) {
                    if (selectedNode.type != "organizations") {
                        openMenu("graph/co/d3/id/" + id + "/type/" + selectedNode.type);
                    } else {
                        openUrl("graph/co/circle/id/" + id + "/type/" + selectedNode.type);
                    }
                } else if (selectedNode.type == "tags")
                    openUrl("graph/co/circle/id/" + selectedNode.label + "/type/" + selectedNode.type);
                // openUrl("graph/co/search/tag/" + selectedNode.label);
            }
        }


        function openMenu(baseLink, id, type) {
            if (!baseLink)
                return;
            let url = baseLink;
            if (id)
                url += "/id/" + id
            if (type)
                url += "/type/" + type
            window.location.href = baseUrl + "/" + url
        }

        function slugify(text) {
            return text.toString().toLowerCase()
                .replace(/\s+/g, '-') // Replace spaces with -
                .replace(/[^\w\-]+/g, '') // Remove all non-word chars
                .replace(/\-\-+/g, '-') // Replace multiple - with single -
                .replace(/^-+/, '') // Trim - from start of text
                .replace(/-+$/, ''); // Trim - from end of text
        }

        function openUrl(url) {
            // const fullUrl = baseUrl + '/' + url;
            // if (typeof $ != "undefined")
            //     ajaxPost("#content", fullUrl, null, (data) => {
            //         history.pushState(null, null, "/" + url);
            //     });
            // else
            //     // REMETTRE "/ph/"+url; en prod
            //     window.location.href = fullUrl;
            openMenu(url)
        }
        window.onpopstate = function(event) {
            if (typeof $ != "undefined")
                ajaxPost("#content", document.location.href);
            else
                // REMETTRE "/ph/"+url; en prod
                window.location.href = fullUrl;
        };
    </script>

</div>