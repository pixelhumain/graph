<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

?>

<script type="text/javascript">
window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};


	var MONTHS = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];

	var COLORS = [
	"#0A2F62",
	"#0B2D66",
	"#064191",
	"#2C97D0",
	"#16A9B1",
	"#0AA178",
	"#74B976",
	"#0AA178",
	"#16A9B1",
	"#2A99D1",
	"#064191",
	"#0B2E68"
	];
</script>


		<div id="canvas-holder" style="margin:25px auto;width:100%">
				<canvas id="chart-area"></canvas>
		</div>
		<?php if($size!="S") { ?>
		<div style="margin:0px auto;width:80%">
			<button id="randomizeData">Randomize Data</button>
			<button id="addDataset">Add Dataset</button>
			<button id="removeDataset">Remove Dataset</button>
		</div>
		<?php } ?>
	<script>
	
		var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};
jQuery(document).ready(function() {
	mylog.log("render","/modules/graph/views/co/pie.php");
		var config = {
			type: 'pie',
    		data: {
				datasets: [{
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
					backgroundColor: COLORS,
					label: 'Dataset 1'
				}],
				labels: [
					'One fdgsdfg fds gsdfgsdf',
					'Two sfdgs fdg fdsgsdf',
					'Three sfg fdsgfdsgsdf',
					'Four sfdg fdsgsfdg',
					'Five sfgs fdgsfdgsd',
					'One sfdg dsfgsfd',
					'Two sfdg dsf gsfd',
					'Three sgf dsfgsd',
					'Four fgs fdgsdf',
					'Five sdfg fdsgdsf',
					"Eleven sfgs fdgsfd"
				]
			},
			options: {
				responsive: true,
				legend : {
					<?php if($size=="S") { ?>display: false<?php } ?>
				},
				title: {
					display: true,
					text: 'Camembert écologique'
				}
			}
		};

			var ctx = document.getElementById('chart-area').getContext('2d');
			window.myPie = new Chart(ctx, config);
		
		<?php if($size!="S") { ?>
		document.getElementById('randomizeData').addEventListener('click', function() {
			config.data.datasets.forEach(function(dataset) {
				dataset.data = dataset.data.map(function() {
					return randomScalingFactor();
				});
			});

			window.myPie.update();
		});

		var colorNames = Object.keys(window.chartColors);
		document.getElementById('addDataset').addEventListener('click', function() {
			var newDataset = {
				backgroundColor: [],
				data: [],
				label: 'New dataset ' + config.data.datasets.length,
			};

			for (var index = 0; index < config.data.labels.length; ++index) {
				newDataset.data.push(randomScalingFactor());

				var colorName = colorNames[index % colorNames.length];
				var newColor = window.chartColors[colorName];
				newDataset.backgroundColor.push(newColor);
			}

			config.data.datasets.push(newDataset);
			window.myPie.update();
		});

		document.getElementById('removeDataset').addEventListener('click', function() {
			config.data.datasets.splice(0, 1);
			window.myPie.update();
		});
		<?php } ?>
	});
	</script>