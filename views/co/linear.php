<!DOCTYPE html>
  <metacharset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <?php

    $cssJS = array(
    
    '/plugins/jquery.dynForm.js',
    
    '/plugins/jquery-validation/dist/jquery.validate.min.js',
    '/plugins/select2/select2.min.js' , 
    '/plugins/moment/min/moment.min.js' ,
    '/plugins/moment/min/moment-with-locales.min.js',
    
    '/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js' , 
    '/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css',
    '/plugins/jquery-cookieDirective/jquery.cookiesdirective.js' , 
    '/plugins/ladda-bootstrap/dist/spin.min.js' , 
    '/plugins/ladda-bootstrap/dist/ladda.min.js' , 
    '/plugins/ladda-bootstrap/dist/ladda.min.css',
    '/plugins/ladda-bootstrap/dist/ladda-themeless.min.css',
    '/plugins/animate.css/animate.min.css',

    '/plugins/d3/d3.v4.min.js' 
); 

HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);



    ?>

    <style>

      .node {
          cursor: pointer;
      }

      .node circle {
        fill: #fff;
        stroke: #2F4F4F;
        stroke-width: 3px;
      }

      .node text {
        
        font: 15px sans-serif;
      }

      .link {
        fill: none;
        stroke: #2F4F4F;
        stroke-width: 2px;
      }

      /*#graph {
      float: left;
      width:2000px;
      background-color: green;
      }*/
      
      #graphtags {
      padding-top: 20px;
     
      float: left;
     
      
      
      }

      #graphtags a{
      color: black;
      text-decoration: none;
      
     
      }

      #sectionList p {
        margin-left: 15px;
      }
      
      #sectionList a{
      color: red;
      text-decoration: none;
      }

      #add p {
        font-size: 30px;
        margin-left: 5px;
      }

    

      #lisearch {
      float:right;
      margin-right: 100px;
      }
      
      #title {
      background-color: #eee;
      height:80px;
      font-size: 2em;
      padding:15px;
      margin-top: 60px;
      }

      #ling{
      margin-top: 10px;
      
      }
    </style>

         
    <div id='title'>

        <?php
            $l = $title;
              if( !empty( @$link ) )
                $l = '<a class="lbh" data-dismiss="modal" href="'.$link.'">'.$title. ' <i class="fa fa-link"></i></a>';

        ?>
      
        <div class='pull-left'><?php echo $l?></div>
        <input id='lisearch' type='text' placeholder='#tag, free search, >types' onkeypress='return runScript(event)'/>
        <br>
        <!-- <input id="ling" type=button onclick=window.location.href='http://127.0.0.1/ph/graph/co/linear/'; value=Linéaire />
        <input id="ling" type=button onclick=window.location.href='http://127.0.0.1/ph/graph/co/d3/'; value=Basic /> -->
    </div>


    <div  id="graphtags">

      <!-- ///////////////// AJOUTER //////////////// -->
      <div class="container">
        <!-- Trigger the modal with a button -->
        
        <button type="button" id="ajout" style="margin-bottom: 10px;" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Modifier</button>
       

        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog">
          
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <h2>Modifier des données</h2>
              </div>

             <form>
              <!-- Titre -->
              <div style="margin-left: 10px;" class="form-group">
                <label style="font-size: 20px;" for="titleForm">Titre</label>
                <input type="text" style="margin-left: ;width:auto;" class="form-control" id="titleForm" name="titleForm" placeholder="ex : Voiture" required>
              </div>
              <!-- texte -->
              <div style="margin-left: 10px;" class="form-group">
                <label style="font-size: 20px;" for="Textarea">Data</label>
                <textarea style="margin-left: ;width:auto;" class="form-control" id="Textarea1" name="Textarea1" placeholder="ex : Audi" required></textarea>
                <br>
                <!-- <textarea style="margin-left: ;width:auto;" class="form-control" id="Textarea2" name="Textarea2" placeholder="ex : Bmw" required></textarea>
                <br>
                <textarea style="margin-left: ;width:auto;" class="form-control" id="Textarea3" name="Textarea3" placeholder="ex : Mercedes" required></textarea> -->
              </div>
             
              <a href='javascript:;' id ="add" style="margin-left: 10px;" type="submit">Modifier</a>

             </form>
            </div>
            
          </div>
        </div>
        
      </div>

     


    <script src="http://d3js.org/d3.v3.min.js"></script>

    <script>
var contextData = {  
          "name": "<?php echo $item['name'] ?>",
          "type": "<?php echo $item['type'] ?>",
          "slug": "<?php echo $item['slug'] ?>",
          "typeSig": "<?php echo $item['type'] ?>",
          "id": "<?php echo $item['id'] ?>"
        };




      //ici ce trouve la piste pour ajouter plusieurs nodes sans limitation: le principe était mettre un séparateur donc la virgule, pour dire qu'après chaque virgule on revient à la ligne dans un premier temps. Grace à la boucle fort la virgule devient un séparateur d'objet : c'est-à-dire qu'après chaque virgule au lieu de donnée ceci : {name: rtt, rtrt, trt}, on a cela {name : rtt}, {name : rtrt}, {name : trt}. Le problème est au niveau de l'affichage au lieu de donnée ceci: data [ {"name" : "rtt"}] dans la base de donnée on a ça : data [ "{name : rtt}" ], je n'ai pas su résoudre ce problème







      // $(document).ready(function() {
        
      //    //AJOUTER DATA
      //   $('#add').on('click', function(e) {
      //     e.preventDefault(); // On empêche de soumettre le formulaire
          

      //     var titre = $("#titleForm").val();
      //     var text1 = $("#Textarea1").val();
      //     // var donnee2 = $("#Textarea2").val();
      //     // var donnee3 = $("#Textarea3").val();
      //     var donnee ="";
          

      //     var don = text1.split(",");
      //     for (var i = 0; i<don.length; i++) {
            
      //       console.log("test", don[i]);
      //       donnee += "{name : "+don[i]+"},";
      //     }

      //     var Gdata = {
      //         collection : "citoyens",
      //         id : "5c46f159a1d9f08e058b456f",
      //         answerSection : "graph.linear",
      //         answers : {
      //            label:[
      //             { title: titre }
      //            ], 
      //            data : [
      //              donnee
      //              ]
      //          }
      //       };
      //     console.log(donnee);
      //     $.ajax({
      //       url: "http://127.0.0.1/ph/survey/co/update2", 
      //       type: "POST", //méthode (post)
      //       data: Gdata, 
      //       dataType: 'json', // JSON
      //       success: function(json) { // Si ça c'est passé avec succès
      //           // ici on teste la réponse
      //         if(json.result ) {
      //           //alert('Ajout OK');
      //           // window.location.reload();
      //           toastr.success("Donnée Ajouté");
      //          //window.location.reload();
      //         } else {
      //           alert('Erreur : '+ json.reponse);
      //         }
      //       }
      //     });
       
        
      //   });//FIN AJOUTER DATA

      //   // $('#delete').on('click', function(e) {
      //   //   e.preventDefault(); 
          

      //   //   var titre = $("#titleForm").val();
      //   //   var donnee1 = $("#Textarea1").val();
          
          
      //   //   $.ajax({
      //   //     url: "http://127.0.0.1/ph/survey/co/delete", 
      //   //     type: "POST", // On récupère la méthode (post)
      //   //     data: {
      //   //       collection : "citoyens",
      //   //       id : "5c46f159a1d9f08e058b456f",
      //   //       answerSection : "graph.linear",
      //   //       answers : {
      //   //         label:[
      //   //           { title: titre }
      //   //         ], 
      //   //         data : [
      //   //           { name : donnee1 }
      //   //         ]
      //   //       }
      //   //     }, 
      //   //     dataType: 'json', // JSON
      //   //     success: function(json) { // Si ça c'est passé avec succès
      //   //       window.location.reload();
      //   //         // ici on teste la réponse
      //   //       if(json.result ) {
      //   //         alert('supp OK');
               
      //   //       } else {
      //   //         alert('Erreur : '+ json.reponse);
      //   //       }
      //   //     }
      //   //   });
       
      //   // });



      // });
      
      function runScript(e) {
       if (e.keyCode == 13) {
           s = document.getElementById("lisearch").value;
           if (s.indexOf("#") == 0 )
                open("graph/co/lisearch/tag/"+s.substring(1) );
            else if (s.indexOf(".") == 0 )
                open("graph/co/lisearch/geo/"+s.substring(1) );
            else if (s.indexOf(">") == 0 )
                open("graph/co/lisearch/type/"+s.substring(1) ) ;
            else
               open("lisearch/q/"+s );
        }
      }

      console.log( "treeData", <?php echo json_encode($data); ?>);

      var treeData = <?php echo json_encode($data); ?>;
      var tags = <?php echo json_encode($tags); ?>;

      
     
      //console.log(treeData);

  // ************** Generate the tree diagram  *****************
      
      var margin = {top: 50, right: 120, bottom: 20, left: 200},
          width = 2000 - margin.right - margin.left,
          //à chaner si les nodes se touche
          height = 1200 - margin.top - margin.bottom;
          
      var i = 0,
          duration = 750,
          root;

      var tree = d3.layout.tree()
          .size([height, width]);

      var diagonal = d3.svg.diagonal()
          .projection(function(d) { return [d.y, d.x]; });

      var svg = d3.select("body").append("svg")
          .attr("width", width + margin.right + margin.left)
          .attr("height", height + margin.top + margin.bottom)
        .append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      root = treeData[0];
      root.x0 = height / 2;
      root.y0 = 0;
        
      update(root);

      d3.select(self.frameElement).style("height", "500px");

      function update(source) {

        // Compute the new tree layout.
        var nodes = tree.nodes(root).reverse(),
            links = tree.links(nodes);
            console.log(links);

        console.log("nodes",nodes);

        // Normalize for fixed-depth.lien //longueur des liens
        nodes.forEach(function(d) { d.y = d.depth * 500; });

        // Update the nodes…
        var node = svg.selectAll("g.node")
            .data(nodes, function(d) { return d.id || (d.id = ++i); });

        // Enter any new nodes at the parent's previous position.
        var nodeEnter = node.enter().append("g")
            .attr("class", "node")
            .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
            .on("click", click);

        nodeEnter.append("circle")
            .attr("r", 1e-6)
            .style("fill", Color);

        nodeEnter.append("text")
            .attr("x", function(d) { return d.children || d._children ? -20 : 20; })
            .attr("dy", ".35em")
            .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
            .text(function(d) { return d.label; })
            .style("fill-opacity", 1e-6);

        // Transition nodes to their new position.
        var nodeUpdate = node.transition()
            .duration(duration)
            .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

        nodeUpdate.select("circle")
            .attr("r", 10)
            .style("fill", Color);

        nodeUpdate.select("text")
            .style("fill-opacity", 1);

        // Transition exiting nodes to the parent's new position.
        var nodeExit = node.exit().transition()
            .duration(duration)
            .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
            .remove();

        nodeExit.select("circle")
            .attr("r", 1e-6);

        nodeExit.select("text")
            .style("fill-opacity", 1e-6);

        // Update the links…
        var link = svg.selectAll("path.link")
            .data(links, function(d) { return d.target.id; });

        // Enter any new links at the parent's previous position.
        link.enter().insert("path", "g")
            .attr("class", "link")
            .attr("d", function(d) {
              var o = {x: source.x0, y: source.y0};
              return diagonal({source: o, target: o});
            });

        // Transition links to their new position.
        link.transition()
            .duration(duration)
            .attr("d", diagonal);

        // Transition exiting nodes to the parent's new position.
        link.exit().transition()
            .duration(duration)
            .attr("d", function(d) {
              var o = {x: source.x, y: source.y};
              return diagonal({source: o, target: o});
            })
            .remove();

        // Stash the old positions for transition.
        nodes.forEach(function(d) {
          d.x0 = d.x;
          d.y0 = d.y;
        });
      }

      // Toggle children on click.
      function click(d) {
        //console.log("<<<<<<<<<<<<<<<<<<<<<<<",d, d.parent.id);
        alert(d.id);
        if(d.id == "organization" || d.id == "event" || d.id == "project" || d.id == "tags" || d.id == "proposal" || d.id == "actions"  || d.id == "lifepath" || d.id == "follower")
        {
          if (d.children) {
            d._children = d.children;
            d.children = null;
          } else {
            d.children = d._children;
            d._children = null;
          }
        }



        //lien tags
        else if(d.parent.id == "tags"){
          open( "/ph/graph/co/lisearch/tag/"+d.label );
        }
       
        //lien proposal, actions, lifepath 
        // else if(d.parent.id == "proposal" || d.parent.id == "actions" || d.parent.id == "lifepath"){
        //   open();
        // }

        else if(d.id == "nom"){
          $('#myModal').modal('show');
        }

        else if(d.id =="addorg"){
          //$('#ajax-modal').modal('show');
          dyFObj.openForm("organization")
          //open("http://127.0.0.1/ph/assets/38cc7bd8/js/dynForm/project.js");

        }

        else if(d.id =="addproj"){
          dyFObj.openForm("project")
        }

        else if(d.id =="addevents"){
          dyFObj.openForm("event")
        }

        else if(d.id =="addlife"){
          dyFObj.openForm("curiculum")
        }

        else if(d.id =="addpropo"){
          dyFObj.openForm("proposal")
        }

        else if(d.id =="addaction"){
          dyFObj.openForm("action")
        }


        else {
          open("linear/type/"+d.parent.id+"/id/"+d.id);
        }
        
        update(d);
      }



      function Color(d){
        // console.log("ddddddddddddddddd",d);
        if(d.parent != null)
        {
          if(d.id == "proposal" || d.parent.id == "proposal")
              return "#229296";
          else if(d.id == "event" || d.parent.id == "event")
              return "#ffa200";
          else if(d.id == "project" || d.parent.id == "project")
              return "#8c5aa1";
          else if( d.id == "organization" || d.parent.id == "organization")
              return "#93c020";
          else if(d.id == "follower" || d.parent.id == "follower")
              return "#f904c1";
          else if(d.id == "lifepath" || d.parent.id == "lifepath")
              return "gold"; 
          else if(d.id == "actions" || d.parent.id == "actions")
              return "#fc0505";
          else if (d.id == "tags" || d.parent.id == "tags")
              return "#a8b8bb"
          else return "lightgrey";
        }
      }

    // $( document ).ready(function() {
    //     $("g.node").on("click", function(event){
    //       event.preventDefault();
    //       alert();
    //       console.log("<<<<<<<<<<<<<<<<<<<<<<<",d);
    //     });
    // });

    </script>
           