
<style type="text/css">
	.tagBarLinks {list-style:none; width: 100%; margin: 0 auto;}
	.tagBarLinks li{ width:50px;height:50px;margin: 25px 10px; border: 2px solid #009E74; border-radius: 10px; display: block;}
	.tagBarLinks li i{margin:5px;}
	.tagBarLinks li img{width:45px;border-radius: 10px; }
</style>
<?php 
$list = array(

    array(
    	"name" => "pie",
    	"img" => "/images/pie.png",
        "icon" => "fa-pie-chart",
        "color" => "#ccc",
        "link" => "pie"
    ), 
    array(
    	"name" => "bar",
    	"img" => "/images/bar.png",
        "icon" => "fa-bar-chart-o",
        "color" => "#ccc",
        "link" => "bar"
    ), 
    array(
    	"name" => "radar",
    	"img" => "/images/radar.png",
        "icon" => "fa-bar-chart-o",
        "color" => "#ccc",
        "link" => "radar"
    ), 
    array(
    	"name" => "polar",
    	"img" => "/images/polar.png",
        "icon" => "fa-bar-chart-o",
        "color" => "#ccc",
        "link" => "polar"
    )
    , 
    array(
    	"name" => "scatter",
    	"img" => "/images/scatter.png",
        "icon" => "fa-bar-chart-o",
        "color" => "#ccc",
        "link" => "scatter"
    )
                
);
 ?>
<div class="col-xs-1 no-padding text-center" style="background-color: white;">
	<ul class="tagBarLinks ">
		<?php foreach ($list as $key => $v) { 
			$lbl = '<img src="'.Yii::app()->getModule( "graph" )->getAssetsUrl().$v["img"].'">';
			$link = (isset($v["link"])) ? $v["link"] : "" ;
			?>
			<li><a class="graphSwitch" data-type="<?php echo $link; ?>" href="javascript:;"><?php echo $lbl; ?><br/><?php echo $v['name']; ?></a></li>
		<?php } ?>
	</ul>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
	mylog.log("render","/modules/graph/views/co/menuSwitcher.php");
	$(".graphSwitch").off().on("click",function() { 
	ajaxPost('#graphSection', baseUrl+'/graph/co/dash/g/'+$(this).data("type"), 
					null,
					function(){},"html");
});

});
</script>