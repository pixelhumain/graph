<div class="table table-left">			
	<section>
	<h2 class="table-heading">Dogs at work</h2>			
		<table class="data"><caption><p>Disputes over whether dogs were capable of symbolic thinking turn on a smattering of discoveries spanning more than 200,000 years.</p>
		</caption>
			<thead>
				<tr>
					<th scope="col">Start</th>
					<th scope="col">End</th>
					<th scope="col">Event</th>
					<th scope="col">Description</th>
					<th scope="col">Link</th>
					<th scope="col">Photo</th>
					<th scope="col">Credit</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>13 June 2018</td>
					<td>27 June 2018</td>
					<td>Strelka and Belka's successful orbit</td>
					<td>Laika was the first dog sent into space, but Strelka (Little Arrow) and Belka (Squirrel) -- launched on Sputnik 5 in 1960 for a one-day mission -- were the first to return alive. As a result, much more was learned from their mission. Strelka later gave birth to a litter of puppies, one of which, Pushinka, was given to President John F. Kennedy's daughter, Caroline.</td>
					<td></td>
					<td><img alt="#" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/9353/300.png"></td>
					<td>http://placedog.com/</td>
				</tr>
				<tr>
					<td>20 August 2018</td>
					<td>20 August 2018</td>
					<td>How Peritas saved civilization</td>
					<td>Without his dog, Peritas, Alexander the Great might have been Alexander the So-So. When the warrior was swarmed by the troops of Persia's Darius III, Peritas leapt and bit the lip of an elephant charging his master. Alexander lived to pursue his famed conquest, forging the empire underlying Western civilization as we know it.</td>
					<td></td>
					<td><img alt="#" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/9353/300-1.png"></td>
					<td>http://placedog.com/</td>
				</tr>
				<tr>
					<td>2 September 2019</td>
					<td>20 September 2019</td>
					<td>Charlie, Kennedy's Cuban Missile Crisis companion</td>
					<td>At the height of 1962's Cuban missile crisis, President Kennedy had his son's Welsh terrier Charlie summoned to the chaotic War Room. The president held the terrier in his lap, petting him and appearing, by all accounts, to relax. Eventually he announced that he was ready to "make some decisions" -- those that de-escalated the conflict.</td>
					<td></td>
					<td><img alt="#" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/9353/300-2.png"></td>
					<td>http://placedog.com/</td>
				</tr>
				<tr>
					<td>13 September 2019</td>
					<td>26 September 2019</td>
					<td>Jofi, the first therapy dog</td>
					<td>Sigmund Freud usually kept a chow named Jofi in his office during psychotherapy sessions, believing the dog comforted the patients. Freud's notes on these interactions, detailed in his diaries, form the basis of modern-day pet-assisted therapy. </td>
					<td></td>
					<td><img alt="#" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/9353/300-3.png"></td>
					<td>http://placedog.com/</td>
				</tr>
				<tr>
					<td>11 October 2019</td>
					<td>27 October 2019</td>
					<td>Newfoundland saves Napoleon</td>
					<td>Napoleon Bonaparte owed his life to a nameless Newfoundland. As Bonaparte fled the island of Elba in 1815, where he was exiled, choppy seas pitched him overboard. A fisherman's dog jumped in after the drowning despot and kept him afloat. Napoleon lived to experience his own defeat at the Battle of Waterloo.</td>
					<td></td>
					<td><img alt="#" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/9353/300-4.png"></td>
					<td>http://placedog.com/</td>
				</tr>
				<tr>
					<td>4 November 2019</td>
					<td>4 November 2019</td>
					<td>Nixon professes love for Checkers</td>
					<td>In his 1952 "Checkers speech," Richard Nixon -- then a candidate for vice president who was accused of pawing $18,000 in illegal campaign contributions -- admitted to accepting an American cocker spaniel, Checkers, as a gift. </td>
					<td></td>
					<td><img alt="#" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/9353/300-5.png"></td>
					<td>http://placedog.com/</td>
				</tr>
				<tr>
					<td>23 November 2019</td>
					<td>30 November 2019</td>
					<td>Peps, Wagner's harshest critic</td>
					<td>Without Peps, composer Richard Wagner's Cavalier King Charles spaniel, that helicopter scene in the film "Apocalypse Now" (scored to "The Ride of the Valkyries") might sound very different. Wagner would have Peps sit on a special chair as he played his latest compositions and, based upon the dog's reactions, he'd keep or toss each passage.</td>
					<td></td>
					<td><img alt="#" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/9353/300-6.png"></td>
					<td>http://placedog.com/</td>
				</tr>
				<tr>
					<td>24 May 2020</td>
					<td>3 July 2020</td>
					<td>Donnchadh and the American Revolution</td>
					<td>In 1306, when Edward I of England sought to bring down Robert the Bruce (and his ploy to rule Scotland), his men used Robert's own dog, Donnchadh, to find him.</td>
					<td></td>
					<td><img alt="#" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/9353/300-7.png"></td>
					<td>http://placedog.com/</td>
				</tr>
				<tr>
					<td>2 June 2020</td>
					<td>24 July 2020</td>
					<td>Urian bites Pope, separates church and state</td>
					<td>Henry VIII sent Cardinal Wolsey to meet with Pope Clement VII, hoping the pontiff would grant the ruler an annulment of his marriage to Catherine of Aragon. When the pope extended his bare toe to be kissed (as was the custom) by Wolsey, the Cardinal's dog, Urian, sprang forward and bit the pope.</td>
					<td></td>
					<td><img alt="#" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/9353/300-8.png"></td>
					<td>http://placedog.com/</td>
				</tr>
			</tbody>
		</table>
			
	</section>
</div>

