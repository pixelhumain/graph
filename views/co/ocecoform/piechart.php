<?php
$cssAnsScriptFilesTheme = array(
    "/plugins/Chart-2.8.0/Chart.min.js",
    "/plugins/Chart-2.8.0/chartjs-plugin-datalabels.min.js",
); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>

<div id="" style="margin:0px auto;width:100%; ">
    <canvas style="position: relative" id="canvas-bar<?php echo $id?>" ></canvas>
</div>
<script>

    var cData = <?php echo json_encode($data); ?>;
    var cLabel = "<?php echo $label; ?>";
    var cLabels = <?php echo (!empty($labels)? json_encode($labels): "[]"); ?>;
    var cColors = <?php echo json_encode($colors); ?>;
    var graphidk = "<?php echo $id?>";

    var unity<?php echo $id?> = "<?php echo (!empty($unity)? $unity : ""); ?>";

    var numberWithCommas<?php echo $id?> = function(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")+" "+unity<?php echo $id?>;
    };

    jQuery(document).ready(function() {


        var dChartData = [{
            label: cLabel,
            data: cData,
            borderColor: cColors,
            weight: 1,
            backgroundColor: cColors,
            datalabels : {
                anchor: 'end',
                align: 'end',
                color: function(context) {
                    return context.dataset.backgroundColor ;
                },
                font : {
                    size : 13,
                    weight: 'bold'
                }
            }
        }];

        var dChartOptions =  {
            // rotation: 1 * Math.PI,
            //       circumference: 1 * Math.PI,
            onClick: (c, i) => {
                /*const points = myBarr.getElementsAtEventForMode(click, 'nearest', { intersect: true }, true);
                console.log(myBarr.getElementAtEvent(click));
                if (points.length) {
                    const firstPoint = points[0];
                    const valueM = myBarr.data.labels[firstPoint];
                    //console.log(valueM);
                }*/
                e = i[0];
                console.log(e._index)
                // var x_value = this.data.labels[e._index];
                aapgraphcallback("<?php echo $id?>", e._index);
            },
            interaction: {
                mode: 'nearest'
            },
            events: ['click', 'hover'],
            legend: {
                display: false
            },
            tooltips: {
                backgroundColor: '#FFF',
                titleFontSize: 16,
                titleFontColor: '#0066ff',
                bodyFontColor: '#000',
                bodyFontSize: 16,
                displayColors: false,
                footerFontColor : '#686565'
            },
            responsive : true,
            weight : 0.2,
            plugins: {
                datalabels: {
                    anchor: 'end',
                    align: 'end',
                    formatter: function(value, context) {
                        return context.chart.data.labels[context.dataIndex];
                    },
                    color: function(context) {
                        return context.dataset.backgroundColor ;
                    },
                    font : {
                        size : 25,
                        weight: 'bold'
                    }
                }
            },
            scales: {
                r: {
                    ticks: {
                        backdropPadding: {
                            x: 1000,
                            y: 4
                        }
                    }
                }
            }
        };

        var ctx<?php echo $id?> = document.getElementById('canvas-bar<?php echo $id?>').getContext('2d');
        window.myBarr<?php echo $id?> = new Chart(ctx<?php echo $id?>, {
            plugins: [ChartDataLabels],
            type: 'pie',
            data: {
                labels: cLabels,
                datasets: dChartData
            },
            options: {
                plugins: {
                    tooltip: {
                        callbacks: {
                            label: function(context) {
                                let label = context.dataset.label || '';

                                if (label) {
                                    label += ': ';
                                }
                                if (context.parsed.y !== null) {
                                    label += new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(context.parsed.y);
                                }
                                return label;
                            }
                        }
                    },
                    datalabels: {
                        formatter: (value, ctx) => {

                            let sum = ctx.chart.data.datasets[0].data.reduce((a, b) => parseInt(a) + parseInt(b), 0);
                            let percentage = (value*100 / sum).toFixed(2)+"%";
                            //return percentage;
                            //alert(JSON.stringify(ctx.chart.data.datasets[0].data));

                            return value + "€ ("+percentage+")";


                        },
                        color: '#fff',
                    }
                }
            }

        });


    });
</script>
