<?php
    HtmlHelper::registerCssAndScriptsFiles(['/plugins/d3/d3-v3/d3.min.js'], Yii::app()->request->baseUrl);
    HtmlHelper::registerCssAndScriptsFiles(['/plugins/d3/d3-v3/sankeyaap.js'], Yii::app()->request->baseUrl);
    HtmlHelper::registerCssAndScriptsFiles(['/plugins/d3/d3-v3/color.js'], Yii::app()->request->baseUrl);
    HtmlHelper::registerCssAndScriptsFiles(['/plugins/d3/d3-v3/interpolate.js'], Yii::app()->request->baseUrl);
    HtmlHelper::registerCssAndScriptsFiles(['/plugins/d3/d3-v3/chromatic.js'], Yii::app()->request->baseUrl);

    ?>
<style>

    .node rect {
        cursor: move;
        fill-opacity: .9;
        shape-rendering: crispEdges;
    }

    .node text {
        pointer-events: none;
        text-shadow: 0 1px 0 #fff;
    }

    .link {
        fill: none;
        stroke: #000;
        stroke-opacity: .2;
    }

    .link:hover {
        stroke-opacity: .5;
    }

</style>

<p id="chart<?php echo $id?>">

    <script>

        var cData = {};

        cData.links = <?php echo $data["links"]; ?>;
        cData.nodes = <?php echo $data["nodes"]; ?>;
        cLabels = <?php echo json_encode($labels); ?>;

        var cColors = <?php echo json_encode($colors); ?>;

        var cColorsS = {};

        if (typeof ocecoform != "undefined" &&
            typeof ocecoform.tiles != "undefined" &&
            typeof ocecoform.tiles.totparfinanceur != "undefined" &&
            typeof ocecoform.tiles.totparfinanceur.values != "undefined" &&
            typeof ocecoform.tiles.totparfinanceur.values.labels != "undefined" &&
            typeof ocecoform.tiles.totparfinanceur.values.colors != "undefined"
        ){
            $.each(ocecoform.tiles.totparfinanceur.values.labels, function(idd, vdd){
                cColorsS[vdd] = ocecoform.tiles.totparfinanceur.values.colors[idd];
            });
        }

        var units = "";

        var margin = {top: 50, right: 10, bottom: 10, left: 10},
            width = 1000 - margin.left - margin.right,
            height = 450 - margin.top - margin.bottom;

        /*const color = d3.scaleOrdinal()
            .domain([
                'rfgf',
                'rfgf',
                'rfgf',
                'rfgf',
                'rfgf'
            ])
            .range([
                '#90eb9d',
                '#f9d057',
                '#f29e2e',
                '#00ccbc',
                '#d7191c'
            ]);*/

        //var color = d3.scaleOrdinal([`#383867`, `#584c77`, `#33431e`, `#a36629`, `#92462f`, `#b63e36`, `#b74a70`, `#946943`]);

        var formatNumber = d3.format(",.0f"),    // zero decimal places
            format = function(d) { return formatNumber(d) + " " + units; }
            //,color = d3.scale.category20()
            , color = d3.scaleLinear().domain([1, 20]).interpolate(d3.interpolateHcl)
                .range([d3.rgb("#bbff00"), d3.rgb('#1100ff')])

        ;

        // append the svg canvas to the page
        var svg = d3.select("#chart<?php echo $id?>").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("x", 200)
            .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");

        const defs = svg.append('defs');

        // Set the sankey diagram properties
        var sankey = d3.sankey()
            .nodeWidth(12)
            .nodePadding(10)
            .size([width, height]);

        var path = sankey.link();

        //var sankeygreenhouse = cData;

        // load the data
        //d3.data(sankeygreenhouse, function(error, graph) {

        //var graph = sankeygreenhouse;

        var graph = cData;

        var nodeMap = {};

        graph.nodes.forEach(function(x) { nodeMap[x.name] = x; });
            graph.links = graph.links.map(function(x) {
                return {
                    source: nodeMap[x.source],
                    target: nodeMap[x.target],
                    value: x.value
                };
            });

            sankey
                .nodes(graph.nodes)
                .links(graph.links)
                .layout(13);

// add in the links
            var link = svg.append("g").selectAll(".link")
                .data(graph.links)
                .enter().append("path")
                .attr("class", "link")
                .attr("d", path)
                .style("stroke-width", function(d) { return Math.max(1, d.dy); })
                //new
                .style('stroke-opacity', 0.18)
                .on('mouseover', function() {
                    d3.select(this).style('stroke-opacity', 0.5);
                })
                .on('mouseout', function() {
                    d3.select(this).style('stroke-opacity', 0.2);
                })
                //end new
                .sort(function(a, b) { return b.dy - a.dy; });

// add the link titles
            link.append("title")
                .text(function(d) {
                    return d.source.name + " → " +
                        d.target.name + "\n" + format(d.value); });

// add in the nodes
            var node = svg.append("g").selectAll(".node")
                .data(graph.nodes)
                .enter().append("g")
                .attr("class", "node")
                .attr("transform", function(d) {
                    return "translate(" + d.x + "," + d.y + ")"; })
                /*.call(d3.behavior.drag()
                    .origin(function(d) { return d; })
                    .on("dragstart", function() {
                        this.parentNode.appendChild(this); })
                    .on("drag", dragmove))*/
                .call(d3.drag()
                    .subject(d => d)
                    .on('start', function() {
                        this.parentNode.appendChild(this); })
                    .on('drag', dragmove))

            ;

        var columnNames = cLabels;
        if (columnNames.length == 2){
            var distanceArr = [0, width - 120];
        } else {
            var distanceArr = [0, (width/2) - 50,  width - 120];
        }
        var pos=0;

        $.each(columnNames , function(cnid, cn){
            svg.append("text")
                .attr("x", distanceArr[cnid])
                .attr("y", -10)
                .style("fill", "#506db4")
                .style("font-family", "Montserrat")
                .style("font-size", "17px")
                .style("z-index", "1000")
                .style("font-weight", "bold")
                .text(cn)
                .raise();;
        });

// add the rectangles for the nodes
            node.append("rect")
                .attr("height", function(d) { return d.dy; })
                .attr("width", sankey.nodeWidth())
                .style("fill", function(d , i) {
                    //return d.color = color(d.name.replace(/ .*/, "")); })
                    //console.log("eeeee",d, i);
                    if (typeof cColorsS[d.name] != "undefined"){
                        return cColorsS[d.name];
                    }else {
                        return d.color = color(((d.level+1)*2) + i);
                    }})
                /*.style('fill', d => {
                    if(color.domain().indexOf(d.name) > -1){
                        return d.color = color(d.name);
                    } else {
                        return d.color = '#ccc';
                    }
                })*/
                /*.style("stroke", function(d) {
                    return d3.rgb(d.color).darker(2); })*/
                .append("title")
                .text(function(d) {
                    return d.name + "\n" + format(d.value); });

// add in the title for the nodes
            node.append("text")
                .attr("x", -6)
                .attr("y", function(d) { return d.dy / 2; })
                .attr("dy", ".35em")
                .attr("text-anchor", "end")
                .style("fill", "#444c42")
                .style("font", "normal 16px/1.5 'Open Sans', sans-serif")
                .style("font-size", "15px")
                .attr("transform", null)
                .text(function(d) { return d.name; })
                .filter(function(d) { return d.x < width / 2; })
                .attr("x", 6 + sankey.nodeWidth())
                .attr("text-anchor", "start");

        // add gradient to links
        link.style('stroke', function(d, i) {
            console.log('d from gradient stroke func', d);

            // make unique gradient ids
            const gradientID = 'gradient'+i;

            const startColor = d.source.color;
            const stopColor = d.target.color;

            const linearGradient = defs.append('linearGradient')
                .attr('id', gradientID);

            linearGradient.selectAll('stop')
                .data([
                    {offset: '10%', color: startColor },
                    {offset: '90%', color: stopColor }
                ])
                .enter().append('stop')
                .attr('offset', d => {
                    console.log('d.offset', d.offset);
                    return d.offset;
                })
                .attr('stop-color', d => {
                    console.log('d.color', d.color);
                    return d.color;
                });

            return "url(#"+gradientID+")";
        })

// the function for moving the nodes
            function dragmove(d) {
                d3.select(this).attr("transform",
                    "translate(" + (
                        d.x = Math.max(0, Math.min(width - d.dx, d3.event.x))
                    ) + "," + (
                        d.y = Math.max(0, Math.min(height - d.dy, d3.event.y))
                    ) + ")");
                sankey.relayout();
                link.attr("d", path);
            }
        //});

    </script>

</body>
</html>
