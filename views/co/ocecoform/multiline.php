<?php 
	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
		"/plugins/Chart-2.8.0/chartjs-plugin-datalabels.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

	if (isset($objectif) && (int)$objectif != 0) {
		$max = (int)$objectif + (int)(((int)$objectif*25)/100);
	} else {
		$objectif = 0;
		$max = 0;
	}
?>

<div id="" class="myFixedHeightContainer" style="margin:0px auto;width:100%;">
	<canvas id="canvas-bar<?php echo $id?>"></canvas>
</div>
<script>

	var cData = <?php echo json_encode(@$data); ?>;
	var cLabel = "<?php echo $label; ?>";
	var cLabels = <?php echo (!empty($labels)? json_encode($labels): "[]"); ?>;
	var cColors = <?php echo json_encode($colors); ?>;

	jQuery(document).ready(function() {
	
	var dChartData = [];

	var duo = ["end", "start"];

	var horizonalLinePlugin = {
	  afterDraw: function(chartInstance) {
	    var yScale = chartInstance.scales["y-axis-0"];
	    var canvas = chartInstance.chart;
	    var ctx = canvas.ctx;
	    var index;
	    var line;
	    var style;

	    if (chartInstance.options.horizontalLine) {
	      for (index = 0; index < chartInstance.options.horizontalLine.length; index++) {
	        line = chartInstance.options.horizontalLine[index];

	        if (!line.style) {
	          style = "rgba(169,169,169, .6)";
	        } else {
	          style = line.style;
	        }

	        if (line.y) {
	          yValue = yScale.getPixelForValue(line.y);
	        } else {
	          yValue = 0;
	        }

	        ctx.lineWidth = 3;

	        if (yValue) {
	          ctx.beginPath();
	          ctx.moveTo(yValue, yValue);
	          ctx.lineTo(canvas.width, yValue);
	          ctx.strokeStyle = style;
	          ctx.stroke();
	        }

	        if (line.text) {
	          ctx.fillStyle = style;
	          ctx.fillText(line.text, canvas.width - 60, yValue + ctx.lineWidth - 10);
	          ctx.font = "15px Verdana";
	        }
	      }
	      return;
	    }
	  }
	};
	Chart.pluginService.register(horizonalLinePlugin);

	$.each(cLabels, function(vvId, vvVal){
		dChartData.push({
			label: cLabel[vvId],
	        data: cData[vvId],
	        borderColor: cColors[vvId],
	        backgroundColor: cColors[vvId],
	        borderWidth: 4,
	        datalabels: {
		    	align: function(context) {
		        	return context.active ? duo[vvId] : 'center';
		        }
		    }
		});
	});
	
	var dChartOptions =  {
		// rotation: 1 * Math.PI,
  //       circumference: 1 * Math.PI,
        legend: {
            display: false,
        },
        tooltip: {
            enabled: false
        },
        responsive : true,
        maintainAspectRatio: false,
        scales: {
	        yAxes: [{
	            ticks: {
	            	suggestedMax : <?php echo (int)$objectif; ?>,
	                beginAtZero: true
	            }
	        }],
	         y: {
		        stacked: true
		      }
		},
		plugins: {
	      datalabels: {
	        backgroundColor: function(context) {
	          return context.active ? 'white' : context.dataset.backgroundColor;
	        },
	        borderColor: function(context) {
	          return context.dataset.backgroundColor;
	        },
	        borderRadius: function(context) {
	          return 0;
	        },
	        borderWidth: 0,
	        color: function(context) {
	          return context.active ?  context.dataset.backgroundColor : 'white' ;
	        },
	        font: {
	          weight: 'bold'
	        },
	        formatter: function(value, context) {
	          return context.active ? value : "";
	        },
	        offset: 1,
	        padding: 0,
	        textAlign: 'center'
	      }
	    },

	    // Core options
	    aspectRatio: 5 / 3,
	    layout: {
	      padding: {
	        bottom: 16,
	        right: 40,
	        left: 8,
	        top: 40
	      }
	    },
	    hover: {
	      mode: 'index',
	      intersect: false
	    },
	    elements: {
	      line: {
	        fill: false,
	        tension: 0.4
	      }
	    },

	    horizontalLine: [{
	      y: <?php echo (int)$objectif; ?>,
	      style: "#ef5b65",
	      text: "Objectif",
	      font : {
	      	size : '13'
	      }
	    }]
	};

	var ctx<?php echo $id?> = document.getElementById('canvas-bar<?php echo $id?>').getContext('2d');
	window.myBarr = new Chart(ctx<?php echo $id?>, {
		plugins: [ChartDataLabels],
		type: 'line',
		data: {
			labels: cLabels[0],
			datasets: dChartData
		},
		options: dChartOptions
		
	});

		
});
</script>
