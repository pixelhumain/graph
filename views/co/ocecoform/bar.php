<?php 
	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
		"/plugins/Chart-2.8.0/chartjs-plugin-datalabels.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>

<div id="" style="">
	<canvas style="position: relative" id="canvas-bar<?php echo $id?>"></canvas>
</div>
<script>

	var cData = <?php echo json_encode($data); ?>;
	var cLabel = "<?php echo $label; ?>";
	var cLabels = <?php echo (!empty($labels)? json_encode($labels): "[]"); ?>;
	var cColors = <?php echo json_encode($colors); ?>;
	var graphidk = "<?php echo $id?>";

    var unity = "<?php echo (!empty($unity)? $unity : ""); ?>";

    var numberWithCommas = function(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")+" "+unity;
    };

	jQuery(document).ready(function() {
	

	var dChartData = [{
		label: cLabel,
        data: cData,
        borderColor: cColors,
        backgroundColor: cColors,
        borderWidth: 4,
        datalabels : {
        	anchor: 'center',
	        align: 'end',
	        color: '#0e0d0d',
	        font : {
	        	size : 13,
	        	weight: 'bold'
	        },
            formatter: function(value, context) {
                return numberWithCommas(parseInt(value));
            }
        }
	}];
	
	var dChartOptions =  {
		// rotation: 1 * Math.PI,
  //       circumference: 1 * Math.PI,
        onClick: (c, i) => {
            /*const points = myBarr.getElementsAtEventForMode(click, 'nearest', { intersect: true }, true);
            console.log(myBarr.getElementAtEvent(click));
            if (points.length) {
                const firstPoint = points[0];
                const valueM = myBarr.data.labels[firstPoint];
                //console.log(valueM);
            }*/
            e = i[0];
            console.log(e._index)
            // var x_value = this.data.labels[e._index];
            aapgraphcallback("<?php echo $id?>", e._index);
        },
        interaction: {
            mode: 'nearest'
        },
        events: ['click'],
        legend: {
            display: false
        },
        tooltip: {
            enabled: false
        },
        responsive : true,
        scales: {
	        yAxes: [{
                display: false,
	            ticks: {
	                beginAtZero: true
	            }
	        }],
	        xAxes: [{
	            ticks: {
	                fontSize : '15'
	            }
	        }]
	    },
	    datalabels: {
	        formatter: (value, ctx) => {
	          return value !== 0 ?
	            value.toLocaleString( ) :
	            'N/A'
	        },
	        anchor: 'end',
	        align: 'end',
	        color: '#000',
	        font : {
	        	size : 25,
	        	weight: 'bold'
	        }
	    }
	};

	var ctx<?php echo $id?> = document.getElementById('canvas-bar<?php echo $id?>').getContext('2d');

	window.myBarr<?php echo $id?> = new Chart(ctx<?php echo $id?>, {
		plugins: [ChartDataLabels],
		type: 'bar',
		data: {
			labels: cLabels,
			datasets: dChartData
		},
		options: dChartOptions
		
	});

        function clickHandler(click){
            const points = myBarr.getElementsAtEventForMode(click, 'nearest', { intersect: true }, true);
            console.log(points);
            if (points.length) {
                const firstPoint = points[0];
                console.log(firstPoint)
            }
        }

        ctx<?php echo $id?>.onclick= clickHandler;

});
</script>
