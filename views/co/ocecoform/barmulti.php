<?php
$cssAnsScriptFilesTheme = array(
    "/plugins/Chart-2.8.0/Chart.min.js"
); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>

<div id="" style="">
    <canvas style="position: relative" id="canvas-bar<?php echo $id?>"></canvas>
</div>
<script>

    var cData = <?php echo json_encode($data); ?>;
    var cLabel = <?php if (is_array($label)){ echo json_encode($label);} else {echo '"'.$label.'"';}; ?>;
    var cLabels = <?php echo (!empty($labels)? json_encode($labels): "[]"); ?>;
    var cColors = <?php echo json_encode($colors); ?>;
    var graphidk = "<?php echo $id?>";

    const footer = function (tooltipItems) {
        let sum = 0;

        tooltipItems.forEach(function(tooltipItem) {
            sum += tooltipItem.parsed.y;
        });
        return 'total : ' + sum + +" €";
    };

    var numberWithCommas = function(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")+" €";
    };

    jQuery(document).ready(function() {


        var dChartData = [{
            label: cLabel,
            data: {
                "labels" : cLabels,
                "data" : cData,
            },
            borderColor: cColors,
            backgroundColor: cColors,
            borderWidth: 4,
            datalabels : {
                anchor: 'center',
                align: 'end',
                color: '#fff',
                font : {
                    size : 15,
                    weight: 'bold'
                }
            }
        }];

        var dChartOptions =  {
            type: 'bar',
            data: {
                labels: cLabels,
                datasets: cData
            },
            borderColor: cColors,
            backgroundColor: cColors,
            options: {
                tooltips: {
                    mode: 'label',
                    callbacks: {
                        title: function(tooltipItems, data) {
                            return data.labels[tooltipItems.index] + ' ';
                        },
                        label: function(tooltipItem, data) {
                            return data.datasets[tooltipItem.datasetIndex].label + ": " + numberWithCommas(tooltipItem.yLabel);
                        },
                    }
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        ticks: {
                            callback: function(value) { return value.substring(5, value.length); },
                        },
                        gridLines: { display: false },
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            callback: function(value) { return numberWithCommas(value); },
                        },
                    }],
                }, // scales
                legend: {display: true}
            }

        };

        var ctx<?php echo $id?> = document.getElementById('canvas-bar<?php echo $id?>').getContext('2d');

        window.myBarr = new Chart(ctx<?php echo $id?>, {
            type: 'bar',
            data: {
            labels: cLabels,
                datasets: cData
        },
            options: {
                tooltips: {
                    callbacks: {

                        label: function(tooltipItem, data) {
                            return data.datasets[tooltipItem.datasetIndex].label + ": " + numberWithCommas(tooltipItem.yLabel) ;
                        },
                        afterLabel: function(tooltipItem, data) {
                            var dataset = data['datasets'];
                            var percent = 0;
                            var total = 0;
                            $.each(dataset, function(dsid, ds){
                                total += parseInt(ds.data[tooltipItem.index]);
                            });
                            console.log("total", total);
                            percent =  Math.round((tooltipItem.yLabel / total) * 100);
                            return '(' + percent + '%)';
                        },
                        footer : function(tooltipItem , data) {
                            var allvalue = "";
                            var dataset = data['datasets'];
                            var total = 0;
                            $.each(dataset, function(dsid, ds){
                                console.log( tooltipItem, "data", ds);
                                allvalue += ds.label + " : " + numberWithCommas(parseInt(ds.data[tooltipItem[0].index])) + "\n";
                                total += parseInt(ds.data[tooltipItem[0].index]);
                            });

                            return allvalue + 'total : ' + numberWithCommas(total) ;
                        }
                    },
                    backgroundColor: '#FFF',
                    titleFontSize: 16,
                    titleFontColor: '#0066ff',
                    bodyFontColor: '#000',
                    bodyFontSize: 16,
                    displayColors: false,
                    footerFontColor : '#686565'
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        gridLines: { display: false },
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            callback: function(value) { return numberWithCommas(value); },
                        },
                    }],
                }, // scales
                legend: {display: true}
            }

        });

        function clickHandler(click){
            const points = myBarr.getElementsAtEventForMode(click, 'nearest', { intersect: true }, true);
            console.log(points);
            if (points.length) {
                const firstPoint = points[0];
                console.log(firstPoint)
            }
        }

        ctx<?php echo $id?>.onclick= clickHandler;

    });
</script>
