<?php 
	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
		"/plugins/Chart-2.8.0/chartjs-plugin-doughnutlabel.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>

<div id="container ocecoouter" style="margin:0px auto;width:100%">
	<canvas style="position: relative" id="canvas-bar<?php echo @$id?>"></canvas>
</div>
<script>
	var cData = <?php echo json_encode($data); ?>;
	var cLabel = "<?php echo $label; ?>";
	var cLabels = <?php echo (!empty($labels)? json_encode($labels): "[]"); ?>;
	var cColors = <?php echo json_encode($colors); ?>;
	jQuery(document).ready(function() {
	
	//Data
	mylog.log("render","/modules/graph/views/co/halfDoughnut.php");


	var hDChartData = [{
		label: cLabel,
        data: cData,
        backgroundColor: cColors,
        borderColor: cColors,
        borderWidth: 7
	}];
	
	var hDChartOptions =  {
		rotation: 1 * Math.PI,
        circumference: 1 * Math.PI,
        legend: {
            display: false
        },
        tooltip: {
            enabled: false
        },
        cutoutPercentage: 95,
        responsive: true,
        scales: {
        x: {
            grid: {
              offset: true
            }
        }
    	},
        plugins: {
	      doughnutlabel: {
	        labels: [
	          {
	            text: '<?php echo (int)$percentLabel."%" ?>',
	            font: {
	              size: '13',
	              weight : 'bold',
	              family : 'Raleway , sans-serif'
	            }
	          }
	        ]
	      }
    	}
    
	}

	var ctx = document.getElementById('canvas-bar<?php echo @$id?>').getContext('2d');
	window.myBar = new Chart(ctx, {
		type: 'doughnut',
		data: {
			labels: cLabels,
			datasets: hDChartData
		},
		options: hDChartOptions
		
	});

		
});
</script>
