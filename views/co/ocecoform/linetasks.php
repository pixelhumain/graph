<?php 
	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>

<div id="container ocecoouter" style="margin:0px auto;width:100%;">
	<canvas id="canvas-bar<?php echo $id?>"></canvas>
</div>
<script>
	window.chartColors = {
		red: 'rgb(255, 99, 132)',
		orange: 'rgb(255, 159, 64)',
		yellow: 'rgb(255, 205, 86)',
		green: 'rgb(75, 192, 192)',
		blue: 'rgb(54, 162, 235)',
		purple: 'rgb(153, 102, 255)',
		grey: 'rgb(201, 203, 207)'
	};


	var materialcolor = [
		window.chartColors.red , window.chartColors.orange , window.chartColors.yellow, window.chartColors.green, window.chartColors.blue, window.chartColors.purple, window.chartColors.grey
	];

	var COLORS = [
		"#0A2F62",
		"#0B2D66",
		"#064191",
		"#2C97D0",
		"#16A9B1",
		"#0AA178",
		"#74B976",
		"#0AA178",
		"#16A9B1",
		"#2A99D1",
		"#064191",
		"#0B2E68"
	];

	var randomScalingFactor = function() {
		return Math.round(Math.random() * 100);
	};

	jQuery(document).ready(function() {
	
	//Data
	mylog.log("render","/modules/graph/views/co/doughnut.php");


	var dChartData = [
	// {
		// label: ["Financé", 'Total à financer', "Reste"],
  //       data: [72, 105, 33],
  //       backgroundColor: [
  //       	'rgba(0, 0, 0, 1)',
  //           'rgba(46, 204, 113, 1)',
  //           'rgba(221, 221, 221, 1)'
  //       ],
  //       borderColor: [
  //       	'rgba(0, 0, 0, 1)',
  //           'rgba(46, 204, 113, 1)',
  //           'rgba(221, 221, 221, 1)'
  //       ],
  //       borderWidth: 7

	// }
			{
                label: 'Financé',
                backgroundColor: "rgba(0, 0, 0, 0)",
                borderColor: "rgba(0, 0, 0, 1)",
                data: [72, 100, 50, 10]
            }, {
                label: 'Total à financer',
                backgroundColor: "rgba(46, 204, 113, 0)",
                borderColor: "rgba(46, 204, 113, 1)",
                data: [105, 100, 60, 0]
            },
            {
                label: 'Reste',
                backgroundColor: "rgba(221, 221, 221, 0)",
                borderColor : 'rgba(224, 224, 0, 1)',
                data: [33, 40, 10, 10]
            }
	];
	
	var evoChartOptions =  {
		// rotation: 1 * Math.PI,
  //       circumference: 1 * Math.PI,
        legend: {
            display: false
        },
        tooltip: {
            enabled: false
        },
        responsive: true,
        scales: {
            xAxes: [{
                ticks: {
                    display: false //this will remove only the label
                },
                gridLines : {
		            display : false 
		        }
            }],
            yAxes: [{
                ticks: {
                    display: false //this will remove only the label
                },
                gridLines : {
		            display : false 
		        }
            }],
        }
	}

	var ctx = document.getElementById('canvas-bar<?php echo $id?>').getContext('2d');
	window.myBarr = new Chart(ctx, {
		type: 'line',
		data: {
			labels: ["Total financement", "financé", "Reste"],
			datasets: dChartData
		},
		options: evoChartOptions
		
	});

	// ctx.width = "260px";
	// ctx.height = "260px";

		
});
</script>
