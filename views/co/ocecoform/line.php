<?php 
	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
		"/plugins/Chart-2.8.0/chartjs-plugin-datalabels.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>

<div id="" style="margin:0px auto;width:100%;">
	<canvas id="canvas-bar<?php echo $id?>"></canvas>
</div>
<script>

	var cData = <?php echo json_encode($data); ?>;
	var cLabel = "<?php echo $label; ?>";
	var cLabels = <?php echo (!empty($labels)? json_encode($labels): "[]"); ?>;
	var cColors = <?php echo json_encode($colors); ?>;

	jQuery(document).ready(function() {
	

	var dChartData = [{
		label: cLabel,
        data: cData,
        borderColor: cColors,
        borderWidth: 4
	}];
	
	var dChartOptions =  {
		// rotation: 1 * Math.PI,
  //       circumference: 1 * Math.PI,
        legend: {
            display: false
        },
        tooltip: {
            enabled: false
        },
        responsive : true,
        scales: {
            xAxes: [{
                ticks: {
                    display: false //this will remove only the label
                },
                gridLines : {
		            display : false 
		        }
            }],
            yAxes: [{
                ticks: {
                    display: false //this will remove only the label
                },
                gridLines : {
		            display : false 
		        }
            }]
        },
        elements: {
            point:{
                radius: 0
            }
        }
	};

	var ctx<?php echo $id?> = document.getElementById('canvas-bar<?php echo $id?>').getContext('2d');
	window.myBarr = new Chart(ctx<?php echo $id?>, {
		type: 'line',
		data: {
			labels: cLabels,
			datasets: dChartData
		},
		options: dChartOptions
		
	});

		
});
</script>
