<meta charset="utf-8">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Baloo+2:wght@600&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@600&display=swap" rel="stylesheet">
<?php if (!$isAjax) { ?>
    <?php
    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile(Yii::app()->getModule("graph")->getAssetsUrl() . '/plugins/d3/d3.v6.min.js');
    $cs->registerScriptFile(Yii::app()->getModule("graph")->getAssetsUrl() . '/plugins/list-js/list.min.js');
    $cs->registerScriptFile(Yii::app()->getModule("graph")->getAssetsUrl() . '/plugins/svg-text/svg-text.js');
    $cs->registerScriptFile(Yii::app()->getModule("graph")->getAssetsUrl() . '/js/rect-collide.js');
    $cs->registerScriptFile(Yii::app()->getModule("graph")->getAssetsUrl() . '/js/graph.js');
    $cs->registerScriptFile(Yii::app()->request->baseUrl . '/plugins/jQuery/jquery-2.1.1.min.js');
    $cssJs = array(
        '/js/api.js',
        '/plugins/jquery.dynForm.js',
        '/plugins/bootstrap/css/bootstrap.min.css',
        '/plugins/bootstrap/js/bootstrap.min.js',
        '/plugins/bootbox/bootbox.min.js',
        '/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css',
        '/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js',
        '/plugins/blockUI/jquery.blockUI.js',
        '/plugins/jquery-cookie/jquery.cookie.js',
        '/plugins/jquery-validation/dist/jquery.validate.min.js',
        '/plugins/font-awesome/css/font-awesome.min.css',

    );
    HtmlHelper::registerCssAndScriptsFiles($cssJs, Yii::app()->request->baseUrl);
    HtmlHelper::registerCssAndScriptsFiles(['/css/graph.css', '/css/module.css'], Yii::app()->getModule("graph")->getAssetsUrl());

    $cssAnsScriptFilesModule = array(
        '/js/default/search.js',
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule("co2")->getAssetsUrl());

    ?>
<?php } ?>
<div id="content">
    <div id="side_menu" class="side_menu">
        <div class="burger_box">
            <div class="menu-icon-container">
                <a href="#" class="menu-icon js-menu_toggle closed">
                    <span class="menu-icon_box">
                        <span class="menu-icon_line menu-icon_line--1"></span>
                        <span class="menu-icon_line menu-icon_line--2"></span>
                        <span class="menu-icon_line menu-icon_line--3"></span>
                    </span>
                </a>
            </div>
        </div>
        <div id="submenu-container" style="display: block;">
            <span id="btn-toggle-search" class="fa fa-search submenu-icon"></span>
            <span id="btn-toggle-sort" class="fa submenu-icon fa-sort-alpha-asc"></span>
        </div>
        <div id="search-container" style="display: none;">
            <div class="btn-group w-100">
                <input id="search-input" type="search" class="form-control w-100">
                <span id="search-clear" class="fa fa-close"></span>
            </div>
        </div>
        <div class="h-100">
            <div class="panel-group h-100" id="accordion">
            </div>
        </div>
    </div>
    <div id="graph-container">
    </div>

    <script>
        activeModuleId = "graph";
        var debug = false;

        var baseUrl = "<?php echo Yii::app()->getRequest()->getBaseUrl(true); ?>";

        const data = <?= json_encode($data) ?>;

        console.log(JSON.stringify(data))

        var tooltip = new GraphTooltip("graph-container");
        tooltip.defaultImage = "<?php echo $defaultImage ?>";
        var relationGraph = new RelationGraph(data)
        relationGraph.setOnClickNode(selectNode)
        relationGraph.setOnZoom(correctSize)
        relationGraph.setAfterDraw(() => {
            correctSize()
        })
        relationGraph.draw("graph-container")


        function selectNode(event, dataItem) {
            tooltip.node = d3.select(event.target);
            tooltip.setContent(dataItem);
            tooltip.goToNode()
            tooltip.show()
        }


        function selectNodeUsingMenu(id) {
            const target = (d3.select(id));
            tooltip.setNode(target, target.node().__data__);
            tooltip.show()
        }

        function openMenu(baseLink, id, type) {
            if (!baseLink)
                return;
            let url = baseLink;
            if (id)
                url += "/id/" + id
            if (type)
                url += "/type/" + type
            window.location.href = baseUrl + "/" + url
        }

        function openUrl(url) {
            // const fullUrl = baseUrl + '/' + url;
            // if (typeof $ != "undefined")
            //     ajaxPost("#content", fullUrl, null, (data) => {
            //         history.pushState(null, null, "/" + url);
            //     });
            // else
            // REMETTRE "/ph/"+url; en prod
            window.location.href = fullUrl;
        }

        window.onpopstate = function(event) {
            if (typeof $ != "undefined")
                ajaxPost("#content", document.location.href);
            else
                // REMETTRE "/ph/"+url; en prod
                window.location.href = fullUrl;
        };
        window.onresize = () => {
            correctSize();
        }

        function correctSize() {
            tooltip.follow();
        }
    </script>
</div>
</div>