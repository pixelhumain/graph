<?php
    $graphAssets = [
        '/plugins/d3/d3.v6.min.js',
        '/js/graph.js','/css/graph.css'
    ];
    HtmlHelper::registerCssAndScriptsFiles($graphAssets, Yii::app()->getModule("graph")->getAssetsUrl());
?>
<?php if($preview){ ?>
    <style>
    #modal-preview-coop{
        left: 0;
        box-shadow: none !important;
    }

    #close-graph-modal{
        border-radius: 50px;
        border: 4px solid #f5f5f5;
        font-size: 16pt;
    }
</style>
<?php } ?>
<div class="community-graph-title">
    <?php if($preview){ ?>
        <button id="close-graph-modal" class="btn btn-default pull-right btn-close-preview">
            <i class="fa fa-times"></i>
        </button>
    <?php } ?>
    <h3><?= $title["name"] ?></h3>
    <ol class="breadcrumb"></ol>
</div>
<div id="communityNode" style="width: 100%">
</div>

<script>
    var element = <?= json_encode($element); ?>;
    var width = 1024, height = 600;
    var communityGraph<?= (string)$title["_id"] ?> = null;
    $(function(){
        communityGraph<?= (string)$title["_id"] ?> = new CommunityNodeGraph(element, '.breadcrumb');
        communityGraph<?= (string)$title["_id"] ?>.draw("#communityNode");

        coInterface.bindLBHLinks();
        /*data.svg = d3.select("#communityNode")
            .append("svg").attr("viewBox", [0, 0, width, height]);
        update();*/
    });

    /*function ticked(){
        data.links.attr("x1", function (d) {
            return d.source.x;
        })
            .attr("y1", function (d) {
                return d.source.y;
            })
            .attr("x2", function (d) {
                return d.target.x;
            })
            .attr("y2", function (d) {
                return d.target.y;
            });
        //same for the nodes, using a functor:
        data.nodes.attr("transform", function (d) {
            return "translate(" + d.x + "," + d.y + ")";
        });

    }

    function update(){
        var nodesAndLinks = getNodeAndLinks(data.data);
        var nodes = nodesAndLinks.nodes.reverse();
        var links = nodesAndLinks.links;
        data.svg.selectAll('g').remove();
        data.links = data.svg.append("g")
            .attr("stroke", "#999")
            .attr("class", "links")
            .attr("stroke-opacity", 0.6)
            .selectAll()
            .data(links)
            .join("line")
            .attr("class", d => {
                return "links"+d.source.id;
            })
            .attr("stroke-width", 2);


        data.nodes = data.svg.append("g").selectAll("g")
            .attr("class", "nodes")
            .data(nodes)
            .join((enter) => {
                let g = enter.append("g");
                g.append("circle")
                    .attr("r", d => {
                        return d.group == "root" ? 15 : (typeof d.size != "undefined" ? (d.size < 10 ? 10 : (d.size > 30 ? 30 : d.size)) : 10);
                    })
                    .attr("fill", d => d.group == "root" ? "green" : color(d.group));
                g.append("text").attr("x", d =>  d.group == "root" ? 15 : (typeof d.size != "undefined" ? (d.size < 10 ? 10 : (d.size > 30 ? 30 : d.size)) : 10)).attr("y", 5).text(function(d){
                    return d.group == "root" ? "" : (typeof d.profil != "undefined" && d.profil != null ? "" : d.name);
                })
                g.append("image").attr("xlink:href", d => {
                    return typeof d.profil != "undefined" ? d.profil : "";
                })
                    .attr("x", d => d.group == "root" ? "-15px" : (typeof d.size != "undefined" ? (d.size < 10 ? "-10px" : (d.size > 30 ? "-30px" : `-${d.size}px`)) : "-10px"))
                    .attr("clip-path",'inset(0 0 0 0 round 50%)')
                    .attr("y", d => d.group == "root" ? "-15px" : (typeof d.size != "undefined" ? (d.size < 10 ? "-10px" : (d.size > 30 ? "-30px" : `-${d.size}px`)) : "-10px"))
                    .attr("width", d => d.group == "root" ? "30px" : (typeof d.size != "undefined" ? (d.size < 10 ? "20px" : (d.size > 30 ? "60px" : `${d.size*2}px`)) : "20px"))
                    .attr("height", d => d.group == "root" ? "30px" : (typeof d.size != "undefined" ? (d.size < 10 ? "20px" : (d.size > 30 ? "60px" : `${d.size*2}px`)) : "20px"))
                return g;
            });

        data.nodes.call(d3.drag()
            .on("start", dragstarted)
            .on("drag", dragged)
            .on("end", dragended));
        data.nodes.on("click", function(event, d){
            if(d.children){
                d._children = d.children;
                d.children = null;
            } else if(d._children) {
                d.children = d._children;
                d._children = null;
            }else if (d.id.match(/^[0-9a-fA-F]{24}$/)) {
                getAPIData(d.group, d.id, d);
            }
            update()
        })
        data.force = d3.forceSimulation(nodes)
            .force('link', d3.forceLink(links).id((d) => {
                return d.id;
            }).distance(d => {
                var dist;
                if(d.source.group == "root"){
                    dist = 100;
                }else if(typeof d.target.size != "undefined"){
                    dist = (d.target.size > 150) ? 150 : (d.target.size < 10 ? d.target.size * 10 : d.target.size * 2);
                }else if(typeof d.source.children != "undefined" && d.source.children != null){
                    dist = (d.source.children.length > 10) ? 5*d.source.children.length : 50;
                }else{
                    dist = 50;
                }
                return dist;
            }))
            .force("collide", d3.forceCollide(11).iterations(4))
            .force("charge", d3.forceManyBody().strength(-100))
            .force("x", d3.forceX(width/2))
            .force("y", d3.forceY(height/2))
            /!*.force('position', d3
                .forceRadial(
                    (d) => (d.group == "root" || !d.id.match(/^[0-9a-fA-F]{24}$/) ? width*0.01 : width * 0.5),
                    width / 2,
                    height / 2
                )
                .strength(0.1)
            )*!/
            .force("radial", d3.forceRadial((width / 2)*0.8, width / 2, height / 2))
            .on("tick", ticked)

        /!*data.force.nodes(nodes).on("tick", ticked);
        data.force.force('link').links(links).distance(d => {
            var dist = 100;
            if(d.source.group == "root"){
                dist = 200;
            }else if(typeof d.source.children != "undefined" && d.source.children != null){
                dist = (d.source.children.length > 10) ? 5*d.source.children.length : 50;
            }else{
                dist = 50;
            }
            return dist;
        })*!/
    }

    function getAPIData(getData, id, data){
        ajaxPost(null,
            baseUrl+"/graph/co/communitydata",
            {
                "type": getData,
                id: id
            },
            function(response){
                if(response.children) {
                    data.children = response.children;
                    /!*data.parent.oldChild = data.parent.children;
                    data.parent.children = data;*!/
                    update();
                }
            },
            function(error){

            }
        )
    }

    function getNodeAndLinks(data){
        var nodes = {};
        var links = [];
        function recurse(node){
            if(node.children){
                node.children.forEach((elt) => {
                    links.push({source: node.id, target: elt.id})
                    recurse(elt);
                })

            }
            nodes[node.id] = node;
        }
        recurse(data);

        return {
            nodes: Object.values(nodes),
            links: links
        };
    }

    function dragstarted(event) {
        if (!event.active) data.force.alphaTarget(0.5).restart();

        event.subject.fx = event.subject.x;
        event.subject.fy = event.subject.y;
    }

    // Update the subject (dragged node) position during drag.
    function dragged(event) {
        event.subject.fx = event.x;
        event.subject.fy = event.y;
    }

    // Restore the target alpha so the simulation cools after dragging ends.
    // Unfix the subject position now that it’s no longer being dragged.
    function dragended(event) {
        if (!event.active) data.force.alphaTarget(0);
        event.subject.fx = null;
        event.subject.fy = null;
    }*/
    function getAPIData(getData, id, data){
        if(["projects", "organizations", "citoyens"].includes(getData) || (typeof data.type != "undefined" && ["projects", "organizations", "citoyens"].includes(data.type))){
            var type = getData;
            if(typeof data.type != "undefined"){
                type = data.type;
            }
            urlCtrl.openPreview(`#page.type.${type}.id.${id}`);
        }else{
            var params = {
                "type": getData,
                id: id
            };
            ajaxPost(null,
                baseUrl+"/graph/co/communitydata",
                params,
                function(response){
                    if(response.children) {
                        data.children = response.children;
                        graph._addBreadcrumbs(data.parent);
                        graph.update();
                    }
                },
                function(error){

                }
            )
        }
    }
</script>