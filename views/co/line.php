<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

?>

<script type="text/javascript">
window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};


	var MONTHS = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];

	var COLORS = [
	"#0A2F62",
	"#0B2D66",
	"#064191",
	"#2C97D0",
	"#16A9B1",
	"#0AA178",
	"#74B976",
	"#0AA178",
	"#16A9B1",
	"#2A99D1",
	"#064191",
	"#0B2E68"
	];
</script>

<div style="width:100%;">
		<canvas id="canvas-line"></canvas>
	</div>
	<script>
	var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};
		var config = {
			type: 'line',
			data: {
				labels: [
					"janvier 2018",
					"mai 2018",
					"septembre 2018",
					"février 2019",
					"juillet 2019"
				],
				datasets: [ {
					label: 'Oceatoon',
					//backgroundColor: window.chartColors.red,
					borderColor: window.chartColors.red,
					data: [3,5,8,19,80],
					//fill: true,
				},
				{
					label: 'Raphael',
					//backgroundColor: window.chartColors.blue,
					borderColor: window.chartColors.blue,
					data: [-53,45,28,219,-80],
					//fill: true,
				},
				{
					label: 'Bouboule',
					//backgroundColor: window.chartColors.orange,
					borderColor: window.chartColors.orange,
					data: [53,-45,-28,-219,80],
					//fill: true,
				}]
			},
			options: {
				responsive: true,
				
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'par Mois'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Activité'
						}
					}]
				}
			}
		};
jQuery(document).ready(function() {
			var ctx = document.getElementById('canvas-line').getContext('2d');
			window.myLine = new Chart(ctx, config);
		});
	</script>
