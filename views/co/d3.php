<meta charset="utf-8">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Baloo+2:wght@600&display=swap" rel="stylesheet">
    <?php
    $graphAssets = [
        '/plugins/d3/d3.v6.min.js',
        '/js/graph.js'
    ];
    HtmlHelper::registerCssAndScriptsFiles($graphAssets, Yii::app()->getModule("graph")->getAssetsUrl());

    $cssJs = array(
        '/plugins/jQuery/jquery-2.1.1.min.js',
        '/js/api.js',
        '/plugins/bootstrap/css/bootstrap.min.css',
        '/plugins/bootstrap/js/bootstrap.min.js',
        '/plugins/bootbox/bootbox.min.js',
        '/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css',
        '/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js',
        '/plugins/blockUI/jquery.blockUI.js',
        '/plugins/jquery-cookie/jquery.cookie.js',
        '/plugins/jquery-validation/dist/jquery.validate.min.js',
        '/plugins/font-awesome/css/font-awesome.min.css',
    );
    HtmlHelper::registerCssAndScriptsFiles($cssJs, Yii::app()->request->baseUrl);
    ?>

    <style>
        #graphtags {
            padding-top: 20px;
            background-color: #fff;
            float: left;
            height: 600px;
            width: 20%;
            display: none;
        }

        #graphtags a {
            color: #333;
            text-decoration: none;
        }

        #sectionList a {
            color: red;
            text-decoration: none;
        }

        #search {
            float: right;
            margin-right: 100px;
        }

        #title {
            background-color: #eee;
            height: 80px;
            font-size: 2em;
            padding: 15px;
        }
    </style>
    <div id='title'>

        <?php
        $l = $title;
        if (!empty(@$link))
            $l = '<a class="lbh" data-dismiss="modal" href="/' . $link . '">' . $title . ' <i class="fa fa-link"></i></a>';
        ?>

        <div class='pull-left'>
            <div class="dropdown" style="display: inline-block;">
                <a href="#" class="dropdown-toggle padding-5" style="padding-top: 7px !important;" data-toggle="dropdown" role="button">
                    <i class="fa fa-bars"></i></a>
                <ul class="dropdown-menu arrow_box dropdown-languages-nouser" role="menu">
                    <li class="bold">View</li>
                    <li class="active"><a href="javascript:;">Graph</a></li>
                    <li><a href="javascript:;" onclick="openMenu('graph/co/circle', '<?= (string) $item['_id'] ?>', '<?= $type ?>')">Circle Packing</a></li>
                    <li><a href="javascript:;" onclick="alert('todo')">MindMap</a></li>
                    <li><a href="javascript:;" onclick="alert('todo')">Finder</a></li>
                    <li class="bold">Filters</li>
                    <li><a href="javascript:;" onclick="toggleSideGraph('#graphtags')">tag List</a></li>
                    <li><a href="javascript:;" onclick="alert('todo')">community</a></li>
                    <li><a href="javascript:;" onclick="alert('todo')">projects</a></li>
                    <li class="bold">Graph By</li>
                    <li><a href="javascript:;" onclick="alert('todo')">links by projects</a></li>
                    <li><a href="javascript:;" onclick="alert('todo')">activity</a></li>
                </ul>
            </div>
            <?php echo $l ?>
        </div>
    </div>

    <div id="graphtags">
        <a href="javascript:;" class="pull-right " onclick=""><i class="fa fa-times"></i></a>
        <div id="sectionList"></div>
    </div>

    <div id="graph-container">
    </div>

    <script>
        var tags = <?php echo json_encode($tags); ?>;
        var nodes_data = <?php echo json_encode($data); ?>;
        console.log("data", nodes_data);
        console.log("tags", tags);
        console.log("list", <?php echo json_encode(@$list); ?>);
        tags.forEach(function(t) {
            if (t != "") {
                document.getElementById("graphtags").innerHTML = document.getElementById("graphtags").innerHTML + "<a href=\"javascript:openUrl('graph/co/search/tag/" + encodeURIComponent(t) + "')\">#" + t + "</a><br/>";
            }
        })
        function circleSize(d) {
            console.log("SIZE", d);
            r = 10;
            if (d.data.group == "root" || d.data.type == "group")
                return 20;
            if (d.data.linkSize > 0)
                r += d.data.linkSize;
            if (r > 30)
                r = 30;
            return r;
        }

        function color(d, i, n){
            if (d.data.type == "group") {
                return "#c62f80";
            }
            if (d.data.group == "root") {
                return "black";
            }
            if (d.data.group == "TAGS") {
                return "steelblue";
            }
            if (d.data.group == "PROJECTS") {
                return "purple";
            }
            switch (d.data.type) {
                case "citoyens": return "#FFC600";
                case "organizations": return "#93C020";
                case "projects": return "purple";
                case "events": return "#FFA200";
            }
            return "#cccccc"
        }

        function groupIcons(d,i,n) {
            console.log(d);
            if(d.data.type == "root"){
                return "fa fa-home";
            }
            switch (d.data.label.toLowerCase()) {
                case "tags":
                    return "fa fa-tags";
                case "projects":
                    return "fa fa-lightbulb-o";
                case "friends":
                    return "fa fa-user";
                case "memberOf":
                    return "fa fa-users";
            }
            return "fa fa-users"
        }


        function selectNode(event, d) {
            const id = d.data.id;
            if (d.data.type == 'root')
                return;
            else if (d.data.group == "TAGS") {
                $("#graphtags").css("width", "20%");
                $("#graph").css("width", "80%");
                $("#graphtags").css("display", "block");
            } else if (d.data.level == 1) {
                $("#graphtags").css("width", "20%");
                $("#graph").css("width", "80%");
                $("#graphtags").toggleClass("display", "block");
                document.getElementById("sectionList").innerHTML = "<b>" + d.data.label + "</b><br/>";

                links_data.forEach(function(t) {
                    if (t.source.id == id) {
                        document.getElementById("sectionList").innerHTML += "<a href='javascript:openUrl(\"graph/co/d3/id/" + t.target.id + "/type/" + t.target.type + "\")'> " + t.target.label + "</a><br/>";
                    }

                })
                document.getElementById("sectionList").innerHTML += "<br/><br/>"
            } else if (id.length > 20) {
                openUrl("graph/co/d3/id/" + id + "/type/" + d.data.type);
                //alert( id+"/"+(d.data.group-1)+"/"+types[d.data.group-1] );
            } else if (d.data.type == "tag") {
                openUrl("graph/co/circle/type/tags/id/" + d.data.label);
            }
        }
        

        setTimeout(() => {
            graph = new NetworkGraph(nodes_data)
            graph.draw('#graph-container')
            graph.setCircleSize((d) => circleSize(d))
            graph.setColor(color)
            graph.setGroupIcon(groupIcons)
            graph.setOnClickNode(selectNode)
        },200)
        
    </script>


    <script>
        function openUrl(url) {
            const fullUrl = window.location.origin + '/' + url;
            window.location.href = fullUrl;
        }

        
        function toggleSideGraph(id) {
            if ($(id).css('display') == "none") {
                $(id).css('display', 'block');
                $('#graph').css('display', 'none');
            } else {
                $(id).css('display', 'none');
                $('#graph').css('display', 'block');
            }
        }

        function runScript(e) {
            if (e.keyCode == 13) {
                s = document.getElementById("search").value;
                if (s.indexOf("#") == 0)
                    openUrl("graph/co/search/tag/" + s.substring(1));
                else if (s.indexOf(".") == 0)
                    openUrl("graph/co/search/geo/" + s.substring(1));
                else if (s.indexOf(">") == 0)
                    openUrl("graph/co/search/type/" + s.substring(1));
                else
                    openUrl("graph/co/search/q/" + s);
            }
        }
    </script>