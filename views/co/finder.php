<?php
$cssAnsScriptFilesTheme = array(
  '/plugins/finderjs-1.3.1/example/site.css',
  '/plugins/finderjs-1.3.1/example/finderjs.css',
  '/plugins/finderjs-1.3.1/build/jquery.finder.js',
);

HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->getModule( "graph" )->getAssetsUrl());

$title = "<span class='text-red'>Contenu Manquant</span>".$action ;
$url = "";
$faIcon = "";
$params = Yii::$app->request->getQueryParams();
if( "cte/all" == Yii::app()->controller->id."/".$action ){
    $url = Yii::app()->controller->id."/".$action."/slug/".$slug."/view/1";
}
else if(isset($params['slug'])){
    $el = Slug::getElementBySlug($slug);
    $title = $el["el"]['name'];
    $url = "api/children/slug/".$slug;
    switch($el["type"]) {
      case "organizations" : $faIcon = "fa-circle-o";
      break;
      case "citoyens" : $faIcon = "fa-user";
      break;
      case "events" : $faIcon = "fa-calendar";
      break;
      case "projects" : $faIcon = "fa-lightbulb-o";
      break;
      default : $faIcon = "fa-search";
      break;
    }
} else if($action == "badge"){
    $url = "cte/badge";
    $title = "Domaine d'actions";
} else if(isset($params['tag'])) {
  $title = $tag;
  $url = "api/children?tag=".$tag;
  $faIcon = "fa-hashtag";
} else if(isset($params['answerid'])) {
  $answerData = PHDB::findOneById(Answer::COLLECTION, $params['answerid']);
  $contextId = array_keys($answerData['context'])[0];
  $contextType = $answerData['context'][$contextId]['type'];
  if(isset($answerData['links']['updated'])) 
    $title = $answerData['context'][$contextId]['name'].", reponse de ".$answerData['links']['updated'][$answerData['user']]['name'];
  else {
    $u = PHDB::findOneById(Person::COLLECTION, $answerData['user'], array('name', 'slug', 'id'));
    $title = $answerData['context'][$contextId]['name'].", reponse de ".$u['name'];
  }
  // let_dump($contextType);
  $url = "api/children?answerid=".$params['answerid'];
  $faIcon = "fa-wpforms";
} else if(isset($params["coformid"])) {
  $formData = PHDB::findOneById(Form::COLLECTION, $params['coformid']);
  $contextId = array_keys($formData['parent'])[0];
  $contextType = $formData['parent'][$contextId]['type'];
  $title = $formData['name'];
  $url = "api/children?coformid=".$params['coformid'];
  $faIcon = "fa-wpforms";
} else if(isset($params["ocecoformid"]) && isset($params["parentformoceco"])) {
  $formData = PHDB::findOneById(Form::COLLECTION, $params['ocecoformid']);
  $contextId = array_keys($formData['parent'])[0];
  $contextType = $formData['parent'][$contextId]['type'];
  $title = $formData['name'];
  $url = "api/children?ocecoformid=".$params['ocecoformid']."&parentformoceco=".$params['parentformoceco'];
  $faIcon = "fa-wpforms";
} else if(isset($params["answerocecoid"])) {
  $answerData = PHDB::findOneById(Form::ANSWER_COLLECTION, $params['answerocecoid']);
  $contextId = array_keys($answerData['context'])[0];
  $contextType = $answerData['context'][$contextId]['type'];
  $title = isset($answerData['answers']) ? $answerData['answers']['aapStep1']['titre'] : 'Projet';
  $url = "api/children?answerocecoid=".$params['answerocecoid'];
  $faIcon = "fa-wpforms";
}
?>

<style>
  .d-flex {
    display: flex;
  }
  .mt-2 {
    margin-top: 1em;
  }
  .mt-4 {
    margin-top: 2em;
  }
  .mb-0 {
    margin-bottom: 0 !important;
  }
  .pb-0 {
    padding-bottom: 0 !important;
  }
  .ml-6p {
    margin-left: 6px;
  }
  .pt-2p {
    padding-top: 2px;
  }
  .fixed-height {
    max-height: 470px;
    width: 104%;
    overflow-y: auto;
  }
  .my-leaf-row {
    display: flex;
    flex-direction: column;
    font-size: 1.3em;
    text-align: center;
  }
  /* .my-leaf-row .fa {
    color: #A7A7A7;
    font-size: 2em;
    margin: 10px 0;
  } */
  /* .fjs-col:first-child:nth-last-child(2),
  .fjs-col:first-child:nth-last-child(3) {
    min-width: 234px !important;
  } */
  .fjs-col:first-child:nth-last-child(2),
  .fjs-col:first-child:nth-last-child(3),
  .fjs-col:first-child:nth-last-child(4)
  {
    min-width: 234px !important;
  }
  .fjs-col:first-child:nth-last-child(3) ~ .fjs-col:nth-of-type(2) {
    max-width: 270px;
  }
  .fjs-col:first-child:nth-last-child(4) ~ .fjs-col:nth-of-type(2) {
    max-width: 270px;
  }
  .fjs-col:first-child:nth-last-child(4) ~ .fjs-col:nth-of-type(3) {
    max-width: 270px;
  }
  .history-tl-container ul.tl li {
    margin-left: 10px !important;
  }
  ul.tl li .item-detail {
    font-size: 14px !important;
  }
  .d-none {
    display: none !important;
  }
  .mainDash {
    background-color: #fff !important;
  }
  .survey-container {
    padding-top: 1px !important;
  }
  .breadcrumb>li+li:before {
    padding: 0 5px;
    color: #7da53d;
    content: '\f101';
    font-family: FontAwesome;
  }
  .section-container .breadcrumb>li {
    color: #333333 !important;
  }
  .fts-18p {
    font-size: 18px !important;
  }
</style>

<main class="site-content">
  
  <section class="example">
    
    <div class="section-container finder-container">
       <div class="col-xs-12" style="margin-bottom: 10px">
            <div class="pull-left d-flex" style="font-size:28px;">
                <i class="fa <?php echo $faIcon ?> pt-2p"></i> <span id="breadcrumb"><?php echo $title ?> </span>
            </div>
            <div class="pull-right d-flex">
                <input type="text" id="Findersearch" name="search" style="padding-right: 20px"/>
                <i class="fa fa-search" style="margin-top: 5px; margin-left: -19px"></i>
           </div>
        </div>
            <div id="container2"></div>
        </div>
    </div>
  </section>

</main>



<script type="text/javascript">
let emitter = null;
let filAriane = [];

$(document).ready(function() {
  console.log("build","co finder");
  container = document.getElementById('container2');
  
  let title = <?php echo json_encode($title) ?>;
  if(title != null){
    title = title.toLowerCase().trim().replace(/^\w/, (c) => c.toUpperCase());
    filAriane.push(title);
  }
  
  $('#Findersearch').on('keyup', function(e){
      let query = $.trim($(this).val()).toLowerCase();
      
      $('li.fjs-item').each(function(){
           let $this = $(this).find('.fjs-item-content')[0];
           //console.log(">>>>>>>>>>>>>>  ",$this.firstChild.data );
          if($this.firstChild.data.toLowerCase().indexOf(query) === -1)
            $(this).fadeOut();
          else 
            $(this).fadeIn();
      });
  });

  

  setTimeout(function() { 
    emitter = finder(container, remoteSource,{
      createItemContent:createItemContent
    } );

    
    emitter.on('item-selected', function(item) {
        console.warn('Item selected', item.item["_item"]);
        $('#breadcrumb').empty();
        // filAriane.length = 1;
        let breadCrumb = `<ul id="breadcrumblist" class='breadcrumb bg-white d-block mb-0 pb-0'><li>${filAriane[0]}</li>`;
        $('#container2').find('.fjs-active').each(function(index) {
          let label = $(this).text();
          if(label.indexOf("(") > -1) 
            label = label.substring(0, label.indexOf("(") - 1);
          
          breadCrumb += `<li>${label.toLowerCase().trim().replace(/^\w/, (c) => c.toUpperCase())}</li>`
          // filAriane.push(label);
        });
        breadCrumb += `</ul>`;
        $("#breadcrumb").html(breadCrumb);
        
        if( item.item["_item"].open == "column" ){
            emitter.emit( 'create-column' , createSimpleColumn(item.item["_item"]) );
            let idUtil = null;

            if(item.item["_item"].id)
              idUtil = item.item["_item"].id;
            if(item.item["_item"].id.$id)
              idUtil = item.item["_item"].id.$id;

            emitter.emit('column-created', getAPropos(item.item["_item"].type, idUtil))
        } else if(item.item["_item"].open == "column-new") {
          emitter.emit( 'create-column' , createSimpleColumn(item.item["_item"]) );
          let idUtil = item.item["_item"].id;
          
          console.log("id", idUtil);
          <?php if(isset($contextId)) {
          ?>
            emitter.emit('column-created', getResponse(idUtil, <?php echo json_encode($contextId) ?>, <?php echo json_encode($contextType) ?>))
          <?php 
          }
          ?>
        } else if(item.item["_item"].open == "column-answers") {
          emitter.emit( 'create-column' , createSimpleColumn(item.item["_item"]) );
          let idUtil = null;

          if(item.item["_item"].id)
            idUtil = item.item["_item"].id;
          if(item.item["_item"].id.$id)
            idUtil = item.item["_item"].id.$id;

          emitter.emit('column-created', getResponses(idUtil))
        } else if(item.item["_item"].open == "column-oceco-answer") {
          emitter.emit( 'create-column' , createSimpleColumn(item.item["_item"]) );
          let idUtil = null;

          if(item.item["_item"].id)
            idUtil = item.item["_item"].id;
          if(item.item["_item"].id.$id)
            idUtil = item.item["_item"].id.$id;

          emitter.emit('column-created', getOcecoResponses(idUtil))
        } else if( item.item["_item"].open == "page" ){
            window.open(baseUrl+'/costum/co/index/id/ctenat#"@'+item.item["_item"].slug);
            //TODO : preventDefault event
        } else if( item.item["_item"].open == "finder" ){
            window.location.href = baseUrl+'/graph/<?php echo Yii::app()->controller->id ?>/finder/slug/'+item.item["_item"].slug+"/open/element";
            //TODO : preventDefault event
        } else if(item.item["_item"].open == "finder-tag") {
          // window.location.href = `${baseUrl}/graph/<?php echo Yii::app()->controller->id ?>/finder?tag=${item.item["_item"].label}&open=element`;
          window.open(`${baseUrl}/graph/<?php echo Yii::app()->controller->id ?>/finder?tag=${item.item["_item"].label}&open=element`, "_blank")
        } else if(item.item["_item"].open == "finder-coform") {
          window.location.href = `${baseUrl}/graph/<?php echo Yii::app()->controller->id ?>/finder?formid=${item.item["_item"].id}&open=element`;
        }
     });

   }, 500);
  
});


//url: 'http://127.0.0.1/ph/api/person/get/id/585bdfdaf6ca47b6118b4583/format/finder' ,
//url: 'http://127.0.0.1/ph/graph/co/linear/id/5c90ac1e539f2229268b4573/type/organization/view/1' ,  
function getAPropos(type, id) {
  let url = `${baseUrl}/co2/element/about/type/${type}/id/${id}?tpl=ficheInfoElement`;
  ajaxPost(
    ".finder-container #detail",
    url,
    {},
    function() {

    },
    function(error) {
      mylog.log(error)
    },
    "html"
  )
}
function getResponse(id, contextId, contextType) {
  let url = `${baseUrl}/survey/answer/index/id/${id}/mode/r/contextId/${contextId}/contextType/${contextType}`;
  ajaxPost(
    ".finder-container #detail",
    url,
    {},
    function() {
      $(".finder-container a.lbh").addClass("d-none")
    },
    function(error) {
      mylog.log(error)
    },
    "html"
  )
}
function getResponses(id) {
  let url = `${baseUrl}/survey/answer/dashboard/answer/${id}`;
  ajaxPost(
    ".finder-container #detail",
    url,
    {},
    function() {
      $('.finder-container .surveys.grid').addClass('list');
      $('.finder-container #answerList').removeClass('grid');
      $('.finder-container #answerList').addClass('list');
      
      $('.finder-container #displayOptions, .finder-container #btn-configform, .finder-container #btn-graph, .finder-container #btn-actiongroup, .finder-container #btnsort, .finder-container #searchinput, .finder-container #itemperpage, .finder-container #list-pagination').addClass("d-none")
    },
    function(error) {
      mylog.log(error)
    },
    "html"
  )
}
function getOcecoResponses(answerId) {
  let url = `${baseUrl}/costum/co/oceco/formid/${answerId}`;
  ajaxPost(
    "",
    url,
    {},
    function(data) {
      let html = `
        <div class="row list-group-item" style="margin:5px" data-label="nouv." id="list-${data['_id']['$id']}" data-id="${data['_id']['$id']}">
          <div class="">
            <div class="media col-md-4 col-lg-4 ">
              <div class="contain-img-left">
                <figure class="">
                  <a href="javascript:;" class="aapgetaapview aaptitlegoto" data-id="${data['_id']['$id']}">
                    <img src="${data['profilImageUrl'] ? data['profilImageUrl'] : '/assets/f04f76ee/images/thumbnail-default.jpg'}" alt="projet" class="img-responsive " style="width: 100%;">
                  </a>
                </figure>
              </div>
            </div>  
            <div class="col-md-8 col-lg-8 col-xs-12 padding-top-5">
              <h4 class="list-group-item-heading">
                  <a href="javascript:;" class="aapgetaapview aaptitlegoto minimalist-name" data-id="${data['_id']['$id']}" style="display:inline"> ${data['answers']['aapStep1']['titre']} </a>
              </h4>

              <div class="list-item-progress prop-compact-progression" style="display:none">
                <div class="progress" data-percentage="0">
                    <span class="progress-left">
                        <span class="progress-bar"></span>
                    </span>
                    <span class="progress-right">
                        <span class="progress-bar"></span>
                    </span>
                    <div class="progress-value">
                        <div>
                            --<br>
                            <span><i class="fa "></i></span>
                        </div>
                    </div>
                </div>
              </div>
              <small>
                <i class="fa fa-user" style="color: #7da53d"></i> 
                Par <b><i>${data['user']['name'] ? data['user']['name'] : 'Non renseigné'}</i></b>
                &nbsp;&nbsp;&nbsp;&nbsp; 
                Le  <i class="fa fa-calendar-o" style="color: #7da53d"></i> <b>
                <i>${data['created_at'] ? data['created_at'] : data['created']}  </i></b>
                &nbsp; à &nbsp;
                <i class="fa fa-clock-o" style="color: #7da53d"></i> <b>
                <i>${data['created_at_h'] ? data['created_at_h'] : data['created']} </i></b> 
              </small><br>
              <hr style="border-top: 2px dashed #3f4e58;margin-bottom: 10px;">
              <small>
                  <i><b>
                                                                              </b></i>
              </small>                                  
              <p class="list-group-item-text project-description list-item-description list-markdown">
                <p>${data['answers']['aapStep1']['description']}</p>
              </p>
                                            
              <ul class="tag-list margin-top-5">
              </ul>
            </div>
          </div>
        </div>`;
          $(".finder-container #detail").html(html)
    },
    function(error) {
      mylog.log(error)
    },
    null
  )
}
function getOcecoAnswer(id) {
  let url = `${baseUrl}/survey/answer/index/id/${id}/mode/r`;
  ajaxPost(
    ".finder-container #detail",
    url,
    {},
    function() {},
    function(error) {
      mylog.log(error)
    },
    "html"
  )
}
$(".finder-container #container2").on('DOMSubtreeModified', '#detail', function() {
  $(".finder-container a.lbh").addClass('d-none')
  $('.finder-container .surveys.grid').addClass('list');
  $('.finder-container #answerList').removeClass('grid');
  $('.finder-container #answerList').addClass('list');
  $('.finder-container #displayOptions, .finder-container #btn-configform, .finder-container #btn-graph, .finder-container #btn-actiongroup, .finder-container #btnsort, .finder-container #searchinput, .finder-container #itemperpage, .finder-container #list-pagination').addClass("d-none")
})
function remoteSource(parent, cfg, callback){
    mylog.log("remoteSource",parent);
  let action = "<?php echo $action ?>";

  let url =  baseUrl+'/graph/<?php echo $url ?>';
  console.log("url leka <?php echo $url; ?>")
  
  if(url == baseUrl+'/graph/')
    alert("url invalide");
//alert(url)
  mylog.log("url","finder data", url);
  filAriane.push()
  if(!parent) {
    ajaxPost(
      "",
      url,
      {},
      function(data) {
        callback(data);
      },
      function(error) {
        mylog.log(error)
      },
      "json"
    )
  }
}

function createItemContent(cfg, item) {
  let data = item.children || cfg.data;
  
  let frag = document.createDocumentFragment();
  let prepEnd = document.createElement('i');
    if ('icon' in item) {
      let c = 'fa '+item.icon;
      if ( 'color' in item ) 
        c += ' text-'+item.color;

      prepEnd.className = c;
    }
    //prepEnd.textContent = "xxx";
    frag.appendChild( prepEnd );

    let text = document.createElement('span');
    text.className = "fjs-item-content";
    let label = document.createTextNode( item[cfg.labelKey] );
    text.appendChild( label );
    frag.appendChild( text );

    let appEnd = document.createElement('i');
    if ('children' in item) 
      appEnd.className = 'fa fa-caret-right';
    
    if ('url' in item) 
      appEnd.className = 'fa fa-external-link';
    
    frag.appendChild( appEnd );
  return frag;
  
}

function createSimpleColumn (item) { 
    let frag = document.createDocumentFragment();
    
    let block = document.createElement('div');
    block.className = "fjs-col col-xs-12";
    let textContainer = document.createElement('div');
    textContainer.className = "my-leaf-row";

    let icon = document.createElement('i');
    icon.className = "open-external-link fa fa-external-link-square";
    // if ('icon' in item) 
    //     icon.className = 'fa fa-3x '+item.icon; 
    // let label = document.createTextNode((item.label) ? item.label : "NO LABEL");
    // let text = document.createElement('p');
    // let btnLabel = document.createTextNode("Ouvrir dans finder");
    // let btn = document.createElement("a");
    // btn.className = "tooltips ml-6p";
    // btn.setAttribute('href', "javascript:;");
    // btn.setAttribute("data-toggle", "tooltip");
    // btn.setAttribute("data-placement", "bottom");
    // btn.setAttribute("data-original-title", "Ouvrir dans le finder");
    // btn.appendChild(icon);
    // let btnBlock = document.createElement('div');
    // btnBlock.className = "d-flex justify-content-start";
    let detailBlock = document.createElement('div');
    // let ulList = document.createElement('ul');
    // ulList.className = "breadcrumb bg-white d-block mb-0 pb-0";
    // filAriane.forEach((title, index) => {
    //   const li = document.createElement('li');
    //   li.innerText = title;
    //   if(index == filAriane.length - 1)
    //     li.appendChild(btn);
    //   ulList.appendChild(li)
    // })
    detailBlock.id = "detail"
    detailBlock.className = "col-xs-12 mt-2 fixed-height row";
    // let btn = document.createElement('button');
    // btn.className = "btn btn-primary pull-right";
    // btn.id = "openFinder";
    // btn.setAttribute("data-item", item.slug);
    let btn = $(`<a href="javascript:;" class="tooltips ml-6p" data-toggle="tooltip" data-placement="bottom" data-original-title="Ouvrir dans le finder"><i class="fts-18p open-external-link fa fa-external-link-square"></i></a>`)
    
    let finderUrl = baseUrl+'/graph/<?php echo Yii::app()->controller->id ?>/finder/slug/'+item.slug+"/open/element";
    if(item.open == "column-new") {
      finderUrl = baseUrl+'/graph/<?php echo Yii::app()->controller->id ?>/finder/answerid/'+item.id+"/open/element";
    }
    if(item.open == "column-answers") {
      finderUrl = baseUrl+'/graph/<?php echo Yii::app()->controller->id ?>/finder/coformid/'+item.id+"/open/element";
    }
    if(item.open == "column-oceco-answer") {
      finderUrl = baseUrl+'/graph/<?php echo Yii::app()->controller->id ?>/finder/answerocecoid/'+item.id+"/open/element";
    }
    // btn.addEventListener('click', function () {
    //   window.location.href = finderUrl
    // });
    btn.click(function() {
      window.open(finderUrl, '_blank');
    })
    $(".finder-container #breadcrumblist").append(btn);
    
    // $("#breadcrumb").append(btn);
    // btn.appendChild( btnLabel );
    // btnBlock.appendChild( icon );
    // text.appendChild( label );
    // btnBlock.appendChild( text )
    // btnBlock.appendChild( ulList );
    
    // textContainer.appendChild( btnBlock );
    // block.appendChild( textContainer );
    block.appendChild( detailBlock );
    frag.appendChild( block );
    return frag;//"<div><b>"+label+"</b></div>";
}

</script>
