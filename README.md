Any graphs generated for CO or ph::ontology driven Apps 


## FEATURES 

### graph list
- [Network Graph](http://communecter74-dev/graph/co/d3/id/5534fd9da1aa14201b0041cb/type/citoyens)
- [Circle Packing V1](http://communecter74-dev/graph/co/circle/type/tags/id/Morbihan) (Tags and all linked entities)
- [Circle Packing V2](http://communecter74-dev/graph/co/organization/id/60631a86c7f7b859ca733385) (Organizations and all Other organizations linked by tags)
### visualize data in a D3 relational graph 

- [see a person's graph](http://127.0.0.1/ph/graph/co/d3/id/54edb794f6b95c3c2a000941/type/organization)

#### search data by free word search, tags and types 
- [search a tag](http://127.0.0.1/ph/graph/co/search/tag/Alimentation)
- [search a word](http://127.0.0.1/ph/graph/co/search/q/open)
- [search a type](http://127.0.0.1/ph/graph/co/search/type/organization)

#### structured Menu
- [Menu](http://127.0.0.1/ph/graph/co/menu/type/home)

### Homepage
/graph shows the Readme.md content information


## USAGE
### Network Graph
```
http://baseUrl/graph/co/d3/id/:id/type/:poi

id: the entity ID
poi: the Collections (organizations, citoyens...)

```

### Circle Packing V1
```
http://baseUrl/graph/co/circle/type/tags/id/:tag

tag: the tag

```

### Circle Packing V2
```
http://baseUrl/graph/co/organization/id/:id
id: The organization ID

```
## TODO
- test loading it with CO2 theme
- load module configs from /config into moduleClass
- menu system

## Ideas, proposals; wishList
- rebuild geo statistics graphs
- utiliser le fichier config de network
- /graph/co/d3
    + add avatar
- make the search work like COLANG
    + comp.mails : open a graph to see the different kind of mail providers
    + comp.geo : compare geographic distributioin on DB content 
    + bar.geo : open a bar graph 
    + map.geo : open a map graph of counts 
- adding btns & features on viewed element 
    + dbl click bg, select added type > fill dynform > save & reload
    + when viewing an element add btns to add to follow , join...etc

## BUGS
- CO favicon on /graph
- move api.js to empty layout 